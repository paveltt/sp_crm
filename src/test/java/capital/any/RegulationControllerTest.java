package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.RegulationDetails;
import capital.any.model.base.User;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class RegulationControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(RegulationControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext; 
	@Autowired 
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private IUserService userService;
	private MockMvc mockMvc;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${user.regulated.no.money}")
	private String userRegulatedNoMoney;
	@Value("${user.not.regulated}")
	private String userNotRegulated;
	@Value("${user.not.regulated.unfitting}")
	private String userNotRegulatedUnfitting;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();	
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void approveUser() throws Exception {
		logger.debug("Approve user.");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(writerActive);	
		User user = userService.getUserByEmail(userNotRegulated);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/regulation/user/approve")
								.with(user(userDetails))
								.sessionAttr(ISessionService.USER, user))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(), 
								new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("Approve user.", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void approveUserDenied() throws Exception {
		logger.debug("Approve user.");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(writerActive);	
		User user = userService.getUserByEmail(userRegulatedNoMoney);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/regulation/user/approve")
								.sessionAttr(ISessionService.USER, user)
								.with(user(userDetails)))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(), 
								new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OPERATION_DENIED, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("Approve user.", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void questionnaireFittingAfterTraining() throws Exception {
		logger.debug("Questionnaire fitting after training.");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(writerActive);	
		User user = userService.getUserByEmail(userNotRegulatedUnfitting);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/regulation/questionnaire/fittingAfterTraining")
								.sessionAttr(ISessionService.USER, user)
								.with(user(userDetails)))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(), 
								new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("Questionnaire fitting after training.", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void getRegulationDetails() throws Exception {
		logger.debug("Test - getRegulationDetails");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(writerActive);	
		User user = userService.getUserByEmail(userNotRegulatedUnfitting);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/regulation/info/get")
					.sessionAttr(ISessionService.USER, user)
					.with(user(userDetails)))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			Response<RegulationDetails> responseObject = 
			(Response<RegulationDetails>) mapper
				.readValue(result
						.getResponse()
						.getContentAsString(),
						new TypeReference<Response<RegulationDetails>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
			
		} catch (Exception e) {
			logger.debug("Error - getRegulationDetails", e);
			throw e;
		}
	}
}
