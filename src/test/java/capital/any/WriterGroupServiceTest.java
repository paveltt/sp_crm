package capital.any;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.WriterGroup;
import capital.any.service.writerGroup.IWriterGroupService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration   
public class WriterGroupServiceTest {

	@Autowired
	private IWriterGroupService writerGroupService;
	
	@Test
	@Transactional
    public void insertWriterGroup() {
		WriterGroup writerGroup = new WriterGroup();
		writerGroup.setName("support");		
		writerGroupService.insert(writerGroup);
	}
		
}
	
