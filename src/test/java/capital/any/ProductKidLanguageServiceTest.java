package capital.any;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import capital.any.model.base.Language;
import capital.any.model.base.Product;
import capital.any.model.base.ProductKidLanguage;
import capital.any.service.base.productKidLanguage.IProductKidLanguageService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ProductKidLanguageServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(ProductKidLanguageServiceTest.class);
	
	@Autowired
	private IProductKidLanguageService productKidLanguageService;
	
	@Test
	public void insert() {
		Product product = new Product();
		product.setId(10134);
		Language language = new Language();
		language.setId(2);
		ProductKidLanguage productKidLanguage = new ProductKidLanguage(product, language);
		try {
			productKidLanguageService.insert(productKidLanguage);
		} catch (Exception e) {
			logger.error("Problem to insert product language.");
		}
	}
	
	@Test
	public void get() {;
		Product product = new Product();
		product.setId(10134);
		List<ProductKidLanguage> list = productKidLanguageService.get(product);
		Assert.assertNotNull(list);
	}
}
