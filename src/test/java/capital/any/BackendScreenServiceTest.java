package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.BeScreen;
import capital.any.service.backendScreen.IBackendScreenService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration   
public class BackendScreenServiceTest {

	@Autowired
	private IBackendScreenService backendScreenService;
	
	@Test
	@Transactional
    public void insertWriterGroup() {
		BeScreen beScreen = new BeScreen();
		beScreen.setName("user_change_password");
		beScreen.setSpaceId(1);
		backendScreenService.insert(beScreen);
	}
		
}
	
