package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import capital.any.model.base.analytics.AnalyticsDeposit;
import capital.any.service.analyticsDeposit.IAnalyticsDepositService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class AnalyticsDepositServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsDepositServiceTest.class);
	
	@Autowired
	private IAnalyticsDepositService analyticsDepositService; 
	
	@Test
	public void get() {
		logger.debug("Test get in AnalyticsDepositService. ");
		AnalyticsDeposit analyticsDeposit = analyticsDepositService.get();
		logger.debug("analyticsDeposit: " + analyticsDeposit.toString());
		Assert.assertNotNull(analyticsDeposit);
	}

}
