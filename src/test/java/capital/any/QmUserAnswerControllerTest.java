package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.QmQuestion;
import capital.any.model.base.User;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class QmUserAnswerControllerTest {
	public static final Logger logger = LoggerFactory.getLogger(QmUserAnswerControllerTest.class);
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private IUserService userService;
	
	private MockMvc mockMvc;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${user.regulated.no.money}")
	private String userRegulatedNoMoney;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void get() throws Exception {
		logger.debug("Test - get user answers");
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(writerActive);	
		User user = userService.getUserByEmail(userRegulatedNoMoney);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/questionnaire/user/answer/get")
					.sessionAttr(ISessionService.USER, user)
					.with(user(userDetails)))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			Response<Map<Integer, QmQuestion>> responseObject = 
			(Response<Map<Integer, QmQuestion>>) mapper
				.readValue(result
						.getResponse()
						.getContentAsString(),
						new TypeReference<Response<Map<Integer, QmQuestion>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.debug("Error - get user answers", e);
			throw e;
		}
	}
}
