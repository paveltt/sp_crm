package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.User;
import capital.any.model.base.User.Clazz;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;
import capital.any.service.transaction.ITransactionService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class TransactionControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(TransactionControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext; 
	@Autowired 
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	private MockMvc mockMvc;
	@Autowired
	private IUserService userService;
	@Autowired @Qualifier("TransactionServiceCRM")
	private ITransactionService transactionService;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${user.exist}")
	private String userExistEmail;
	@Value("${id.not.exist}")
	private Integer userNotExistId;
	@Value("${user.with.money}")
	private Integer userWithMoneyId;	
	
	private User userExist;
	private User userWithMoney;
	private Map<Integer, Transaction> testTransaction;
	private Map<Integer, Transaction> testTransaction2;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		userExist = userService.getUserByEmail(userExistEmail);
		userWithMoney = userService.getUserByEmail(userExistEmail);
		
		SqlFilters filter = new SqlFilters();
		filter.setUserId(userExist.getId());
		List<Transaction> transactions = transactionService.getTransactions(filter);
		testTransaction = new HashMap<Integer, Transaction>();
		for (Transaction t : transactions) {
			testTransaction.put(t.getStatus().getId(), t);
		}
		filter.setUserId(userWithMoney.getId());
		List<Transaction> transactions2 = transactionService.getTransactions(filter);
		testTransaction2 = new HashMap<Integer, Transaction>();
		for (Transaction t : transactions2) {
			testTransaction2.put(t.getStatus().getId(), t);
		}
	}
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void getByUserId() throws Exception {
		User user = new User();
		user.setId(userExist.getId());
		
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/getByUserId")
					.sessionAttr(ISessionService.USER, userExist)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseObject = 
					(Response<List<Transaction>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<List<Transaction>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getByUserId ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getTransactions() throws Exception {
		logger.debug("getTransactions");
		SqlFilters filters = new SqlFilters();
//		filters.setUserClassId(Clazz.REAL.getToken());
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_MONTH, -22);
//		filters.setSettledAtFrom(cal.getTime());
//		
//		Calendar calTo = Calendar.getInstance();
//		calTo.add(Calendar.DAY_OF_MONTH, -15);
//		filters.setSettledAtTo(calTo.getTime());
		Request<SqlFilters> request = new Request<SqlFilters>(filters, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/transaction/getAll")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<List<Transaction>> responseobject = (Response<List<Transaction>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Transaction>>>() {});
//		for (Transaction tran : (List<Transaction>)responseobject.getData()) {
//			logger.debug("tran created " + tran.getTimeCreated() + " settled " + tran.getTimeSettled());
//		}
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
		
	@SuppressWarnings("unchecked")
	@Test
	public void getTransactionsByUserIdExist() throws Exception {	
		User user = new User();
		user.setId(userExist.getId());
		SqlFilters filter = new SqlFilters();		
		Request<SqlFilters> request = new Request<SqlFilters>(filter, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/getByUserId")
					.sessionAttr(ISessionService.USER, user)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseObject = 
					(Response<List<Transaction>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<List<Transaction>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getTransactionsByUserIdExist ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getTransactionsByUserIdExistWithDate() throws Exception {	
		User user = new User();
		user.setId(userExist.getId());
		SqlFilters filter = new SqlFilters();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -75);
		filter.setFrom(cal.getTime());
		
		Calendar calTo = Calendar.getInstance();
		calTo.add(Calendar.DAY_OF_MONTH, -70);
		filter.setTo(calTo.getTime());
		Request<SqlFilters> request = new Request<SqlFilters>(filter, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/getByUserId")
					.sessionAttr(ISessionService.USER, user)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseObject = 
					(Response<List<Transaction>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<List<Transaction>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getTransactionsByUserIdExistWithDate ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getTransactionsByUserIdNotExist() throws Exception {
		User user = new User();
		user.setId(userNotExistId);
		
		Request<SqlFilters> request = new Request<SqlFilters>(new SqlFilters(), ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/getByUserId")
					.sessionAttr(ISessionService.USER, user)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseObject = 
					(Response<List<Transaction>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<List<Transaction>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getTransactionsByUserIdNotExist ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getTransactionByStatus() throws Exception {
		TransactionStatus status = new TransactionStatus(TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId());
		SqlFilters filters = new SqlFilters();
//		Calendar calFrom = Calendar.getInstance();
//		calFrom.add(Calendar.DAY_OF_MONTH, -30);
//		filters.setFrom(calFrom.getTime());
//		Calendar calTo = Calendar.getInstance();
//		calTo.add(Calendar.DAY_OF_MONTH, -17);
//		filters.setTo(calTo.getTime());
		filters.setTransactionStatusId(status.getId());
		Request<SqlFilters> request = new Request<SqlFilters>(filters, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/getByStatus")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Transaction>> responseobject = (Response<List<Transaction>>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<List<Transaction>>>(){});
//			for (Transaction tran : (List<Transaction>)responseobject.getData()) {
//				logger.debug(tran.getTimeCreated() + "tran " + tran);
//			}
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant get transactions by status ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
//	@Ignore
	@Transactional
	public void approveDeposit() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Transaction transaction = new Transaction();
		transaction.setId(testTransaction.get(TransactionStatusEnum.PENDING.getId()).getId());
		transaction.setStatus(new TransactionStatus(TransactionStatusEnum.SUCCEED.getId()));			
		Request<Transaction> request = new Request<Transaction>(transaction, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/deposit/approve")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant approve deposit", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
//	@Ignore
	@Transactional
	public void withdrawalFirstApproved() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Transaction transaction = new Transaction();
		transaction.setId(testTransaction.get(TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId()).getId());
			
		Request<Transaction> request = new Request<Transaction>(transaction, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/withdrawal/firstApproved")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Transaction> responseobject = (Response<Transaction>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<Transaction>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant withdrawalFirstApproved ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
//	@Ignore
	@Transactional
	public void withdrawalSecondApproved() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Transaction transaction = new Transaction();
		transaction.setId(testTransaction.get(TransactionStatusEnum.WAITING_FOR_SECOND_APPROVE.getId()).getId());
			
		Request<Transaction> request = new Request<Transaction>(transaction, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/withdrawal/secondApproved")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Transaction> responseobject = (Response<Transaction>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<Transaction>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant withdrawalSecondApproved ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
//	@Ignore
	@Transactional
	public void cancelWithdrawal() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Transaction transaction = new Transaction();
		transaction.setId(testTransaction.get(TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId()).getId());
			
		Request<Transaction> request = new Request<Transaction>(transaction, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/transaction/withdrawal/cancel")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Transaction> responseobject = (Response<Transaction>) mapper.readValue(result.getResponse().getContentAsString(), 
					new TypeReference<Response<Transaction>>(){});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant cancelWithdrawal ", e);
			throw e;
		}
	}
}
