package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import capital.any.model.base.analytics.AnalyticsLogin;
import capital.any.service.analyticsLogin.IAnalyticsLoginService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class AnalyticsLoginServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsLoginServiceTest.class);
	
	@Autowired
	private IAnalyticsLoginService analyticsLoginService;
	
	@Test
	public void getAnalyticsLogin() {
		logger.debug("Test getAnalyticsLogin.");
		AnalyticsLogin analyticsLogin = analyticsLoginService.getAnalyticsLogin();
		Assert.assertNotNull(analyticsLogin);
	}
}
