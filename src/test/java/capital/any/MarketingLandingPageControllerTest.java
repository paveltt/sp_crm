package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.MarketingLandingPage;
import capital.any.model.base.Writer;
import capital.any.security.CustomUserDetailService;
import capital.any.service.writer.IWriterService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class MarketingLandingPageControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingLandingPageControllerTest.class);
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private IWriterService writerService;
	
	private MockMvc mockMvc;
	private MarketingLandingPage marketingLandingPage;
	private Writer writer;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${marketing.default.id}")
	private Integer marketingDefaultId;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		writer = writerService.getWriterByEmailOrUserName(writerActive);
		String rand = UUID.randomUUID().toString().substring(0,6);
		marketingLandingPage = new MarketingLandingPage();
		marketingLandingPage.setName(rand + "_LP");
		marketingLandingPage.setPath("register");
		marketingLandingPage.setWriterId(writer.getId());
	}
	
	@Test
	@Transactional
	public void insert() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingLandingPage> request = new Request<MarketingLandingPage>(marketingLandingPage, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/marketing/lp/insert")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<MarketingLandingPage> responseObject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<MarketingLandingPage>>() {});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.debug("can't insert marketing landing page " + e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void edit() throws Exception {
		marketingLandingPage.setId(2);
		marketingLandingPage.setName("lp test update");
		marketingLandingPage.setPath("terms");
		marketingLandingPage.setWriterId(writer.getId());
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingLandingPage> request = new Request<MarketingLandingPage>(marketingLandingPage, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/marketing/lp/edit")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@Test
	public void getAll() throws Exception {
		logger.debug("About to get all marketing landing page");
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.post("/marketing/lp/getAll")
				.contentType(MediaType.APPLICATION_JSON)
				.with(user(writer))
				.accept(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<Map<Integer, MarketingLandingPage>> responseObject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<Integer, MarketingLandingPage>>>() {});
		Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getMarketingLandingPage() throws Exception {
		marketingLandingPage.setId(marketingDefaultId);
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingLandingPage> request = new Request<MarketingLandingPage>(marketingLandingPage, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/marketing/lp/getMarketingLandingPage")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
}
