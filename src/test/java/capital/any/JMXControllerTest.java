package capital.any;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;

/**
 * @author Eran.l
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class JMXControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(JMXControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	
	private MockMvc mockMvc;
		
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	@Transactional
	public void reloadHC() throws Exception {
		logger.debug("reloadHC");			
		Request<Object> request = new Request<Object>(null, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.debug("reloadHC json: " + json);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/jmx/reloadHC")
								  .contentType(MediaType.APPLICATION_JSON)
								  .content(json).accept(MediaType.APPLICATION_JSON))
								  .andDo(MockMvcResultHandlers.print())
								  .andExpect(MockMvcResultMatchers.status().isOk())
								  .andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<Boolean> responseObject = (Response<Boolean>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Boolean>>() {});
		Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
	}
	
}
