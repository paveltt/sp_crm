package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.base.enums.ActionSource;
import capital.any.model.ClientSpace;
import capital.any.model.base.ChangePassword;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.User;
import capital.any.model.base.User.Gender;
import capital.any.model.base.User.Step;
import capital.any.model.base.UserStep;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class UserControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(UserControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext; 
	@Autowired 
	private ObjectMapper mapper;
	@Autowired
	private IUserService userService;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	private MockMvc mockMvc;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${user.exist}")
	private String userExist;
	@Value("${user.not.exist}")
	private String userNotExist;
	@Value("${id.not.exist}")
	private Integer userNotExistId;
	@Value("${user.writer.pass}")
	private String userWriterPass;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	@Transactional
	public void insertUser() throws Exception {
		User user = new User();
		String rand = UUID.randomUUID().toString().substring(0,6);
		user.setPassword(userWriterPass);
		user.setCurrencyId(3);
		user.setFirstName("FirstName junit CRM ");
		user.setLastName("LastName junit CRM");
		user.setStreet("Street junit CRM");
		user.setZipCode("ZC t CRM");
		user.setEmail(rand + "CRM@anycapital.com");
		user.setComments("Comments junit CRM");
		user.setIp("127.0.0.1");
		user.setTimeBirthDate(new Date(442713600000L));
		user.setIsContactByEmail(1);
		user.setIsContactByPhone(1);
		user.setIsContactBySms(1);
		user.setIsAcceptedTerms(1);
		user.setMobilePhone("12345678");
		user.setLanguageId(2);
		user.setUtcOffset(-120);
		user.setWriterId(-1);
		user.setGender(Gender.FEMALE);
		//user.setStreetNo("1");
		//user.setUtcOffsetModified("UTC+0");
		//user.setLanguageId(2);
//		user.setUtcOffset("GMT+02:00");
		//user.setCityName("aaaaa");
		//user.setUserAgent(rs.getString("user_agent"));
		//user.setHttpReferer(rs.getString("http_referer"));
		//user.setCountryId(2);
		//user.setWriterId(rs.getInt("writer_id"));
		//user.setAoUserId(rs.getInt("ao_user_id"));
		user.setActionSource(ActionSource.CRM);
		user.setUtcOffsetCreated("GMT+0");
		user.setCountryByPrefix(226);
		user.setTIN("test TIN");
		
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/signup")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			@SuppressWarnings("unchecked")
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("insertUser CRM junit", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void get() throws Exception {		
		logger.debug("get");
		capital.any.model.User user = new capital.any.model.User();
		user.setEmail(userExist);
		//user.setId(1);
		Request<capital.any.model.User> request = new Request<capital.any.model.User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/get")
					.contentType(MediaType.APPLICATION_JSON)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<capital.any.model.User> responseobject = (Response<capital.any.model.User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<capital.any.model.User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! get ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserByEmailExist() throws Exception {		
		logger.debug("getUserByEmailExist");
		capital.any.model.User user = new capital.any.model.User();
		user.setEmail(userExist);
		Request<capital.any.model.User> request = new Request<capital.any.model.User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/getByEmail")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<capital.any.model.User> responseobject = (Response<capital.any.model.User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<capital.any.model.User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getUserByEmailExist ", e);
			throw e;
		}
	} 

	@SuppressWarnings("unchecked")
	@Test
	public void getUserByEmailNotExist() throws Exception {
		logger.debug("getUserByEmailNotExist");
		User user = new User();
		user.setEmail(userNotExist);
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/getByEmail")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.INVALID_INPUT, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! get user by email not exist ", e);
			throw e;
		}
	} 
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserByIdExist() throws Exception {		
		logger.debug("getUserByIdExist");
		User u = userService.getUserByEmail(userExist);
		User user = new User();
		user.setId(u.getId());
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/getById")
					.sessionAttr(ISessionService.USER, u)
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant get user by id ", e);
			throw e;
		}
	} 
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserByIdNotExist() throws Exception {
		logger.debug("getUserByIdNotExist");
		User user = new User();
		user.setId(userNotExistId);
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/getById")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<User> responseobject = (Response<User>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<User>>() {});
			Assert.assertEquals(ResponseCode.INVALID_INPUT, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant get user by id not exist ", e);
			throw e;
		}
	} 
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void editUser() throws Exception {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(writerActive);
		User user = userService.getUserByEmail(userExist);
		user.setFirstName("lior_was_here");
		user.setUserStep(new UserStep(Step.APPROVE.getId()));
		user.setTIN("TIN updated");
		
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/edit")
					.with(user(userDetails))
					.sessionAttr(ISessionService.USER, user)
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void changePassword() throws Exception {
		UserDetails userDetails = custemUserDetailService.loadUserByUsername(writerActive);
		User user = userService.getUserByEmail(userExist);
		ChangePassword changePassword = new ChangePassword();
		changePassword.setCurrentPassword(userWriterPass);
		changePassword.setNewPassword(userWriterPass);
		changePassword.setRetypePassword(userWriterPass);
		
		Request<ChangePassword> request = new Request<ChangePassword>(changePassword, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/changePassword")
					.with(user(userDetails))
					.sessionAttr(ISessionService.USER, user)
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)		
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
						.getResponse()
						.getContentAsString(),
						new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getClientsSpace() throws Exception {
		logger.debug("getClientSpace");
		SqlFilters sqlFilters = new SqlFilters();
//		sqlFilters.setName("Z");
//		sqlFilters.setEmail("kosta@test.com");
//		sqlFilters.setCountryId(190L);
		Calendar from = Calendar.getInstance();
		from.add(Calendar.DAY_OF_MONTH, -30);
		sqlFilters.setFrom(from.getTime());
		sqlFilters.setTo(new Date());
		Request<SqlFilters> request = new Request<SqlFilters>(sqlFilters, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/clientspace")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<ClientSpace>> responseobject = (Response<List<ClientSpace>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<ClientSpace>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant getClientSpace ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void advancedSearchUser() throws Exception {
		logger.debug("advancedSearchUser");
		SqlFilters sqlFilters = new SqlFilters();
		sqlFilters.setFirstName("junit");		
		Request<SqlFilters> request = new Request<SqlFilters>(sqlFilters, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/user/advancedSearchUser")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<capital.any.model.User>> responseobject = (Response<List<capital.any.model.User>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<capital.any.model.User>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant advancedSearchUser ", e);
			throw e;
		}
	}
}
