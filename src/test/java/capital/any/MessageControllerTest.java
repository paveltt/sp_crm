package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.base.enums.ActionSource;
import capital.any.base.enums.LanguageEnum;
import capital.any.base.enums.MsgResTypeEnum;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResFile;
import capital.any.model.base.MsgResLanguage;
import capital.any.security.CustomUserDetailService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class MessageControllerTest {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageControllerTest.class);
	
	@Autowired
	private WebApplicationContext webApplicationContext; 
	
	@Autowired 
	private ObjectMapper mapper;
	
	private MockMvc mockMvc;
	
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	
	@Value("${writer.active}")
	private String writerActive;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void getMessageResourcesFull() throws Exception {
		MsgRes msgRes = new MsgRes();
		msgRes.setActionSourceId(ActionSource.WEB.getId());
		Request<MsgRes> request = new Request<MsgRes>(msgRes, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		mockMvc.perform(MockMvcRequestBuilders.post("/message/getMessageResourcesFull")
					.contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());				
	}
	
	@Test
	public void getMessageResources() throws Exception {
		MsgRes msgRes = new MsgRes();
		msgRes.setActionSourceId(ActionSource.WEB.getId());
		Request<MsgRes> request = new Request<MsgRes>(msgRes, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " + json);
		mockMvc.perform(MockMvcRequestBuilders.post("/message/getMessageResources")
					.contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());				
	}
	
	@Test	
	@Transactional
	public void insertMessageResources() throws Exception {
		List<MsgResLanguage> list = new ArrayList<MsgResLanguage>();		
		MsgRes msgRes = new MsgRes();
		msgRes.setKey("test_junit");
		msgRes.setActionSourceId(ActionSource.WEB.getId());
		msgRes.setTypeId(MsgResTypeEnum.REGULAR.getId());
		MsgResLanguage msgResLanguage = new MsgResLanguage();
		msgResLanguage.setMsgResLanguageId(LanguageEnum.EN.getId());
		msgResLanguage.setValue("test_junit_en");
		msgResLanguage.setMsgRes(msgRes);
		list.add(msgResLanguage);		
		MsgResLanguage msgResLanguage1 = new MsgResLanguage();
		msgResLanguage1.setMsgResLanguageId(LanguageEnum.DE.getId());
		msgResLanguage1.setValue("test_junit_de");
		msgRes.setTypeId(MsgResTypeEnum.REGULAR.getId());
		list.add(msgResLanguage1);						
		Request<List<MsgResLanguage>> request = new Request<List<MsgResLanguage>>(list, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug(json);
//		String json = "{\"data\":[{\"msgRes\":{\"id\":0,\"key\":\"test1.test1\",\"actionSourceId\":0},\"msgResLanguageId\":2,\"value\":\"test3333\"}],\"actionSource\":2}";
		mockMvc.perform(MockMvcRequestBuilders.post("/message/insertMessageResource")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json).accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status()
					.isOk());
	}
	
	@Test
	@Transactional
	public void insertMessageResourcesLargeValue() throws Exception {
		List<MsgResLanguage> list = new ArrayList<MsgResLanguage>();		
		MsgRes msgRes = new MsgRes();
		msgRes.setKey("test-large");
		msgRes.setActionSourceId(ActionSource.WEB.getId());
		msgRes.setTypeId(MsgResTypeEnum.LARGE_VALUE.getId());
		MsgResLanguage msgResLanguage = new MsgResLanguage();
		msgResLanguage.setMsgResLanguageId(LanguageEnum.EN.getId());
		msgResLanguage.setLargeValue("very large value!!");
		msgResLanguage.setMsgRes(msgRes);
		list.add(msgResLanguage);		
		Request<List<MsgResLanguage>> request = new Request<List<MsgResLanguage>>(list, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		System.out.println(json);
//		String json = "{\"data\":[{\"msgRes\":{\"id\":0,\"key\":\"test1.test1\",\"actionSourceId\":0},\"msgResLanguageId\":2,\"value\":\"test3333\"}],\"actionSource\":2}";
		mockMvc.perform(MockMvcRequestBuilders.post("/message/insertMessageResource")
					.contentType(MediaType.APPLICATION_JSON).content(json).accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	public Response<List<MsgResLanguage>> getMsgResLanguageByKeyCommon() throws Exception {
		MsgRes msgres = new MsgRes();
		msgres.setKey("test5");
		msgres.setActionSourceId(ActionSource.CRM.getId());
		
		Request<MsgRes> r = new Request<MsgRes>(msgres, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		Response<List<MsgResLanguage>> responseobject = null;
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/message/get/msgreslanguage/bykey")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();;
			ObjectMapper mapper = new ObjectMapper();
			responseobject = (Response<List<MsgResLanguage>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<MsgResLanguage>>>() {});
			
		} catch (Exception e) {
			logger.debug("cant update Msg Res Language ", e.getMessage());
			throw e;
		}
		return responseobject;
	}
	
	@Test
	public void getMsgResLanguageByKey() throws Exception {
		logger.debug("get Msg Res Language By Key");
		Response<List<MsgResLanguage>> responseobject = getMsgResLanguageByKeyCommon();
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
	
	
	@Test
	@Transactional
	public void updateMsgResLanguage() throws Exception {
		logger.debug("update MsgResLanguage");
		Response<List<MsgResLanguage>> msgResLanguagesResponse = getMsgResLanguageByKeyCommon();
		List<MsgResLanguage> msgResLanguages = (List<MsgResLanguage>) msgResLanguagesResponse.getData();
		msgResLanguages.forEach(mrl -> mrl.getMsgRes().setActionSourceId(ActionSource.CRM.getId()));
		Request<List<MsgResLanguage>> r = new Request<List<MsgResLanguage>>(msgResLanguages, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/message/update/msgreslanguage")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();;
			ObjectMapper mapper = new ObjectMapper();
			Response<List<MsgResLanguage>> responseobject = (Response<List<MsgResLanguage>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<MsgResLanguage>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update Msg Res Language ", e.getMessage());
			throw e;
		}
	}
	
	public Response<List<MsgResLanguage>> getKeyMissingLanguageCommon() throws Exception {
		logger.debug("get Key Missing Language");
		MsgRes msgres = new MsgRes();
		msgres.setKey("eo1.eo1.test");
		msgres.setActionSourceId(ActionSource.CRM.getId());
		
		Request<MsgRes> r = new Request<MsgRes>(msgres, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		Response<List<MsgResLanguage>> responseobject = null;
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/message/get/msgreslanguage/missinglanguages")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			responseobject = (Response<List<MsgResLanguage>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<MsgResLanguage>>>() {});			
		} catch (Exception e) {
			logger.debug("get Key Missing Language ", e.getMessage());
			throw e;
		}
		return responseobject;
	}
	
	@Test
	public void getKeyMissingLanguage() throws Exception {
		logger.debug("get Key Missing Language");
		Response<List<MsgResLanguage>> responseobject = getKeyMissingLanguageCommon();
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
	
	
	
	@Test	
	@Transactional
	public void insertMessageResourcesLanguages() throws Exception {
		Response<List<MsgResLanguage>> msgResLanguagesResponse = getKeyMissingLanguageCommon();
		List<MsgResLanguage> msgResLanguages = (List<MsgResLanguage>) msgResLanguagesResponse.getData();
		for (MsgResLanguage msgResLanguage : msgResLanguages) {
			msgResLanguage.setValue("testDE6");
		}	
		Request<List<MsgResLanguage>> request = new Request<List<MsgResLanguage>>(msgResLanguages, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " +json);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/message/insert/MessageResourceLanguage")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json).accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status()
					.isOk()).andReturn();;
		Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
	
	@Test
	@Transactional
	public void insertMessageResourcesByFile() throws Exception {
		java.io.File file = new java.io.File("docs/fileUploadExample/bulk-translation.xlsx");		       
        FileInputStream fi1 = new FileInputStream(file);
        MockMultipartFile fstmp = new MockMultipartFile("fileUpload", file.getName(), "multipart/form-data", fi1);
		MsgResFile msgResFile = new MsgResFile();
		msgResFile.setActionSourceId(ActionSource.CRM.getId());
		msgResFile.setAddKey(false);
		msgResFile.setLanguageId(1);
		msgResFile.setTypeId(1);
		Request<MsgResFile> request = new Request<MsgResFile>(msgResFile, ActionSource.WEB);
		String json = mapper.writeValueAsString(request);
		logger.debug("body: " +json);		
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.fileUpload("/message/insert/MessageResourceByFile")
				.file(fstmp)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.param("request", json)
				.with(user(custemUserDetailService.loadUserByUsername(writerActive))))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		
		Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
}
