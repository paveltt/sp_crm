package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import capital.any.service.messageResourceType.IMessageResourceTypeService;

/**
 * 
 * @author Eyal.o
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)
public class MessageResourceTypeServiceTest {
	
	@Autowired
	private IMessageResourceTypeService messageResourceTypeService;
	
	@Test
	public void get() {
		Assert.assertNotNull(messageResourceTypeService.get());
	}

}
