package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.WriterChangePassword;
import capital.any.model.base.Writer;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.writer.WriterService;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class WriterControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(WriterControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private WriterService writerService;
	
	private MockMvc mockMvc;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${user.exist}")
	private String userExist;
	@Value("${user.writer.pass}")
	private String userWriterPass;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void insert() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		try {
			String rand = UUID.randomUUID().toString().substring(0,8);
			ObjectMapper mapper = new ObjectMapper();
			Writer newWriter = new Writer();
			newWriter.setFirstName("test");
			newWriter.setLastName("test");
			newWriter.setEmail(rand + writerActive);
			newWriter.setPassword(userWriterPass);
			newWriter.setUserName(rand);
			newWriter.setUtcOffset(-180);
			newWriter.setGroupId(1);									
			Request<Writer> request = new Request<Writer>(newWriter, ActionSource.CRM);
			String json = mapper.writeValueAsString(request);
			logger.info("body: " + json);
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/writer/insert")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("Error on insert writer", e);
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void changePassword() throws Exception {
		UserDetails sessionWriter = custemUserDetailService.loadUserByUsername(writerActive);
		try {
			WriterChangePassword writerChangePassword = new WriterChangePassword();
			writerChangePassword.setPassword(userWriterPass);
			writerChangePassword.setRetypePassword(userWriterPass);
			Request<WriterChangePassword> request = new Request<WriterChangePassword>(writerChangePassword, ActionSource.CRM);
			String json = mapper.writeValueAsString(request);
			logger.info("body: " + json);
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/writer/changePassword")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(sessionWriter))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("Error on update writer password", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void update() throws Exception {
		UserDetails sessionWriter = custemUserDetailService.loadUserByUsername(writerActive);
		try {
			Writer writer = writerService.getWriterByEmailOrUserName(writerActive);
			writer.setGroupId(2);									
			Request<Writer> request = new Request<Writer>(writer, ActionSource.CRM);
			String json = mapper.writeValueAsString(request);
			logger.info("body: " + json);
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/writer/update")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(sessionWriter))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("Error on update writer", e);
			throw e;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void getById() throws Exception {
		UserDetails sessionWriter = custemUserDetailService.loadUserByUsername(writerActive);
		try {
			Writer w = writerService.getWriterByEmailOrUserName(writerActive);
			Writer writer = new Writer();
			writer.setId(w.getId()); // junit writer
			Request<Writer> request = new Request<Writer>(writer, ActionSource.CRM);
			String json = mapper.writeValueAsString(request);
			logger.info("body: " + json);
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/writer/get/id")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(sessionWriter))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("Error on get writer by id", e);
			throw e;
		}
	}
	
	@Test
	public void get() throws Exception {
		logger.debug("About to get all writers");
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.post("/writer/get")
				.contentType(MediaType.APPLICATION_JSON)
				.with(user(writer))
				.accept(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<Map<Integer, Writer>> responseobject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<Integer, Writer>>>() {});
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
	
}
