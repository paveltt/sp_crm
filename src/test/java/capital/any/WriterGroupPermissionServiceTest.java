package capital.any;


import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.model.BePermission;
import capital.any.model.BeScreen;
import capital.any.model.WriterGroup;
import capital.any.model.WriterGroupPermission;
import capital.any.service.writerGroupPermission.IWriterGroupPermissionService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration   
public class WriterGroupPermissionServiceTest {

	@Autowired
	private IWriterGroupPermissionService writerGroupPermissionService;
	
	@Autowired 
	private ObjectMapper mapper;
	
	@Test
	@Transactional
    public void insertWriterGroupPermission() {		
		WriterGroupPermission writerGroupPermission = new WriterGroupPermission();
		WriterGroup writerGroup = new WriterGroup();
		writerGroup.setId(1);
		writerGroupPermission.setWriterGroup(writerGroup);
		BeScreen beScreen = new BeScreen();
		beScreen.setId(8);
		writerGroupPermission.setBeScreen(beScreen);				
		BePermission bePermission1 = new BePermission();
		bePermission1.setId(1);
		BePermission bePermission2 = new BePermission();
		bePermission2.setId(2);
		BePermission bePermission3 = new BePermission();
		bePermission3.setId(3);
		ArrayList<BePermission> bePermissionList = new ArrayList<BePermission>();
		bePermissionList.add(bePermission1);
		bePermissionList.add(bePermission2);
		bePermissionList.add(bePermission3);
		writerGroupPermission.setBePermissionList(bePermissionList);
		writerGroupPermissionService.insert(writerGroupPermission);
	}
	
	@Test
    public void getWriterGroupPermission() throws JsonProcessingException {		
		System.out.println(mapper.writeValueAsString(writerGroupPermissionService.getWriterPermissions(2)));	
	}
			
}
	
