package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.io.FileInputStream;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.File;
import capital.any.model.base.FileRequiredDocGroup;
import capital.any.model.base.FileStatus;
import capital.any.model.base.FileType;
import capital.any.model.base.User;
import capital.any.model.base.Writer;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.file.IFileService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class FileControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(FileControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;	
	@Autowired
	private IUserService userService;
	@Autowired
	private IFileService fileService;
	
	private MockMvc mockMvc;
	private File file;
	private File testFile;
	private User user;
	
	@Value("${writer.active}")
	private String writerActive;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		
		user = userService.getUserByEmail("junit.inv.test@anyoption.com");
		List<File> list = fileService.getByUserId(user.getId());
		for (File f : list) {
			testFile = f;
			break;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	@Ignore
	public void insert() throws Exception {
		java.io.File f = new java.io.File("C:\\1.PNG");
        System.out.println(f.isFile()+"  "+f.getName()+f.exists());
        FileInputStream fi1 = new FileInputStream(f);
        MockMultipartFile fstmp = new MockMultipartFile("fileUpload", f.getName(), "multipart/form-data", fi1);
		file = new File();
		file.setActionSource(ActionSource.CRM);
		file.setWriter(new Writer(3));
		file.setName("bbbbb.png");
		FileType fileType = new FileType();
		fileType.setId(1);
		file.setFileType(fileType);				
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<File> request = new Request<File>(file, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.fileUpload("/file/insert")
					.file(fstmp)
					.contentType(MediaType.MULTIPART_FORM_DATA)
					.sessionAttr(ISessionService.USER, user)
					.param("request", json)
					.with(user(writer)))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<File> responseobject = (Response<File>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<File>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("can't insert file ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getFileByUserId() throws Exception {		
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/file/getByUserId")
					.sessionAttr(ISessionService.USER, user)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<File>> responseObject = 
					(Response<List<File>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<List<File>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getFileByUserId ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void update() throws Exception {
		File file = new File();
		FileType fileType = new FileType();
		fileType.setId(4);		
		file.setFileType(fileType);
		file.setId(testFile.getId());
		file.setPrimary(true);
		FileStatus fileStatus = new FileStatus();
		fileStatus.setId(3);
		file.setFileStatus(fileStatus);
		Request<File> request = new Request<File>(file, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/file/update")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getFileByUserId ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getFileById() throws Exception {
		File file = new File();
		file.setId(testFile.getId());
		Request<File> request = new Request<File>(file, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/file/get/file/id")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.sessionAttr(ISessionService.USER, user)
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getFileByUserId ", e);
			throw e;
		}
	}
	
	/******** Finotec **********/
	/*@SuppressWarnings("unchecked")
	@Test
	public void getRequiredDocs() throws Exception {		
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/file/getRequiredDocs")
					.sessionAttr(ISessionService.USER, user)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Map<Integer, RegulationFileRule>> responseObject = 
					(Response<Map<Integer, RegulationFileRule>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Map<Integer, RegulationFileRule>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getRequiredDocs ", e);
			throw e;
		}
	}*/
	
	/******** ODT **********/
	@SuppressWarnings("unchecked")
	@Test
	public void getRequiredDocs() throws Exception {		
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/file/getRequiredDocs")
					.sessionAttr(ISessionService.USER, user)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<FileRequiredDocGroup>> responseObject = 
					(Response<List<FileRequiredDocGroup>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<List<FileRequiredDocGroup>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getRequiredDocs ", e);
			throw e;
		}
	}
}
