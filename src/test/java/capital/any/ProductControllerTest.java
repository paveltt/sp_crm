package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.base.enums.LanguageEnum;
import capital.any.base.enums.ProductTypeEnum;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.filter.ProductFilters;
import capital.any.model.base.DayCountConvention;
import capital.any.model.base.Language;
import capital.any.model.base.Market;
import capital.any.model.base.Product;
import capital.any.model.base.ProductAutocall;
import capital.any.model.base.ProductBarrier;
import capital.any.model.base.ProductBarrierType;
import capital.any.model.base.ProductCallable;
import capital.any.model.base.ProductCoupon;
import capital.any.model.base.ProductCouponFrequency;
import capital.any.model.base.ProductKid;
import capital.any.model.base.ProductKidLanguage;
import capital.any.model.base.ProductMarket;
import capital.any.model.base.ProductMaturity;
import capital.any.model.base.ProductSimulation;
import capital.any.model.base.ProductType;
import capital.any.model.base.SqlFilters;
import capital.any.security.CustomUserDetailService;
import capital.any.service.product.IProductService;

/**
 * @author EyalG
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class ProductControllerTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductControllerTest.class);
	
	@Autowired
	private WebApplicationContext webApplicationContext; 
	
	@Autowired
	private IProductService productService;
	
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	
	@Autowired 
	private ObjectMapper mapper;
	
	private MockMvc mockMvc;	
	
	private Product p;
	
	private long testProductId;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${id.not.exist}")
	private Integer idNotExist;
	
	@Before
	public void setup() {

		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		p = new Product();
		
		p.setBondFloorAtissuance(97);
		p.setBonusLevel(50);
		p.setCurrencyId(3);
		p.setDenomination(100000);
		p.setIssuePrice(100);
		p.setIssueSize(10000000);
		p.setLevelOfParticipation(105);
		p.setLevelOfProtection(95);
		p.setMaxYield(100);
		p.setMaxYieldPA(102);
		p.setProductInsuranceTypeId(1);
		p.setProductType(new ProductType());
		p.getProductType().setId(3);;
		p.setQuanto(false);
		p.setStrikeLevel(100);
		//p.setProductStatusId(1);
		ProductMaturity productMaturity = new ProductMaturity();
		productMaturity.setId(1);
		p.setProductMaturity(productMaturity);
		
		
		p.setProductCoupons(new ArrayList<ProductCoupon>());
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 1);
		p.setSubscriptionStartDate(cal.getTime());
		cal.add(Calendar.DAY_OF_MONTH, 1);
		p.setSubscriptionEndDate(cal.getTime());
		cal.add(Calendar.DAY_OF_MONTH, 1);
		p.setInitialFixingDate(cal.getTime());
		cal.add(Calendar.DAY_OF_MONTH, 1);
		p.setIssueDate(cal.getTime());
		cal.add(Calendar.DAY_OF_MONTH, 1);
		p.setFirstExchangeTradingDate(cal.getTime());
		cal.add(Calendar.DAY_OF_MONTH, 1);
		p.setFinalFixingDate(cal.getTime());
		cal.add(Calendar.DAY_OF_MONTH, 1);
		p.setLastTradingDate(cal.getTime());
		cal.add(Calendar.DAY_OF_MONTH, 1);
		p.setRedemptionDate(cal.getTime());		
		
		
		p.setPriority(1);
		
		//for the full test
		ArrayList<ProductAutocall> listProductAutocall = new ArrayList<ProductAutocall>();
		Calendar productcallDate = Calendar.getInstance();
		ProductAutocall productAutocall = new ProductAutocall();
		productAutocall.setExaminationValue(70);
		productcallDate.add(Calendar.DAY_OF_MONTH, 11);
		productAutocall.setExaminationDate(productcallDate.getTime());
		listProductAutocall.add(productAutocall);
		
		ProductAutocall productAutocall1 = new ProductAutocall();
		productAutocall1.setExaminationValue(60);
		productcallDate.add(Calendar.DAY_OF_MONTH, 1);
		productAutocall1.setExaminationDate(productcallDate.getTime());
		listProductAutocall.add(productAutocall1);
		p.setProductAutocalls(listProductAutocall);
		
		p.setProductCallables(new ArrayList<ProductCallable>());
		ProductCallable productCallable = new ProductCallable();
		productcallDate.add(Calendar.DAY_OF_MONTH, 1);
		productCallable.setExaminationDate(productcallDate.getTime());
		productCallable.setProductId(p.getId());
		p.getProductCallables().add(productCallable);
				
		ProductCallable productCallable1 = new ProductCallable();
		productcallDate.add(Calendar.DAY_OF_MONTH, 1);
		productCallable1.setExaminationDate(productcallDate.getTime());
		productCallable1.setProductId(p.getId());
		p.getProductCallables().add(productCallable1);
		
		ProductMarket productMarket = new ProductMarket();
		Market market = new Market();
		market.setId(14);
		productMarket.setMarket(market);
		productMarket.setProductId(p.getId());
		productMarket.setStartTradeLevel(100);
		p.setProductMarkets(new ArrayList<ProductMarket>());
		p.getProductMarkets().add(productMarket);		
		
		ProductSimulation productSimulation = new ProductSimulation();
		productSimulation.setFinalFixingLevel(80);
		productSimulation.setPosition(1);
		productSimulation.setProductId(p.getId());
		
		ProductSimulation productSimulation1 = new ProductSimulation();
		productSimulation1.setFinalFixingLevel(100);
		productSimulation1.setPosition(2);
		productSimulation1.setProductId(p.getId());
		
		ProductSimulation productSimulation2 = new ProductSimulation();
		productSimulation2.setFinalFixingLevel(120);
		productSimulation2.setPosition(3);
		productSimulation2.setProductId(p.getId());

		p.setProductSimulation(new ArrayList<ProductSimulation>());
		p.getProductSimulation().add(productSimulation);
		p.getProductSimulation().add(productSimulation1);
		p.getProductSimulation().add(productSimulation2);
		
		Map<Long, Product> hm = productService.getProducts();
		try {
			for (Product product : hm.values()) {
				testProductId = product.getId();
				break;
			}
		} catch (Exception e) {
			logger.debug("cant find product id for test", e);
		}

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getAllProducts() throws Exception {
		logger.debug("getAllProducts");
		SqlFilters filter = new SqlFilters();
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		Date parse = sf.parse("20161115");
		filter.setSubscriptionStartDate(parse);
		Request<SqlFilters> r = new Request<SqlFilters>(filter, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);       
		MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/getAllProducts")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.content(json)
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Product>> responseobject = (Response<List<Product>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Product>>>() {});
//			for (Product product : (List<Product>)responseobject.getData()) {
//				logger.debug("product " + product);
//			}
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());	
	}
		
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	@Ignore
	public void insertProduct() throws Exception {
		Request<Product> r = new Request<Product>(p, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);       
		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/insertProduct")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Product> responseobject = (Response<Product>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Product>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert product " + e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void insertProductFull() throws Exception {
		logger.debug("insertProductFull");
		
//		p.getProductType().setId(2);
//		p.setLevelOfProtection(95);
//		p.setLevelOfParticipation(120);
//		p.setStrikeLevel(100);
//		p.getProductCoupons().clear();
//		
//		ProductBarrier productBarrier = new ProductBarrier(); 
//		Calendar cBarrier = Calendar.getInstance();
//		cBarrier.add(Calendar.MONTH, 4);
//		productBarrier.setBarrierStart(cBarrier.getTime());
//		cBarrier.add(Calendar.MONTH, 1);
//		productBarrier.setBarrierEnd(cBarrier.getTime());
//		productBarrier.setBarrierLevel(130);
//		ProductBarrierType productBarrierType = new ProductBarrierType();
//		productBarrierType.setId(1);
//		productBarrier.setProductBarrierType(productBarrierType);
//		p.setProductBarrier(productBarrier);
		
		Request<Product> r = new Request<Product>(p, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/insertProductFull")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Product> responseobject = (Response<Product>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Product>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert full product " + e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getProductFull() throws Exception {
		logger.debug("getProductFull");
		Product product = new Product();
		product.setId(testProductId);
		Request<Product> r = new Request<Product>(product, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/getProductFull")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Product> responseobject = (Response<Product>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Product>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant get full product ", e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getProductFullWrongId() throws Exception {
		logger.debug("getProductFullWrongId");
		Product product = new Product();
		product.setId(idNotExist);
		Request<Product> r = new Request<Product>(product, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/getProductFull")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Product> responseobject = (Response<Product>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Product>>() {});
			Assert.assertEquals(ResponseCode.INVALID_INPUT, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant get full product " + e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getInsertProductFilters() throws Exception {
		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/insertProduct/init")
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<ProductFilters> responseobject = (Response<ProductFilters>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<ProductFilters>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant get Insert Product Filters " + e.getMessage());
			throw e;
		}
	}
	
	//CPCP
	@Test
	@Transactional
	public void insertAndSelectProductFullType1() throws Exception {
		logger.debug("insert and select ProductFull type 1");
		p.getProductType().setId(1);
		p.setLevelOfProtection(95);
		p.setLevelOfParticipation(108);
		p.getProductCoupons().clear();
		testInsertSelect();
	}

	//BCPC
	@Test
	@Transactional
	public void insertAndSelectProductFullType2() throws Exception {
		logger.debug("insert and select ProductFull type 2");
		p.getProductType().setId(2);
		p.setLevelOfProtection(95);
		p.setLevelOfParticipation(120);
		p.setStrikeLevel(100);
		p.getProductCoupons().clear();
		
		ProductBarrier productBarrier = new ProductBarrier(); 
		Calendar cBarrier = Calendar.getInstance();
		cBarrier.add(Calendar.MONTH, 4);
		productBarrier.setBarrierStart(cBarrier.getTime());
		cBarrier.add(Calendar.MONTH, 1);
		productBarrier.setBarrierEnd(cBarrier.getTime());
		productBarrier.setBarrierLevel(130);
		ProductBarrierType productBarrierType = new ProductBarrierType();
		productBarrierType.setId(1);
		productBarrier.setProductBarrierType(productBarrierType);
		p.setProductBarrier(productBarrier);
		
		
		testInsertSelect();
		p.setProductBarrier(null);
	}
	
	//CPCC
	@Test
	@Transactional
	public void insertAndSelectProductFullType3() throws Exception {
		logger.debug("insert and select ProductFull type 3");
		p.getProductType().setId(3);
		p.setLevelOfProtection(95);
		
		ProductCoupon productCoupon = new ProductCoupon();
		Calendar productcouponDate = Calendar.getInstance();
		productcouponDate.add(Calendar.MONTH, 3);
		productCoupon.setObservationDate(productcouponDate.getTime());
		productcouponDate.add(Calendar.DATE, 1);
		productCoupon.setPaymentDate(productcouponDate.getTime());
		productCoupon.setPayRate(15);
		productCoupon.setProductId(p.getId());
		productCoupon.setTriggerLevel(100);
		p.getProductCoupons().clear();
		p.getProductCoupons().add(productCoupon);
		
		DayCountConvention dayCountConvention = new DayCountConvention();
		dayCountConvention.setId(4);
		p.setDayCountConvention(dayCountConvention);
		
		ProductCouponFrequency pcf = new ProductCouponFrequency();
		pcf.setId(ProductCouponFrequency.IN_FINE);
		p.setProductCouponFrequency(pcf);
		
		testInsertSelect();
	}
	
	//RC
	@Test
	@Transactional
	public void insertAndSelectProductFullType4() throws Exception {
		logger.debug("insert and select ProductFull type 4");
		p.getProductType().setId(4);
		p.setStrikeLevel(70);
		
		DateFormat formatter = new SimpleDateFormat("dd/MM/yy");
		Date date = null;
		try {
			date = formatter.parse("15/05/17");
		} catch (ParseException e) {
			logger.error("", e);
		}
		
		ProductCoupon productCoupon = new ProductCoupon();
		Calendar productcouponDate = Calendar.getInstance();
		productcouponDate.add(Calendar.MONTH, 3);
		productCoupon.setObservationDate(productcouponDate.getTime());
		productcouponDate.add(Calendar.DATE, 1);
		productCoupon.setPaymentDate(date);
		productCoupon.setPayRate(12);
		productCoupon.setProductId(p.getId());
		productCoupon.setTriggerLevel(0);
		p.getProductCoupons().clear();
		p.getProductCoupons().add(productCoupon);
		
		DayCountConvention dayCountConvention = new DayCountConvention();
		dayCountConvention.setId(4);
		p.setDayCountConvention(dayCountConvention);
		
		ProductCouponFrequency pcf = new ProductCouponFrequency();
		pcf.setId(ProductCouponFrequency.IN_FINE);
		p.setProductCouponFrequency(pcf);
		
		testInsertSelect();
	}
	
	//BRC
	@Test
	@Transactional
	public void insertAndSelectProductFullType5() throws Exception {
		logger.debug("insert and select ProductFull type 5");
		p.getProductType().setId(5);
		
		ProductBarrier productBarrier = new ProductBarrier(); 
		Calendar cBarrier = Calendar.getInstance();
		cBarrier.add(Calendar.MONTH, 4);
		productBarrier.setBarrierStart(cBarrier.getTime());
		cBarrier.add(Calendar.MONTH, 1);
		productBarrier.setBarrierEnd(cBarrier.getTime());
		productBarrier.setBarrierLevel(59);
		ProductBarrierType productBarrierType = new ProductBarrierType();
		productBarrierType.setId(1);
		productBarrier.setProductBarrierType(productBarrierType);
		p.setProductBarrier(productBarrier);
		
		ProductCoupon productCoupon = new ProductCoupon();
		Calendar productcouponDate = Calendar.getInstance();
		productcouponDate.add(Calendar.MONTH, 3);
		productCoupon.setObservationDate(productcouponDate.getTime());
		productcouponDate.add(Calendar.DATE, 1);
		productCoupon.setPaymentDate(productcouponDate.getTime());
		productCoupon.setPayRate(12);
		productCoupon.setProductId(p.getId());
		productCoupon.setTriggerLevel(0);
		p.getProductCoupons().clear();
		p.getProductCoupons().add(productCoupon);
		
		DayCountConvention dayCountConvention = new DayCountConvention();
		dayCountConvention.setId(4);
		p.setDayCountConvention(dayCountConvention);
		
		ProductCouponFrequency pcf = new ProductCouponFrequency();
		pcf.setId(ProductCouponFrequency.IN_FINE);
		p.setProductCouponFrequency(pcf);
		
		testInsertSelect();
		p.setProductBarrier(null);
	}
	
	//IBRC
	@Test
	@Transactional
	public void insertAndSelectProductFullType6() throws Exception {
		logger.debug("insert and select ProductFull type 6");
		p.getProductType().setId(6);
		
		ProductBarrier productBarrier = new ProductBarrier(); 
		Calendar cBarrier = Calendar.getInstance();
		cBarrier.add(Calendar.MONTH, 4);
		productBarrier.setBarrierStart(cBarrier.getTime());
		cBarrier.add(Calendar.MONTH, 1);
		productBarrier.setBarrierEnd(cBarrier.getTime());
		productBarrier.setBarrierLevel(150);
		ProductBarrierType productBarrierType = new ProductBarrierType();
		productBarrierType.setId(1);
		productBarrier.setProductBarrierType(productBarrierType);
		p.setProductBarrier(productBarrier);

		ProductCoupon productCoupon = new ProductCoupon();
		Calendar productcouponDate = Calendar.getInstance();
		productcouponDate.add(Calendar.MONTH, 3);
		productCoupon.setObservationDate(productcouponDate.getTime());
		productcouponDate.add(Calendar.DATE, 1);
		productCoupon.setPaymentDate(productcouponDate.getTime());
		productCoupon.setPayRate(12);
		productCoupon.setProductId(p.getId());
		productCoupon.setTriggerLevel(0);
		p.getProductCoupons().clear();
		p.getProductCoupons().add(productCoupon);
		
		DayCountConvention dayCountConvention = new DayCountConvention();
		dayCountConvention.setId(4);
		p.setDayCountConvention(dayCountConvention);
		
		ProductCouponFrequency pcf = new ProductCouponFrequency();
		pcf.setId(ProductCouponFrequency.IN_FINE);
		p.setProductCouponFrequency(pcf);
		
		testInsertSelect();
		p.setProductBarrier(null);
	}
	
	//EC
	@Test
	@Transactional
	public void insertAndSelectProductFullType7() throws Exception {
		logger.debug("insert and select ProductFull type 7");
		p.getProductType().setId(ProductTypeEnum.EXPRESS_CERTIFICATE.getId());
		
		ProductBarrier productBarrier = new ProductBarrier(); 
		Calendar cBarrier = Calendar.getInstance();
		cBarrier.add(Calendar.MONTH, 4);
		productBarrier.setBarrierStart(cBarrier.getTime());
		cBarrier.add(Calendar.MONTH, 1);
		productBarrier.setBarrierEnd(cBarrier.getTime());
		productBarrier.setBarrierLevel(60);
		ProductBarrierType productBarrierType = new ProductBarrierType();
		productBarrierType.setId(1);
		productBarrier.setProductBarrierType(productBarrierType);
		p.setProductBarrier(productBarrier);

		ProductCoupon productCoupon = new ProductCoupon();
		Calendar productcouponDate = Calendar.getInstance();
		productcouponDate.add(Calendar.MONTH, 3);
		productCoupon.setObservationDate(productcouponDate.getTime());
		productcouponDate.add(Calendar.DATE, 1);
		productCoupon.setPaymentDate(productcouponDate.getTime());
		productCoupon.setPayRate(15);
		productCoupon.setProductId(p.getId());
		productCoupon.setTriggerLevel(100);
		p.getProductCoupons().clear();
		p.getProductCoupons().add(productCoupon);
		
		DayCountConvention dayCountConvention = new DayCountConvention();
		dayCountConvention.setId(4);
		p.setDayCountConvention(dayCountConvention);
		
		ProductCouponFrequency pcf = new ProductCouponFrequency();
		pcf.setId(ProductCouponFrequency.IN_FINE);
		p.setProductCouponFrequency(pcf);
		
		testInsertSelect();
		p.setProductBarrier(null);
	}
	
	//OC
	@Test
	@Transactional
	public void insertAndSelectProductFullType8() throws Exception {
		logger.debug("insert and select ProductFull type 8");
		p.getProductType().setId(8);
		p.setLevelOfParticipation(115);
//		p.setCapLevel(140);
		p.getProductCoupons().clear();
		testInsertSelect();
	}
	
	//BC
	@Test
	@Transactional
	public void insertAndSelectProductFullType9() throws Exception {
		logger.debug("insert and select ProductFull type 9");
		p.getProductType().setId(9);
		p.setBonusLevel(115);
		p.getProductCoupons().clear();
		
		ProductBarrier productBarrier = new ProductBarrier(); 
		Calendar cBarrier = Calendar.getInstance();
		cBarrier.add(Calendar.MONTH, 4);
		productBarrier.setBarrierStart(cBarrier.getTime());
		cBarrier.add(Calendar.MONTH, 1);
		productBarrier.setBarrierEnd(cBarrier.getTime());
		productBarrier.setBarrierLevel(60);
		ProductBarrierType productBarrierType = new ProductBarrierType();
		productBarrierType.setId(1);
		productBarrier.setProductBarrierType(productBarrierType);
		p.setProductBarrier(productBarrier);
		
		testInsertSelect();
		p.setProductBarrier(null);
	}
	
	//BOC
	@Test
	@Transactional
	@Ignore // type 10 not in use
	public void insertAndSelectProductFullType10() throws Exception {
		logger.debug("insert and select ProductFull type 10");
		p.getProductType().setId(10);
		p.setBonusLevel(115);
		p.setLevelOfParticipation(120);
		p.getProductCoupons().clear();
		
		ProductBarrier productBarrier = new ProductBarrier(); 
		Calendar cBarrier = Calendar.getInstance();
		cBarrier.add(Calendar.MONTH, 4);
		productBarrier.setBarrierStart(cBarrier.getTime());
		cBarrier.add(Calendar.MONTH, 1);
		productBarrier.setBarrierEnd(cBarrier.getTime());
		productBarrier.setBarrierLevel(60);
		ProductBarrierType productBarrierType = new ProductBarrierType();
		productBarrierType.setId(1);
		productBarrier.setProductBarrierType(productBarrierType);
		p.setProductBarrier(productBarrier);
		
		testInsertSelect();
		p.setProductBarrier(null);
	}
	
	public void testInsertSelect() throws Exception {
		
		Request<Product> r = new Request<Product>(p, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body " + json);
		
		MvcResult result = null;
		try {
			result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/insertProductFull")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		
			ObjectMapper mapper = new ObjectMapper();
			Response<Product> responseobject = (Response<Product>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Product>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
			
			result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/getProductFull")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(result.getResponse().getContentAsString())
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
				responseobject = (Response<Product>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Product>>() {});
				Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant insert and get full product " + e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void updateAskBid() throws Exception {
		logger.debug("update Ask Bid");
		Product product = new Product();
		product.setId(testProductId);
		product.setAsk(111);
		product.setBid(222);
		Request<Product> r = new Request<Product>(product, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/updateAskBid")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();;
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update Ask Bid ", e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Ignore //need unpublish product
	@Transactional
	public void publish() throws Exception {
		logger.debug("publish");
		Product product = new Product();
		product.setId(testProductId);
		Request<Product> r = new Request<Product>(product, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/publish")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update status ", e.getMessage());
			throw e;
		}
	}
	
	
	
	@Test
	@Transactional
	public void updateSuspend() throws Exception {
		logger.debug("update Suspend");
		Product product = new Product();
		product.setId(testProductId);
		product.setSuspend(false);
		Request<Product> r = new Request<Product>(product, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/updateSuspend")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update Suspend ", e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void updateStartTradeLevel() throws Exception {
		logger.debug("update Start Trade Level");
		ArrayList<ProductMarket> productMarkets = new ArrayList<ProductMarket>();
		
		Market market = new Market();
		market.setId(136);
		ProductMarket productMarket = new ProductMarket();
		productMarket.setMarket(market);
		productMarket.setProductId(testProductId);
		productMarket.setStartTradeLevel(102);
		productMarkets.add(productMarket);
		Request<ArrayList<ProductMarket>> r = new Request<ArrayList<ProductMarket>>(productMarkets, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/updateStartTradeLevel")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update Start Trade Level ", e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void updateEndTradeLevel() throws Exception {
		logger.debug("update End Trade Level");
		ArrayList<ProductMarket> productMarkets = new ArrayList<ProductMarket>();
		
		Market market = new Market();
		market.setId(136);
		ProductMarket productMarket = new ProductMarket();
		productMarket.setMarket(market);
		productMarket.setProductId(testProductId);
		productMarket.setEndTradeLevel((double) 103);
		productMarkets.add(productMarket);
		Request<ArrayList<ProductMarket>> r = new Request<ArrayList<ProductMarket>>(productMarkets, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/updateEndTradeLevel")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update End Trade Level ", e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void updateProductBarrierOccur() throws Exception {
		logger.debug("update End Trade Level");
		ProductBarrier productBarrier = new ProductBarrier();
		productBarrier.setProductId(testProductId);
		productBarrier.setBarrierOccur(false);
		Request<ProductBarrier> r = new Request<ProductBarrier>(productBarrier, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/updateProductBarrierOccur")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update End Trade Level ", e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getProductFilters() throws Exception {
		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/products/init")
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<ProductFilters> responseobject = (Response<ProductFilters>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<ProductFilters>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant get select Products Filters " + e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void getAndEditProduct() throws Exception {
		logger.debug("getAndEditProduct");
		Product product = new Product();
		product.setId(testProductId);
		Request<Product> r = new Request<Product>(product, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/getProductFull")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			
			Response<Product> responseobject = (Response<Product>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Product>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
			
			
			product = (Product) responseobject.getData();
			product.setDenomination(987654);
			r = new Request<Product>(product, ActionSource.CRM);
			json = mapper.writeValueAsString(r);	       
			result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/insertProductFull")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			responseobject = (Response<Product>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Product>>() {});
			Assert.assertEquals(((Product)responseobject.getData()).getDenomination(), 987654);
		} catch (Exception e) {
			logger.debug("cant get full product ", e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void updateProductsPriority() throws Exception {
		logger.debug("update Products Priority");
		ArrayList<Product> productPriority = new ArrayList<Product>();
		
		
		Product product1 = new Product();
		product1.setId(testProductId);
		product1.setPriority(1);
		
		productPriority.add(product1);
		
		Request<ArrayList<Product>> r = new Request<ArrayList<Product>>(productPriority, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/updateProductsPriority")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update Products Priority ", e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getPriorityProducts() throws Exception {
		logger.debug("getPriorityProducts");		
		MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/getPriorityProducts")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Product>> responseobject = (Response<List<Product>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Product>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());	
	}
	
	@Test
	@Ignore //need product with coupon
	@Transactional
	public void updateProductCouponObservationLevel() throws Exception {
		logger.debug("update ProductCoupon observation Level");
		ProductCoupon productCoupon = new ProductCoupon();
		productCoupon.setProductId(testProductId);
		productCoupon.setId(65);
		productCoupon.setObservationLevel(1238.60);
		Request<ProductCoupon> r = new Request<ProductCoupon>(productCoupon, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/product/productcoupon/update/observationlevel")
								.contentType(MediaType.APPLICATION_JSON)
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update End Trade Level ", e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void updateKid() throws Exception {
		logger.debug("update KID");
		java.io.File ile = new java.io.File("docs/fileUploadExample/KID.pdf");
        FileInputStream fileInputStream = new FileInputStream(ile);
        MockMultipartFile fstmp = new MockMultipartFile("fileUpload", ile.getName(), "multipart/form-data", fileInputStream);
        logger.info("fstmp.getContentType(): " + fstmp.getContentType() + ", fstmp.getName()" + fstmp.getOriginalFilename());
		// Product
		Product product = new Product();
		product.setId(testProductId);
		Language language = new Language();
		language.setId(LanguageEnum.EN.getId());
		ProductKid productKid = new ProductKid();
		productKid.setProduct(product);
		productKid.setLanguage(language);
		Request<ProductKid> r = new Request<ProductKid>(productKid, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.fileUpload("/product/updateKid")
								.file(fstmp)
								.contentType(MediaType.MULTIPART_FORM_DATA)
								.param("request", json)
								.with(user(custemUserDetailService.loadUserByUsername(writerActive))))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();;
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant update KID ", e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getProductKidLanguages() throws Exception {
		logger.debug("getProductKidLanguages");
		Product product = new Product();
		product.setId(testProductId);
		Request<Product> r = new Request<Product>(product, ActionSource.CRM);
		String json = mapper.writeValueAsString(r);	       
		logger.debug("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/product/getProductKidLanguages")
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<ProductKidLanguage>> responseobject = (Response<List<ProductKidLanguage>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<ProductKidLanguage>>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("cant get full product ", e.getMessage());
			throw e;
		}
	}
	
}
