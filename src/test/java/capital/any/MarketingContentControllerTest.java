package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.MarketingContent;
import capital.any.model.base.Writer;
import capital.any.security.CustomUserDetailService;
import capital.any.service.writer.IWriterService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class MarketingContentControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingContentControllerTest.class);
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private IWriterService writerService;
	
	private MockMvc mockMvc;
	private MarketingContent marketingContent;
	private Writer writer;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${marketing.default.id}")
	private Integer marketingDefaultId;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		writer = writerService.getWriterByEmailOrUserName(writerActive);
		marketingContent = new MarketingContent();
		marketingContent.setName("Test");
		marketingContent.setComments("comments");
		marketingContent.setPageContent("pageContent");
		marketingContent.setWriterId(writer.getId());
		marketingContent.setHTMLFilePath("1.html");
	}
	
	@Test
	@Transactional
	public void insert() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingContent> request = new Request<MarketingContent>(marketingContent, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {			
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.fileUpload("/marketing/content/insert")
					.file("test", null)
					.contentType(MediaType.MULTIPART_FORM_DATA)					
					.param("request", json)
					.with(user(writer)))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<MarketingContent> responseObject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<MarketingContent>>() {});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.debug("can't insert marketing content " + e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void edit() throws Exception {
		marketingContent.setId(2);
		marketingContent.setName("Test edit");
		marketingContent.setComments("comments");
		marketingContent.setPageContent("pageContent");
		marketingContent.setWriterId(writer.getId());
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingContent> request = new Request<MarketingContent>(marketingContent, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.fileUpload("/marketing/content/edit")
					.file("test", null)
					.contentType(MediaType.MULTIPART_FORM_DATA)					
					.param("request", json)
					.with(user(writer)))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@Test
	public void getAll() throws Exception {
		logger.debug("About to get all marketing content");
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.post("/marketing/content/getAll")
				.contentType(MediaType.APPLICATION_JSON)
				.with(user(writer))
				.accept(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<Map<Integer, MarketingContent>> responseObject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<Integer, MarketingContent>>>() {});
		Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getMarketingContent() throws Exception {
		marketingContent.setId(marketingDefaultId);
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingContent> request = new Request<MarketingContent>(marketingContent, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/marketing/content/getMarketingContent")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
}
