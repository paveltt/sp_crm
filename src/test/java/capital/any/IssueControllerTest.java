package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Issue;
import capital.any.model.base.IssueChannel;
import capital.any.model.base.IssueDirection;
import capital.any.model.base.IssueReachedStatus;
import capital.any.model.base.IssueReaction;
import capital.any.model.base.IssueSubject;
import capital.any.model.base.User;
import capital.any.model.base.Writer;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;
import capital.any.service.writer.IWriterService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class IssueControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(IssueControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private IUserService userService;
	@Autowired
	private IWriterService writerService;
	
	private MockMvc mockMvc;
	private Issue issue;
	private User user;
	
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;
	@Value("${writer.active}")
	private String writerActive;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		user = userService.getUserByEmail(userRegulatedWithMoney);
		Writer writer = writerService.getWriterByEmailOrUserName(writerActive);
		issue = new Issue();
		issue.setIssueChannel(new IssueChannel(1));
		issue.setIssueSubject(new IssueSubject(1));
		issue.setIssueDirection(new IssueDirection(1));
		issue.setIssueReachedStatus(new IssueReachedStatus(1));
		issue.setIssueReaction(new IssueReaction(1));
		issue.setComments("test");
		issue.setSignificantNote(true);
		issue.setUserId(user.getId());
		issue.setWriterId(writer.getId());
	}
	
	@Test
	@Transactional
	public void insert() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<Issue> request = new Request<Issue>(issue, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/issue/insert")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Issue> responseobject = (Response<Issue>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Issue>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("can't insert issue " + e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getIssuesByUserId() throws Exception {	
		User user = new User();
		user.setId(user.getId());
		
		Request<User> request = new Request<User>(user, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/issue/getByUserId")
					.sessionAttr(ISessionService.USER, user)
					.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Map<Long, Issue>> responseObject = 
					(Response<Map<Long, Issue>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Map<Long, Issue>>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("ERROR! getIssuesByUserId ", e);
			throw e;
		}
	}
}
