package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.MarketingSource;
import capital.any.model.base.Writer;
import capital.any.security.CustomUserDetailService;
import capital.any.service.writer.IWriterService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class MarketingSourceControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(MarketingSourceControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private IWriterService writerService;
	
	private MockMvc mockMvc;
	private MarketingSource marketingSource;
	private Writer writer;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${marketing.default.id}")
	private Integer marketingDefaultId;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		writer = writerService.getWriterByEmailOrUserName(writerActive);
		marketingSource = new MarketingSource();
		marketingSource.setName("test source");
		marketingSource.setWriterId(writer.getId());
	}
	
	@Test
	@Transactional
	public void insert() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingSource> request = new Request<MarketingSource>(marketingSource, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/marketing/source/insert")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<MarketingSource> responseobject = (Response<MarketingSource>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<MarketingSource>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("can't insert marketing source " + e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void edit() throws Exception {
		marketingSource.setId(marketingDefaultId);
		marketingSource.setName("google");
		marketingSource.setWriterId(6);
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingSource> request = new Request<MarketingSource>(marketingSource, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/marketing/source/edit")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@Test
	public void getAll() throws Exception {
		logger.debug("About to get all marketing sources");
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.post("/marketing/source/getAll")
				.contentType(MediaType.APPLICATION_JSON)
				.with(user(writer))
				.accept(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<Map<Long, MarketingSource>> responseobject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<Long, MarketingSource>>>() {});
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getMarketingSource() throws Exception {
		marketingSource.setId(marketingDefaultId);
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<MarketingSource> request = new Request<MarketingSource>(marketingSource, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/marketing/source/getMarketingSource")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
}
