package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.io.FileInputStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.security.CustomUserDetailService;

/**
 * @author eran.levy
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ContactControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(ContactControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;	
	
	private MockMvc mockMvc;
	
	@Value("${writer.active}")
	private String writerActive;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();		
	}
	
	@Test
	@Transactional
	public void uploadContacts() throws Exception {
		java.io.File file = new java.io.File("docs/fileUploadExample/upload-contacts.xlsx");
        FileInputStream fileInputStream = new FileInputStream(file);
        MockMultipartFile fstmp = new MockMultipartFile("fileUpload", file.getName(), "multipart/form-data", fileInputStream);
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		Request<Object> request = new Request<Object>(null, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.fileUpload("/contact/uploadContacts")
					.file(fstmp)
					.contentType(MediaType.MULTIPART_FORM_DATA)
					.param("request", json)
					.with(user(writer)))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseobject = (Response<Object>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Object>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.error("can't upload contacts ", e);
			throw e;
		}
	}
	
	
}
