package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.base.enums.EmailTo;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.EmailAction;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentEmailAction;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.User;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.investment.IInvestmentService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class EmailActionControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(EmailActionControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private IUserService userService;
	@Autowired
	private IInvestmentService investmentService;
	
	private MockMvc mockMvc;
	private Investment testInvestmet;
	private User user;
	
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;
	@Value("${writer.active}")
	private String writerActive;
	@Value("${user.regulated.limit}")
	private String userRegulatedLimit;
	
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		
		user = userService.getUserByEmail(userRegulatedWithMoney);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -360);
		Date dateBefore30Days = cal.getTime();
		SqlFilters filters = new SqlFilters();
		filters.setUserId(user.getId());
		filters.setFrom(dateBefore30Days);
		filters.setTo(new Date());;
		List<Investment> list = investmentService.getByUserId(filters);
		for (Investment inv : list) {
			testInvestmet = inv;
			break;
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void insertAfterRegister() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		
		// For register
		EmailActionRequest<Object> emailActionRequest = new EmailActionRequest<Object>();
		emailActionRequest.setData(null);
		emailActionRequest.setUser(user);
		emailActionRequest.setEmailAction(new EmailAction(capital.any.base.enums.EmailAction.REGISTER.getId()));
		emailActionRequest.setActionSource(ActionSource.CRM);
		emailActionRequest.setEmailTo(EmailTo.BOTH.getId());
		
		Request<EmailActionRequest<?>> request = new Request<EmailActionRequest<?>>(emailActionRequest, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/emailAction/insert")
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("can't insert after register ", e);
			throw e;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void insertAfterInvestment() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		
		// For investment
		InvestmentEmailAction investmentEmailAction = new InvestmentEmailAction();
		Investment inv = new Investment();
		inv.setId(testInvestmet.getId());
		investmentEmailAction.setInvestment(inv);
		
		EmailActionRequest<InvestmentEmailAction> emailActionRequest = new EmailActionRequest<InvestmentEmailAction>();
		emailActionRequest.setData(investmentEmailAction);
		emailActionRequest.setUser(user);
		emailActionRequest.setEmailAction(new EmailAction(capital.any.base.enums.EmailAction.INSERT_INVESTMENT_OPEN.getId()));
		emailActionRequest.setActionSource(ActionSource.CRM);
		
		Request<EmailActionRequest<InvestmentEmailAction>> request = new Request<EmailActionRequest<InvestmentEmailAction>>(emailActionRequest, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/emailAction/insert")
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("can't insert after investment ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void insertAfterDeposit() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		
		// For transaction
		Transaction transaction = new Transaction();
		transaction.setId(10881);
		
		EmailActionRequest<Transaction> emailActionRequest = new EmailActionRequest<Transaction>();
		emailActionRequest.setData(transaction);
		emailActionRequest.setUser(user);
		emailActionRequest.setEmailAction(new EmailAction(capital.any.base.enums.EmailAction.DEPOSIT_PENDING_BANK_WIRE.getId()));
		emailActionRequest.setActionSource(ActionSource.CRM);
		
		Request<EmailActionRequest<Transaction>> request = new Request<EmailActionRequest<Transaction>>(emailActionRequest, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/emailAction/insert")
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("can't insert after deposit ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void getListByAction() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		
		EmailActionRequest<InvestmentEmailAction> emailActionRequest = new EmailActionRequest<InvestmentEmailAction>();
		emailActionRequest.setData(null);
		emailActionRequest.setUser(user);
		emailActionRequest.setEmailAction(new EmailAction(capital.any.base.enums.EmailAction.INSERT_INVESTMENT_OPEN.getId()));
		emailActionRequest.setActionSource(ActionSource.CRM);
		
		Request<EmailActionRequest<InvestmentEmailAction>> request = new Request<EmailActionRequest<InvestmentEmailAction>>(emailActionRequest, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/emailAction/getListByAction")
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("can't get list by action", e);
			throw e;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void getListByActionWire() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		User user = userService.getUserByEmail(userRegulatedLimit);
		
		EmailActionRequest<Transaction> emailActionRequest = new EmailActionRequest<Transaction>();
		emailActionRequest.setData(null);
		emailActionRequest.setUser(user);
		emailActionRequest.setEmailAction(new EmailAction(capital.any.base.enums.EmailAction.DEPOSIT_PENDING_BANK_WIRE.getId()));
		emailActionRequest.setActionSource(ActionSource.CRM);
		
		Request<EmailActionRequest<Transaction>> request = new Request<EmailActionRequest<Transaction>>(emailActionRequest, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/emailAction/getListByAction")
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("get list by action wire ", e);
			throw e;
		}
		
	}

}
