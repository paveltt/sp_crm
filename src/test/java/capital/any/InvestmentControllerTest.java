package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Investment;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.User;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.investment.IInvestmentService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;

/**
 * @author EyalG
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class InvestmentControllerTest {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentControllerTest.class);
	
	@Autowired
	private WebApplicationContext webApplicationContext; 
	
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	
	private MockMvc mockMvc;
	@Autowired private ObjectMapper mapper;
	private Investment testInvestmet;
	
	@Autowired
	private IUserService userService;
	@Autowired
	private IInvestmentService investmentService;
	
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;
	@Value("${writer.active}")
	private String writerActive;
	
	@Before
	public void setup() {

		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
		
		User user = userService.getUserByEmail(userRegulatedWithMoney);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -30);
		Date dateBefore30Days = cal.getTime();
		SqlFilters filters = new SqlFilters();
		filters.setUserId(user.getId());
		filters.setFrom(dateBefore30Days);
		filters.setTo(new Date());;
		List<Investment> list = investmentService.getByUserId(filters);
		for (Investment inv : list) {
			testInvestmet = inv;
			break;
		}
	}
	
	@Test
	public void getAllInvesments() throws Exception {
		logger.debug("getAllInvesments");
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		SqlFilters filters = new SqlFilters();
//		filters.setUserClassId(Clazz.REAL.getToken());
		Request<SqlFilters> request = new Request<SqlFilters>(filters, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/get")
								.with(user(writer))
								.content(json)
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Investment>> responseobject = (Response<List<Investment>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Investment>>>() {});
			
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());	
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getUserInvesments() throws Exception {
		logger.debug("getUserInvesments");		
		
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		User user = userService.getUserByEmail(userRegulatedWithMoney);
		SqlFilters filters = new SqlFilters();
//		Calendar cal = Calendar.getInstance();
//		cal.add(Calendar.DAY_OF_MONTH, -22);
//		filters.setFrom(cal.getTime());
		
//		Calendar calTo = Calendar.getInstance();
//		calTo.add(Calendar.DAY_OF_MONTH, -1);
//		filters.setTo(calTo.getTime());
		Request<SqlFilters> request = new Request<SqlFilters>(filters, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);	       
		logger.debug("body: " + json);
		
		MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/get/session/user/id")
								.sessionAttr(ISessionService.USER, user)
								.with(user(writer))
								.content(json)
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<List<Investment>> responseobject = (Response<List<Investment>>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<List<Investment>>>() {});
			
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());	
	}
	
	@Test
	@Ignore //need not canceled investment
	@Transactional
	public void cancelInvestment() throws Exception {
		logger.debug("cancel Investment");
		Investment investment = new Investment();
		investment.setId(testInvestmet.getId());
		Request<Investment> request = new Request<Investment>(investment, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.debug("request body: " + json);
		MvcResult result = mockMvc.perform(
								MockMvcRequestBuilders.post("/investment/cancel")
								.with(user(custemUserDetailService.loadUserByUsername(writerActive)))
								.contentType(MediaType.APPLICATION_JSON)
								.content(json)
								.accept(MediaType.APPLICATION_JSON))
								.andDo(MockMvcResultHandlers.print())
								.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Investment> responseobject = (Response<Investment>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Investment>>() {});
			
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());	
	}
	
}