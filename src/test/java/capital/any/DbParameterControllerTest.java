package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.DBParameter;
import capital.any.security.CustomUserDetailService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class DbParameterControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(DbParameterControllerTest.class);
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	
	private MockMvc mockMvc;
	
	@Value("${writer.active}")
	private String writerActive;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}
	
	public MvcResult insert(UserDetails writer) throws Exception {
		DBParameter dbParameter = new DBParameter();
		dbParameter.setName(UUID.randomUUID().toString().substring(0, 10)); 
		dbParameter.setStringValue("test");
		dbParameter.setComments("test");		
		Request<DBParameter> request = new Request<DBParameter>(dbParameter, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("Body: " + json);
		MvcResult result = null;
		try {
			result = mockMvc.perform(
					MockMvcRequestBuilders.post("/dbparameter/insert")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		} catch (Exception e) {
			logger.debug("can't insert DBParameter source " + e.getMessage());
			throw e;
		}
		return result;
	}
	
	@Test
	@Transactional
	public void insert() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		try {
			MvcResult result = insert(writer);
			ObjectMapper mapper = new ObjectMapper();
			Response<DBParameter> responseobject = (Response<DBParameter>) mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<DBParameter>>() {});
			Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
		} catch (Exception e) {
			logger.debug("can't insert DBParameter source " + e.getMessage());
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Transactional
	public void update() throws Exception {
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		try {
			ObjectMapper mapper = new ObjectMapper();
			DBParameter dbParameter = new DBParameter();
			dbParameter.setId(capital.any.base.enums.DBParameter.COMPANY_ADDRESS.getId());
			dbParameter.setName("test update");
			dbParameter.setStringValue("string update");
			Request<DBParameter> request = new Request<DBParameter>(dbParameter, ActionSource.CRM);
			String json = mapper.writeValueAsString(request);
			logger.info("body: " + json);
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/dbparameter/update")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.with(user(writer))
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			Response<Object> responseObject = 
					(Response<Object>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Object>>(){});
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void getAll() throws Exception {
		logger.debug("About to get all dbparameter");
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.post("/dbparameter/get")
				.contentType(MediaType.APPLICATION_JSON)
				.with(user(writer))
				.accept(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<Map<Integer, DBParameter>> responseobject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<Map<Integer, DBParameter>>>() {});
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}
	
	@Test
	@Transactional
	public void getById() throws Exception {
		logger.debug("About to get dbparameter by id");
		UserDetails writer = custemUserDetailService.loadUserByUsername("junit");
		DBParameter dbParameter = new DBParameter();
		dbParameter.setId(capital.any.base.enums.DBParameter.COMPANY_ADDRESS.getId());
		Request<DBParameter> request = new Request<DBParameter>(dbParameter, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);
		logger.info("body: " + json);
		MvcResult result = mockMvc.perform(
				MockMvcRequestBuilders.post("/dbparameter/get/id")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(user(writer))
				.accept(MediaType.APPLICATION_JSON))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
		ObjectMapper mapper = new ObjectMapper();
		Response<DBParameter> responseobject = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Response<DBParameter>>() {});
		Assert.assertEquals(ResponseCode.OK, responseobject.getResponseCode());
	}	
}
