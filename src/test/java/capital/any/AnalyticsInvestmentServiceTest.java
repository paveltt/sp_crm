package capital.any;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import capital.any.model.base.analytics.AnalyticsInvestment;
import capital.any.service.analyticsInvestment.IAnalyticsInvestmentService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class AnalyticsInvestmentServiceTest {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsInvestmentServiceTest.class);
	
	@Autowired
	private IAnalyticsInvestmentService analyticsInvestmentService;
	
	@Test
	public void get() {
		logger.debug("About to get AnalyticsInvestment.");
		AnalyticsInvestment analyticsInvestment = analyticsInvestmentService.getAnalyticsInvestment();
		Assert.assertNotNull(analyticsInvestment);
	}
}
