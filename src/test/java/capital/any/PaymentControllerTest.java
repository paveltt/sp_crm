package capital.any;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.base.enums.ActionSource;
import capital.any.model.base.PaymentRequest;
import capital.any.model.base.PaymentResponse;
import capital.any.model.base.Transaction.TransactionPaymentTypeEnum;
import capital.any.model.base.TransactionPaymentType;
import capital.any.model.base.User;
import capital.any.model.base.Wire;
import capital.any.security.CustomUserDetailService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.base.user.IUserService;

/**
 * @author LioR SoLoMoN
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class PaymentControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(PaymentControllerTest.class);
	private static final String DO_DEPOSIT_URL = "/payment/deposit";
	private static final String DO_WITHDRAW_URL = "/payment/withdraw";
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private CustomUserDetailService custemUserDetailService;
	@Autowired
	private IUserService userService;
	@Autowired
	private ObjectMapper mapper;
	private MockMvc mockMvc;
	
	@Value("${writer.active}")
	private String writerActive;
	@Value("${user.regulated.with.money}")
	private String userRegulatedWithMoney;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.apply(springSecurity())
				.build();
	}

	@SuppressWarnings("unchecked")
	@Test	
	//@Repeat(20)
	@Transactional
	public void doWireDeposit() throws Exception {
		PaymentRequest<Wire> pd = new PaymentRequest<Wire>();
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		User user = userService.getUserByEmail(userRegulatedWithMoney);
		
		Wire wire = new Wire();
		wire.setBranchAddress("CRM");
		wire.setBankName("test");
		wire.setIban("test");
		wire.setSwift("test");
		wire.setAccountInfo("test");
		wire.setAccountNum(123456);
		wire.setBranchNumber(1);
		wire.setBeneficiaryName("test");
		
		pd.setAmount(10000);
		pd.setDetails(wire);
		pd.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		pd.setComments("try do deposit");
		
		Request<PaymentRequest<Wire>> request = new Request<PaymentRequest<Wire>>(pd, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);       
		logger.debug("doDeposit json: " + json);		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post(DO_DEPOSIT_URL)
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<PaymentResponse> responseObject = 
					(Response<PaymentResponse>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<PaymentResponse>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant insert deposit ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test	
	@Transactional
	public void doAdminDeposit() throws Exception {
		PaymentRequest<Object> pd = new PaymentRequest<Object>();
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		User user = userService.getUserByEmail(userRegulatedWithMoney);

		pd.setAmount(2000);
		//pd.setDetails();
		pd.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.ADMIN.getId()));
		pd.setComments("try do deposit");
		
		Request<PaymentRequest<Object>> request = new Request<PaymentRequest<Object>>(pd, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);       
		logger.debug("doDeposit json: " + json);		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post(DO_DEPOSIT_URL)
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<PaymentResponse> responseObject = 
					(Response<PaymentResponse>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<PaymentResponse>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant insert admin deposit ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test	
	@Transactional
	//@Ignore
	public void doWireWithdraw() throws Exception {
		PaymentRequest<Wire> pd = new PaymentRequest<Wire>();
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		User user = userService.getUserByEmail(userRegulatedWithMoney);
		
		Wire wire = new Wire();
		wire.setBranchAddress("CRM");
		wire.setBankName("test");
		wire.setIban("test");
		wire.setSwift("test");
		wire.setAccountInfo("test");
		wire.setAccountNum(123456);
		wire.setBranchNumber(1);
		wire.setBeneficiaryName("test");
		
		pd.setAmount(20000);
		pd.setDetails(wire);
		pd.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.WIRE.getId()));
		pd.setComments("try do Withdraw");
		
		Request<PaymentRequest<Wire>> request = new Request<PaymentRequest<Wire>>(pd, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);       
		logger.debug("doWithdraw json: " + json);		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post(DO_WITHDRAW_URL)
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<PaymentResponse> responseObject = 
					(Response<PaymentResponse>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<PaymentResponse>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant insert Withdraw ", e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test	
	@Transactional
	//@Ignore
	public void doAdminWithdraw() throws Exception {
		PaymentRequest<Object> pd = new PaymentRequest<Object>();
		UserDetails writer = custemUserDetailService.loadUserByUsername(writerActive);
		User user = userService.getUserByEmail(userRegulatedWithMoney);
		
		pd.setAmount(10000);
		//pd.setDetails();
		pd.setPaymentType(new TransactionPaymentType(TransactionPaymentTypeEnum.ADMIN.getId()));
		pd.setComments("try do Withdraw");
		
		Request<PaymentRequest<Object>> request = new Request<PaymentRequest<Object>>(pd, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);       
		logger.debug("doWithdraw json: " + json);		
		try {
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post(DO_WITHDRAW_URL)
					.sessionAttr(ISessionService.USER, user)
					.with(user(writer))
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<PaymentResponse> responseObject = 
					(Response<PaymentResponse>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<PaymentResponse>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
		} catch (Exception e) {
			logger.error("cant insert Withdraw ", e);
			throw e;
		}
	}
}
