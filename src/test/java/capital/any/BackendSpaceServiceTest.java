package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.BeSpace;
import capital.any.service.backendSpace.IBackendSpaceService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration   
public class BackendSpaceServiceTest {

	@Autowired
	private IBackendSpaceService backendSpaceService;
	
	@Test
	@Transactional
    public void insertWriterGroup() {
		BeSpace beSpace1 = new BeSpace();
		beSpace1.setName("users");				
		backendSpaceService.insert(beSpace1);
		BeSpace beSpace2 = new BeSpace();
		beSpace2.setName("content");				
		backendSpaceService.insert(beSpace2);	
	}
		
}
	
