package capital.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import capital.any.model.BePermission;
import capital.any.service.backendPermission.IBackendPermissionService;

/**
 * @author eranl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration   
public class BackendPermissionServiceTest {

	@Autowired
	private IBackendPermissionService backendPermissionService;
	
	@Test
	@Transactional
    public void insertBackendPermission() {
		BePermission bePermission = new BePermission();
		bePermission.setName("add");
		backendPermissionService.insert(bePermission);
	}
		
}
	
