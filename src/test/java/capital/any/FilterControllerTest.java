package capital.any;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.filter.FilterService;
import capital.any.base.enums.ActionSource;

@RunWith(SpringJUnit4ClassRunner.class)  
@SpringApplicationConfiguration(classes = Application.class)   
@WebAppConfiguration
public class FilterControllerTest {
	private static final Logger logger = LoggerFactory.getLogger(FilterControllerTest.class);
	private MockMvc mockMvc;
	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private ObjectMapper mapper;
	
	@Before
	public void setup() {
		mockMvc = MockMvcBuilders
				.webAppContextSetup(webApplicationContext)
				.build();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFilters() throws Exception {
		logger.debug("testFilters");
		List<String> filterList = new ArrayList<String>();
		filterList.add(FilterService.MAP_FILE_STATUS);
			
		Request<List<String>> request = new Request<List<String>>(filterList, ActionSource.CRM);
		String json = mapper.writeValueAsString(request);       
		logger.debug("get filters json: " + json);		
			MvcResult result = mockMvc.perform(
					MockMvcRequestBuilders.post("/filters/get")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json)
					.accept(MediaType.APPLICATION_JSON))
					.andDo(MockMvcResultHandlers.print())
					.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
			ObjectMapper mapper = new ObjectMapper();
			Response<Map<String, Map<Integer, ?>>> responseObject = 
					(Response<Map<String, Map<Integer, ?>>>) mapper
						.readValue(result
								.getResponse()
								.getContentAsString(),
								new TypeReference<Response<Map<String, Map<Integer, ?>>>>(){});	
			Assert.assertEquals(ResponseCode.OK, responseObject.getResponseCode());
	}
}
