package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Issue;
import capital.any.model.base.User;
import capital.any.security.ISecurityService;
import capital.any.service.base.issue.IIssueService;
import capital.any.service.base.session.ISessionService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/issue", method = RequestMethod.POST)
public class IssueController {
	private static final Logger logger = LoggerFactory.getLogger(IssueController.class);
	
	@Autowired
	private IIssueService issueService;
	@Autowired
    private ISecurityService securityService;
	@Autowired
	private ISessionService sessionService;
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<Issue>> insert(@RequestBody Request<Issue> request) {
		Response<Issue> response = null;
		try {
			if (securityService.isAuthenticate()) {
				Issue issue = (Issue) request.getData();
				issueService.insert(issue);
				response = new Response<Issue>(issue, ResponseCode.OK);
			} else {
				response = new Response<Issue>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("Problem with insert issue. ", e);
			response = new Response<Issue>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Issue>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getByUserId")
	public ResponseEntity<Response<Map<Long, Issue>>> getIssuesByUserId(@RequestBody Request<Object> request, HttpSession session) {
		logger.info("about to get Issues By UserId.");
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Long, Issue> hm = null;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				hm = issueService.getIssuesByUserId(user.getId());
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on get Issues By UserId.", e);
		}
		Response<Map<Long, Issue>> response = new Response<Map<Long, Issue>>(hm, responseCode, messages);
		return new ResponseEntity<Response<Map<Long, Issue>>>(response, HttpStatus.OK);
	}
}
