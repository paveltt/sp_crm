package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.EmailActionRequest;
import capital.any.model.base.EmailActionResponse;
import capital.any.model.base.User;
import capital.any.model.base.Writer;
import capital.any.security.ISecurityService;
import capital.any.service.base.emailAction.IEmailActionService;
import capital.any.service.base.session.ISessionService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/emailAction", method = RequestMethod.POST)
public class EmailActionController {
	private static final Logger logger = LoggerFactory.getLogger(EmailActionController.class);
	
	@Autowired
	private IEmailActionService emailActionService;
	@Autowired
    private ISecurityService securityService;
	@Autowired
	private ISessionService sessionService;
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<Object>> insert(@RequestBody Request<EmailActionRequest<?>> request, HttpSession session) {
		logger.info("about to insert email actions.");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {
				EmailActionRequest<?> emailActionRequest = (EmailActionRequest<?>) request.getData();
				emailActionRequest.setUser((User)sessionService.getAttribute(session, ISessionService.USER));
				emailActionRequest.setActionSource(request.getActionSource());
				Writer writer = securityService.getLoginFromSession();
				emailActionRequest.setWriter(writer);
				emailActionService.insertEmailByAction(emailActionRequest);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on insert email action. ", e);
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getListByAction")
	public ResponseEntity<Response<Object>> getListByAction(@RequestBody Request<EmailActionRequest<?>> request, HttpSession session) {
		logger.info("about to get list by action.");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		EmailActionResponse<?> emailActionResponse = null;
		try {
			if (securityService.isAuthenticate()) {
				EmailActionRequest<?> emailActionRequest = (EmailActionRequest<?>) request.getData();
				emailActionRequest.setUser((User)sessionService.getAttribute(session, ISessionService.USER));
				emailActionRequest.setActionSource(request.getActionSource());
				emailActionResponse = emailActionService.getListByAction(emailActionRequest);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on get email action. ", e);
		}
		Response<Object> response = new Response<Object>(emailActionResponse, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
}
