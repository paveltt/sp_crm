package capital.any.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Writer;
import capital.any.security.ISecurityService;
import capital.any.service.contact.IContactService;

/**
 * @author eran.levy
 *
 */
@RestController
@RequestMapping(value = "/contact", method = RequestMethod.POST)
public class ContactController {
	private static final Logger logger = LoggerFactory.getLogger(ContactController.class);
	
	@Autowired
	private ISecurityService securityService;
	@Autowired @Qualifier("ContactServiceCRM")
	private IContactService contactService;
		
	@RequestMapping(value = "/uploadContacts")
	public ResponseEntity<Response<Object>> uploadContacts(@RequestParam(value = "request") String requestJson,
			@RequestParam(value = "fileUpload", required = false) MultipartFile fileUpload) {		
		logger.debug("about to upload contacts file" + fileUpload.getName() + " " + fileUpload.getOriginalFilename() + " " + requestJson);
		ObjectMapper mapper = new ObjectMapper();
		Request<Object> request = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			request = (Request<Object>) mapper.readValue(requestJson, new TypeReference<Request<Object>>() {});
			if (securityService.isAuthenticate()) {
				Writer writer = securityService.getLoginFromSession();
				contactService.uploadContacts(fileUpload, writer.getId());
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on upload contacts", e);
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);		
	}
		
}
