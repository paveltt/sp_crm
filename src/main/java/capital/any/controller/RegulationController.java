package capital.any.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.base.enums.ActionSource;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.dao.base.writer.BaseWriterFactory;
import capital.any.model.base.AbstractWriter;
import capital.any.model.base.RegulationDetails;
import capital.any.model.base.RegulationUserInfo;
import capital.any.model.base.User;
import capital.any.model.base.UserHistory;
import capital.any.security.ISecurityService;
import capital.any.service.base.regulation.IRegulationService;
import capital.any.service.base.regulationUserInfo.IRegulationUserInfoService;
import capital.any.service.base.session.ISessionService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/regulation", method = RequestMethod.POST)
public class RegulationController {
	private static final Logger logger = LoggerFactory.getLogger(RegulationController.class);
	
	@Autowired
	private IRegulationService regulationService;
	@Autowired
    private ISecurityService securityService;
	@Autowired
	private ISessionService sessionService;
	@Autowired
	private IRegulationUserInfoService regulationUserInfoService;
	@Autowired
	private BaseWriterFactory writerFactory;
	
	@RequestMapping(value = "/user/approve")
	public ResponseEntity<Response<Object>> approveUser(HttpSession session) {
		logger.debug("Approve user start.");
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);				
				int writerId = securityService.getLoginFromSession().getId();
				AbstractWriter writer = writerFactory.createWriter(writerId);
				writer.setId(writerId);
				responseCode = regulationService.approveUser(user, new UserHistory(ActionSource.CRM, writer));
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! approveUser", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	/******** Finotec **********/
	/*@RequestMapping(value = "/questionnaire/fittingAfterTraining")
	public ResponseEntity<Response<Object>> questionnaireFittingAfterTraining(HttpSession session) {
		logger.debug("Questionnaire fitting after training.");
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				int writerId = securityService.getLoginFromSession().getId();
				RegulationUserInfo regulationUserInfo = regulationUserInfoService.get(user.getId());
				AbstractWriter writer = writerFactory.createWriter(writerId);
				writer.setId(writerId);
				regulationUserInfo.setWriter(writer);
				RegulationDetails regulationDetails = new RegulationDetails(null, user, regulationUserInfo);
				responseCode = regulationService.questionnaireFittingAfterTraining(regulationDetails);
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! Questionnaire fitting after training", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}*/
	
	@RequestMapping(value = "/info/get")
	public ResponseEntity<Response<RegulationDetails>> getRegulationDetails(HttpSession session) {
		logger.info("about to get regulation details.");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		RegulationDetails regulationDetails = null;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				regulationDetails = regulationService.getRegulationDetails(user.getId());
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getRegulationDetails. ", e);
		}
		Response<RegulationDetails> response = new Response<RegulationDetails>(regulationDetails, responseCode);
		return new ResponseEntity<Response<RegulationDetails>>(response, HttpStatus.OK);
	}
	
	/******** ODT **********/
	@RequestMapping(value = "/questionnaire/fittingAfterTraining")
	public ResponseEntity<Response<Object>> questionnaireFittingAfterTraining(HttpSession session) {
		logger.debug("Questionnaire fitting after training.");
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				int writerId = securityService.getLoginFromSession().getId();
				RegulationUserInfo regulationUserInfo = regulationUserInfoService.get(user.getId());
				AbstractWriter writer = writerFactory.createWriter(writerId);
				writer.setId(writerId);
				regulationUserInfo.setWriter(writer);
				RegulationDetails regulationDetails = new RegulationDetails(null, user, regulationUserInfo);
				responseCode = regulationService.questionnaireFittingAfterTrainingODT(regulationDetails, new UserHistory(ActionSource.CRM, writer));
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! Questionnaire fitting after training", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
}
