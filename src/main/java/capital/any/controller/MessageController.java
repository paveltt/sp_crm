package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResFile;
import capital.any.model.base.MsgResLanguage;
import capital.any.security.ISecurityService;
import capital.any.service.messageResource.IMessageResourceService;
import capital.any.service.messageResourceLanguage.IMessageResourceLanguageService;

/**
 * @author eranl
 *
 */
@RestController
@RequestMapping(value = "/message", method = RequestMethod.POST)
public class MessageController {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageController.class);
	
	@Autowired
	private IMessageResourceService messageResourceService;
		
	@Autowired
	private IMessageResourceLanguageService messageResourceLanguageService;
	
	@Autowired
	private ISecurityService securityService;
	
	@RequestMapping(value = "/getMessageResourcesFull")
	public ResponseEntity<Response<Object>> getMessageResourcesFull(@RequestBody Request<MsgRes> request) throws JsonProcessingException {
		logger.info("Going to getMessageResourcesFull " + request.getActionSource());
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Integer, Map<String, MsgResLanguage>> hm = null;
		try {
			MsgRes msgRes = (MsgRes) request.getData();
			hm = messageResourceService.getMessageResourcesFull().get(msgRes.getActionSourceId());			
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getMessageResourcesFull ", e);
		}
		Response<Object> response = new Response<Object>(hm, responseCode, messages);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getMessageResources")
	public ResponseEntity<Response<Object>> getMessageResourcesBySourceActionId(@RequestBody Request<MsgRes> request) throws JsonProcessingException {
		logger.info("Going to getMessageResourcesBySourceActionId " + request.getActionSource());
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Integer, Map<String, String>> hm = null;
		try {
			MsgRes msgRes = (MsgRes) request.getData();
			hm = messageResourceService.get().get(msgRes.getActionSourceId());			
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getMessageResources ", e);
		}
		Response<Object> response = new Response<Object>(hm, responseCode, messages);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
			
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insertMessageResource") 
	public ResponseEntity<Response<MsgResLanguage>> insertMessageResources(@RequestBody Request<List<MsgResLanguage>> request) {
		logger.info("Going to insertMessageResources " + request.getData());
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {
				ArrayList<MsgResLanguage> msgResLanguages = (ArrayList<MsgResLanguage>) request.getData();
				msgResLanguages.forEach(msgResLanguage -> msgResLanguage.setWriterId(securityService.getLoginFromSession().getId()));
				messageResourceService.insertMessageResources(msgResLanguages.get(0).getMsgRes(), msgResLanguages);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on insertMessageResources ", e);
		}
		Response<MsgResLanguage> response = new Response<MsgResLanguage>(null, responseCode);
		return new ResponseEntity<Response<MsgResLanguage>>(response, HttpStatus.OK);
	}
	
	//TODO not in use - delete
	@RequestMapping(value = "/insertMessageResourceLargeValue") 
	public ResponseEntity<Response<MsgResLanguage>> insertMessageResourcesLargeValue(@RequestBody Request<List<MsgResLanguage>> request) {
		logger.info("Going to insertMessageResourcesLargeValue " + request.getData());
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		try {
			ArrayList<MsgResLanguage> msgResLanguages = (ArrayList<MsgResLanguage>) request.getData();
			messageResourceService.insertMessageResources(msgResLanguages.get(0).getMsgRes(), msgResLanguages);
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on insertMessageResources ", e);
		}
		Response<MsgResLanguage> response = new Response<MsgResLanguage>(new MsgResLanguage(), responseCode, messages);
		return new ResponseEntity<Response<MsgResLanguage>>(response, HttpStatus.OK);
	}
	
	//TODO naming convention - mapping
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/update/msgreslanguage") 
	public ResponseEntity<Response<List<MsgResLanguage>>> updateMsgResLanguage(@RequestBody Request<List<MsgResLanguage>> request) {
		logger.info("Going to update Msg Res Language " + request.getData());
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<MsgResLanguage> msgResLanguages = null;
		try {
			if (securityService.isAuthenticate()) {
				msgResLanguages = (List<MsgResLanguage>) request.getData();
				msgResLanguages.forEach(msgResLanguage -> {
					msgResLanguage.setWriterId(securityService.getLoginFromSession().getId());
				});
				
				if (messageResourceLanguageService.update(msgResLanguages)) {
					responseCode = ResponseCode.OK;
				} else {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on updateMsgResLanguage ", e);
		}
		Response<List<MsgResLanguage>> response = new Response<List<MsgResLanguage>>(msgResLanguages, responseCode);
		return new ResponseEntity<Response<List<MsgResLanguage>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/msgreslanguage/bykey") 
	public ResponseEntity<Response<List<MsgResLanguage>>> getMsgResLanguageByKey(@RequestBody Request<MsgRes> request) {
		logger.info("Going to get Msg Res Language " + request.getData());
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<MsgResLanguage> msgResLanguages = null;
		try {
			if (securityService.isAuthenticate()) {
				MsgRes msgRes = (MsgRes) request.getData();
				msgResLanguages = messageResourceLanguageService.getByKey(msgRes);
				responseCode = ResponseCode.OK;				
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getMsgResLanguage ", e);
		}
		Response<List<MsgResLanguage>> response = new Response<List<MsgResLanguage>>(msgResLanguages, responseCode);
		return new ResponseEntity<Response<List<MsgResLanguage>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/msgreslanguage/missinglanguages") 
	public ResponseEntity<Response<List<MsgResLanguage>>> getKeyMissingLanguage(@RequestBody Request<MsgRes> request) {
		logger.info("Going to get Key Missing Language " + request.getData());
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<MsgResLanguage> msgResLanguages = null;
		try {
			if (securityService.isAuthenticate()) {
				MsgRes msgRes = (MsgRes) request.getData();
				msgResLanguages = messageResourceLanguageService.getKeyMissingLanguage(msgRes);
				responseCode = ResponseCode.OK;				
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getKeyMissingLanguage ", e);
		}
		Response<List<MsgResLanguage>> response = new Response<List<MsgResLanguage>>(msgResLanguages, responseCode);
		return new ResponseEntity<Response<List<MsgResLanguage>>>(response, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insert/MessageResourceLanguage") 
	public ResponseEntity<Response<Object>> insertMessageResourcesLanguage(@RequestBody Request<List<MsgResLanguage>> request) {
		logger.info("Going to insertMessageResourcesLanguage " + request.getData());
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {
				ArrayList<MsgResLanguage> msgResLanguages = (ArrayList<MsgResLanguage>) request.getData();
				msgResLanguages.forEach(msgResLanguage -> msgResLanguage.setWriterId(securityService.getLoginFromSession().getId()));
				messageResourceLanguageService.insertMessageResourcesLanguages(msgResLanguages);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on insertMessageResources ", e);
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/insert/MessageResourceByFile")
	public ResponseEntity<Response<Object>> insertMessageResourcesByFile(@RequestParam(value = "request") String requestJson,
			@RequestParam(value = "fileUpload", required = false) MultipartFile fileUpload) {
		logger.debug("about to insertMessageResourcesByFile " + fileUpload.getName() + " " + fileUpload.getOriginalFilename() + " " + requestJson);
		ObjectMapper mapper = new ObjectMapper();
		Request<MsgResFile> request = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			request = (Request<MsgResFile>) mapper.readValue(requestJson, new TypeReference<Request<MsgResFile>>() {});
			if (securityService.isAuthenticate()) {
				MsgResFile msgResFile = (MsgResFile) request.getData();
				msgResFile.setWriterId(securityService.getLoginFromSession().getId());
				msgResFile.setFileUpload(fileUpload);
				messageResourceService.insertMessageResourcesByFile(msgResFile);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on insertMessageResourcesByFile", e);
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}

}
