package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.MarketingSource;
import capital.any.model.base.Writer;
import capital.any.security.ISecurityService;
import capital.any.service.base.marketingSource.IMarketingSourceService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/marketing/source", method = RequestMethod.POST)
public class MarketingSourceController {
	private static final Logger logger = LoggerFactory.getLogger(MarketingSourceController.class);
	
	@Autowired
	private IMarketingSourceService marketingSourceService;
	@Autowired
    private ISecurityService securityService;
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<MarketingSource>> insert(@RequestBody Request<MarketingSource> request) {
		Response<MarketingSource> response = null;
		try {
			if (securityService.isAuthenticate()) {
				MarketingSource marketingSource = (MarketingSource) request.getData();
				Writer writer = securityService.getLoginFromSession();
				marketingSource.setWriterId(writer.getId());
				marketingSourceService.insert(marketingSource);
				response = new Response<MarketingSource>(marketingSource, ResponseCode.OK);
			} else {
				response = new Response<MarketingSource>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("can't insert marketing source");
			response = new Response<MarketingSource>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<MarketingSource>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/edit")
	public ResponseEntity<Response<Object>> edit(@RequestBody Request<MarketingSource> request) {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		try {
			if (securityService.isAuthenticate()) {
				MarketingSource marketingSource = (MarketingSource) request.getData();
				Writer writer = securityService.getLoginFromSession();
				marketingSource.setWriterId(writer.getId());
				marketingSourceService.update(marketingSource);
				responseCode = ResponseCode.OK;
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! edit marketing source", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAll")
	public ResponseEntity<Response<Map<Integer, MarketingSource>>> getAll() {
		logger.info("about to getAll marketing sources.");
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Integer, MarketingSource> hm = null;
		try {
			if (securityService.isAuthenticate()) {
				hm = marketingSourceService.getMarketingSources();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getAll marketing sources.", e);
		}
		Response<Map<Integer, MarketingSource>> response = new Response<Map<Integer, MarketingSource>>(hm, responseCode, messages);
		return new ResponseEntity<Response<Map<Integer, MarketingSource>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getMarketingSource")
	public ResponseEntity<Response<MarketingSource>> getMarketingSource(@RequestBody Request<MarketingSource> request) {
		Response<MarketingSource> r;
		ResponseCode responseCode = ResponseCode.OK;
		MarketingSource marketingSource = null;
		try {
			if (securityService.isAuthenticate()) {
				marketingSource = (MarketingSource) request.getData();
				marketingSource = marketingSourceService.getMarketingSource(marketingSource.getId());
				if (null == marketingSource) {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("can't get marketing source", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		r = new Response<MarketingSource>(marketingSource, responseCode);
		return new ResponseEntity<Response<MarketingSource>>(r, HttpStatus.OK);
	}
}
