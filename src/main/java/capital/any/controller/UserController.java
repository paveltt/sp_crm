package capital.any.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Error;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.dao.base.writer.BaseWriterFactory;
import capital.any.model.ClientSpace;
import capital.any.model.base.AbstractWriter;
import capital.any.model.base.ChangePassword;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.User;
import capital.any.model.base.User.AddUserValidatorGroup;
import capital.any.model.base.User.EditUserValidatorGroup;
import capital.any.model.base.UserHistory;
import capital.any.security.ISecurityService;
import capital.any.service.base.marketingTracking.IMarketingTrackingService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.user.IUserService;

/**
 * @author eranl
 *
 */
@RestController
@RequestMapping(value = "/user", method = RequestMethod.POST)
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private ISessionService sessionService;
	@Autowired @Qualifier("UserServiceCRM")
	private IUserService userService;
	@Autowired
	private ISecurityService securityService;
	@Autowired
	private IMarketingTrackingService marketingTrackingService;
	@Autowired
	private BaseWriterFactory writerFactory;
	
	@RequestMapping(value = "/signup")
	public ResponseEntity<Response<User>> insertUser(@Validated({AddUserValidatorGroup.class}) @RequestBody Request<User> request, HttpServletRequest servletRequest) {
		logger.debug("start insert User request = " + request);	
		User user = null;
		Response<User> response = null;
		try {
        	if (securityService.isAuthenticate()) {
        		user = (User)request.getData();
        		user.setActionSource(request.getActionSource());
        		user.setWriterId(securityService.getLoginFromSession().getId());
        		user.setMarketingTracking(marketingTrackingService.getMarketingTrackingByMap(null, null));
    			response = userService.insertUser(servletRequest, user);
    		} else {
    			response = new Response<User>(null, ResponseCode.ACCESS_DENIED);
    		}
        } catch (Exception e) {
            logger.error("cant signup", e);
            response = new Response<User>(null, ResponseCode.UNKNOWN_ERROR);
        }
		logger.debug("end insert User response = " + response);
    	return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<capital.any.model.User>> get(@RequestBody Request<capital.any.model.User> request, HttpSession session) {
		logger.debug("start get request = " + request);	
		Response<capital.any.model.User> response = null;
		ResponseCode responseCode = ResponseCode.INVALID_INPUT;
		List<String> msg = new ArrayList<String>();
		msg.add("no user found");
		Error error = new Error();
		error.setReason("error.no.user.found");
		capital.any.model.User user = null;
		try {
			if (securityService.isAuthenticate()) {
				user = (capital.any.model.User)request.getData();
				if (user != null) {
					if (user.getId() > 0) {
						user = userService.getUserById(user.getId());
					} else {	 
						user = userService.getUserByEmail(user.getEmail());
					}
					if (user != null) {
						responseCode = ResponseCode.OK;
						sessionService.addAttribute(session, user, ISessionService.USER);
						error = null;
						msg = null;
					}
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch(Exception e) {
			logger.error("ERROR! get user by id " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
			error.setReason("error.please.try.again");
			msg.clear();
			msg.add("please try again");
		}
		
		response = new Response<capital.any.model.User>(user, responseCode, msg, error);
		logger.debug("end get response = " + response);
		return new ResponseEntity<Response<capital.any.model.User>>(response, HttpStatus.OK);
	}
	 
	@RequestMapping(value = "/getById")
	public ResponseEntity<Response<User>> getUserById(@RequestBody Request<User> request,HttpSession session) {
		logger.debug("start get User By Id request = " + request);	
		Response<User> response = null;
		ResponseCode responseCode = ResponseCode.OK;
		try {
			User user = (User)sessionService.getAttribute(session, ISessionService.USER);
			if (user == null) {
				responseCode = ResponseCode.INVALID_INPUT;
			}
			response = new Response<User>(user, responseCode);
		} catch(Exception e) {
			logger.error("ERROR! get user by id " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		logger.debug("end get User By Id response = " + response);
		return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getByEmail")
	public ResponseEntity<Response<User>> getUserByEmail(@RequestBody Request<User> request) {
		logger.debug("start get User By Email request = " + request);
		Response<User> response = null;
		ResponseCode responseCode = ResponseCode.OK;
		try {
			User user = userService.getUserByEmail(((User)request.getData()).getEmail());		
			if (user == null) {
				responseCode = ResponseCode.INVALID_INPUT;
			}
			response = new Response<User>(user, responseCode);
		} catch(Exception e) {
			logger.error("ERROR! get user by email " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		logger.debug("end get User By Email response = " + response);
		return new ResponseEntity<Response<User>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/edit")
	public ResponseEntity<Response<Object>> edit(@Validated({EditUserValidatorGroup.class}) @RequestBody Request<User> request, HttpSession session) {
		logger.debug("start edit request = " + request);
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR; 
		try {
			User user = (User)request.getData();
			responseCode = ResponseCode.OK;
			int writerId = securityService.getLoginFromSession().getId();
			AbstractWriter writer = writerFactory.createWriter(writerId);
			writer.setId(writerId);
			userService.update(user, new UserHistory(request.getActionSource(), writer));
		} catch (Exception e) {
			logger.error("ERROR! edit user", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		logger.debug("end edit response = " + response);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changePassword")
	public ResponseEntity<Response<Object>> changePassword(@RequestBody Request<ChangePassword> request,
			HttpSession session) {
		logger.debug("start change Password request = " + request);
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR; 
		try {
			if (securityService.isAuthenticate()) {
				ChangePassword changePassword = (ChangePassword)request.getData();
				changePassword.setUser((User)sessionService.getAttribute(session, ISessionService.USER));
				changePassword.setCurrentPassword(new ShaPasswordEncoder().encodePassword(changePassword.getCurrentPassword(), null));
				int writerId = securityService.getLoginFromSession().getId();
				AbstractWriter writer = writerFactory.createWriter(writerId);
				writer.setId(writerId);
				userService.changePassword(changePassword, new UserHistory(request.getActionSource(), writer));
				responseCode = ResponseCode.INVALID_PASSWORD;
				if (changePassword.isChanged()) {
					responseCode = ResponseCode.OK;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! change user password", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		logger.debug("end change Password response = " + response);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/clientspace")
	public ResponseEntity<Response<List<ClientSpace>>> getClientSpace(@Validated({SqlFilters.searchClientSpaceValidatorGroup.class}) @RequestBody Request<SqlFilters> request) {
		logger.debug("start getClientSpace request = " + request);
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED; 
		List<ClientSpace> clientSpace = null;
		try {
			if (securityService.isAuthenticate()) {
				clientSpace = userService.getClientSpace((SqlFilters)request.getData());
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			logger.error("ERROR! getClientSpace", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<List<ClientSpace>> response = new Response<List<ClientSpace>>(clientSpace, responseCode);
		logger.debug("end getClientSpace response = " + response);
		return new ResponseEntity<Response<List<ClientSpace>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/advancedSearchUser")
	public ResponseEntity<Response<List<capital.any.model.User>>> advancedSearchUser(@RequestBody Request<SqlFilters> request) {
		logger.debug("start advancedSearchUser request = " + request);
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED; 
		List<capital.any.model.User> userList = null;
		try {
			if (securityService.isAuthenticate()) {
				userList = userService.advancedSearchUser((SqlFilters)request.getData());
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			logger.error("ERROR! advancedSearchUser", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<List<capital.any.model.User>> response = new Response<List<capital.any.model.User>>(userList, responseCode);
		logger.debug("end advancedSearchUser response = " + response);
		return new ResponseEntity<Response<List<capital.any.model.User>>>(response, HttpStatus.OK);
	}
}
