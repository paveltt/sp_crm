package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.service.application.IApplicationService;

/**
 * @author LioR SoLoMoN
 *
 */
@RestController
@RequestMapping(value = "/app", method = RequestMethod.POST)
public class ApplicationController {
	private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
	//private Map<String, Writer> customers = null;
	@Autowired @Qualifier("ApplicationServiceCRM")
	private IApplicationService applicationService;
	//private IWriterService writerService; 
	

	/**
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/init")
	public ResponseEntity<Response<Map<String, Object>>> getInitParameters(@RequestBody Request<Object> request) {
		logger.info("init");
		List<String> messages = new ArrayList<String>();
			
		Response<Map<String, Object>> response = 
				new Response<Map<String, Object>>(applicationService.init(), ResponseCode.OK, messages);
		
		logger.info(response.toString());	
		return new ResponseEntity<Response<Map<String, Object>>>(response, HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/domain-url")
	public ResponseEntity<Response<String>> getDomainURL(@RequestBody Request<Object> request) {
		logger.debug("getDomainURL");		
		Response<String> response = new Response<String>(applicationService.getDomainURL(), ResponseCode.OK);
		return new ResponseEntity<Response<String>>(response , HttpStatus.OK);	
	}
}
