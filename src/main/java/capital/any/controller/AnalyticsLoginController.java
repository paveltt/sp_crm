package capital.any.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.analytics.AnalyticsLogin;
import capital.any.security.ISecurityService;
import capital.any.service.analyticsLogin.IAnalyticsLoginService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/analytics/login", method = RequestMethod.POST)
public class AnalyticsLoginController {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsLoginController.class);

	@Autowired
    private ISecurityService securityService;
	@Autowired
	private IAnalyticsLoginService analyticsLoginService;
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<AnalyticsLogin>> getAnalyticsLogin() {
		logger.info("About to get AnalyticsLogin.");
		ResponseCode responseCode = ResponseCode.OK;
		AnalyticsLogin analyticsLogin = null;
		try {
			if (securityService.isAuthenticate()) {
				analyticsLogin = analyticsLoginService.getAnalyticsLogin();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting AnalyticsLogin. ", e);
		}
		Response<AnalyticsLogin> response = new Response<AnalyticsLogin>(analyticsLogin, responseCode);
		return new ResponseEntity<Response<AnalyticsLogin>>(response, HttpStatus.OK);
	}
}
