package capital.any.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentHistory;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.User;
import capital.any.model.base.Writer;
import capital.any.security.ISecurityService;
import capital.any.service.IHttpClientService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.investment.IInvestmentService;

/**
 * @author Eyal G
 *
 */
@RestController
@RequestMapping(value = "/investment", method = RequestMethod.POST)
public class InvestmentController {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentController.class);
	
	@Autowired
	private IInvestmentService investmentService;
	
	@Autowired
	private ISecurityService securityService;
	
	@Autowired
	private IHttpClientService httpClientService;
	
	@Autowired
	private ISessionService sessionService;
	
	
	
//	@RequestMapping(value = "/insert")
//	public ResponseEntity<Response<Investment>> insert(@Validated({ InsertInvestmentValidatorGroup.class }) @RequestBody Request<Investment> request, HttpSession session) {
//		Investment investment = (Investment)request.getData();
//		Response<Investment> response;
//		try {
//	        if (securityService.isAuthenticate()) {
//	        	investment.setUser(securityService.getLoginFromSession());
//				InvestmentHistory investmentHistory = new InvestmentHistory();
//				investmentHistory.setActionSource(request.getActionSource());
//				investment.setInvestmentHistory(investmentHistory);
//				InvestmentParamters investmentParamters = new InvestmentParamters();
//				investmentParamters.setInvestment(investment);
//				investmentParamters.setSessionId(session.getId());
//				response = investmentService.insertWithValidation(investmentParamters);
//	        } else {
//	        	response = new Response<Investment>(null, ResponseCode.ACCESS_DENIED);
//	        }
//		} catch (Exception e) {
//			logger.error("cant insert investment", e);
//			response = new Response<Investment>(null, ResponseCode.UNKNOWN_ERROR);
//		}
//		return new ResponseEntity<Response<Investment>>(response, HttpStatus.OK);
//	}
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<List<Investment>>> getAllInvestments(@RequestBody Request<SqlFilters> request) {
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Investment> investments = null;
		try {
			if (securityService.isAuthenticate()) {
				SqlFilters filters = (SqlFilters)request.getData();
	        	investments = investmentService.get(filters);
	        	responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			logger.error("Can't get all investments", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<List<Investment>> response = new Response<List<Investment>>(investments, responseCode);
		return new ResponseEntity<Response<List<Investment>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/session/user/id")
	public ResponseEntity<Response<List<Investment>>> getAllInvestments(@RequestBody Request<SqlFilters> request, HttpSession session) {
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Investment> investments = null;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				SqlFilters filters = (SqlFilters) request.getData();
				filters.setUserId(user.getId());
	        	investments = investmentService.getByUserId(filters);
	        	responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			logger.error("Can't get user investments", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<List<Investment>> response = new Response<List<Investment>>(investments, responseCode);
		return new ResponseEntity<Response<List<Investment>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/cancel")
	public ResponseEntity<Response<Investment>> cancel(@RequestBody Request<Investment> request, HttpServletRequest httpRequest) {
		logger.debug("cancel investment request " + request);
		Response<Investment> response;
		Investment investment = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {
				investment = (Investment) request.getData();
				Writer writer = securityService.getLoginFromSession();
				InvestmentHistory investmentHistory = new InvestmentHistory();
				investmentHistory.setActionSource(request.getActionSource());
				investmentHistory.setWriterId(writer.getId());
				investmentHistory.setIp(httpClientService.getIP(httpRequest));
				investment.setInvestmentHistory(investmentHistory);
				responseCode = investmentService.cancel(investment);				
			}
			response = new Response<Investment>(investment, responseCode);
		} catch (Exception e) {
			logger.error("Can't get all investment", e);
			response = new Response<Investment>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Investment>>(response, HttpStatus.OK);
	}	
}
