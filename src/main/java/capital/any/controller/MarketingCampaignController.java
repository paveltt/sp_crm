package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.MarketingCampaign;
import capital.any.model.base.Writer;
import capital.any.security.ISecurityService;
import capital.any.service.base.marketingCampaign.IMarketingCampaignService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/marketing/campaign", method = RequestMethod.POST)
public class MarketingCampaignController {
	private static final Logger logger = LoggerFactory.getLogger(MarketingCampaignController.class);
	
	@Autowired
	private IMarketingCampaignService marketingCampaignService;
	@Autowired
    private ISecurityService securityService;
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<MarketingCampaign>> insert(@RequestBody Request<MarketingCampaign> request) {
		Response<MarketingCampaign> response = null;
		try {
			if (securityService.isAuthenticate()) {
				MarketingCampaign marketingCampaign = (MarketingCampaign) request.getData();
				Writer writer = securityService.getLoginFromSession();
				marketingCampaign.setWriterId(writer.getId());
				marketingCampaignService.insert(marketingCampaign);
				response = new Response<MarketingCampaign>(marketingCampaign, ResponseCode.OK);
			} else {
				response = new Response<MarketingCampaign>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("can't insert marketing Campaign");
			response = new Response<MarketingCampaign>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<MarketingCampaign>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/edit")
	public ResponseEntity<Response<Object>> edit(@RequestBody Request<MarketingCampaign> request) {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		try {
			if (securityService.isAuthenticate()) {
				MarketingCampaign marketingCampaign = (MarketingCampaign) request.getData();
				Writer writer = securityService.getLoginFromSession();
				marketingCampaign.setWriterId(writer.getId());
				marketingCampaignService.update(marketingCampaign);
				responseCode = ResponseCode.OK;
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! edit marketing Campaign", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAll")
	public ResponseEntity<Response<Map<Long, MarketingCampaign>>> getAll() {
		logger.info("about to getAll marketing Campaigns.");
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Long, MarketingCampaign> hm = null;
		try {
			if (securityService.isAuthenticate()) {
				hm = marketingCampaignService.getMarketingCampaigns();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getAll marketing Campaigns.", e);
		}
		Response<Map<Long, MarketingCampaign>> response = new Response<Map<Long, MarketingCampaign>>(hm, responseCode, messages);
		return new ResponseEntity<Response<Map<Long, MarketingCampaign>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getMarketingCampaign")
	public ResponseEntity<Response<MarketingCampaign>> getMarketingCampaign(@RequestBody Request<MarketingCampaign> request) {
		Response<MarketingCampaign> r;
		ResponseCode responseCode = ResponseCode.OK;
		MarketingCampaign marketingCampaign = null;
		try {
			if (securityService.isAuthenticate()) {
				marketingCampaign = (MarketingCampaign) request.getData();
				marketingCampaign = marketingCampaignService.getMarketingCampaign(marketingCampaign.getId());
				if (null == marketingCampaign) {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("can't get marketing Campaign", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		r = new Response<MarketingCampaign>(marketingCampaign, responseCode);
		return new ResponseEntity<Response<MarketingCampaign>>(r, HttpStatus.OK);
	}
}
