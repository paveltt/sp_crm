package capital.any.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.analytics.AnalyticsUser;
import capital.any.security.ISecurityService;
import capital.any.service.analyticsUser.IAnalyticsUserService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/analytics/user", method = RequestMethod.POST)
public class AnalyticsUserController {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsUserController.class);
	
	@Autowired
    private ISecurityService securityService;
	@Autowired
	private IAnalyticsUserService analyticsUserService;
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<AnalyticsUser>> getAnalyticsUser() {
		logger.info("About to get AnalyticsUser.");
		ResponseCode responseCode = ResponseCode.OK;
		AnalyticsUser analyticsUser = null;
		try {
			if (securityService.isAuthenticate()) {
				analyticsUser = analyticsUserService.getAnalyticsUser();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting analyticsUser. ", e);
		}
		Response<AnalyticsUser> response = new Response<AnalyticsUser>(analyticsUser, responseCode);
		return new ResponseEntity<Response<AnalyticsUser>>(response, HttpStatus.OK);
	}
}
