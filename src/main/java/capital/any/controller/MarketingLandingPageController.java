package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.MarketingLandingPage;
import capital.any.model.base.Writer;
import capital.any.security.ISecurityService;
import capital.any.service.base.marketingLandingPage.IMarketingLandingPageService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/marketing/lp", method = RequestMethod.POST)
public class MarketingLandingPageController {
	private static final Logger logger = LoggerFactory.getLogger(MarketingLandingPageController.class);
	
	@Autowired
	private IMarketingLandingPageService marketingLandingPageService;
	@Autowired
    private ISecurityService securityService;
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<MarketingLandingPage>> insert(@RequestBody Request<MarketingLandingPage> request) {
		Response<MarketingLandingPage> response = null;
		try {
			if (securityService.isAuthenticate()) {
				MarketingLandingPage marketingLandingPage = (MarketingLandingPage) request.getData();
				Writer writer = securityService.getLoginFromSession();
				marketingLandingPage.setWriterId(writer.getId());
				marketingLandingPageService.insert(marketingLandingPage);
				response = new Response<MarketingLandingPage>(marketingLandingPage, ResponseCode.OK);
			} else {
				response = new Response<MarketingLandingPage>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("can't insert marketing landing page", e);
			response = new Response<MarketingLandingPage>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<MarketingLandingPage>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/edit")
	public ResponseEntity<Response<Object>> edit(@RequestBody Request<MarketingLandingPage> request) {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		try {
			if (securityService.isAuthenticate()) {
				MarketingLandingPage marketingLandingPage = (MarketingLandingPage) request.getData();
				Writer writer = securityService.getLoginFromSession();
				marketingLandingPage.setWriterId(writer.getId());
				marketingLandingPageService.update(marketingLandingPage);
				responseCode = ResponseCode.OK;
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! edit marketing landing page");
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAll")
	public ResponseEntity<Response<Map<Integer, MarketingLandingPage>>> getAll() {
		logger.info("about to getAll marketing landing page.");
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Integer, MarketingLandingPage> hm = null;
		try {
			if (securityService.isAuthenticate()) {
				hm = marketingLandingPageService.getAll();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getAll marketing landing page.", e);
		}
		Response<Map<Integer, MarketingLandingPage>> response = new Response<Map<Integer,MarketingLandingPage>>(hm, responseCode, messages);
		return new ResponseEntity<Response<Map<Integer,MarketingLandingPage>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getMarketingLandingPage")
	public ResponseEntity<Response<MarketingLandingPage>> getMarketingLandingPage(@RequestBody Request<MarketingLandingPage> request) {
		Response<MarketingLandingPage> r;
		ResponseCode responseCode = ResponseCode.OK;
		MarketingLandingPage marketingLandingPage = null;
		try {
			if (securityService.isAuthenticate()) {
				marketingLandingPage = (MarketingLandingPage) request.getData();
				marketingLandingPage = marketingLandingPageService.getMarketingLandingPage(marketingLandingPage.getId());
				if (null == marketingLandingPage) {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("can't get marketing landing page", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		r = new Response<MarketingLandingPage>(marketingLandingPage, responseCode);
		return new ResponseEntity<Response<MarketingLandingPage>>(r, HttpStatus.OK);
	}
}
