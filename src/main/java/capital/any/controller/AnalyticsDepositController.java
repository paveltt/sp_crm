package capital.any.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.analytics.AnalyticsDeposit;
import capital.any.security.ISecurityService;
import capital.any.service.analyticsDeposit.IAnalyticsDepositService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/analytics/deposit", method = RequestMethod.POST)
public class AnalyticsDepositController {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsDepositController.class);
	
	@Autowired
    private ISecurityService securityService;
	@Autowired
	private IAnalyticsDepositService analyticsDepositService;
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<AnalyticsDeposit>> get() {
		logger.info("About to get AnalyticsDeposit. ");
		ResponseCode responseCode = ResponseCode.OK;
		AnalyticsDeposit analyticsDeposit = null;
		try {
			if (securityService.isAuthenticate()) {
				analyticsDeposit = analyticsDepositService.get();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting AnalyticsDeposit. ", e);
		}
		Response<AnalyticsDeposit> response = new Response<AnalyticsDeposit>(analyticsDeposit, responseCode);
		return new ResponseEntity<Response<AnalyticsDeposit>>(response, HttpStatus.OK);
	}

}
