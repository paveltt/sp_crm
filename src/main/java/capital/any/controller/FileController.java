package capital.any.controller;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.File;
import capital.any.model.base.FileRequiredDocGroup;
import capital.any.model.base.User;
import capital.any.security.ISecurityService;
import capital.any.service.base.aws.IAwsService;
import capital.any.service.base.file.IFileService;
import capital.any.service.base.fileRequiredDoc.IFileRequiredDocService;
import capital.any.service.base.session.ISessionService;


/**
 * @author eran.levy
 *
 */
@RestController
@RequestMapping(value = "/file", method = RequestMethod.POST)
public class FileController {
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);
	
	@Autowired
	private IFileService fileService;
	@Autowired
    private ISecurityService securityService;
	@Autowired
	private ISessionService sessionService;
	@Autowired
	private IAwsService awsService;
	@Autowired
	private IFileRequiredDocService fileRequiredDocService;
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<File>> insert(@RequestParam(value = "request") String requestJson, HttpSession session,
			@RequestParam(value = "fileUpload", required = false) MultipartFile fileUpload) {
		logger.debug("insert file  " + fileUpload.getName() + " " + fileUpload.getOriginalFilename() + " " + requestJson);
		ObjectMapper mapper = new ObjectMapper();
		Request<File> request = null;
		Response<File> response = null;
		try {
			request = (Request<File>) mapper.readValue(requestJson, new TypeReference<Request<File>>() {});
			if (securityService.isAuthenticate()) {
				File file = (File) request.getData();
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				file.setUserId(user.getId());
				file.setWriter(securityService.getLoginFromSession());
				file.setActionSource(request.getActionSource());
				file.setName(fileUpload.getOriginalFilename());
				fileService.insertAndUpload(file, fileUpload);
				response = new Response<File>(file, ResponseCode.OK);
			} else {
				response = new Response<File>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e1) {
			logger.error("Problem with insert user file. ", e1);
			response = new Response<File>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<File>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getByUserId")
	public ResponseEntity<Response<List<File>>> getByUserId(@RequestBody Request<Object> request, HttpSession session) {
		logger.info("about to get all files by userId");
		ResponseCode responseCode = ResponseCode.OK;
		List<File> list = null;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				list = fileService.getByUserId(user.getId());
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting all files by userId.", e);
		}
		Response<List<File>> response = new Response<List<File>>(list, responseCode);
		return new ResponseEntity<Response<List<File>>>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public void getByUserId(@PathVariable int id, HttpServletResponse response, HttpSession session) {
		logger.info("about to download file id " + id);
		try {
			if (securityService.isAuthenticate()) {
				File file = new File();
				file.setId(id);
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				file.setUserId(user.getId());
				File fileFromDb = fileService.getByIdAndUserId(file);
				if (fileFromDb != null) {
			        InputStream fileFromAws = awsService.getUserFile(fileFromDb);
			        String originalName = fileFromDb.getName().split(File.NAME_SEPARATOR)[1];
			        SimpleDateFormat dt1 = new SimpleDateFormat("ddMMyyyy");
			        String fileName = "userId_" + fileFromDb.getUserId() + "_type_" + fileFromDb.getFileType().getId() + "_date_" +  dt1.format(fileFromDb.getTimeCreated()) + "_" + originalName;
			        java.io.File f = new java.io.File(fileName);
			        String mimeType = new MimetypesFileTypeMap().getContentType(f);
			        logger.info("mimeType " + mimeType);
			        response.setContentType(mimeType);
			        response.setHeader("Content-Disposition", "attachment; filename=" + fileName); 
			        ServletOutputStream out = response.getOutputStream();
			        IOUtils.copy(fileFromAws, out);
			        response.flushBuffer();
				} else {
					response.sendError(HttpServletResponse.SC_NOT_FOUND);
				}
			} else {
				response.sendError(HttpServletResponse.SC_FORBIDDEN);
			}			
	    } catch (Exception e) {
	    	logger.error("Cant download file ", e);
	    }
	}
	
	@RequestMapping(value = "/update")
	public ResponseEntity<Response<Object>> update(@RequestBody Request<File> requst, HttpSession session) {
		logger.info("start update file");
		ResponseCode responseCode = ResponseCode.OK;
		try {
			if (securityService.isAuthenticate()) {
				File file = (File) requst.getData();
				if (fileService.updateFile(file)) {
					responseCode = ResponseCode.OK;
				} else {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on update file.", e);
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/file/id")
	public ResponseEntity<Response<File>> getById(@RequestBody Request<File> requst, HttpSession session) {
		logger.info("start get file by id");
		ResponseCode responseCode = ResponseCode.OK;
		File fileFromDb = null;
		try {
			if (securityService.isAuthenticate()) {
				File file = (File) requst.getData();
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				file.setUserId(user.getId());
				fileFromDb = fileService.getByIdAndUserId(file);
				if (fileFromDb != null) {
					responseCode = ResponseCode.OK;
				} else {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error get file by id.", e);
		}
		Response<File> response = new Response<File>(fileFromDb, responseCode);
		return new ResponseEntity<Response<File>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/preview/{id}", method = RequestMethod.GET)
	public byte[] getPreview(@PathVariable int id, HttpServletResponse response, HttpSession session) {
		logger.info("about to download file id " + id);
		try {
			if (securityService.isAuthenticate()) {
				File file = new File();
				file.setId(id);
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				file.setUserId(user.getId());
				File fileFromDb = fileService.getByIdAndUserId(file);
				if (fileFromDb != null) {
			        InputStream fileFromAws = awsService.getUserFile(fileFromDb); //IMG_1831.JPG
			        return IOUtils.toByteArray(fileFromAws);
				} else {
					response.sendError(HttpServletResponse.SC_NOT_FOUND);
				}
			} else {
				response.sendError(HttpServletResponse.SC_FORBIDDEN);
			}
	    } catch (Exception e) {
	    	logger.error("Cant download file ", e);
	    }
		return null;
	}
	
	/******** Finotec **********/
	/*@RequestMapping(value = "/getRequiredDocs")
	public ResponseEntity<Response<Map<Integer, RegulationFileRule>>> getRequiredDocs(HttpSession session) {
		logger.info("about to get required docs");
		ResponseCode responseCode = ResponseCode.OK;
		Map<Integer, RegulationFileRule> map = null;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				map = regulationFileRuleService.getRequiredDocs(user.getId());
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting required docs by userId.", e);
		}
		Response<Map<Integer, RegulationFileRule>> response = new Response<Map<Integer, RegulationFileRule>>(map, responseCode);
		return new ResponseEntity<Response<Map<Integer, RegulationFileRule>>>(response, HttpStatus.OK);
	}*/
	
	/******** ODT **********/
	@RequestMapping(value = "/getRequiredDocs")
	public ResponseEntity<Response<List<FileRequiredDocGroup>>> getRequiredDocs(HttpSession session) {
		logger.info("about to get required docs");
		ResponseCode responseCode = ResponseCode.OK;
		List<FileRequiredDocGroup> list = null;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				list = fileRequiredDocService.getRequiredDocs(user.getId());
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting required docs by userId.", e);
		}
		Response<List<FileRequiredDocGroup>> response = new Response<List<FileRequiredDocGroup>>(list, responseCode);
		return new ResponseEntity<Response<List<FileRequiredDocGroup>>>(response, HttpStatus.OK);
	}
}
