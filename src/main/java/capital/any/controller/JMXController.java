package capital.any.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.service.jmx.IJMXService;

/**
 * @author Eran.l
 *
 */
@RestController
@RequestMapping(value = "/jmx", method = RequestMethod.POST)
public class JMXController {
	private static final Logger logger = LoggerFactory.getLogger(JMXController.class);
	@Autowired
	private IJMXService JMXService;
	
	@RequestMapping(value = "/reloadHC")
	public ResponseEntity<Response<Boolean>> reloadHC(@RequestBody Request<String> request) {
		Response<Boolean> response = null;
		ResponseCode responseCode = null;
		boolean result = true;
		try {
			JMXService.reloadHC();
			responseCode = ResponseCode.OK;
		} catch (Exception e) {
			result = false;
			logger.error("ERROR! problem with reloadHC", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<Boolean>(result, responseCode);
		return new ResponseEntity<Response<Boolean>>(response, HttpStatus.OK);
	}
}
