package capital.any.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.DBParameter;
import capital.any.security.ISecurityService;
import capital.any.service.dbParameter.IDBParameterService;


/**
 * @author eyal.goren
 *
 */
@RestController
@RequestMapping(value = "/dbparameter", method = RequestMethod.POST)
public class DbParameterController {
	private static final Logger logger = LoggerFactory.getLogger(DbParameterController.class);
	
	@Autowired
	@Qualifier("DBParameterServiceCRM")
	private IDBParameterService dbParameterService;
	
	@Autowired
	private ISecurityService securityService;
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<DBParameter>> insert(@RequestBody Request<DBParameter> request) {
		logger.debug("insert dbparameter  " + request);
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		DBParameter dbParameter = null;
		try {
			if (securityService.isAuthenticate()) {
				dbParameter = (DBParameter) request.getData();
				dbParameterService.insert(dbParameter);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on dbparameter ", e);
		}
		Response<DBParameter> response = new Response<DBParameter>(dbParameter, responseCode);
		return new ResponseEntity<Response<DBParameter>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<Map<Integer, DBParameter>>> get() {
		logger.info("about to get all DBParameter");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		Map<Integer, DBParameter> list = null;
		try {
			if (securityService.isAuthenticate()) {
				list = dbParameterService.get();
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting all DBParameter", e);
		}
		Response<Map<Integer, DBParameter>> response = new Response<Map<Integer, DBParameter>>(list, responseCode);
		return new ResponseEntity<Response<Map<Integer, DBParameter>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update")
	public ResponseEntity<Response<Object>> update(@RequestBody Request<DBParameter> requst) {
		logger.info("start update DBParameter");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {
				DBParameter dbParameter = (DBParameter) requst.getData();
				if (dbParameterService.update(dbParameter)) {
					responseCode = ResponseCode.OK;
				} else {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on update DBParameter.", e);
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/id")
	public ResponseEntity<Response<DBParameter>> getById(@RequestBody Request<DBParameter> requst) {
		logger.info("start get DBParameter by id");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		DBParameter dbParameter = null;
		try {
			if (securityService.isAuthenticate()) {
				DBParameter dbParameterRequst = (DBParameter) requst.getData();
				dbParameter = dbParameterService.get(dbParameterRequst.getId());
				if (dbParameter != null) {
					responseCode = ResponseCode.OK;
				} else {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error get DBParameter by id.", e);
		}
		Response<DBParameter> response = new Response<DBParameter>(dbParameter, responseCode);
		return new ResponseEntity<Response<DBParameter>>(response, HttpStatus.OK);
	}
}
