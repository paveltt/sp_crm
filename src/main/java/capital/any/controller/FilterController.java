package capital.any.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.filter.FilterService;
import capital.any.service.product.IProductService;

/**
 * @author LioR SoLoMoN
 *
 */
@RestController
@RequestMapping(value = "/filters", method = RequestMethod.POST)
public class FilterController {
	private static final Logger logger = LoggerFactory.getLogger(FilterController.class);
	@Autowired @Qualifier("ProductServiceCRM")
	private IProductService productService; 
	@Autowired @Qualifier("FilterServiceCRM")
	private FilterService filterService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<Map<String, Map<Integer, ?>>>> getFilters(@RequestBody Request<List<String>> request) {
		Response<Map<String, Map<Integer, ?>>> response = null;
		Map<String, Map<Integer, ?>> filtersResult = new HashMap<String, Map<Integer,?>>();
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		List<String> filters = (List<String>)request.getData();
		try {
			for (String name : filters) {
				try {
					filtersResult.put(name, filterService.get(name));
				} catch (Exception e) {
					logger.error("ERROR! problem with filter: " + name, e);
				} 
			}			
			responseCode = ResponseCode.OK;
		} catch (Exception e) {
			logger.error("ERROR! problem with filters", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<Map<String, Map<Integer, ?>>>(filtersResult, responseCode);
		return new ResponseEntity<Response<Map<String, Map<Integer, ?>>>>(response, HttpStatus.OK);
	}
}
