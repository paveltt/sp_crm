package capital.any.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.filter.ProductFilters;
import capital.any.model.base.Product;
import capital.any.model.base.ProductBarrier;
import capital.any.model.base.ProductCoupon;
import capital.any.model.base.ProductMarket;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Writer;
import capital.any.model.base.ProductCoupon.UpdateObservationLevelValidatorGroup;
import capital.any.model.base.ProductHistory;
import capital.any.model.base.ProductKid;
import capital.any.model.base.ProductKidLanguage;
import capital.any.security.ISecurityService;
import capital.any.service.productBarrier.IProductBarrierService;
import capital.any.service.productCoupon.IProductCouponService;
import capital.any.service.base.productKidLanguage.IProductKidLanguageService;
import capital.any.service.product.IProductService;
import capital.any.service.productMarket.IProductMarketService;
import capital.any.model.base.Product.insertOrUpdateProductValidatorGroup;
import capital.any.model.base.Product.updateAskBidValidatorGroup;

/**
 * @author eranl
 *
 */
@RestController
@RequestMapping(value = "/product", method = RequestMethod.POST)
public class ProductController {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired @Qualifier("ProductServiceCRM")
	private IProductService productService;
	
	@Autowired @Qualifier("ProductMarketServiceCRM")
	private IProductMarketService productMarketService;
	
	@Autowired @Qualifier("ProductBarrierServiceCRM")
	private IProductBarrierService productBarrierService;
	
	@Autowired @Qualifier("ProductCouponServiceCRM")
	private IProductCouponService productCouponService;
	
	@Autowired
	private ISecurityService securityService;
	
	@Autowired
	private IProductKidLanguageService productKidLanguageService;
	
	@RequestMapping(value = "/insertProduct")
	@Deprecated
	public ResponseEntity<Response<Product>> insertProduct(@Validated({insertOrUpdateProductValidatorGroup.class}) @RequestBody Request<Product> request) {
		Product p = (Product)request.getData();
		Response<Product> r;
		try {
			productService.insert(p);
			r = new Response<Product>(p, ResponseCode.OK);
		} catch (Exception e) {
			logger.error("cant insert product full", e);
			r = new Response<Product>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Product>>(r, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/insertProductFull")
	public ResponseEntity<Response<Product>> insertProductFull(@Validated({ insertOrUpdateProductValidatorGroup.class }) @RequestBody Request<Product> request) {
		logger.info("start insertProductFull request " + request);
		Response<Product> response = null;
		try {
			if (securityService.isAuthenticate()) {
				Product product = (Product) request.getData();
				Writer writer = securityService.getLoginFromSession();
				if (product.getId() > 0) { //update
					if (!productService.updateProduct(product, new ProductHistory(request.getActionSource(), writer.getId()))) {
						response = new Response<Product>(null, ResponseCode.INVALID_INPUT);
					}
				} else { //insert
					productService.insertFull(product, new ProductHistory(request.getActionSource(), writer.getId()));
				}
				if (response == null) {
					response = new Response<Product>(product, ResponseCode.OK);
				}
			} else {
				response = new Response<Product>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant insert product full", e);
			response = new Response<Product>(null, ResponseCode.UNKNOWN_ERROR);
		}
		logger.info("end insertProductFull response " + response);
		return new ResponseEntity<Response<Product>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getProductFull")
	public ResponseEntity<Response<Product>> getProductFull(@RequestBody Request<Product> request) {
		Response<Product> r;
		Product product = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {				
				product = (Product) request.getData();			
				product = productService.getProduct(product.getId());
				responseCode = ResponseCode.OK;
				if (null == product) {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			}
		} catch (Exception e) {
			logger.error("cant get product full", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		r = new Response<Product>(product, responseCode);
		return new ResponseEntity<Response<Product>>(r, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/insertProduct/init")
	public ResponseEntity<Response<ProductFilters>> getInsertProductFilters() {
		Response<ProductFilters> r;
		try {
			ProductFilters productFilters = productService.getInsertProductFilters();
			r = new Response<ProductFilters>(productFilters, ResponseCode.OK);
		} catch (Exception e) {
			r = new Response<ProductFilters>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<ProductFilters>>(r, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products/init")
	public ResponseEntity<Response<ProductFilters>> getProductsFilters() {
		Response<ProductFilters> r;
		try {
			ProductFilters productFilters = productService.getProductsFilters();
			r = new Response<ProductFilters>(productFilters, ResponseCode.OK);
		} catch (Exception e) {
			r = new Response<ProductFilters>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<ProductFilters>>(r, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateAskBid")
	public ResponseEntity<Response<Object>> updateAskBid(@Validated({ updateAskBidValidatorGroup.class }) @RequestBody Request<Product> request) {
		Response<Object> r;
		try {
			if (securityService.isAuthenticate()) {
				Product p = (Product) request.getData();
				ResponseCode responseCode = ResponseCode.OK;
				if (p.getId() == 0) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					Writer writer = securityService.getLoginFromSession();
					productService.updateAskBid(p, new ProductHistory(request.getActionSource(), writer.getId()));
				}
				r = new Response<Object>(p, responseCode);
			} else {
				r = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant get product full", e);
			r = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(r, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/publish")
	public ResponseEntity<Response<Object>> updateStatus(@RequestBody Request<Product> request) {
		Response<Object> response;
		try {
			if (securityService.isAuthenticate()) {
				Product product = (Product) request.getData();
				ResponseCode responseCode = ResponseCode.OK;
				if (product.getId() == 0) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					Writer writer = securityService.getLoginFromSession();
					if (!productService.publish(product, new ProductHistory(request.getActionSource(), writer.getId()))) {
						responseCode = ResponseCode.INVALID_INPUT;
					}
				}
				response = new Response<Object>(null, responseCode);
			} else {
				response = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant get update Status", e);
			response = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllProducts")
	public ResponseEntity<Response<List<Product>>> getAllProducts(@RequestBody Request<SqlFilters> request) {
		logger.debug("getAllProducts request " + request);
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Product> list = null;
		try {
			if (securityService.isAuthenticate()) {
				SqlFilters filters = (SqlFilters) request.getData();
				list = productService.getProducts(filters);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getAllProducts ", e);
		}
		Response<List<Product>> response = new Response<List<Product>>(list, responseCode);
		logger.debug("end getAllProducts responseCode " + response.getResponseCode());
		return new ResponseEntity<Response<List<Product>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateSuspend")
	public ResponseEntity<Response<Object>> updateSuspend(@RequestBody Request<Product> request) {
		Response<Object> r;
		try {
			if (securityService.isAuthenticate()) {
				Product p = (Product) request.getData();
				ResponseCode responseCode = ResponseCode.OK;
				if (p.getId() == 0) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					Writer writer = securityService.getLoginFromSession();
					productService.updateSuspend(p, new ProductHistory(request.getActionSource(), writer.getId()));
				}
				r = new Response<Object>(null, responseCode);
			} else {
				r = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant get update Status", e);
			r = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(r, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateStartTradeLevel")
	public ResponseEntity<Response<Object>> updateStartTradeLevel(@RequestBody Request<List<ProductMarket>> request) {
		Response<Object> response;
		try {
			if (securityService.isAuthenticate()) {
				List<ProductMarket> productMarkets = (List<ProductMarket>) request.getData();
				ResponseCode responseCode = ResponseCode.OK;
				//we check only first market (now we got only 1)
				if (productMarkets.get(0).getProductId() == 0 || productMarkets.get(0).getMarket().getId() == 0) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					Writer writer = securityService.getLoginFromSession();
					productMarketService.updateStartTradeLevel(productMarkets, new ProductHistory(request.getActionSource(), writer.getId()));
				}
				response = new Response<Object>(null, responseCode);
			} else {
				response = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant update Start Trade Level", e);
			response = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateEndTradeLevel")
	public ResponseEntity<Response<Object>> updateEndTradeLevel(@RequestBody Request<List<ProductMarket>> request) {
		Response<Object> response;
		try {
			if (securityService.isAuthenticate()) {
				List<ProductMarket> productMarkets = (List<ProductMarket>) request.getData();
				ResponseCode responseCode = ResponseCode.OK;
				//we check only first market (now we got only 1)
				if (productMarkets.get(0).getProductId() == 0 || productMarkets.get(0).getMarket().getId() == 0) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					Writer writer = securityService.getLoginFromSession();
					productMarketService.updateEndTradeLevel(productMarkets, new ProductHistory(request.getActionSource(), writer.getId()));
				}
				response = new Response<Object>(null, responseCode);
			} else {
				response = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant update Start Trade Level", e);
			response = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateProductBarrierOccur")
	public ResponseEntity<Response<Object>> updateProductBarrierOccur(@RequestBody Request<ProductBarrier> request) {
		Response<Object> response;
		try {
			if (securityService.isAuthenticate()) {
				ProductBarrier productBarrier = (ProductBarrier) request.getData();
				ResponseCode responseCode = ResponseCode.OK;
				if (productBarrier.getProductId() == 0) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					Writer writer = securityService.getLoginFromSession();
					productBarrierService.updateProductBarrierOccur(productBarrier, new ProductHistory(request.getActionSource(), writer.getId()));
				}
				response = new Response<Object>(null, responseCode);
			} else {
				response = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant update Product Barrier Occur", e);
			response = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateProductsPriority")
	public ResponseEntity<Response<Object>> updateProductsPriority(@RequestBody Request<List<Product>> request) {
		Response<Object> response;
		try {
			if (securityService.isAuthenticate()) {
				List<Product> products = (List<Product>) request.getData();
				ResponseCode responseCode = ResponseCode.OK;
				Writer writer = securityService.getLoginFromSession();
				if (!productService.updateProductsPriority(products, new ProductHistory(request.getActionSource(), writer.getId()))) {
					responseCode = ResponseCode.INVALID_INPUT;
				}
				response = new Response<Object>(null, responseCode);
			} else {
				response = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("can't update Products Priority", e);
			response = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getPriorityProducts")
	public  ResponseEntity<Response<List<Product>>> getPriorityProducts() {
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Product> products = null;
		try {
			if (securityService.isAuthenticate()) {
				products = productService.getProductsForPriority();
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getPriorityProducts ", e);
		}
		Response<List<Product>> response = new Response<List<Product>>(products, responseCode);
		return new ResponseEntity<Response<List<Product>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/productcoupon/update/observationlevel")
	public ResponseEntity<Response<Object>> updateProductCouponObservationLevel(@Validated({UpdateObservationLevelValidatorGroup.class}) @RequestBody Request<ProductCoupon> request) {
		Response<Object> response;
		try {
			if (securityService.isAuthenticate()) {
				ProductCoupon productCoupon = (ProductCoupon) request.getData();
				ResponseCode responseCode = ResponseCode.OK;
				Writer writer = securityService.getLoginFromSession();
				if (!productCouponService.updateObservationLevel(productCoupon, new ProductHistory(request.getActionSource(), writer.getId()))) {
					responseCode = ResponseCode.INVALID_INPUT;
				}
				response = new Response<Object>(null, responseCode);
			} else {
				response = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant update Product Coupon Observation Level", e);
			response = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/updateKid")
	public ResponseEntity<Response<Object>> updateKid(@RequestParam(value = "request") String requestJson,
				@RequestParam(value = "fileUpload", required = false) MultipartFile fileUpload) {
		ObjectMapper mapper = new ObjectMapper();
		Request<ProductKid> request = null;
		Response<Object> r;
		try {
			request = (Request<ProductKid>) mapper.readValue(requestJson, new TypeReference<Request<ProductKid>>() {});
			if (securityService.isAuthenticate()) {
				ProductKid productKid = (ProductKid) request.getData();
				Product p = productKid.getProduct();
				ResponseCode responseCode = ResponseCode.OK;
				if (p.getId() == 0) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					Writer writer = securityService.getLoginFromSession();
					productService.updateKidAndUpload(productKid, new ProductHistory(request.getActionSource(), writer.getId()), fileUpload);
				}
				r = new Response<Object>(p, responseCode);
			} else {
				r = new Response<Object>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("cant update KID", e);
			r = new Response<Object>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<Object>>(r, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getProductKidLanguages")
	public ResponseEntity<Response<List<ProductKidLanguage>>> getProductKidLanguages(@RequestBody Request<Product> request) {
		List<ProductKidLanguage> list = null;
		Response<List<ProductKidLanguage>> r;
		Product product = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {				
				product = (Product) request.getData();			
				list = productKidLanguageService.get(product);
				responseCode = ResponseCode.OK;
				if (null == product) {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			}
		} catch (Exception e) {
			logger.error("cant get product kid languages", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		r = new Response<List<ProductKidLanguage>>(list, responseCode);
		return new ResponseEntity<Response<List<ProductKidLanguage>>>(r, HttpStatus.OK);
	}
}
