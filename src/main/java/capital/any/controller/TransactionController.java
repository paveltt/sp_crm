package capital.any.controller;

import java.util.List;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.User;
import capital.any.security.ISecurityService;
import capital.any.service.base.session.ISessionService;
import capital.any.service.transaction.ITransactionService;

/**
 * @author eranl
 *
 */
@RestController
@RequestMapping(value = "/transaction", method = RequestMethod.POST)
public class TransactionController {	
	private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
	@Autowired
	private ISessionService sessionService;
	@Autowired @Qualifier("TransactionServiceCRM")
	private ITransactionService transactionService;
	@Autowired
	private ISecurityService securityService;
	
	@RequestMapping(value = "/getAll")
	public ResponseEntity<Response<List<Transaction>>> getAll(@RequestBody Request<SqlFilters> request) {
		logger.info("about to get All transactions; " + request);
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Transaction> hm = null;
		try {
			if (securityService.isAuthenticate()) {
				SqlFilters filters = (SqlFilters) request.getData();
				hm = transactionService.getTransactions(filters);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getAll transactions ", e);
		}
		Response<List<Transaction>> response = new Response<List<Transaction>>(hm, responseCode);
		logger.debug("end get All response = " + response);
		return new ResponseEntity<Response<List<Transaction>>>(response, HttpStatus.OK);
	}
			
	@RequestMapping(value = "/getByUserId")
	public ResponseEntity<Response<List<Transaction>>> getTransactionsByUserId(@RequestBody Request<SqlFilters> request,HttpSession session) {
		logger.debug("start get get Transactions By User Id request = " + request);
		Response<List<Transaction>> response = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Transaction> items = null;
		try {
			if (securityService.isAuthenticate()) {
				User user = (User)sessionService.getAttribute(session, ISessionService.USER);
				SqlFilters filters = (SqlFilters) request.getData();
				filters.setUserId(user.getId());
				items = transactionService.getTransactions(filters);
				if (null == items) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					responseCode = ResponseCode.OK;
				}				
			}
		} catch(Exception e) {
			logger.error("ERROR! get transactions by user id " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<List<Transaction>>(items, responseCode);
		logger.debug("end get Transactions By User Id response = " + response);
		return new ResponseEntity<Response<List<Transaction>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getByStatus")
	public ResponseEntity<Response<List<Transaction>>> getTransactionsByStatus(@RequestBody Request<SqlFilters> request) {
		logger.debug("start get Transactions By Status request = " + request);
		Response<List<Transaction>> response = null;
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		List<Transaction> items = null;
		try {
			if (securityService.isAuthenticate()) {
				SqlFilters filters = (SqlFilters) request.getData();
				items = transactionService.getTransactions(filters);	
				if (null == items) {
					responseCode = ResponseCode.INVALID_INPUT;
				} else {
					responseCode = ResponseCode.OK;
				}				
			}
		} catch(Exception e) {
			logger.error("ERROR! get transactions by status " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<List<Transaction>>(items, responseCode);
//		logger.debug("end get Transactions By Status response = " + response);
		return new ResponseEntity<Response<List<Transaction>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deposit/approve")
	public ResponseEntity<Response<Object>> approveDeposit(@RequestBody Request<Transaction> request) {
		logger.debug("start approve Deposit request = " + request);
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {
				Transaction transaction = (Transaction)request.getData();
				TransactionHistory transactionHistory = 
						new TransactionHistory(
								securityService.getLoginFromSession(),
								request.getActionSource());
				transactionService.approveDeposit(transaction, transactionHistory);	
				responseCode = ResponseCode.OK;
			}
		} catch(Exception e) {
			logger.error("ERROR! approveDeposit" , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		logger.debug("end approve Deposit response = " + response);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
		
	@RequestMapping(value = "/withdrawal/firstApproved")
	public ResponseEntity<Response<Transaction>> withdrawalFirstApproved(@RequestBody Request<Transaction> request, HttpSession session) {
		logger.debug("start withdrawal First Approved request = " + request);
		Response<Transaction> response = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		Transaction transaction = null;
		try {
			if (securityService.isAuthenticate()) {
				transaction = (Transaction)request.getData();
				TransactionHistory transactionHistory = 
						new TransactionHistory(
								securityService.getLoginFromSession(),
								request.getActionSource());			
				responseCode = transactionService.withdrawalFirstApproved(transaction, transactionHistory);			
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch(Exception e) {
			logger.error("ERROR!firstApproved " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<Transaction>(transaction, responseCode);
		logger.debug("end withdrawal First Approved response = " + response);
		return new ResponseEntity<Response<Transaction>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/withdrawal/secondApproved")
	public ResponseEntity<Response<Transaction>> withdrawalSecondApproved(@RequestBody Request<Transaction> request, HttpSession session) {		
		logger.debug("start withdrawal Second Approved request = " + request);
		Response<Transaction> response = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		Transaction transaction = null;
		try {
			if (securityService.isAuthenticate()) {
				transaction = (Transaction)request.getData();
				TransactionHistory transactionHistory = 
						new TransactionHistory(
								securityService.getLoginFromSession(),
								request.getActionSource());			
				responseCode = transactionService.withdrawalSecondApproved(transaction, transactionHistory);			
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch(Exception e) {
			logger.error("ERROR! secondApproved " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<Transaction>(transaction, responseCode);
		logger.debug("end withdrawal Second Approved response = " + response);
		return new ResponseEntity<Response<Transaction>>(response, HttpStatus.OK);
	}
	 
	@RequestMapping(value = "/withdrawal/cancel")
	public ResponseEntity<Response<Transaction>> cancelWithdrawal(@RequestBody Request<Transaction> request, HttpSession session) {		
		logger.debug("start cancel Withdrawal request = " + request);
		Response<Transaction> response = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		Transaction transaction = null;
		try {
			if (securityService.isAuthenticate()) {
				transaction = (Transaction)request.getData();
				TransactionHistory transactionHistory = 
						new TransactionHistory(
								securityService.getLoginFromSession(),
								request.getActionSource());		
				responseCode = transactionService.cancelWithdrawal(transaction, transactionHistory);			
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch(Exception e) {
			logger.error("ERROR! secondApproved " , e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		response = new Response<Transaction>(transaction, responseCode);
		logger.debug("end cancel Withdrawal response = " + response);
		return new ResponseEntity<Response<Transaction>>(response, HttpStatus.OK);
	}
}
