package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.MarketingContent;
import capital.any.model.base.Writer;
import capital.any.security.ISecurityService;
import capital.any.service.base.marketingContent.IMarketingContentService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/marketing/content", method = RequestMethod.POST)
public class MarketingContentController {
	private static final Logger logger = LoggerFactory.getLogger(MarketingContentController.class);
	
	@Autowired
	private IMarketingContentService marketingContentService;
	@Autowired
    private ISecurityService securityService;
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<MarketingContent>> insert(@RequestParam(value = "request") String requestJson, HttpSession session,
	@RequestParam(value = "fileUpload", required = false) MultipartFile fileUpload) {		
		Response<MarketingContent> response = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			Request<MarketingContent> request = (Request<MarketingContent>) mapper.readValue(requestJson, new TypeReference<Request<MarketingContent>>() {});
			if (securityService.isAuthenticate()) {
				MarketingContent marketingContent = (MarketingContent) request.getData();
				Writer writer = securityService.getLoginFromSession();
				marketingContent.setWriterId(writer.getId());
				if (fileUpload != null) {
					marketingContent.setHTMLFilePath(fileUpload.getOriginalFilename());
					marketingContent.setFileUpload(fileUpload);					
				}
				marketingContentService.insert(marketingContent);
				response = new Response<MarketingContent>(marketingContent, ResponseCode.OK);
			} else {
				response = new Response<MarketingContent>(null, ResponseCode.ACCESS_DENIED);
			}
		} catch (Exception e) {
			logger.error("can't insert marketing content", e);
			response = new Response<MarketingContent>(null, ResponseCode.UNKNOWN_ERROR);
		}
		return new ResponseEntity<Response<MarketingContent>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/edit")
	public ResponseEntity<Response<Object>> edit(@RequestParam(value = "request") String requestJson, HttpSession session,
			@RequestParam(value = "fileUpload", required = false) MultipartFile fileUpload) {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		ObjectMapper mapper = new ObjectMapper();
		try {
			Request<MarketingContent> request = (Request<MarketingContent>) mapper.readValue(requestJson, new TypeReference<Request<MarketingContent>>() {});
			if (securityService.isAuthenticate()) {
				MarketingContent marketingContent = (MarketingContent) request.getData();
				Writer writer = securityService.getLoginFromSession();
				marketingContent.setWriterId(writer.getId());
				if (fileUpload != null) {
					marketingContent.setHTMLFilePath(fileUpload.getOriginalFilename());
					marketingContent.setFileUpload(fileUpload);
				}
				marketingContentService.update(marketingContent);
				responseCode = ResponseCode.OK;
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! edit marketing content");
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAll")
	public ResponseEntity<Response<Map<Integer, MarketingContent>>> getAll() {
		logger.info("about to getAll marketing content.");
		ResponseCode responseCode = ResponseCode.OK;
		List<String> messages = new ArrayList<String>();
		Map<Integer, MarketingContent> hm = null;
		try {
			if (securityService.isAuthenticate()) {
				hm = marketingContentService.getAll();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getAll marketing content.", e);
		}
		Response<Map<Integer, MarketingContent>> response = new Response<Map<Integer,MarketingContent>>(hm, responseCode, messages);
		return new ResponseEntity<Response<Map<Integer,MarketingContent>>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getMarketingContent")
	public ResponseEntity<Response<MarketingContent>> getMarketingContent(@RequestBody Request<MarketingContent> request) {
		Response<MarketingContent> r;
		ResponseCode responseCode = ResponseCode.OK;
		MarketingContent marketingContent = null;
		try {
			if (securityService.isAuthenticate()) {
				marketingContent = (MarketingContent) request.getData();
				marketingContent = marketingContentService.getMarketingContent(marketingContent.getId());
				if (null == marketingContent) {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("can't get marketing content", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		r = new Response<MarketingContent>(marketingContent, responseCode);
		return new ResponseEntity<Response<MarketingContent>>(r, HttpStatus.OK);
	}
}
