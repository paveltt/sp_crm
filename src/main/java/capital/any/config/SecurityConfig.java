package capital.any.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import capital.any.security.CustomUserDetailService;
import capital.any.security.RestLogoutSuccessHandler;
import capital.any.security.RestAccessDeniedHandler;
import capital.any.security.RestAuthenticationFailureHandler;
import capital.any.security.RestAuthenticationSuccessHandler;
import capital.any.security.RestUnauthorizedEntryPoint;
 
/**
 * @author LioR SoLoMoN
 *
 */
@Configuration 
@EnableWebSecurity 
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@ComponentScan(basePackages = {"capital.any.security"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	private static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);
	public static final String REMEMBER_ME_KEY = "rememberme_key";	
	@Autowired
	private CustomUserDetailService userDetailsService;
	@Autowired 
	private RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;
	@Autowired
	private RestLogoutSuccessHandler restLogoutSuccessHandler;
	@Autowired
	private RestUnauthorizedEntryPoint restUnauthorizedEntryPoint;
	@Autowired
	private RestAuthenticationFailureHandler restAuthenticationFailureHandler;
	@Autowired
	private RestAccessDeniedHandler restAccessDeniedHandler;
	
	@Autowired
	public void configAuthBuilder(AuthenticationManagerBuilder builder) throws Exception {
		builder.userDetailsService(userDetailsService);
	}
	
	public SecurityConfig() {
        super();
        logger.info("loading SecurityConfig ... ");
    }
	
	@Override
    public void configure(WebSecurity web) throws Exception {
       /* web.ignoring().antMatchers("/resources/**", "/index.html", "/login.html",
                 "/template/**", "/", "/error/**");*/
    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {	
		//@formatter:off
		http
			.formLogin()
				.loginProcessingUrl("/rest/security/login-processing")					
				.successHandler(restAuthenticationSuccessHandler)
				.failureHandler(restAuthenticationFailureHandler)
				.loginPage("/rest/security/login-page") 
				.usernameParameter("username")
				.passwordParameter("password")		   				
				.and()
			/*.apply(new SpringSocialConfigurer()
	                .postLoginUrl("/")
	                .defaultFailureUrl("/#/login")
	                .alwaysUsePostLoginUrl(true))
	                .and()*/
			.httpBasic()
				.and()
			.exceptionHandling()
				.authenticationEntryPoint(restUnauthorizedEntryPoint)
            	.accessDeniedHandler(restAccessDeniedHandler)
				.and()
			.logout()
				//.logoutUrl("/logout")
				.logoutSuccessHandler(restLogoutSuccessHandler)
		  		.deleteCookies("JSESSIONID")
		  		.and()		  		
	  		/*.rememberMe()
                .tokenRepository(persistentTokenRepository())
                .key(REMEMBER_ME_KEY);	*/
		  	.authorizeRequests()
		  	 //Anyone can access the urls
		  		.antMatchers(
		  				"/js/**",
		  				"/css/**",
		  				"/plugins/**",
		  				"/bootstrap/**",
		  				"/dist/**",		  				
		  				"/message/**",
		  				/*"/login.html",*/
		  				"/index.html"
        		 ).permitAll()
		  		.antMatchers("/login.html").anonymous()
		  		.antMatchers("/secure/*").authenticated()
		  		//.anyRequest().authenticated()
		  		.and()
			.csrf()
				.disable()		
				
				;		
	}
	
	/*@Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
        tokenRepository.setDataSource(userDetailsService);
        return tokenRepository;
    }*/

	/*private Filter csrfHeaderFilter() {
		return new OncePerRequestFilter() {
			@Override
			protected void doFilterInternal(HttpServletRequest request,
					HttpServletResponse response, FilterChain filterChain)
					throws ServletException, IOException {
				CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class
						.getName());
				if (csrf != null) {
					Cookie cookie = WebUtils.getCookie(request, "XSRF-TOKEN");
					String token = csrf.getToken();
					if (cookie == null || token != null
							&& !token.equals(cookie.getValue())) {
						cookie = new Cookie("XSRF-TOKEN", token);
						cookie.setPath("/");
						response.addCookie(cookie);
					}
				}
				filterChain.doFilter(request, response);
			}
		};
	}

	private CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		repository.setHeaderName("X-XSRF-TOKEN");
		return repository;
	}*/
} 
