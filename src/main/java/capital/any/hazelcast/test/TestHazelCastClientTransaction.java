package capital.any.hazelcast.test;

import com.hazelcast.core.IMap;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.model.base.Currency;

/**
 * @author LioR SoLoMoN
 *
 */
public class TestHazelCastClientTransaction {
	public static void main(String[] args) {
		IMap<?, ?> map = HazelCastClientFactory.getClient().getMap(Constants.MAP_CURRENCIES);
		
		for (Object m : map.values()) {
			System.out.println(m);
		}
		
		
		
		Currency c = new Currency(10000, "LiorCurr", "LioR.currency", "LC", 65);
		//HazelCastClientFactory.getClient().insertIntoTransactionMap(Constants.MAP_CURRENCIES, c, c.getId());
		
		map = HazelCastClientFactory.getClient().getMap(Constants.MAP_CURRENCIES);
		
		for (Object m : map.values()) {
			System.out.println(m);
		}
		

	}
}
