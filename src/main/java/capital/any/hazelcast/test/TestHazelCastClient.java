package capital.any.hazelcast.test;

import java.util.Map;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;

/**
 * @author LioR SoLoMoN
 *
 */
public class TestHazelCastClient {
	public static void main(String[] args) {
		Map<?, ?> map = HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_TYPES);
		for (Object m : map.values()) {
			System.out.println(m);
		}
	}
}
