package capital.any.dao.backendScreen;

import capital.any.model.BeScreen;

/**
 * @author eranl
 *
 */
public interface IBackendScreenDao {
	
	public static final String INSERT_BACKEND_SCREEN = 
			"  INSERT " +
		    "  INTO BE_SCREENS " +
			"  ( " +
		    "     name, " +
		    " 	  space_id " + 
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    "     :name, " +
		    " 	  :spaceId " +
		    "  ) ";
			
	void insert(BeScreen beScreen);
	
}
