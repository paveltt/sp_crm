package capital.any.dao.backendScreen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.BeScreen;

/**
 * @author eranl
 *
 */
@Repository
public class BackendScreenDao implements IBackendScreenDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public void insert(BeScreen beScreen) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("name", beScreen.getName())						
								.addValue("spaceId", beScreen.getSpaceId());
		
		namedParameterJdbcTemplate.update(INSERT_BACKEND_SCREEN, namedParameters);
	}

	
}
