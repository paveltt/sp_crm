//package capital.any.dao.backendScreen;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//
//import capital.any.model.BeScreen;
//
///**
// * @author eranl
// *
// */
//@Component
//public class BackendScreenMapper implements RowMapper<BeScreen> {
//			
//	@Override
//	public BeScreen mapRow(ResultSet rs, int row) throws SQLException {
//		BeScreen beScreen = new BeScreen();
//		beScreen.setId((rs.getInt("id")));
//		beScreen.setName((rs.getString("name")));
//		beScreen.setSpaceId(rs.getInt("space_id"));
//		return beScreen;		
//	}
//	
//	public BeScreen mapRow(ResultSet rs) throws SQLException {
//		return this.mapRow(rs, 0);
//	}
//	
//}
