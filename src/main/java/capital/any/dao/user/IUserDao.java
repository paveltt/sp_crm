package capital.any.dao.user;

import java.util.List;

import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.base.enums.Table;
import capital.any.model.ClientSpace;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.User;

/**
 * @author EyalG
 *
 */
public interface IUserDao extends capital.any.dao.base.user.IUserDao {
	
	public static final String SELECT_CLIENT_SPACE = 
			"SELECT " +
				"u.id AS user_id " +
				", u.email AS email " +
				", CONCAT(u.first_name, ' ', u.last_name) AS name " +
				", c.display_name AS country_display_name " +
			    ", c.id AS country_id " +
			    ", !ISNULL(ma.id) AS depositor " +
			    ", CONCAT(mc.name, ' ', ms.name) AS campaign " +
			    ", inv.sum_open_inv + u.balance AS balance " +
				", inv.sum_open_inv AS invested_amount " +
			    ", u.balance AS disposable_amount " +
				", inv.count_open_inv AS open_investments " +
				", user_products.count_open_inv_on_prod AS number_of_products " +
				", IF(u.step = " + User.Step.APPROVE.getId() + ", 1, 0) AS compliance_approved " +
				", u.time_created " +
				", regulation_qs.display_name AS regulation_qs_display_name " + 
			"FROM " +
				"users u " +
				"LEFT JOIN regulation_user_info r_ui ON r_ui.user_id = u.id " + 
                "LEFT JOIN regulation_questionnaire_statuses regulation_qs ON r_ui.regulation_questionnaire_status_id = regulation_qs.id " +  
			    "LEFT JOIN marketing_attribution ma ON ma.user_id = u.id AND ma.table_id = " + Table.TRANSACTIONS.getId() + " " +
				"LEFT JOIN (SELECT " + 
								"i.user_id, " + 
								"SUM(i.amount) AS sum_open_inv, " +
								"COUNT(1) AS count_open_inv " +
						   "FROM " +
								"investments i " +
						   "WHERE " +
								"i.investment_status_id IN " + "( " + InvestmentStatusEnum.PENDING.getId() + ", " + InvestmentStatusEnum.OPEN.getId() + ") " + /* open */
						   "GROUP BY " +
								"i.user_id ) AS inv ON u.id = inv.user_id " +
				"LEFT JOIN (SELECT " +
								"i.user_id, " + 
								"COUNT(Distinct i.product_id) AS count_open_inv_on_prod " +
						   "FROM " +
								"investments i " +
						   "WHERE " +
								"i.investment_status_id in " + "( " + InvestmentStatusEnum.PENDING.getId() + ", " + InvestmentStatusEnum.OPEN.getId() + ") " + /* open */
						   "GROUP BY " +
						   		"i.user_id) AS user_products ON u.id = user_products.user_id, " +
				"countries c, " +
			    "marketing_attribution mar, " +
			    "marketing_tracking mt, " +
			    "marketing_campaigns mc, " +
			    "marketing_sources ms " +
			"WHERE " +
				"c.id = u.country_by_prefix " +
			    "AND mar.user_id = u.id " +
			    "AND mar.table_id = " + Table.USERS.getId() + " " +
			    "AND mar.tracking_id = mt.id " +
			    "AND mc.id = mt.campaign_id " +
			    "AND ms.id = mc.source_id " +
			    "AND 1 = IF(ISNULL(:userId), 1, IF(:userId = u.id, 1, 0)) " +
			    "AND 1 = IF(ISNULL(:name), 1, IF(UPPER(:name) = UPPER(u.first_name) OR UPPER(:name) = UPPER(u.last_name), 1, 0)) " +
			    "AND 1 = IF(ISNULL(:email), 1, IF(UPPER(:email) = UPPER(u.email), 1, 0)) " +
			    "AND 1 = IF(ISNULL(:country), 1, IF(UPPER(:country) = UPPER(u.country_by_prefix), 1, 0)) " +
			    "AND 1 = IF(ISNULL(:userClassId), 1, IF(:userClassId = u.class_id, 1, 0)) " +
			    "AND u.time_created >= :from " +
			    "AND u.time_created <= :to ";
	
	
//			    "AND EXISTS (SELECT " +
//			    				"1 " +
//			    			"FROM " +
//			    				"logins l " +
//			    			"WHERE " +
//				    			"l.user_id = u.id " +
//							    "AND l.time_created >= :from " +
//							    "AND l.time_created <= :to " +
//			    			")";
	
	public static final String ADVANCED_SEARCH_USER = 
			" SELECT " +
			"	u.*, " +
			"	concat('(', country_by_prefix.phone_code , ') ', u.mobile_phone) AS mobile_with_prefix, " +
			"	country_by_prefix.DISPLAY_NAME AS country_p_display, " +
			"	country_by_user.DISPLAY_NAME AS country_u_display, " +
			"	country_by_ip.DISPLAY_NAME AS country_i_display, " +
			"	cu.DISPLAY_NAME AS currency_display, " +
			"	l.DISPLAY_NAME AS language_display " +
			" FROM " +
			" 	users u " +
			"		LEFT JOIN countries country_by_prefix ON country_by_prefix.id = u.country_by_prefix " +
			"     	LEFT JOIN countries country_by_user ON country_by_user.id = u.country_by_user " +
			"     	LEFT JOIN countries country_by_ip ON country_by_ip.id = u.country_by_ip " +
			"     	LEFT JOIN currencies cu ON cu.id = u.currency_id " +
			"     	LEFT JOIN languages l ON l.id = u.language_id " +	 
			" WHERE " +
			"	1 = IF(ISNULL(:userId), 1, IF(:userId = u.id, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:email), 1, IF(UPPER(u.email) like UPPER(:email), 1, 0)) " +
			"	AND 1 = IF(ISNULL(:firstName), 1, IF(UPPER(u.first_name) like UPPER(:firstName), 1, 0)) " +
			"	AND 1 = IF(ISNULL(:lastName), 1, IF(UPPER(u.last_name) like UPPER(:lastName), 1, 0)) " +
			"	AND 1 = IF(ISNULL(:mobile), 1, IF((u.mobile_phone) like (:mobile), 1, 0)) " +        
			"	AND 1 = IF(ISNULL(:userClassId), 1, IF(:userClassId = u.class_id, 1, 0)) ";
	
	/**
	 * get client space screen users information
	 * @param {@link SqlFilters)
	 * @return list of {@link ClientSpace}
	 */
	List<ClientSpace> getClientSpace(SqlFilters sqlFilters);
	
	/**
	 * advanced search user 
	 * @param {@link SqlFilters)
	 * @return list of {@link User}
	 */
	List<capital.any.model.User> advancedSearchUser(SqlFilters sqlFilters);

}
