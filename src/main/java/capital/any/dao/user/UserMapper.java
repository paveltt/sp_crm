package capital.any.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import capital.any.dao.base.MapperBase;
import capital.any.model.User;
import capital.any.service.UtilService;

/**
 * @author eranl
 *
 */
@Component
@Repository("UserMapperCRM")
public class UserMapper extends MapperBase implements RowMapper<User> {
	
	@Autowired
	capital.any.dao.base.user.UserMapper userMapper;	
	@Autowired
	UtilService utilService;
	
	@Override
	public User mapRow(ResultSet rs, int row) throws SQLException {
		capital.any.model.base.User userBase = userMapper.mapRow(rs, row); 
		User user = new User();
		BeanUtils.copyProperties(userBase, user);
		user.setMobilePhoneWithPrefix(rs.getString("mobile_with_prefix"));
		user.setCountryByIpStr(utilService.isArgumentEmptyOrNull(rs.getString("country_i_display")) ? "n-a" : rs.getString("country_i_display"));
		user.setCountryByUserStr(utilService.isArgumentEmptyOrNull(rs.getString("country_u_display")) ? "n-a" : rs.getString("country_u_display"));
		user.setCountryByPrefixStr(utilService.isArgumentEmptyOrNull(rs.getString("country_p_display")) ? "n-a" : rs.getString("country_p_display"));	
		user.setCurrencyStr(rs.getString("currency_display"));
		user.setLanguageStr(rs.getString("language_display"));
		return user;
	}
	
	public User mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
