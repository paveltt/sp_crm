package capital.any.dao.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.ClientSpace;
import capital.any.model.base.Country;

/**
 * @author eranl
 *
 */
@Component
public class ClientSpaceMapper extends MapperBase implements RowMapper<ClientSpace>{
	
	@Override
	public ClientSpace mapRow(ResultSet rs, int row) throws SQLException {
		ClientSpace clientSpace = new ClientSpace();
		clientSpace.setUserId(rs.getLong("user_id"));
		clientSpace.setEmail(rs.getString("email"));
		clientSpace.setName(rs.getString("name"));
		Country country = new Country();
		country.setId(rs.getInt("country_id"));
		country.setDisplayName(rs.getString("country_display_name"));
		clientSpace.setCountry(country);
		clientSpace.setDepositor(rs.getInt("depositor") == 1 ? true : false);
		clientSpace.setBalance(rs.getLong("balance"));
		clientSpace.setCampaignName(rs.getString("campaign"));
		clientSpace.setComplianceApproved(rs.getInt("compliance_approved") == 1 ? true : false);
		clientSpace.setDisposableAmount(rs.getLong("disposable_amount"));
		clientSpace.setInvestedAmount(rs.getLong("invested_amount"));
		clientSpace.setNumberOfProducts(rs.getInt("number_of_products"));
		clientSpace.setOpenInvestments(rs.getInt("open_investments"));
		clientSpace.setTimeCreated(convertTimestampToDate(rs.getTimestamp("time_created")));
		String regulationQuestionnaire = rs.getString("regulation_qs_display_name");
		if (regulationQuestionnaire == null) {
			regulationQuestionnaire = "n-a";
		}
		clientSpace.setRegulationQuestionnaire(regulationQuestionnaire);
		return clientSpace;
	}
	
	public ClientSpace mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}

}
