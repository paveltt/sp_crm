package capital.any.dao.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.ClientSpace;
import capital.any.model.base.SqlFilters;
import capital.any.service.UtilService;

/**
 * @author EyalG
 *
 */
@Repository("UserDaoCRM")
public class UserDao extends capital.any.dao.base.user.UserDao implements IUserDao {
	
	@Autowired
	private ClientSpaceMapper clientSpaceMapper; 
	@Autowired
	@Qualifier("UserMapperCRM")
	private capital.any.dao.user.UserMapper userMapperCRM;	
	@Autowired
	private UtilService utilService;
	
	@Override
	public List<ClientSpace> getClientSpace(SqlFilters sqlFilters) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("userId", sqlFilters.getUserId())
				.addValue("name", sqlFilters.getName())
				.addValue("email", sqlFilters.getEmail())
				.addValue("from", sqlFilters.getFrom())
				.addValue("to", sqlFilters.getTo())
				.addValue("country", sqlFilters.getCountryId())
				.addValue("userClassId", sqlFilters.getUserClassId());
		return jdbcTemplate.query(SELECT_CLIENT_SPACE, namedParameters, clientSpaceMapper);
	}

	@Override
	public List<capital.any.model.User> advancedSearchUser(SqlFilters sqlFilters) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("userId", sqlFilters.getUserId())
				.addValue("firstName", utilService.isArgumentEmptyOrNull(sqlFilters.getFirstName()) ? sqlFilters.getFirstName() : sqlFilters.getFirstName() + "%")
				.addValue("lastName", utilService.isArgumentEmptyOrNull(sqlFilters.getLastName()) ? sqlFilters.getLastName() : sqlFilters.getLastName() + "%")
				.addValue("email", utilService.isArgumentEmptyOrNull(sqlFilters.getEmail()) ? sqlFilters.getEmail() : sqlFilters.getEmail() + "%")
				.addValue("mobile", utilService.isArgumentEmptyOrNull(sqlFilters.getMobilePhone()) ? sqlFilters.getMobilePhone() : sqlFilters.getMobilePhone() + "%")
				.addValue("userClassId", sqlFilters.getUserClassId());
		return jdbcTemplate.query(ADVANCED_SEARCH_USER, namedParameters, userMapperCRM);
	}
}
