package capital.any.dao.messageResource;

import java.util.Map;

import capital.any.model.base.MsgResLanguage;

/**
 * @author eran.levy
 *
 */
public interface IMessageResourceDao extends capital.any.dao.base.messageResource.IMessageResourceDao {
	
	/**
	 * @return
	 */
	Map<Integer, Map<Integer, Map<String, MsgResLanguage>>> getMessageResourcesFull();	
	
}
