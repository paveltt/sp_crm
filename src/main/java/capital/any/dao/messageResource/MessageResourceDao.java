package capital.any.dao.messageResource;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MsgResLanguage;

/**
 * @author eranl
 *
 */
@Repository("MessageResourceDaoCRM")
public class MessageResourceDao extends capital.any.dao.base.messageResource.MessageResourceDao implements IMessageResourceDao, ApplicationContextAware {
	
	private ApplicationContext appContext;
	
	@Autowired
	protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	@Qualifier("MessageResourceCallbackHandlerCRM")
	MessageResourceCallbackHandler callbackHandler;

	@Override
	public Map<Integer, Map<Integer, Map<String, MsgResLanguage>>> getMessageResourcesFull() {		
		callbackHandler = appContext.getBean(MessageResourceCallbackHandler.class);
		namedParameterJdbcTemplate.query(GET, callbackHandler);
		return callbackHandler.getResult();
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;		
	}
	
}
