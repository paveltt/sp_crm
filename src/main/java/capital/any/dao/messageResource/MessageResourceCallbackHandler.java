package capital.any.dao.messageResource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.dao.base.messageResource.MessageResourceMapper;
import capital.any.model.base.MsgResLanguage;

/**
 * @author eranl
 *
 */
@Component("MessageResourceCallbackHandlerCRM")
@Scope("prototype")
public class MessageResourceCallbackHandler implements RowCallbackHandler {
	
	private Map<Integer, Map<Integer, Map<String, MsgResLanguage>>> resultActionSource = new HashMap<Integer, Map<Integer, Map<String, MsgResLanguage>>>();
	@Autowired
	private MessageResourceMapper messageResourceMapper;
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {
		MsgResLanguage msgResLanguage = messageResourceMapper.mapRow(rs);
		//get the action source
		Map<Integer, Map<String, MsgResLanguage>> mrActionSource = resultActionSource.get(msgResLanguage.getMsgRes().getActionSourceId());
		if (mrActionSource == null) {
			mrActionSource = new HashMap<Integer, Map<String, MsgResLanguage>>();
		}
		//get the language for this action source
		Map<String, MsgResLanguage> messageResources = mrActionSource.get(msgResLanguage.getMsgResLanguageId());
		if (messageResources == null) {
			messageResources = new HashMap<String, MsgResLanguage>();		
		}
		messageResources.put(msgResLanguage.getMsgRes().getKey(), msgResLanguage);
		mrActionSource.put(msgResLanguage.getMsgResLanguageId(), messageResources);		
		resultActionSource.put(msgResLanguage.getMsgRes().getActionSourceId(), mrActionSource);
	}

	/**
	 * @return the result
	 */
	public Map<Integer, Map<Integer, Map<String, MsgResLanguage>>> getResult() {
		return resultActionSource;
	}	
}
