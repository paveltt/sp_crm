package capital.any.dao.productMarket;

import java.util.List;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductMarket;

/**
 * @author EyalG
 *
 */
@Repository("ProductMarketDaoCRM")
public class ProductMarketDao extends capital.any.dao.base.productMarket.ProductMarketDao implements IProductMarketDao {
	
	@Override
	public void insert(ProductMarket productMarket) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productMarket.getProductId())
						 .addValue("marketId", productMarket.getMarket().getId())
						 .addValue("startTradeLevel", productMarket.getStartTradeLevel());
		
		jdbcTemplate.update(INSERT_PRODUCT_MARKETS,
				namedParameters);
	}
	
	@Override
	public void insert(List<ProductMarket> productMarkets) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(productMarkets.toArray());
		jdbcTemplate.batchUpdate(INSERT_PRODUCT_MARKETS,
				batch);
	}

	@Override
	public boolean updateStartTradeLevel(List<ProductMarket> productMarkets) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(productMarkets.toArray());
		return (jdbcTemplate.batchUpdate
				(UPDATE_PRODUCT_MARKETS_START_TRADE_LEVEL,batch).length != 0);
	}

	@Override
	public boolean updateEndTradeLevel(List<ProductMarket> productMarkets) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(productMarkets.toArray());
		return (jdbcTemplate.batchUpdate
				(UPDATE_PRODUCT_MARKETS_END_TRADE_LEVEL, batch).length != 0);
	}

	@Override
	public boolean updateMarketId(ProductMarket productMarket) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("marketId", productMarket.getMarket().getId())
				.addValue("id", productMarket.getId());
		return (jdbcTemplate.update(UPDATE_PRODUCT_MARKETS_MARKET_ID, namedParameters) > 0);
	}
}
