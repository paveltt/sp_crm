package capital.any.dao.productMarket;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.product.UpdateProductEndTradeLevelEntry;
import capital.any.hazelcast.update.base.product.UpdateProductStartTradeLevelEntry;
import capital.any.model.base.ProductMarket;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductMarketHCDaoCRM")
public class ProductMarketHCDao extends capital.any.dao.base.productMarket.ProductMarketHCDao implements IProductMarketDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductMarketHCDao.class);
	
	@Override
	public void insert(ProductMarket productMarket) {
		logger.info("hazelcast insert product market not implemented");
	}

	@Override
	public void insert(List<ProductMarket> productMarkets) {
		logger.info("hazelcast insert product market list not implemented");	
	}

	@Override
	public boolean updateStartTradeLevel(List<ProductMarket> productMarkets) throws Exception {
		return HazelCastClientFactory.getClient().updateTransactionMap(
				productMarkets.get(0).getProductId(), 
				new UpdateProductStartTradeLevelEntry(productMarkets), 
				Constants.MAP_PRODUCTS);
	}

	@Override
	public boolean updateEndTradeLevel(List<ProductMarket> productMarkets) throws Exception {	
		return HazelCastClientFactory.getClient().updateTransactionMap(
				productMarkets.get(0).getProductId(), 
				new UpdateProductEndTradeLevelEntry(productMarkets), 
				Constants.MAP_PRODUCTS);
	}

	/**
	 * not in use with HC
	 */
	@Override
	public boolean updateMarketId(ProductMarket productMarket) {
		logger.info("hazelcast update marketid in product not implemented");
		return false;
	}
}
