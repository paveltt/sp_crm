package capital.any.dao.productMarket;

import java.util.List;

import capital.any.model.base.ProductMarket;

/**
 * @author EyalG
 *
 */
public interface IProductMarketDao extends capital.any.dao.base.productMarket.IProductMarketDao {
	
	public static final String INSERT_PRODUCT_MARKETS = 
											" INSERT " +
										    " INTO PRODUCT_MARKETS" +
										    " (" +
										    "    product_id, " +
										    "    market_id " +
										    " )" +
										    " VALUES" +
										    " (" +
										    "    :productId, " +
										    "    :marketId " +
										    " ) ";
	
	public static final String UPDATE_PRODUCT_MARKETS_START_TRADE_LEVEL = 
											" UPDATE " +
											"	product_markets " +
											" SET " + 
											"	start_trade_level = :startTradeLevel " +
											" WHERE " + 
											" 	product_id    = :productId " +
											" 	and market_id = :marketId ";
	
	public static final String UPDATE_PRODUCT_MARKETS_END_TRADE_LEVEL = 
											" UPDATE " +
											"	product_markets " +
											" SET " + 
											"	end_trade_level = :endTradeLevel " +
											" WHERE " + 
											" 	product_id    = :productId " +
											" 	and market_id = :marketId ";
	
	public static final String UPDATE_PRODUCT_MARKETS_MARKET_ID = 
											" UPDATE " +
											"	product_markets " +
											" SET " + 
											"	market_id = :marketId " +
											" WHERE " + 
											" 	id = :id ";
		
	void insert(ProductMarket productMarket);
	
	void insert(List<ProductMarket> productMarkets);
	
	/**
	 * update product markets start trade level by product id
	 * @param productMarkets
	 * @throws Exception 
	 */
	boolean updateStartTradeLevel(List<ProductMarket> productMarkets) throws Exception;
	
	/**
	 * update product markets end trade level by product id
	 * @param productMarkets
	 * @throws Exception 
	 */
	boolean updateEndTradeLevel(List<ProductMarket> productMarkets) throws Exception;
	
	/**
	 * update the market id
	 * @param productMarket
	 * @return true if update was done else false
	 */
	boolean updateMarketId(ProductMarket productMarket);

}
