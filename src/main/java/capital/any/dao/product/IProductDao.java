package capital.any.dao.product;

import java.util.List;

import capital.any.base.enums.ProductStatusEnum;
import capital.any.model.base.Product;
import capital.any.model.base.SqlFilters;

/**
 * @author EyalG
 *
 */
public interface IProductDao extends capital.any.dao.base.product.IProductDao {
	
	public static final String INSERT_PRODUCTS = 
			" INSERT " +
		    " INTO products " +
		    " (" +
		    "    currency_id ," +
		    "    subscription_end_date ," +
		    "    product_coupon_frequency_id ," +
		    "    strike_level ," +
		    "    last_trading_date ," +
		    "    product_type_id ," +
		    "    first_exchange_trading_date ," +
		    "    bonus_level ," +
		    "    initial_fixing_date ," +
		    "    max_yield_p_a ," +
		    "    issue_price ," +
		    "    level_of_participation ," +
		    "    issue_date ," +
		    "    is_suspend ," +
		    "    bond_floor_at_issuance ," +
		    "    redemption_date ," +
		    "    time_created ," +
		    "    day_count_convention_id ," +
		    "    subscription_start_date ," +
		    "    denomination ," +
		    "    level_of_protection ," +
		    "    final_fixing_date ," +
		    "    issue_size ," +
		    "    product_status_id ," +
		    "    max_yield ," +
		    "    product_insurance_type_id ," +
		    "    cap_level ," +
		    "    is_quanto, " +
		    " 	 product_maturity_id, " +
		    " 	 priority " +
		    "  )" +
		    "  VALUES" +
		    "  (" +
		    "    :currency_id ," +
		    "    :subscription_end_date ," +
		    "    :product_coupon_frequency_id ," +
		    "    :strike_level ," +
		    "    :last_trading_date ," +
		    "    :product_type_id ," +
		    "    :first_exchange_trading_date ," +
		    "    :bonus_level ," +
		    "    :initial_fixing_date ," +
		    "    :max_yield_p_a ," +
		    "    :issue_price ," +
		    "    :level_of_participation ," +
		    "    :issue_date ," +
		    "    :is_suspend ," +
		    "    :bond_floor_at_issuance ," +
		    "    :redemption_date ," +
		    "    :time_created ," +
		    "    :day_count_convention_id ," +
		    "    :subscription_start_date ," +
		    "    :denomination ," +
		    "    :level_of_protection ," +
		    "    :final_fixing_date ," +
		    "    :issue_size ," +
		    "	 :product_status_id , " +
		    "    :max_yield ," +
		    "    :product_insurance_type_id ," +
		    "    :cap_level ," +
		    "    :is_quanto, " +
		    "	 :product_maturity_id, " +
		    " 	 (SELECT Auto_increment FROM information_schema.tables WHERE table_name='products' AND table_schema = 'anycapital') " +
		    "  ) ";
	
	public static final String UPDATE_PRODUCT_ASK_BID = 
			" UPDATE " +
			" 	products " +
			" SET " +
			"   ask = :ask, " +
			"   bid = :bid " +
			" WHERE "	+ 
			"	id = :id ";
	
	public static final String UPDATE_PRODUCT_SUSPEND = 
			" UPDATE " +
			" 	products " +
			" SET " +
			"   is_suspend = :suspend " +
			" WHERE "	+ 
			"	id = :id ";
	
	
	public static final String UPDATE_PRODUCT = 
			" UPDATE " +
			" 	PRODUCTS " +
		    " SET " +
			"	currency_id               	= :currency_id , " +
		    " 	subscription_end_date       = :subscription_end_date , " +
		    " 	strike_level                = :strike_level , " +
		    " 	last_trading_date           = :last_trading_date , " +
		    " 	product_type_id             = :product_type_id , " +
		    " 	first_exchange_trading_date = :first_exchange_trading_date , " +
		    " 	initial_fixing_date         = :initial_fixing_date , " +
		    " 	max_yield_p_a               = :max_yield_p_a , " +
		    " 	product_coupon_frequency_id = :product_coupon_frequency_id , " +
		    " 	issue_price                 = :issue_price , " +
		    " 	level_of_participation      = :level_of_participation , " +
		    " 	issue_date                  = :issue_date , " +
		    " 	bond_floor_at_issuance      = :bond_floor_at_issuance , " +
		    " 	cap_level                   = :cap_level , " +
		    " 	redemption_date             = :redemption_date , " +
		    " 	day_count_convention_id     = :day_count_convention_id , " +
		    " 	bonus_level                 = :bonus_level , " +
		    " 	subscription_start_date     = :subscription_start_date , " +
		    " 	denomination                = :denomination , " +
		    " 	product_maturity_id         = :product_maturity_id , " +
		    " 	level_of_protection         = :level_of_protection , " +
		    " 	final_fixing_date           = :final_fixing_date , " +
		    " 	issue_size                  = :issue_size , " +
		    " 	max_yield                   = :max_yield , " +
			" 	product_insurance_type_id   = :product_insurance_type_id , " +
		    " 	is_quanto                   = :is_quanto " +
		    " WHERE  " +
		    	"id                         = :id";
	
	public static final String UPDATE_PRODUCT_PRIORITY = 
			" UPDATE " +
			" 	products " +
			" SET " +
			"   priority = :priority " +
			" WHERE "	+ 
			"	id = :id ";
	
	public static final String GET_PRIORITY_PRODUCTS_LIST = 
			"SELECT " + 
			"	*  " +
			"FROM " +
			"	products p " +
			"WHERE " +
			"	p.product_status_id in (" + ProductStatusEnum.SUBSCRIPTION.getId() + "," + ProductStatusEnum.SECONDARY.getId() + ") " +
			"ORDER BY " +
			"	p.priority desc";
	
	public static final String GET_PRODUCTS_BY_SUBSCRIPTION_START_DATE = 
			"SELECT " + 
			"	*  " +
			"FROM " +
			"	products p " +
			"WHERE " +
			"	1 = IF(ISNULL(:subscriptionStartDate), 1, IF(date(p.subscription_start_date) = date(:subscriptionStartDate), 1, 0)) ";
	
	public static final String UPDATE_PRODUCT_KID = 
			" UPDATE " +
			" 	products " +
			" SET " +
			"   kid_name = :kidName " +
			" WHERE "	+ 
			"	id = :id ";
	
	/**
	 * insert product
	 * @param product
	 * @throws Exception 
	 */
	boolean insert(Product product) throws Exception;

	/**
	 * update product ask and bid by product id
	 * @param p the product to update must have ask, bid and id
	 * @throws Exception 
	 */
	boolean updateAskBid(Product p) throws Exception;
	
	/**
	 * update product suspend
	 * @param p
	 * @throws Exception 
	 */
	boolean updateSuspend(Product p) throws Exception;
	
	/**
	 * update product
	 * @param product
	 * @throws Exception 
	 */
	boolean updateProduct(Product product) throws Exception;
	
	/**
	 * update products priority
	 * @param products list to update
	 * @return true if we update else false
	 */
	boolean updateProductsPriority(List<Product> products) throws Exception;
	
	/**
	 * get all products for priority screen
	 * @return list of products
	 */
	List<Product> getProductsForPriority();
	
	/**
	 * get all products that fit the criterion
	 * @param filters
	 * @return {@link List<Product>}
	 */
	List<Product> getProducts(SqlFilters filters);
	
	/**
	 * Update KID name.
	 * @param p
	 * @return
	 * @throws Exception
	 */
	boolean updateKid(Product p) throws Exception;
}
