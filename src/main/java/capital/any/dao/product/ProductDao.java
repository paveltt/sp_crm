package capital.any.dao.product;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Product;
import capital.any.model.base.SqlFilters;

/**
 * @author EyalG
 *
 */
@Repository("ProductDaoCRM")
public class ProductDao extends capital.any.dao.base.product.ProductDao implements IProductDao {
	
	@Override
	public boolean insert(Product product) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
						 ("currency_id", product.getCurrencyId())
				.addValue("subscription_end_date", product.getSubscriptionEndDate())
				.addValue("product_coupon_frequency_id", product.getProductCouponFrequency() != null ? product.getProductCouponFrequency().getId() > 0 ? product.getProductCouponFrequency().getId() : 1 : 1)
				.addValue("strike_level", product.getStrikeLevel())
				.addValue("last_trading_date", product.getLastTradingDate())
				.addValue("product_type_id", product.getProductType().getId())
				.addValue("first_exchange_trading_date", product.getFirstExchangeTradingDate())
				.addValue("bonus_level", product.getBonusLevel())
				.addValue("initial_fixing_date", product.getInitialFixingDate())
				.addValue("max_yield_p_a", product.getMaxYieldPA())
				.addValue("issue_price", product.getIssuePrice())
				.addValue("level_of_participation", product.getLevelOfParticipation())
				.addValue("issue_date", product.getIssueDate())
				.addValue("is_suspend", (product.isSuspend() ? 1 : 0))
				.addValue("bond_floor_at_issuance", product.getBondFloorAtissuance())
				.addValue("redemption_date", product.getRedemptionDate())
				.addValue("time_created", product.getTimeCreated())
				.addValue("day_count_convention_id", product.getDayCountConvention() != null ? product.getDayCountConvention().getId() > 0 ? product.getDayCountConvention().getId() : null : null)
				.addValue("subscription_start_date", product.getSubscriptionStartDate())
				.addValue("denomination", product.getDenomination())
				.addValue("level_of_protection", product.getLevelOfProtection())
				.addValue("final_fixing_date", product.getFinalFixingDate())
				.addValue("issue_size", product.getIssueSize())
				.addValue("product_status_id", product.getProductStatus().getId())
				.addValue("max_yield", product.getMaxYield())
				.addValue("product_insurance_type_id", product.getProductInsuranceTypeId())
				.addValue("cap_level", product.getCapLevel())
				.addValue("is_quanto", (product.isQuanto() ? 1 : 0))
				.addValue("product_maturity_id", product.getProductMaturity().getId());
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		boolean result = (jdbcTemplate.update(INSERT_PRODUCTS, namedParameters, keyHolder, new String[] {"id"}) > 0);
		product.setId(keyHolder.getKey().longValue());
		return result;
	}

	@Override
	public boolean updateAskBid(Product p) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", p.getId()).
				addValue("ask", p.getAsk())
				.addValue("bid", p.getBid());
		return (jdbcTemplate.update(UPDATE_PRODUCT_ASK_BID, namedParameters) > 0);		
	}

	@Override
	public boolean updateSuspend(Product p) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", p.getId()).
					addValue("suspend", p.isSuspend() ? 1 : 0);
		return (jdbcTemplate.update(UPDATE_PRODUCT_SUSPEND, namedParameters) > 0);
	}

	@Override
	public boolean updateProduct(Product product) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
						 ("currency_id", product.getCurrencyId())
				.addValue("subscription_end_date", product.getSubscriptionEndDate())
				.addValue("product_coupon_frequency_id", product.getProductCouponFrequency() != null ? product.getProductCouponFrequency().getId() > 0 ? product.getProductCouponFrequency().getId() : 1 : 1)
				.addValue("strike_level", product.getStrikeLevel())
				.addValue("last_trading_date", product.getLastTradingDate())
				.addValue("product_type_id", product.getProductType().getId())
				.addValue("first_exchange_trading_date", product.getFirstExchangeTradingDate())
				.addValue("bonus_level", product.getBonusLevel())
				.addValue("initial_fixing_date", product.getInitialFixingDate())
				.addValue("max_yield_p_a", product.getMaxYieldPA())
				.addValue("issue_price", product.getIssuePrice())
				.addValue("level_of_participation", product.getLevelOfParticipation())
				.addValue("issue_date", product.getIssueDate())
				.addValue("bond_floor_at_issuance", product.getBondFloorAtissuance())
				.addValue("redemption_date", product.getRedemptionDate())
				.addValue("day_count_convention_id", product.getDayCountConvention() != null ? product.getDayCountConvention().getId() > 0 ? product.getDayCountConvention().getId() : null : null)
				.addValue("subscription_start_date", product.getSubscriptionStartDate())
				.addValue("denomination", product.getDenomination())
				.addValue("level_of_protection", product.getLevelOfProtection())
				.addValue("final_fixing_date", product.getFinalFixingDate())
				.addValue("issue_size", product.getIssueSize())
				.addValue("max_yield", product.getMaxYield())
				.addValue("product_insurance_type_id", product.getProductInsuranceTypeId())
				.addValue("id", product.getId())
				.addValue("cap_level", product.getCapLevel())
				.addValue("is_quanto", (product.isQuanto() ? 1 : 0))
				.addValue("product_maturity_id", product.getProductMaturity().getId());
		
		return (jdbcTemplate.update(UPDATE_PRODUCT, namedParameters) > 0);
	}

	@Override
	public boolean updateProductsPriority(List<Product> products) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(products.toArray());
		int[] results = jdbcTemplate.batchUpdate(UPDATE_PRODUCT_PRIORITY,
												batch);
		for (int i : results) {
			//http://docs.oracle.com/javase/1.5.0/docs/api/java/sql/Statement.html#SUCCESS_NO_INFO
			if (i > 0 || i == -2) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<Product> getProductsForPriority() {
		return jdbcTemplate.query(GET_PRIORITY_PRODUCTS_LIST, productMapper);
	}

	@Override
	public List<Product> getProducts(SqlFilters filters) {
		SqlParameterSource namedParameters = new MapSqlParameterSource
						 ("subscriptionStartDate", filters.getSubscriptionStartDate());
		return jdbcTemplate.query(GET_PRODUCTS_BY_SUBSCRIPTION_START_DATE, namedParameters, productMapper);
	}
	
	@Override
	public boolean updateKid(Product p) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", p.getId()).
				addValue("kidName", p.getKidName());
		return (jdbcTemplate.update(UPDATE_PRODUCT_KID, namedParameters) > 0);
	}
}
