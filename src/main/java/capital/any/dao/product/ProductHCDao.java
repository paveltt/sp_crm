package capital.any.dao.product;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;
import com.hazelcast.transaction.TransactionContext;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.dao.base.product.ProductsSubscriptionStartDatePredicate;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.product.UpdateProductAskBidEntry;
import capital.any.hazelcast.update.base.product.UpdateProductEntry;
import capital.any.hazelcast.update.base.product.UpdateProductKidEntry;
import capital.any.hazelcast.update.base.product.UpdateProductPriorityEntry;
import capital.any.hazelcast.update.base.product.UpdateProductSuspendEntry;
import capital.any.model.base.Product;
import capital.any.model.base.SqlFilters;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductHCDaoCRM")
public class ProductHCDao extends capital.any.dao.base.product.ProductHCDao implements IProductDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductHCDao.class);

	@Override
	public boolean insert(Product product) throws Exception {
		logger.info("ProductHCDaoCRM; insert; " + product.toString());
		boolean result = HazelCastClientFactory.getClient().
				insertIntoTransactionMap(Constants.MAP_PRODUCTS, product, product.getId());
		return result;		
	}

	@Override
	public boolean updateAskBid(Product product) throws Exception {
		return HazelCastClientFactory.getClient().updateTransactionMap(
				product.getId(), 
				new UpdateProductAskBidEntry(product), 
				Constants.MAP_PRODUCTS);
	}

	@Override
	public boolean updateSuspend(Product product) throws Exception {		
		return HazelCastClientFactory.getClient().updateTransactionMap(
				product.getId(), 
				new UpdateProductSuspendEntry(product.isSuspend()), 
				Constants.MAP_PRODUCTS);		
	}

	@Override
	public boolean updateProduct(Product product) throws Exception {		
		return HazelCastClientFactory.getClient().updateTransactionMap(
				product.getId(), 
				new UpdateProductEntry(product), 
				Constants.MAP_PRODUCTS);	
	}

	@Override
	public boolean updateProductsPriority(List<Product> products) throws Exception {		
		boolean result = false;
		TransactionContext transaction = HazelCastClientFactory.getClient().startTransaction();
		try {
			for (Product product : products) {
				result = HazelCastClientFactory.getClient().updateTransactionMap(
						transaction,
						product.getId(),
						new UpdateProductPriorityEntry(product.getPriority()), 
						Constants.MAP_PRODUCTS);
			}
			result = true;
		} catch (Exception e) {
			HazelCastClientFactory.getClient().rollbackTransaction(transaction);
			result = false;
		}
		HazelCastClientFactory.getClient().commitTransaction(transaction);
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProductsForPriority() {
		ArrayList<Product> productsResult = null;
		try {
			IMap<Long, Product> products = (IMap<Long, Product>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCTS);
			if (products != null && !products.isEmpty()) {
				SqlPredicate openProducts = new SqlPredicate("productStatus.getId() IN (" + ProductStatusEnum.UNPUBLISH.getId() + ", " + ProductStatusEnum.SUBSCRIPTION.getId() + ", " + ProductStatusEnum.SECONDARY.getId() + ")");
				productsResult = new ArrayList<Product>(products.values(openProducts));
			}
		} catch (Exception e) {
			logger.warn("cant get products For Priority from hazelcast", e);
		}
		return productsResult;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProducts(SqlFilters filters) {	
		IMap<Long, Product> products = null;
		Collection<Product> productsList = new ArrayList<Product>();
		try {
			products = (IMap<Long, Product>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCTS);
			if (products != null && !products.isEmpty()) {
				if (filters.getSubscriptionStartDate() != null) {
					productsList = products.values(new ProductsSubscriptionStartDatePredicate(filters.getSubscriptionStartDate()));
				} else {
					productsList = products.values();
				}
			}
		} catch (Exception e) {
			logger.error("ERROR!", e);
		}
		return new ArrayList<Product>(productsList);
	}
	
	@Override
	public boolean updateKid(Product product) throws Exception {
		return HazelCastClientFactory.getClient().updateTransactionMap(
				product.getId(), 
				new UpdateProductKidEntry(product), 
				Constants.MAP_PRODUCTS);
	}
}
