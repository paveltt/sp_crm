package capital.any.dao.productBarrier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.product.UpdateProductBarrierEntry;
import capital.any.hazelcast.update.base.product.UpdateProductBarrierOccurEntry;
import capital.any.model.base.ProductBarrier;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("ProductBarrierHCDaoCRM")
public class ProductBarrierHCDao extends capital.any.dao.base.productBarrier.ProductBarrierDao implements IProductBarrierDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductBarrierHCDao.class);
	
	@Override
	public void insert(ProductBarrier productBarrier) {
		logger.info("hazelcast insert product barrier not implemented");
	}

	@Override
	public boolean updateProductBarrierOccur(ProductBarrier productBarrier) throws Exception {
		return HazelCastClientFactory.getClient().updateTransactionMap(
				productBarrier.getProductId(), 
				new UpdateProductBarrierOccurEntry(productBarrier.isBarrierOccur()), 
				Constants.MAP_PRODUCTS);
	}
	
	@Override
	public boolean updateProductBarrier(ProductBarrier productBarrier) throws Exception {	
		return HazelCastClientFactory.getClient().updateTransactionMap(
				productBarrier.getProductId(), 
				new UpdateProductBarrierEntry(productBarrier), 
				Constants.MAP_PRODUCTS);
	} 
}
