package capital.any.dao.productBarrier;

import capital.any.model.base.ProductBarrier;

/**
 * @author Eyal G
 *
 */
public interface IProductBarrierDao extends capital.any.dao.base.productBarrier.IProductBarrierDao {
	
	public static final String INSERT_PRODUCT_BARRIER = 
			  "INSERT INTO " +
			  "	 product_barriers " +
		      "( " +
		      "  barrier_level , " +
		      "  barrier_start , " +
		      "  product_id , " +
		      "  product_barrier_type_id , " +
		      "  barrier_end " +
		      ") " +
		      "VALUES " +
		      "( " +
		      "  :barrierLevel , " +
		      "  :barrierStart , " +
		      "  :productId , " +
		      "  :productParrierTypeId , " +
		      "  :barrierEnd " +
		      ")";
	
	public static final String UPDATE_PRODUCT_BARRIER_OCCUR = 
			  "UPDATE " +
				 "product_barriers " +
		      "SET " +
		      	"is_barrier_occur = :isBarrierOccur " +
		      "WHERE " + 
		      	"product_id = :productId";
	
	public static final String UPDATE_PRODUCT_BARRIER = 
			"UPDATE "
			+ "product_barriers " +
		    "SET "
		    + "barrier_level           = :barrierLevel , "
		    + "barrier_start           = :barrierStart , "
		    + "product_barrier_type_id = :productBarrierTypeId , "
		    + "barrier_end             = :barrierEnd " +
		    "WHERE "
		    + "product_id          = :productId ";
		
	void insert(ProductBarrier productBarrier);
	
	/**
	 * update product barrier if its occur or not
	 * @param productBarrier
	 * @throws Exception 
	 */
	boolean updateProductBarrierOccur(ProductBarrier productBarrier) throws Exception;
	
	/**
	 * update product barrier
	 * @param productBarrier
	 * @throws Exception 
	 */
	boolean updateProductBarrier(ProductBarrier productBarrier) throws Exception;
}
