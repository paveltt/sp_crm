package capital.any.dao.productBarrier;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.ProductBarrier;

/**
 * @author Eyal G
 *
 */
@Repository("ProductBarrierDaoCRM")
public class ProductBarrierDao extends capital.any.dao.base.productBarrier.ProductBarrierDao implements IProductBarrierDao {
	
	@Override
	public void insert(ProductBarrier productBarrier) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("barrierLevel", productBarrier.getBarrierLevel())
						 .addValue("barrierStart", productBarrier.getBarrierStart())
						 .addValue("productId", productBarrier.getProductId())
						 .addValue("productParrierTypeId", productBarrier.getProductBarrierType().getId())
						 .addValue("barrierEnd", productBarrier.getBarrierEnd());
		
		jdbcTemplate.update(INSERT_PRODUCT_BARRIER, namedParameters);
		
	}

	@Override
	public boolean updateProductBarrierOccur(ProductBarrier productBarrier) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productBarrier.getProductId()).
				addValue("isBarrierOccur", productBarrier.isBarrierOccur() ? 1 : 0);
		return (jdbcTemplate.update(UPDATE_PRODUCT_BARRIER_OCCUR, namedParameters) > 0);		
	}
	
	@Override
	public boolean updateProductBarrier(ProductBarrier productBarrier) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("productId", productBarrier.getProductId()).
				addValue("barrierLevel", productBarrier.getBarrierLevel())
				.addValue("barrierEnd", productBarrier.getBarrierEnd())
				.addValue("productBarrierTypeId", productBarrier.getProductBarrierType().getId())
				.addValue("barrierStart", productBarrier.getBarrierStart());
		return (jdbcTemplate.update(UPDATE_PRODUCT_BARRIER, namedParameters) > 0);		
	}
}
