package capital.any.dao.productCoupon;

import java.util.List;

import capital.any.model.base.ProductCoupon;

/**
 * @author EyalG
 *
 */
public interface IProductCouponDao extends capital.any.dao.base.productCoupon.IProductCouponDao {
	
	public static final String INSERT_PRODUCT_COUPONS = 
			" INSERT" +
		    " INTO PRODUCT_COUPONS" +
			" (" +
		    "   pay_rate ," +
		    "   observation_date ," +
		    "   product_id ," +
		    "   payment_date ," +
		    "   trigger_level" +
		    " )" +
		    " VALUES" +
		    " (" +
		    "   :payRate ," +
		    "   :observationDate ," +
		    "   :productId ," +
		    "   :paymentDate ," +
		    "   :triggerLevel " +
		    " ) ";
	
	public static final String UPDATE_PRODUCT_COUPONS = 
			"UPDATE " +
			"	product_coupons " +
		    "SET " +
		    "	pay_rate         = :payRate , " +
		    "	observation_date = :observationDate , " +
		    "	payment_date     = :paymentDate , " +
		    "	trigger_level    = :triggerLevel " +
		    "WHERE " + 
		    "	id = :id";
	
	public static final String UPDATE_PRODUCT_OBSERVATION_LEVEL = 
			"UPDATE " +
			"	product_coupons " +
		    "SET " +
		    "	observation_level = :observationLevel " +
		    "WHERE " + 
		    "	id = :id";
		
	void insert(ProductCoupon productCoupon);
	
	void insert(List<ProductCoupon> productCoupons);
	
	/**
	 * update by id
	 * @param productCoupon
	 * @return true if update was done else false
	 */
	boolean update(ProductCoupon productCoupon);
	
	/**
	 * update Observation Level
	 * @param productCoupon
	 * @return true if update success else false
	 * @throws Exception 
	 */
	boolean updateObservationLevel(ProductCoupon productCoupon) throws Exception;
}