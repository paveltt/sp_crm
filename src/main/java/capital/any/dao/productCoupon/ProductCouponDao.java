package capital.any.dao.productCoupon;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductCoupon;

/**
 * @author EyalG
 *
 */
@Repository("ProductCouponDaoCRM")
public class ProductCouponDao extends capital.any.dao.base.productCoupon.ProductCouponDao implements IProductCouponDao {
	
	@Override
	public void insert(ProductCoupon productCoupon) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("payRate", productCoupon.getPayRate())
						 .addValue("observationDate", productCoupon.getObservationDate())
						 .addValue("productId", productCoupon.getProductId())
						 .addValue("paymentDate", productCoupon.getPaymentDate())
						 .addValue("triggerLevel", productCoupon.getTriggerLevel());
		
		jdbcTemplate.update(INSERT_PRODUCT_COUPONS, namedParameters);
		
	}
	
	@Override
	public void insert(List<ProductCoupon> productCoupons) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(productCoupons.toArray());
		jdbcTemplate.batchUpdate(INSERT_PRODUCT_COUPONS,
												batch);
	}

	@Override
	public boolean update(ProductCoupon productCoupon) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("payRate", productCoupon.getPayRate())
				.addValue("observationDate", productCoupon.getObservationDate())
				.addValue("paymentDate", productCoupon.getPaymentDate())
				.addValue("triggerLevel", productCoupon.getTriggerLevel())
				.addValue("id", productCoupon.getId());
		return (jdbcTemplate.update(UPDATE_PRODUCT_COUPONS, namedParameters) > 0);
	}

	@Override
	public boolean updateObservationLevel(ProductCoupon productCoupon) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("observationLevel", productCoupon.getObservationLevel())
				.addValue("id", productCoupon.getId());
		return (jdbcTemplate.update(UPDATE_PRODUCT_OBSERVATION_LEVEL, namedParameters) > 0);
	}	
}
