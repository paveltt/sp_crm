package capital.any.dao.productCoupon;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.product.UpdateProductCouponObservationLevelEntry;
import capital.any.model.base.ProductCoupon;

/**
 * @author Eyal Goren
 *
 */
@Repository("ProductCouponHCDaoCRM")
public class ProductCouponHCDao extends capital.any.dao.base.productCoupon.ProductCouponHCDao implements IProductCouponDao {
	private static final Logger logger = LoggerFactory.getLogger(ProductCouponHCDao.class);
	
	@Override
	public void insert(ProductCoupon productCoupon) {
		logger.info("ProductCouponHCDao; insert productCoupon; do nothing");		
	}
	
	@Override
	public void insert(List<ProductCoupon> productCoupons) {
		logger.info("ProductCouponHCDao; insert productCoupons; do nothing");
	}

	@Override
	public boolean update(ProductCoupon productCoupon) {
		logger.info("ProductCouponHCDao; update; return false; do nothing");
		return false;
	}

	@Override
	public boolean updateObservationLevel(ProductCoupon productCoupon) throws Exception {		
		return HazelCastClientFactory.getClient().updateTransactionMap(
				productCoupon.getProductId(), 
				new UpdateProductCouponObservationLevelEntry(productCoupon), 
				Constants.MAP_PRODUCTS);
	}
}
