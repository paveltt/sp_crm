package capital.any.dao.writer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import capital.any.model.base.Writer;

/**
 * @author eran.levy
 *
 */
@Repository("WriterDaoCRM")
public class WriterDao extends capital.any.dao.base.writer.WriterDao implements IWriterDao {
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public boolean changePassword(Writer writer) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("id", writer.getId()).
				addValue("password", writer.getPassword());
				return (namedParameterJdbcTemplate.update(CHANGE_PASSWORD, namedParameters) > 0);	
	}
	
}
