package capital.any.dao.writer;

import capital.any.model.base.Writer;

/**
 * @author eran.levy
 *
 */
public interface IWriterDao extends capital.any.dao.base.writer.IWriterDao {
	
	public static final String CHANGE_PASSWORD = 
												" UPDATE " +
												"	writers " +
												" SET " +
												"	password = :password " +
												" WHERE " +
												"	id = :id ";
		
	boolean changePassword(Writer writer);

}
