//package capital.any.dao.productScenario;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.jdbc.core.namedparam.SqlParameterSource;
//import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
//import org.springframework.stereotype.Repository;
//
//import capital.any.model.base.ProductScenario;
//
///**
// * @author EyalG
// *
// */
//@Repository("ProductScenarioDaoCRM")
//public class ProductScenarioDao extends capital.any.dao.base.productScenario.ProductScenarioDao 
//									implements IProductScenarioDao {
//	
//	@Autowired
//	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
//	
//	@Override
//	public void insert(ProductScenario productScenario) {
//		SqlParameterSource namedParameters = 
//				new MapSqlParameterSource("position", productScenario.getPosition())
//						 .addValue("text", productScenario.getText())
//						 .addValue("finalFixingLevel", productScenario.getFinalFixingLevel())
//						 .addValue("productId", productScenario.getProductId());
//		
//		namedParameterJdbcTemplate.update(INSERT_PRODUCT_SCENARIOS,
//				namedParameters);
//	}
//	
//	@Override
//	public void insert(List<ProductScenario> productScenarios) {
//		SqlParameterSource[] batch = 
//				SqlParameterSourceUtils.createBatch(productScenarios.toArray());
//		namedParameterJdbcTemplate.batchUpdate(INSERT_PRODUCT_SCENARIOS,
//												batch);
//	}
//	
//}
