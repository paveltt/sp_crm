package capital.any.dao.productCallable;

import java.util.List;

import capital.any.model.base.ProductCallable;

/**
 * @author EyalG
 *
 */
public interface IProductCallableDao extends capital.any.dao.base.productCallable.IProductCallableDao {
	
	public static final String INSERT_PRODUCT_CALLABLES = 
			" INSERT " +
		    " INTO PRODUCT_CALLABLE" +
			" (" +
		    "   examination_date ," +
		    "   product_id" +
		    " )" +
		    " VALUES" +
		    " (" +
		    "   :examinationDate ," +
		    "   :productId" +
		    " ) ";
	
	public static final String UPDATE_PRODUCT_CALLABLES =
			"UPDATE " +
			"	product_callable " +
		    "SET " +
		    "	examination_date = :examinationDate " +
		    "WHERE " + 
		    "	id = :id ";
		
	void insert(ProductCallable productCallable);
	
	void insert(List<ProductCallable> productCallables);
	
	/**
	 * update by id
	 * @param productCallable
	 * @return true if update was done else false
	 */
	boolean update(ProductCallable productCallable);
}
