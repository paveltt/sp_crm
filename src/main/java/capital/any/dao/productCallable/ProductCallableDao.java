package capital.any.dao.productCallable;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductCallable;

/**
 * @author EyalG
 *
 */
@Repository("ProductCallableDaoCRM")
public class ProductCallableDao extends capital.any.dao.base.productCallable.ProductCallableDao implements IProductCallableDao {
	
	@Override
	public void insert(ProductCallable productCallable) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
						 ("examinationDate", productCallable.getExaminationDate())
				.addValue("productId", productCallable.getProductId());
		
		jdbcTemplate.update(INSERT_PRODUCT_CALLABLES, namedParameters);
		
	}
	
	@Override
	public void insert(List<ProductCallable> productCallables) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(productCallables.toArray());
		jdbcTemplate.batchUpdate(INSERT_PRODUCT_CALLABLES,
												batch);
	}

	@Override
	public boolean update(ProductCallable productCallable) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("examinationDate", productCallable.getExaminationDate())
				.addValue("id", productCallable.getId());
		return (jdbcTemplate.update(UPDATE_PRODUCT_CALLABLES, namedParameters) > 0);
	}
}
