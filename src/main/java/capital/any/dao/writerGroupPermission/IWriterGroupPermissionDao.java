package capital.any.dao.writerGroupPermission;

import java.util.Map;

import capital.any.model.BeSpace;
import capital.any.model.WriterGroupPermission;


/**
 * @author eranl
 *
 */
public interface IWriterGroupPermissionDao {
	
	public static final String INSERT_WRITER_GROUP_PERMISSIONS = 
			"  INSERT " +
		    "  INTO WRITER_GROUP_PERMISSIONS " +
			"  ( " +
		    "     group_id, " +
		    "     be_screen_id, " +
		    "     be_permission_id " +		    
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    "     :groupId, " +
		    "     :beScreenId, " +
		    "     :bePermissionId " +
		    "  ) ";
	
	public static final String GET_WRITER_GROUP_PERMISSIONS = 
			" SELECT " +
			"	bp.id be_permission_id, " +
			"  	bp.name be_permission_name, " +
			"   bsc.id be_screen_id, " +
			"  	bsc.name be_screen_name, " +
			"  	bs.id be_space_id, " +
			"  	bs.name be_space_name " +
			" FROM " +
			"	writers w, " +
			"   writer_group_permissions wgp, " +
			"   be_permissions bp, " +
			"   be_screens bsc, " +
			"   be_spaces bs " +
			" WHERE " +
			"   wgp.group_id = w.group_id " +
			"   AND wgp.be_screen_id = bsc.id " +
			"   AND wgp.be_permission_id = bp.id " +
			"   AND bs.id = bsc.space_id " +
			"	AND wgp.is_active = 1 " +
			"   AND w.id = :writerId " +
			" ORDER BY be_space_id, be_screen_id, be_permission_id ";			
	
	void insert(WriterGroupPermission writerGroupPermission);
			
	Map<String, BeSpace> getWriterPermissions(long writerId);
	
}
