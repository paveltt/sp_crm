package capital.any.dao.writerGroupPermission;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.BePermission;
import capital.any.model.BeSpace;
import capital.any.model.WriterGroupPermission;

/**
 * @author eranl
 *
 */
@Repository
public class WriterGroupPermissionDao implements IWriterGroupPermissionDao, ApplicationContextAware {
	
	private ApplicationContext appContext;

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public void insert(WriterGroupPermission writerGroupPermission) {
		for (BePermission bp: writerGroupPermission.getBePermissionList()) {
			SqlParameterSource namedParameters = 
					new MapSqlParameterSource("groupId", writerGroupPermission.getWriterGroup().getId())
									.addValue("beScreenId", writerGroupPermission.getBeScreen().getId())
									.addValue("bePermissionId", bp.getId());		
		namedParameterJdbcTemplate.update(INSERT_WRITER_GROUP_PERMISSIONS, namedParameters);
		}
	}
		
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext = applicationContext;		
	}

	@Override
	public Map<String, BeSpace> getWriterPermissions(long writerId) {
		WriterGroupPermissionCallbackHandler callbackHandler = appContext.getBean(WriterGroupPermissionCallbackHandler.class);
		SqlParameterSource namedParameters = new MapSqlParameterSource("writerId", writerId);	
		namedParameterJdbcTemplate.query(GET_WRITER_GROUP_PERMISSIONS, namedParameters, callbackHandler);
		return callbackHandler.getResult();
	}

}
