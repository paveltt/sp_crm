package capital.any.dao.writerGroupPermission;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import capital.any.model.BePermission;
import capital.any.model.BeScreen;
import capital.any.model.BeSpace;

/**
 * @author eranl
 *
 */
@Component
@Scope("prototype")
public class WriterGroupPermissionCallbackHandler implements RowCallbackHandler {
	
	private Map<String, BeSpace> result = new HashMap<String, BeSpace>();
	
	@Override
	public void processRow(ResultSet rs) throws SQLException {	
		String spaceName = rs.getString("be_space_name");
		BeSpace space =  result.get(spaceName);								
		if (space == null) {
			space = new BeSpace();
			space.setId(rs.getInt("be_space_id"));
			space.setBeScreeMap(new HashMap<String, BeScreen>());
		}		
		int screenId = rs.getInt("be_screen_id");   
		String screenName = rs.getString("be_screen_name");
		BeScreen screen = space.getBeScreeMap().get(screenName);
		if (screen == null) {
			screen = new BeScreen();
			screen.setId(screenId);
			screen.setBePermissionList(new ArrayList<BePermission>());
		}
		BePermission permission = new BePermission();
		permission.setId(rs.getInt("be_permission_id"));
		permission.setName(rs.getString("be_permission_name"));
		screen.getBePermissionList().add(permission);
		space.getBeScreeMap().put(screenName, screen);
		result.put(spaceName, space);
	}

	/**
	 * @return the result
	 */
	public Map<String, BeSpace> getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(Map<String, BeSpace> result) {
		this.result = result;
	}

}
