package capital.any.dao.messageResourceType;

import java.util.Map;

import capital.any.model.MessageResourceType;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IMessageResourceTypeDao {
	
	public static final String GET = 
			" SELECT " +
			" 	* " +
			" FROM " +
			" 	msg_res_type ";
	
	Map<Integer, MessageResourceType> get();
	
}
