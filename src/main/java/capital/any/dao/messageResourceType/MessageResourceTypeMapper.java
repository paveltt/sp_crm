package capital.any.dao.messageResourceType;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.MessageResourceType;

/**
 * 
 * @author Eyal.o
 *
 */
@Component
public class MessageResourceTypeMapper implements RowMapper<MessageResourceType> {

	@Override
	public MessageResourceType mapRow(ResultSet rs, int rowNum) throws SQLException {
		MessageResourceType messageResourceType = new MessageResourceType();
		messageResourceType.setId(rs.getInt("id"));
		messageResourceType.setName(rs.getString("name"));
		messageResourceType.setDisplayName(rs.getString("display_name"));
		return messageResourceType;
	}
	
	public MessageResourceType mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs);
	}

}
