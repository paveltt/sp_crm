package capital.any.dao.messageResourceType;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import capital.any.model.MessageResourceType;

/**
 * 
 * @author Eyal.o
 *
 */
@Repository
public class MessageResourceTypeDao implements IMessageResourceTypeDao {

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private MessageResourceTypeMapper messageResourceTypeMapper;
	
	@Override
	public Map<Integer, MessageResourceType> get() {
		List<MessageResourceType> tmp = jdbcTemplate.query(GET, messageResourceTypeMapper);
		Map<Integer, MessageResourceType> hm = tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));
		return hm;
	}

}
