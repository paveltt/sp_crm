package capital.any.dao.backendPermission;

import capital.any.model.BePermission;

/**
 * @author eranl
 *
 */
public interface IBackendPermissionDao {
	
	public static final String INSERT_BACKEND_PERMISSION = 
			"  INSERT " +
		    "  INTO BE_PERMISSIONS " +
			"  ( " +
		    "     name " + 
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    "     :name " +
		    "  ) ";
		
	void insert(BePermission bePermission);
	
}
