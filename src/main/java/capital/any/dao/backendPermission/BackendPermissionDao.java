package capital.any.dao.backendPermission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.BePermission;

/**
 * @author eranl
 *
 */
@Repository
public class BackendPermissionDao implements IBackendPermissionDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public void insert(BePermission bePermission) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("name", bePermission.getName());
		
		namedParameterJdbcTemplate.update(INSERT_BACKEND_PERMISSION, namedParameters);
	}

	
}
