package capital.any.dao.backendPermission;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.BePermission;

/**
 * @author eranl
 *
 */
@Component
public class BackendPermissionMapper implements RowMapper<BePermission> {
			
	@Override
	public BePermission mapRow(ResultSet rs, int row) throws SQLException {
		BePermission bePermission = new BePermission();
		bePermission.setId((rs.getInt("be_permission_id")));
		bePermission.setName((rs.getString("be_permission_name")));		
		return bePermission;	
	}
	
	public BePermission mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
	
}
