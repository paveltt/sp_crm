package capital.any.dao.dbParameter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.dbParameter.UpdateDBParameterEntry;
import capital.any.model.base.DBParameter;

/**
 * @author eyal goren
 *
 */
@Repository("DBParameterHCDaoCRM")
public class DBParameterHCDao extends capital.any.dao.base.dbParameter.DBParameterHCDao implements IDBParameterDao {
	private static final Logger logger = LoggerFactory.getLogger(DBParameterHCDao.class);
	
	@Override
	public boolean insertDBParameters(DBParameter dbParameter) throws Exception {
		logger.info("DBParameterHCDaoCRM; insert; " + dbParameter.toString());
		boolean result = HazelCastClientFactory.getClient().
				insertIntoTransactionMap(Constants.MAP_DB_PARAMETER, dbParameter, dbParameter.getId());
		return result;
	}
	
	@Override
	public boolean updateDBParameters(DBParameter dbParameter) throws Exception {
		return HazelCastClientFactory.getClient().updateTransactionMap(
				dbParameter.getId(), 
				new UpdateDBParameterEntry(dbParameter), 
				Constants.MAP_DB_PARAMETER);
	}
}
