package capital.any.dao.dbParameter;

import capital.any.model.base.DBParameter;

/**
 * @author eyal g
 *
 */
public interface IDBParameterDao extends capital.any.dao.base.dbParameter.IDBParameterDao {
	
	public static final String INSERT_DB_PARAMETERS = 
			" INSERT INTO db_parameters ( " +
			"		name, num_value, string_value, " +
			"		date_value, comments) " +
			" VALUES " +
			"		(:name, :numValue, :stringValue, " + 
			"		 :dateValue , :comments) ";
	
	public static final String UPDATE_DB_PARAMETERS_BY_ID =
			" UPDATE " +
			"	db_parameters " +
			" SET " +												
			"	name = :name, " +
			"	num_value = :numValue, "+
			"	string_value = :stringValue, " +
			"	date_value = :dateValue, " +
			"	comments = :comments " +
			" WHERE " +
			" 	id = :id ";
			
	boolean insertDBParameters(DBParameter dbParameter) throws Exception;
	
	boolean updateDBParameters(DBParameter dbParameter) throws Exception;
}
