package capital.any.dao.dbParameter;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import capital.any.model.base.DBParameter;

/**
 * @author Eyalg
 *
 */
@Repository("DBParameterDaoCRM")
public class DBParameterDao extends capital.any.dao.base.dbParameter.DBParameterDao implements IDBParameterDao {
		
	@Override
	public boolean insertDBParameters(DBParameter dbParameter) {
		KeyHolder keyHolder = new GeneratedKeyHolder();		
		SqlParameterSource namedParameters = 
					new MapSqlParameterSource
							 ("name", dbParameter.getName())
					.addValue("numValue", dbParameter.getNumValue())					
					.addValue("stringValue", dbParameter.getStringValue())
					.addValue("dateValue", dbParameter.getDateValue())
					.addValue("comments", dbParameter.getComments());
										
		boolean result = namedParameterJdbcTemplate.update(INSERT_DB_PARAMETERS, namedParameters, keyHolder, new String[] {"id"}) > 0;
		dbParameter.setId(keyHolder.getKey().intValue());
		return result;	
		
	}

	@Override
	public boolean updateDBParameters(DBParameter dbParameter) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
						 ("name", dbParameter.getName())
				.addValue("numValue", dbParameter.getNumValue())					
				.addValue("stringValue", dbParameter.getStringValue())
				.addValue("dateValue", dbParameter.getDateValue())
				.addValue("comments", dbParameter.getComments())
				.addValue("id", dbParameter.getId());
									
		return namedParameterJdbcTemplate.update(UPDATE_DB_PARAMETERS_BY_ID, namedParameters) > 0;		
	}

}
