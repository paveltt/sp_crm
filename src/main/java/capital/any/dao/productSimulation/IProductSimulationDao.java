package capital.any.dao.productSimulation;

import java.util.List;

import capital.any.model.base.ProductSimulation;

/**
 * @author EyalG
 *
 */
public interface IProductSimulationDao extends capital.any.dao.base.productSimulation.IProductSimulationDao {
	
	public static final String INSERT_PRODUCT_SIMULATION = 
			"  INSERT " +
		    "  INTO PRODUCT_SIMULATION " +
			"  ( " +
		    "     position , " +
		    "     final_fixing_level , " +
		    "     product_id " +
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    "     :position , " +
		    "     :finalFixingLevel , " +
		    "     :productId " +
		    "  ) ";
	
	public static final String UPDATE_PRODUCT_SIMULATION = 
			"UPDATE " +
			"	product_simulation " +
		    "SET " +
		    "	position             = :position , " +
		    "	final_fixing_level 	 = :finalFixingLevel " +
		    "WHERE " +
		    "	id = :id";
		
	void insert(ProductSimulation productSimulation);
	
	void insert(List<ProductSimulation> productSimulations);
	
	/**
	 * update product Simulation
	 * @param productSimulation
	 * @return true if update was done else false
	 */
	boolean update(ProductSimulation productSimulation);

}
