package capital.any.dao.productSimulation;


import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductSimulation;

/**
 * @author EyalG
 *
 */
@Repository("ProductSimulationDaoCRM")
public class ProductSimulationDao extends capital.any.dao.base.productSimulation.ProductSimulationDao implements IProductSimulationDao {
	
	@Override
	public void insert(ProductSimulation productSimulation) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("position", productSimulation.getPosition())
						 .addValue("finalFixingLevel", productSimulation.getFinalFixingLevel())
						 .addValue("productId", productSimulation.getProductId());
		
		jdbcTemplate.update(INSERT_PRODUCT_SIMULATION,
				namedParameters);
	}

	@Override
	public void insert(List<ProductSimulation> productSimulations) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(productSimulations.toArray());
		jdbcTemplate.batchUpdate(INSERT_PRODUCT_SIMULATION,
												batch);
	}

	@Override
	public boolean update(ProductSimulation productSimulation) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("position", productSimulation.getPosition())
				.addValue("finalFixingLevel", productSimulation.getFinalFixingLevel())
				.addValue("id", productSimulation.getId());
		return (jdbcTemplate.update(UPDATE_PRODUCT_SIMULATION, namedParameters) > 0);
	}	
}
