package capital.any.dao.messageResourceLanguage;

import java.util.List;

import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author Eyal Goren
 *
 */
public interface IMessageResourceLanguageDao {
	
	public static final String UPDATE = 
			"UPDATE " +
			"	msg_res_language " +
		    "SET " +
		    "	large_value         = :largeValue , " +
		    "  	writer_id           = :writerId , " +
		    "  	time_updated        = now() , " +
		    "  	value               = :value " +
		    "WHERE " +
		    "	msg_res_id        = :msgResId " +
		    "	and msg_res_language_id = :msgResLanguageId";
	
	public static final String GET_BY_KEY = 
			"SELECT " +
			"	mrl.* " +
			"FROM " +
			"	msg_res_language mrl, " +
			"	msg_res mr " +
			"WHERE " +
			"	mrl.msg_res_id = mr.id " +
			" 	AND upper(mr.key) LIKE upper(:key) " +
			" 	AND mr.action_source_id = :actionSourceId ";
	
	public static final String GET_MISSING_LANGUAGE_BY_KEY =
			"SELECT " + 
				"l.id language_id, " +
				"mr.id msg_res_id, " +
				"mr.type_id msg_res_type_id " +
			"FROM " +
				"msg_res mr, " +
				"languages l " +
				"LEFT OUTER JOIN (SELECT " +
									"mre.id, " +
			                        "mrl.msg_res_language_id " +
								"FROM " +
									"msg_res mre, " +
									"msg_res_language mrl " +
								"WHERE " +
									"mre.key = :key " +
			                        "and mre.action_source_id = :actionSourceId " +
									"and mrl.msg_res_id = mre.id " +
								") msg " +
								"ON l.id = msg.msg_res_language_id " +
			"WHERE " +
				"msg.id IS null " +
				"AND l.is_active = 1 " +
			    "AND mr.key = :key " +
			    "AND mr.action_source_id = :actionSourceId ";
	
	/**
	 * update msg Res Language
	 * @param msgResLanguages
	 * @return true if we update else false
	 * @throws Exception 
	 */
	boolean update(List<MsgResLanguage> msgResLanguages) throws Exception;
	
	/**
	 * get all msg res languages for specific key and Action source id
	 * @param MsgRes
	 * @return List<MsgResLanguage> 
	 */
	List<MsgResLanguage> getByKey(MsgRes msgRes);

	/**
	 * get missing language by for specific key
	 * @param msgRes
	 * @return list of all missing languages
	 */
	List<MsgResLanguage> getKeyMissingLanguage(MsgRes msgRes);
	
}
