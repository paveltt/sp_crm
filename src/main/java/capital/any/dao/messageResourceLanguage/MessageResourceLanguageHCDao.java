package capital.any.dao.messageResourceLanguage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.hazelcast.update.base.message.resource.UpdateMessageResourceEntry;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("MessageResourceLanguageHCDao")
public class MessageResourceLanguageHCDao implements IMessageResourceLanguageDao {
	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	protected MessageResourceLanguageMapper messageResourceLanguageMapper;
	
	@Override
	public boolean update(List<MsgResLanguage> msgResLanguages) throws Exception {
		return HazelCastClientFactory.getClient().updateTransactionMap(
				msgResLanguages.get(0).getMsgRes().getActionSourceId(),
				new UpdateMessageResourceEntry(msgResLanguages), 
				Constants.MAP_MESSAGE_RESOURCE);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MsgResLanguage> getByKey(MsgRes msgRes) {
		Map<Integer, Map<Integer, Map<String, String>>> result = null;
		List<MsgResLanguage> messageResourceLanguages = null; 
		try {
			result = (Map<Integer, Map<Integer, Map<String, String>>>) 
					HazelCastClientFactory.getClient().getMap(Constants.MAP_MESSAGE_RESOURCE);
			messageResourceLanguages = new ArrayList<MsgResLanguage>();
			if (result != null) {
				for (Entry<Integer, Map<String, String>> element : result.get(msgRes.getActionSourceId()).entrySet()) {
					//TODO relevant?
					//messageResourceLanguages.add(element.getValue().get(msgRes.getKey()));
				}
			}
		} catch (Exception e) {
			//logger.warn("cant get MsgRes from hazelcast", e);
		}
		return messageResourceLanguages;
	}

	@Override
	//no need we will take from db
	public List<MsgResLanguage> getKeyMissingLanguage(MsgRes msgRes) {
		// TODO Auto-generated method stub
		return null;
	}
}
