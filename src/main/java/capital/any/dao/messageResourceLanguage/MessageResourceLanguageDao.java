package capital.any.dao.messageResourceLanguage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author Eyal Goren
 *
 */
@Repository
public class MessageResourceLanguageDao implements IMessageResourceLanguageDao {

	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	protected MessageResourceLanguageMapper messageResourceLanguageMapper;
	
	@Override
	public boolean update(List<MsgResLanguage> msgResLanguages) {
		SqlParameterSource[] namedParameters = 
				SqlParameterSourceUtils.createBatch(msgResLanguages.toArray());
		jdbcTemplate.batchUpdate(UPDATE, namedParameters);
		//in batch update we cant know if there was update or not
		return true;
	}

	@Override
	public List<MsgResLanguage> getByKey(MsgRes msgRes) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("key", msgRes.getKey())
				.addValue("actionSourceId", msgRes.getActionSourceId());
		return jdbcTemplate.query(GET_BY_KEY, namedParameters, messageResourceLanguageMapper);
	}
	
	@Override
	public List<MsgResLanguage> getKeyMissingLanguage(MsgRes msgRes) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("key", msgRes.getKey())
				.addValue("actionSourceId", msgRes.getActionSourceId());
		return jdbcTemplate.query(GET_MISSING_LANGUAGE_BY_KEY, namedParameters, new RowMapper<MsgResLanguage>() {

			@Override
			public MsgResLanguage mapRow(ResultSet rs, int rowNum) throws SQLException {
				MsgResLanguage msgResLanguage = new MsgResLanguage();
				msgResLanguage.setMsgResLanguageId(rs.getInt("language_id"));
				msgResLanguage.setMsgRes(msgRes);
				msgResLanguage.getMsgRes().setId(rs.getInt("msg_res_id"));
				msgResLanguage.getMsgRes().setTypeId(rs.getInt("msg_res_type_id"));
				return msgResLanguage;
			}
		});
	}
}
