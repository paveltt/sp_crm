package capital.any.dao.messageResourceLanguage;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.dao.base.MapperBase;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author Eyal Goren
 *
 */
@Component
public class MessageResourceLanguageMapper extends MapperBase implements RowMapper<MsgResLanguage> {
			
	@Override
	public MsgResLanguage mapRow(ResultSet rs, int row) throws SQLException {
		MsgResLanguage msgResLanguage = new MsgResLanguage();
		msgResLanguage.setLargeValue(rs.getString("large_value"));
		MsgRes msgRes = new MsgRes();
		msgRes.setId(rs.getInt("msg_res_id"));
		msgResLanguage.setMsgRes(msgRes);	
		msgResLanguage.setMsgResLanguageId(rs.getInt("msg_res_language_id"));
		msgResLanguage.setTimeUpdated(convertTimestampToDate(rs.getTimestamp("time_updated")));
		msgResLanguage.setValue(rs.getString("value"));
		msgResLanguage.setWriterId(rs.getInt("writer_id"));
		return msgResLanguage;	
	}
	
	public MsgResLanguage mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
	
}
