package capital.any.dao.messageResourceLanguageHistory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import capital.any.model.base.MsgResLanguage;

/**
 * 
 * @author eyal.ohana
 *
 */
@Repository
public class MessageResourceLanguageHistoryDao implements IMessageResourceLanguageHistoryDao {

	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	
	@Override
	public boolean insertSelect(List<MsgResLanguage> msgResLanguages) throws Exception {
		SqlParameterSource[] namedParameters = 
				SqlParameterSourceUtils.createBatch(msgResLanguages.toArray());
		jdbcTemplate.batchUpdate(INSERT_SELECT, namedParameters);
		return true;
	}

}
