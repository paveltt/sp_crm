package capital.any.dao.messageResourceLanguageHistory;

import java.util.List;

import capital.any.model.base.MsgResLanguage;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMessageResourceLanguageHistoryDao {
	
	public static final String INSERT_SELECT = 
			" INSERT INTO msg_res_language_history (MSG_RES_ID, LANGUAGE_ID, VALUE, WRITER_ID, LARGE_VALUE) " +
			" SELECT " + 
				" mrl.MSG_RES_ID, " +
				" mrl.MSG_RES_LANGUAGE_ID, " +
				" mrl.VALUE, " +
				" mrl.WRITER_ID, " +
				" mrl.LARGE_VALUE " +
			" FROM " + 
				" msg_res_language mrl" +
			" WHERE " +
				" mrl.msg_res_id = :msgResId " +
				" and mrl.msg_res_language_id = :msgResLanguageId";
	
	boolean insertSelect(List<MsgResLanguage> msgResLanguages) throws Exception;
}
