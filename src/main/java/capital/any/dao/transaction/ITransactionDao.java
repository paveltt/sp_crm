package capital.any.dao.transaction;

import java.util.List;

import capital.any.model.base.Transaction;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionDao extends capital.any.dao.base.transaction.ITransactionDao {
	
	public static final String GET_TRANSACTIONS_BY_STATUS_ID =
			"SELECT " +
			"	t.* " + 
			"	,top.id to_id " +
			"	,top.name to_name " +
			"	,top.display_name to_display_name " +
			"	,tpt.id tpt_id " +
			"	,tpt.name tpt_name " +
			"	,tpt.transaction_class_id tpt_transaction_class_id " +
			"	,tpt.display_name tpt_display_name " +
			"	,tpt.object_class " +
			"	,tpt.handler_class " +
			"	,ts.id ts_id " +
			"	,ts.name ts_name " +
			"	,ts.display_name ts_display_name " +
			"FROM " +
			"	transactions t " +
			"	,transaction_payment_types tpt " +
			"	,transaction_operations top " +
			"	,transaction_statuses ts " +
			"WHERE " +
			"	t.transaction_operation_id = top.id " +
			"	AND t.transaction_payment_type_id = tpt.id " +
			"	AND t.transaction_status_id = ts.id " +
			"   AND 1 = IF(ISNULL(:statusId), 1, IF(ts.id = :statusId, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:from), 1, IF(t.time_created >= :from, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:to), 1, IF(t.time_created <= :to, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:settledAtFrom), 1, IF(t.time_settled >= :settledAtFrom, 1, 0)) " +
			"	AND 1 = IF(ISNULL(:settledAtTo), 1, IF(t.time_settled <= :settledAtTo, 1, 0)) ";
	
	
			
	
	public static final String GET_TRANSACTIONS_BY_REFERENCE_ID = 
			"SELECT " +
				"t.* " + 
			"FROM " +
				"transaction_references tr, " +
				"transactions t " +
			"WHERE " +
				"t.id = tr.transaction_id " +
				"AND tr.reference_id = :referenceId " +
				"AND tr.table_id = :tableId ";

	/**
	 * get transaction by reference id
	 * @param referenceId
	 * @return List of transactions
	 */
	List<Transaction> getTransactionsByReferenceId(long referenceId, int tableId);
	
}
