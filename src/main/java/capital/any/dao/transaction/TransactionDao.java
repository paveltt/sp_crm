package capital.any.dao.transaction;

import java.util.List;

import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Transaction;

/**
 * @author LioR SoLoMoN
 *
 */
@Repository("TransactionDaoCRM")
public class TransactionDao extends capital.any.dao.base.transaction.TransactionDao implements ITransactionDao, ApplicationContextAware {	

	@Override
	public List<Transaction> getTransactionsByReferenceId(long referenceId, int tableId) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("referenceId", referenceId);
		params.addValue("tableId", tableId);
		return jdbcTemplate.query(GET_TRANSACTIONS_BY_REFERENCE_ID, params, transactionMapper);
	}
}
