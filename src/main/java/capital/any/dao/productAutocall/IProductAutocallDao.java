package capital.any.dao.productAutocall;

import java.util.List;

import capital.any.model.base.ProductAutocall;



/**
 * @author EyalG
 *
 */
public interface IProductAutocallDao extends capital.any.dao.base.productAutocall.IProductAutocallDao {
	
	public static final String INSERT_PRODUCT_AUTOCALLS = 
			" INSERT " +
		    " INTO PRODUCT_AUTOCALLS " +
		    " ( " +
		    "   examination_date , " +
		    "	examination_value, " +
		    "   product_id " +
		    " ) " +
		    " VALUES " +
		    " ( " +
		    "   :examinationDate , " +
		    "   :examinationValue , " +
		    "   :productId " +
		    " ) ";
	
	public static final String UPDATE_PRODUCT_AUTOCALLS = 
			"UPDATE " +
			"	product_autocalls " +
		    "SET " +
		    "	examination_value        = :examinationValue " +
		    " , examination_date = :examinationDate " +
		    "WHERE " +
		    "	id = :id ";
		   
		
	void insert(ProductAutocall productAutocall);
	
	void insert(List<ProductAutocall> productAutocalls);
	
	/**
	 * update by id
	 * @param productAutocall
	 * @return true if we update else false
	 */
	boolean update(ProductAutocall productAutocall);

}
