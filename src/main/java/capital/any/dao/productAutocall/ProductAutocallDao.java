package capital.any.dao.productAutocall;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import capital.any.model.base.ProductAutocall;

/**
 * @author EyalG
 *
 */
@Repository("ProductAutocallDaoCRM")
public class ProductAutocallDao extends capital.any.dao.base.productAutocall.ProductAutocallDao implements IProductAutocallDao {
	
	@Override
	public void insert(ProductAutocall productAutocall) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource
						 ("examinationDate", productAutocall.getExaminationDate())
				.addValue("examinationValue", productAutocall.getExaminationValue())					
				.addValue("productId", productAutocall.getProductId());
		
		jdbcTemplate.update(INSERT_PRODUCT_AUTOCALLS, namedParameters);
	}

	@Override
	public void insert(List<ProductAutocall> productAutocalls) {
		SqlParameterSource[] batch = 
				SqlParameterSourceUtils.createBatch(productAutocalls.toArray());
		jdbcTemplate.batchUpdate(INSERT_PRODUCT_AUTOCALLS,
												batch);
	}

	@Override
	public boolean update(ProductAutocall productAutocall) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("examinationValue", productAutocall.getExaminationValue())
				.addValue("examinationDate", productAutocall.getExaminationDate())
				.addValue("id", productAutocall.getId());
		return (jdbcTemplate.update(UPDATE_PRODUCT_AUTOCALLS, namedParameters) > 0);		
	}	
}
