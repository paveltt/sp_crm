//package capital.any.dao.productInfo;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.jdbc.core.namedparam.SqlParameterSource;
//import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
//import org.springframework.stereotype.Repository;
//
//import capital.any.dao.productInfo.IProductInfoDao;
//import capital.any.model.base.ProductInfo;
//
///**
// * @author EyalG
// *
// */
//@Repository("ProductInfoDaoCRM")
//public class ProductInfoDao extends capital.any.dao.base.productInfo.ProductInfoDao implements IProductInfoDao {
//	
//	@Autowired
//	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
//	
//	@Override
//	public void insert(ProductInfo productInfo) {
//		SqlParameterSource namedParameters = 
//				new MapSqlParameterSource("description", productInfo.getDescription())
//						 .addValue("title", productInfo.getTitle())
//						 .addValue("productId", productInfo.getProductId())
//						 .addValue("languageId", productInfo.getLanguageId());
//		
//		namedParameterJdbcTemplate.update(INSERT_PRODUCT_INFO,
//				namedParameters);		
//	}
//	
//	@Override
//	public void insert(List<ProductInfo> productInfos) {
//		SqlParameterSource[] batch = 
//				SqlParameterSourceUtils.createBatch(productInfos.toArray());
//		namedParameterJdbcTemplate.batchUpdate(INSERT_PRODUCT_INFO,
//												batch);
//	}	
//}
