//package capital.any.dao.productInfo;
//
//import java.util.List;
//
//import capital.any.model.base.ProductInfo;
//
///**
// * @author EyalG
// *
// */
//public interface IProductInfoDao extends capital.any.dao.base.productInfo.IProductInfoDao {
//	
//	public static final String INSERT_PRODUCT_INFO = 
//											" INSERT " +
//										    " INTO PRODUCT_INFO " +
//										    " (" +
//										    "    description ," +
//										    "    title ," +
//										    "    product_id ," +
//										    "    language_id" +
//										    " )" +
//										    " values" +
//										    " (" +
//										    "    :description ," +
//										    "    :title ," +
//										    "    :productId ," +
//										    "    :languageId" +
//										    " ) ";
//		
//	void insert(ProductInfo productInfo);
//	
//	void insert(List<ProductInfo> productInfos);
//
//}
