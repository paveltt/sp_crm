package capital.any.dao.backendSpace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.BeSpace;

/**
 * @author eranl
 *
 */
@Repository
public class BackendSpaceDao implements IBackendSpaceDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public void insert(BeSpace beSpace) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("name", beSpace.getName());
		
		namedParameterJdbcTemplate.update(INSERT_BACKEND_SPACE, namedParameters);
	}

	
}
