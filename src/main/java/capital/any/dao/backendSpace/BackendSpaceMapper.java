package capital.any.dao.backendSpace;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.BeSpace;

/**
 * @author eranl
 *
 */
@Component
public class BackendSpaceMapper implements RowMapper<BeSpace> {
			
	@Override
	public BeSpace mapRow(ResultSet rs, int row) throws SQLException {
		BeSpace beSpace = new BeSpace();
		beSpace.setId((rs.getInt("be_space_id")));
		beSpace.setName((rs.getString("be_space_name")));
		return beSpace;		
	}
	
	public BeSpace mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
	
}
