package capital.any.dao.backendSpace;

import capital.any.model.BeSpace;


/**
 * @author eranl
 *
 */
public interface IBackendSpaceDao {
	
	public static final String INSERT_BACKEND_SPACE = 
			"  INSERT " +
		    "  INTO BE_SPACES " +
			"  ( " +
		    "     name " + 
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    "     :name " +
		    "  ) ";
		
	void insert(BeSpace beSpace);
	
}
