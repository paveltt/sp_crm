package capital.any.dao.investment;

import java.util.List;
import java.util.Optional;

import capital.any.model.base.Investment;

/**
 * @author EyalG
 *
 */
public interface IInvestmentDao extends capital.any.dao.base.investment.IInvestmentDao {
	
	public static final String GET_INVESTMENT_BY_ID_AND_STATUSES =
			"SELECT " +
				" * " +
			"FROM " +
				"investments i " +
			"WHERE " +
				"i.id in (:id) " +
				"AND i.investment_status_id in (:statusIds) ";
	
	
	
	/**
	 * get investment by id and status
	 * @param id
	 * @param statusIds
	 * @return Optional<Investment>
	 */
	Optional<Investment> getInvestmentByIdAndStatuses(long id, List<Integer> statusIds);
}
