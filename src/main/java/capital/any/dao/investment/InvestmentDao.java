package capital.any.dao.investment;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.base.Investment;

/**
 * @author EyalG
 *
 */
@Repository("InvestmentDaoCRM")
public class InvestmentDao extends capital.any.dao.base.investment.InvestmentDao implements IInvestmentDao {
	private static final Logger logger = LoggerFactory.getLogger(InvestmentDao.class);
	
	@Override
	public Optional<Investment> getInvestmentByIdAndStatuses(long id, List<Integer> statusIds) {
		SqlParameterSource parameters = new MapSqlParameterSource
				 ("id", id)
				 .addValue("statusIds", statusIds);
		Optional<Investment> investment = Optional.empty();
		try {
			investment = Optional.of(jdbcTemplate.queryForObject(GET_INVESTMENT_BY_ID_AND_STATUSES, parameters, investmentsMapper));
		} catch (DataAccessException e) {
			logger.warn("Can't find investment! return empty", e);
		}
		return investment;
	}

	
}
