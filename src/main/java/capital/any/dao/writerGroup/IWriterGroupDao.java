package capital.any.dao.writerGroup;

import java.util.Map;

import capital.any.model.WriterGroup;

/**
 * @author eranl
 *
 */
public interface IWriterGroupDao {
	
	public static final String INSERT = 
			"  INSERT " +
		    "  INTO writer_groups " +
			"  ( " +
		    "     name " + 
		    "  ) " +
		    "  VALUES " +
		    "  ( " +
		    "     :name " +
		    "  ) ";
	
	public static final String GET = 
			" SELECT " +
			"	* " +
			" FROM " +
			"	writer_groups ";
	
		
	/**
	 * @param writerGroup
	 */
	void insert(WriterGroup writerGroup);
	
	/**
	 * @return
	 */
	Map<Integer, WriterGroup> get();
	
}
