package capital.any.dao.writerGroup;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import capital.any.model.WriterGroup;

/**
 * @author eranl
 *
 */
@Repository
public class WriterGroupDao implements IWriterGroupDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	@Autowired
	private WriterGroupMapper writerGroupMapper;
	
	@Override
	public void insert(WriterGroup writerGroup) {
		SqlParameterSource namedParameters = 
				new MapSqlParameterSource("name", writerGroup.getName());		
		namedParameterJdbcTemplate.update(INSERT, namedParameters);
	}

	@Override
	public Map<Integer, WriterGroup> get() {
		List<WriterGroup> tmp = namedParameterJdbcTemplate.query(GET, writerGroupMapper);
		return tmp.stream().collect(Collectors.toMap(p-> p.getId(), p -> p));		 		 
	}

}
