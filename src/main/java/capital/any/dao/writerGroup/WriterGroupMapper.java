package capital.any.dao.writerGroup;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import capital.any.model.WriterGroup;

/**
 * @author eranl
 *
 */
@Component
public class WriterGroupMapper implements RowMapper<WriterGroup> {
			
	@Override
	public WriterGroup mapRow(ResultSet rs, int row) throws SQLException {
		WriterGroup writerGroup = new WriterGroup();
		writerGroup.setId((rs.getInt("id")));
		writerGroup.setName((rs.getString("name")));
		return writerGroup;		
	}
	
	public WriterGroup mapRow(ResultSet rs) throws SQLException {
		return this.mapRow(rs, 0);
	}
	
}
