package capital.any.model;

import java.io.Serializable;

import javax.validation.constraints.Size;

import capital.any.base.validation.OneStringFieldEqualToAnother;

@OneStringFieldEqualToAnother.List({
	@OneStringFieldEqualToAnother(first = "password", second ="retypePassword",
			message = "error-not-match", groups = WriterChangePassword.WriterChangePasswordValidatorGroup.class),
})

/**
 * @author eran.levy
 *
 */
public class WriterChangePassword implements Serializable {

	private static final long serialVersionUID = -938543845070146341L;
	
	public interface WriterChangePasswordValidatorGroup{};

	@Size(groups = WriterChangePasswordValidatorGroup.class , message ="", min = 6 , max=15)
	private String password;
	private String retypePassword;
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the retypePassword
	 */
	public String getRetypePassword() {
		return retypePassword;
	}
	/**
	 * @param retypePassword the retypePassword to set
	 */
	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WriterChangePassword [password=" + password + ", retypePassword=" + retypePassword + "]";
	}
	
}
