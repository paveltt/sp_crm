package capital.any.model;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author eranl
 *
 */
public class BeSpace implements Serializable {

	private static final long serialVersionUID = -2091915101681691488L;
	private int id;
	private String name;
	private Map<String, BeScreen> beScreeMap;
	
	/**
	 * @return the id
	 */	
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	@JsonIgnore
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the beScreeMap
	 */
	public Map<String, BeScreen> getBeScreeMap() {
		return beScreeMap;
	}
	/**
	 * @param beScreeMap the beScreeMap to set
	 */
	public void setBeScreeMap(Map<String, BeScreen> beScreeMap) {
		this.beScreeMap = beScreeMap;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeSpace [id=" + id + ", name=" + name + ", beScreeMap=" + beScreeMap + "]";
	}
	
}
