package capital.any.model;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class WriterGroup implements Serializable {

	private static final long serialVersionUID = -5140182846794735238L;
	private int id;
	private String name;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WriterGroup [id=" + id + ", name=" + name + "]";
	}
	
}
