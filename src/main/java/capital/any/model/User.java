package capital.any.model;

import java.io.Serializable;

/**
 * @author Eran.l
 *
 */
public class User extends capital.any.model.base.User implements Serializable {

	private static final long serialVersionUID = 3515125156129335035L;

	private String mobilePhoneWithPrefix;
	private String countryByPrefixStr;
	private String countryByUserStr;
	private String countryByIpStr;	
	private String languageStr;
	private String currencyStr;
	
	/**
	 * @return the mobilePhoneWithPrefix
	 */
	public String getMobilePhoneWithPrefix() {
		return mobilePhoneWithPrefix;
	}

	/**
	 * @param mobilePhoneWithPrefix the mobilePhoneWithPrefix to set
	 */
	public void setMobilePhoneWithPrefix(String mobilePhoneWithPrefix) {
		this.mobilePhoneWithPrefix = mobilePhoneWithPrefix;
	}

	/**
	 * @return the countryByUserStr
	 */
	public String getCountryByUserStr() {
		return countryByUserStr;
	}

	/**
	 * @param countryByUserStr the countryByUserStr to set
	 */
	public void setCountryByUserStr(String countryByUserStr) {
		this.countryByUserStr = countryByUserStr;
	}

	/**
	 * @return the countryByIpStr
	 */
	public String getCountryByIpStr() {
		return countryByIpStr;
	}

	/**
	 * @param countryByIpStr the countryByIpStr to set
	 */
	public void setCountryByIpStr(String countryByIpStr) {
		this.countryByIpStr = countryByIpStr;
	}

	/**
	 * @return the languageStr
	 */
	public String getLanguageStr() {
		return languageStr;
	}

	/**
	 * @param languageStr the languageStr to set
	 */
	public void setLanguageStr(String languageStr) {
		this.languageStr = languageStr;
	}

	/**
	 * @return the currencyStr
	 */
	public String getCurrencyStr() {
		return currencyStr;
	}

	/**
	 * @param currencyStr the currencyStr to set
	 */
	public void setCurrencyStr(String currencyStr) {
		this.currencyStr = currencyStr;
	}

	/**
	 * @return the countryByPrefixStr
	 */
	public String getCountryByPrefixStr() {
		return countryByPrefixStr;
	}

	/**
	 * @param countryByPrefixStr the countryByPrefixStr to set
	 */
	public void setCountryByPrefixStr(String countryByPrefixStr) {
		this.countryByPrefixStr = countryByPrefixStr;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [mobilePhoneWithPrefix=" + mobilePhoneWithPrefix + ", countryByPrefixStr=" + countryByPrefixStr
				+ ", countryByUserStr=" + countryByUserStr + ", countryByIpStr=" + countryByIpStr + ", languageStr="
				+ languageStr + ", currencyStr=" + currencyStr + "]";
	}
	
}
