package capital.any.model;

import java.io.Serializable;

/**
 * 
 * @author Eyal.o
 *
 */
public class MessageResourceType implements Serializable {
	private static final long serialVersionUID = 503578983123243900L;
	
	private int id;
	private String name;
	private String displayName;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MessageResourceType [id=" + id + ", name=" + name + ", displayName=" + displayName + "]";
	}

}
