package capital.any.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author eranl
 *
 */
public class BeScreen implements Serializable {

	private static final long serialVersionUID = -332538911026529705L;
	private int id;	
	private String name;
	private int spaceId;
	private List<BePermission> BePermissionList; 
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	@JsonIgnore
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the spaceId
	 */
	@JsonIgnore
	public int getSpaceId() {
		return spaceId;
	}

	/**
	 * @param spaceId the spaceId to set
	 */
	public void setSpaceId(int spaceId) {
		this.spaceId = spaceId;
	}

	/**
	 * @return the bePermissionList
	 */
	public List<BePermission> getBePermissionList() {
		return BePermissionList;
	}

	/**
	 * @param bePermissionList the bePermissionList to set
	 */
	public void setBePermissionList(List<BePermission> bePermissionList) {
		BePermissionList = bePermissionList;
	}

	

}
