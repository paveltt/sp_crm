package capital.any.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author eranl
 *
 */
public class WriterGroupPermission implements Serializable  {

	private static final long serialVersionUID = 3007780097113981984L;
	
	private WriterGroup writerGroup;
	private BeScreen beScreen;
	private List<BePermission> bePermissionList;
	/**
	 * @return the writerGroup
	 */
	public WriterGroup getWriterGroup() {
		return writerGroup;
	}
	/**
	 * @param writerGroup the writerGroup to set
	 */
	public void setWriterGroup(WriterGroup writerGroup) {
		this.writerGroup = writerGroup;
	}
	/**
	 * @return the beScreen
	 */
	public BeScreen getBeScreen() {
		return beScreen;
	}
	/**
	 * @param beScreen the beScreen to set
	 */
	public void setBeScreen(BeScreen beScreen) {
		this.beScreen = beScreen;
	}
	/**
	 * @return the bePermissionList
	 */
	public List<BePermission> getBePermissionList() {
		return bePermissionList;
	}
	/**
	 * @param bePermissionList the bePermissionList to set
	 */
	public void setBePermissionList(List<BePermission> bePermissionList) {
		this.bePermissionList = bePermissionList;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WriterGroupPermission [writerGroup=" + writerGroup + ", beScreen=" + beScreen + ", bePermissionList=" +
				bePermissionList + "]";
	}

}
