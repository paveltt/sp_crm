package capital.any.model;

import java.io.Serializable;
import java.util.Date;

import capital.any.model.base.Country;

public class ClientSpace implements Serializable {

	private static final long serialVersionUID = -334456425661003271L;
	
	private long userId;
	private String email;
	private String name; //user first and last name
	private Country country;
	private boolean isComplianceApproved;
	private boolean isDepositor; //Does the client have a real deposit in status pending/success in SP
	private String campaignName; //display campaign content name and campaign source name by first ever SP login attribution
	private long balance; // - Invested amount+Disposable amount
	private long investedAmount; // - Total sum in EUR of amounts invested in products where the investments are not settled or cancelled
	private long disposableAmount;// - Current client balance
	private int openInvestments;// - Count of client investments that are not settled or cancelled
	private int numberOfProducts;
	private Date timeCreated;
	private String regulationQuestionnaire;
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}
	/**
	 * @return the isComplianceApproved
	 */
	public boolean isComplianceApproved() {
		return isComplianceApproved;
	}
	/**
	 * @param isComplianceApproved the isComplianceApproved to set
	 */
	public void setComplianceApproved(boolean isComplianceApproved) {
		this.isComplianceApproved = isComplianceApproved;
	}
	/**
	 * @return the isDepositor
	 */
	public boolean isDepositor() {
		return isDepositor;
	}
	/**
	 * @param isDepositor the isDepositor to set
	 */
	public void setDepositor(boolean isDepositor) {
		this.isDepositor = isDepositor;
	}
	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}
	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	/**
	 * @return the balance
	 */
	public long getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(long balance) {
		this.balance = balance;
	}
	/**
	 * @return the investedAmount
	 */
	public long getInvestedAmount() {
		return investedAmount;
	}
	/**
	 * @param investedAmount the investedAmount to set
	 */
	public void setInvestedAmount(long investedAmount) {
		this.investedAmount = investedAmount;
	}
	/**
	 * @return the disposableAmount
	 */
	public long getDisposableAmount() {
		return disposableAmount;
	}
	/**
	 * @param disposableAmount the disposableAmount to set
	 */
	public void setDisposableAmount(long disposableAmount) {
		this.disposableAmount = disposableAmount;
	}
	/**
	 * @return the openInvestments
	 */
	public int getOpenInvestments() {
		return openInvestments;
	}
	/**
	 * @param openInvestments the openInvestments to set
	 */
	public void setOpenInvestments(int openInvestments) {
		this.openInvestments = openInvestments;
	}
	/**
	 * @return the numberOfProducts
	 */
	public int getNumberOfProducts() {
		return numberOfProducts;
	}
	/**
	 * @param numberOfProducts the numberOfProducts to set
	 */
	public void setNumberOfProducts(int numberOfProducts) {
		this.numberOfProducts = numberOfProducts;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the regulationQuestionnaire
	 */
	public String getRegulationQuestionnaire() {
		return regulationQuestionnaire;
	}
	/**
	 * @param regulationQuestionnaire the regulationQuestionnaire to set
	 */
	public void setRegulationQuestionnaire(String regulationQuestionnaire) {
		this.regulationQuestionnaire = regulationQuestionnaire;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ClientSpace [userId=" + userId + ", email=" + email + ", name=" + name + ", country=" + country
				+ ", isComplianceApproved=" + isComplianceApproved + ", isDepositor=" + isDepositor + ", campaignName="
				+ campaignName + ", balance=" + balance + ", investedAmount=" + investedAmount + ", disposableAmount="
				+ disposableAmount + ", openInvestments=" + openInvestments + ", numberOfProducts=" + numberOfProducts
				+ ", timeCreated=" + timeCreated + ", regulationQuestionnaire=" + regulationQuestionnaire + "]";
	}
	
}
