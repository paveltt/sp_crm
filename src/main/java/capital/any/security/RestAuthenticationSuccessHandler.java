package capital.any.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Writer;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestAuthenticationSuccessHandler.class);
	private  final static int SESSION_TIMEOUT = 60 * 30;
	@Autowired
	private SecurityService securityService;

	public RestAuthenticationSuccessHandler() {
		super();
		setRedirectStrategy(new NoRedirectStrategy());
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			Authentication authentication) throws IOException, ServletException {

		Writer writer = null;
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		List<String> messages = new ArrayList<String>();
		String message = "authenticate_failed";
		
		if (authentication.getPrincipal() instanceof CustomWriterDetails) {
			writer = ((CustomWriterDetails) authentication.getPrincipal()).getWriter();
			responseCode = ResponseCode.OK;
			message = "authenticate_success";
			httpRequest.getSession().setMaxInactiveInterval(SESSION_TIMEOUT);
		}
		
		messages.add(message);
		Response<Writer> response = new Response<Writer>(writer, responseCode, messages);
		logger.info(response.toString());
		securityService.sendResponse(response, httpResponse, HttpServletResponse.SC_OK);
	}

	protected class NoRedirectStrategy implements RedirectStrategy {
		@Override
		public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url)
				throws IOException {
			logger.debug("sendRedirect");
			// no redirect
		}
	}
}
