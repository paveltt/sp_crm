package capital.any.security;

import java.util.ArrayList;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import capital.any.model.base.Writer;
import capital.any.service.base.writer.IWriterService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service
public class CustomUserDetailService implements UserDetailsService {
	private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailService.class);
	@Autowired
	private IWriterService writerService;

	@Override
	public UserDetails loadUserByUsername(String emailOrUserName) throws UsernameNotFoundException {
		Writer writer = null;
		try {
			writer = writerService.getWriterByEmailOrUserName(emailOrUserName);
		} catch (Exception e) {
			logger.error("A problem occured while trying to get writer!", e);
		}
		if (writer == null) {
			throw new UsernameNotFoundException("writer not found!");
		}
		
		return buildUserDetails(writer);
	}
	
	/**
	 * @param writer
	 * @return
	 */
	public UserDetails buildUserDetails(Writer writer) {
		//Adding roles		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(String.valueOf(writer.getGroupId())));
		CustomWriterDetails customwriter = new CustomWriterDetails(authorities, writer);				
		return customwriter;
	}
	
}
