package capital.any.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.communication.Error;

/**
 * @author LioR SoLoMoN
 *
 */
@Component
public class RestAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestAuthenticationFailureHandler.class);
	@Autowired
	private SecurityService securityService; 
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			AuthenticationException exception) throws IOException, ServletException {
		
    	List<String> messages = new ArrayList<String>();
    	messages.add("authentication_failed");   	
    	Response<?> response = 
    			new Response<Object>(ResponseCode.AUTHENTICATION_FAILURE, 
    			messages, 
    			new Error("error-authentication", null));
    	
    	logger.info(response.toString());
		securityService.sendResponse(response, httpResponse, HttpServletResponse.SC_OK);
	}
}
