package capital.any.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.communication.Error;
import capital.any.communication.Message;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Returns a 401 error code (Unauthorized)
 */
@Component
public class RestUnauthorizedEntryPoint implements AuthenticationEntryPoint {
	private static final Logger logger = LoggerFactory.getLogger(RestUnauthorizedEntryPoint.class);
	@Autowired
	private SecurityService securityService; 

	
	public RestUnauthorizedEntryPoint() {
		super();
	}

	/**
	 * Always returns a 401 error code
	 */
	@Override
	public void commence(HttpServletRequest httpRequest, HttpServletResponse httpResponse, AuthenticationException exception)
			throws IOException, ServletException {
		logger.debug("commence");
		List<Message> errorMessages = new ArrayList<Message>();
    	List<String> messages = new ArrayList<String>();
    	errorMessages.add(new Message(exception.getMessage()));
    	messages.add("Authentication failed");
    	Response<?> response = 
    			new Response<Object>(ResponseCode.ACCESS_DENIED, 
    			messages, 
    			new Error("authenticationError", errorMessages));
    	
    	logger.info(response.toString());
		securityService.sendResponse(response, httpResponse, HttpServletResponse.SC_OK);
	}
}
