package capital.any.security;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import capital.any.model.base.Writer;

/**
 * @author LioR SoLoMoN
 *
 */
public class CustomWriterDetails extends org.springframework.security.core.userdetails.User {
	private static final long serialVersionUID = 2591527292076048232L;
	private Writer writer;
		
	public CustomWriterDetails(Writer writer) {
		super(writer.getEmail(), "", true, false, false, false, null);
		this.writer = writer;
	}
	
	public CustomWriterDetails(Collection<? extends GrantedAuthority> authorities, Writer writer) {
		super(writer.getEmail(), writer.getPassword(),  authorities);
		this.writer = writer;
	}
		
	public CustomWriterDetails(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities, Writer writer) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.writer = writer;
	}

	public Writer getWriter() {
		return writer;
	}

	public void setWriter(Writer writer) {
		this.writer = writer;
	}
}
