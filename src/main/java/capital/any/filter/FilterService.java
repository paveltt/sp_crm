package capital.any.filter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.base.enums.EmailTo;
import capital.any.base.filter.FilterMethod;
import capital.any.service.base.emailAction.IEmailActionService;
import capital.any.service.base.fileStatus.IFileStatusService;
import capital.any.service.messageResourceType.IMessageResourceTypeService;
import capital.any.service.writerGroup.IWriterGroupService;

/**
 * @author eran.levy
 *
 */
@Service("FilterServiceCRM")
public class FilterService extends capital.any.base.filter.FilterService {
	
	public static final String MAP_WRITER_GROUPS = "writer_groups";
	public static final String MAP_EMAIL_ACTIONS = "email_actions";
	public static final String MAP_EMAIL_TO = "email_to";
	public static final String MAP_FILE_STATUS = "map_file_status";
	public static final String MAP_MESSAGE_RESOURCE_TYPES = "message_resource_types";
	//if we add new filter we need to write it here https://anyoption.atlassian.net/wiki/display/DEV/anycapital+filters	

	@Autowired
	private IWriterGroupService writerGroupService;
	@Autowired
	private IEmailActionService emailActionService;
	@Autowired
	private IFileStatusService fileStatusService;
	@Autowired
	private IMessageResourceTypeService messageResourceTypeService;
	
	public FilterService() {
		super();
		methodMap.put(MAP_WRITER_GROUPS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return writerGroupService.get();
			}
		});
		
		methodMap.put(MAP_EMAIL_ACTIONS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return emailActionService.get();
			}
		});
		
		methodMap.put(MAP_EMAIL_TO, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return EmailTo.ID_EMAIL_TO_MAP;
			}
		});
		
		methodMap.put(MAP_FILE_STATUS, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return fileStatusService.get();
			}
		});
		methodMap.put(MAP_MESSAGE_RESOURCE_TYPES, new FilterMethod() {
			@Override
			public Map<Integer, ?> method() {
				return messageResourceTypeService.get();
			}
		});
	}

}
