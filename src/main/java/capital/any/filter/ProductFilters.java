package capital.any.filter;

import java.io.Serializable;
import java.util.Map;

import capital.any.model.base.Currency;
import capital.any.model.base.DayCountConvention;
import capital.any.model.base.Market;
import capital.any.model.base.ProductBarrierType;
import capital.any.model.base.ProductCategory;
import capital.any.model.base.ProductCouponFrequency;
import capital.any.model.base.ProductIssuerInsuranceType;
import capital.any.model.base.ProductMaturity;
import capital.any.model.base.ProductStatus;
import capital.any.model.base.ProductType;

public class ProductFilters implements Serializable {
	
	private static final long serialVersionUID = -7819061099570280757L;

	private Map<Integer, ProductType> productTypes;
	
	private Map<Integer, ProductCategory> productCategories;
	
	private Map<Integer, DayCountConvention> dayCountConventions;
	
	private Map<Integer, Market> markets;
	
	private Map<Integer, ProductIssuerInsuranceType> productIssuerInsuranceTypes;
	
	private Map<Integer, ProductCouponFrequency> productCouponFrequency;
	
	private Map<Integer, Currency> currencies;
	
	private Map<Integer, ProductMaturity> productMaturities;
	
	private Map<Integer, ProductBarrierType> productBarrierTypes;
	
	private Map<Integer, ProductStatus> productStatus;

	public Map<Integer, ProductType> getProductTypes() {
		return productTypes;
	}

	public void setProductTypes(Map<Integer, ProductType> productTypes) {
		this.productTypes = productTypes;
	}

	public Map<Integer, ProductCategory> getProductCategories() {
		return productCategories;
	}

	public void setProductCategories(Map<Integer, ProductCategory> productCategories) {
		this.productCategories = productCategories;
	}

	public Map<Integer, DayCountConvention> getDayCountConventions() {
		return dayCountConventions;
	}

	public void setDayCountConventions(Map<Integer, DayCountConvention> dayCountConventions) {
		this.dayCountConventions = dayCountConventions;
	}

	public Map<Integer, ProductIssuerInsuranceType> getProductIssuerInsuranceTypes() {
		return productIssuerInsuranceTypes;
	}

	public void setProductIssuerInsuranceTypes(Map<Integer, ProductIssuerInsuranceType> productIssuerInsuranceTypes) {
		this.productIssuerInsuranceTypes = productIssuerInsuranceTypes;
	}

	/**
	 * @return the productCouponFrequency
	 */
	public Map<Integer, ProductCouponFrequency> getProductCouponFrequency() {
		return productCouponFrequency;
	}

	/**
	 * @param productCouponFrequency the productCouponFrequency to set
	 */
	public void setProductCouponFrequency(Map<Integer, ProductCouponFrequency> productCouponFrequency) {
		this.productCouponFrequency = productCouponFrequency;
	}

	/**
	 * @return the markets
	 */
	public Map<Integer, Market> getMarkets() {
		return markets;
	}

	/**
	 * @param markets the markets to set
	 */
	public void setMarkets(Map<Integer, Market> markets) {
		this.markets = markets;
	}

	/**
	 * @return the currencies
	 */
	public Map<Integer, Currency> getCurrencies() {
		return currencies;
	}

	/**
	 * @param currencies the currencies to set
	 */
	public void setCurrencies(Map<Integer, Currency> currencies) {
		this.currencies = currencies;
	}

	/**
	 * @return the productMaturities
	 */
	public Map<Integer, ProductMaturity> getProductMaturities() {
		return productMaturities;
	}

	/**
	 * @param productMaturities the productMaturities to set
	 */
	public void setProductMaturities(Map<Integer, ProductMaturity> productMaturities) {
		this.productMaturities = productMaturities;
	}

	/**
	 * @return the productBarrierTypes
	 */
	public Map<Integer, ProductBarrierType> getProductBarrierTypes() {
		return productBarrierTypes;
	}

	/**
	 * @param productBarrierTypes the productBarrierTypes to set
	 */
	public void setProductBarrierTypes(Map<Integer, ProductBarrierType> productBarrierTypes) {
		this.productBarrierTypes = productBarrierTypes;
	}

	/**
	 * @return the productStatus
	 */
	public Map<Integer, ProductStatus> getProductStatus() {
		return productStatus;
	}

	/**
	 * @param productStatus the productStatus to set
	 */
	public void setProductStatus(Map<Integer, ProductStatus> productStatus) {
		this.productStatus = productStatus;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductFilters [productTypes=" + productTypes + ", productCategories=" + productCategories
				+ ", dayCountConventions=" + dayCountConventions + ", markets=" + markets
				+ ", productIssuerInsuranceTypes=" + productIssuerInsuranceTypes + ", productCouponFrequency="
				+ productCouponFrequency + ", currencies=" + currencies + ", productMaturities=" + productMaturities
				+ ", productBarrierTypes=" + productBarrierTypes + ", productStatus=" + productStatus + "]";
	}
}
