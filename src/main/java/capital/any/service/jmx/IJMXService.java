package capital.any.service.jmx;

/**
 * @author Eran.l
 *
 */
public interface IJMXService {	
		
	/**
	 * @throws Exception
	 */
	void reloadHC() throws Exception;
}
