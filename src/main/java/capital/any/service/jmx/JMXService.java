package capital.any.service.jmx;

import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Eran.l
 *
 */
@Service
public class JMXService implements IJMXService {

	private static final Logger logger = LoggerFactory.getLogger(JMXService.class);
	
	@Value("${jmx.rmi.host}")
	private String host;
	@Value("${jmx.rmi.port}")
	private int port;
	
	@Override
	public void reloadHC() throws Exception {
		String url = "service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/jmxrmi";
		JMXServiceURL serviceUrl = new JMXServiceURL(url);
		JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceUrl, null);
		try {
			MBeanServerConnection mbeanConn = jmxConnector.getMBeanServerConnection();
			// query to get the beans
			Set<ObjectName> beanSet = mbeanConn.queryNames(null, null);
			logger.info("beanSet" + beanSet);
			mbeanConn.invoke(new ObjectName("capital.any.hazelcast:name=hazelcastService,type=HazelcastService"), "reloadMaps", null, null);											 
		} finally {
			jmxConnector.close();
		}
	}
}
