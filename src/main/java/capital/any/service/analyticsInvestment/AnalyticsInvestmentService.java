package capital.any.service.analyticsInvestment;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.base.analyticsInvestment.IAnalyticsInvestmentDao;
import capital.any.model.base.analytics.AnalyticsInvestment;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service("AnalyticsInvestmentServiceCRM")
public class AnalyticsInvestmentService extends capital.any.service.base.analyticsInvestment.AnalyticsInvestmentService implements IAnalyticsInvestmentService {
	
	@Autowired
	@Qualifier("AnalyticsInvestmentHCDao")
	private IAnalyticsInvestmentDao analyticsInvestmentHCDao;
	
	@Override
	public AnalyticsInvestment getAnalyticsInvestment() {
		AnalyticsInvestment analyticsInvestment = null;
		Map<Integer, AnalyticsInvestment> map = analyticsInvestmentHCDao.getAnalyticsInvestment();
		if (null == map || map.size() <= 0) {
			analyticsInvestment = get();
	        if (analyticsInvestment == null) {
	              analyticsInvestment = new AnalyticsInvestment();
	        }
	        analyticsInvestment.setListAnalyticsInvestmentProduct(getByProduct());
	        
		} else {
			analyticsInvestment = map.get(0);
		}
		return analyticsInvestment;
	}	

}
