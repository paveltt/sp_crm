package capital.any.service.analyticsInvestment;

import capital.any.model.base.analytics.AnalyticsInvestment;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IAnalyticsInvestmentService {

	AnalyticsInvestment getAnalyticsInvestment();

}
