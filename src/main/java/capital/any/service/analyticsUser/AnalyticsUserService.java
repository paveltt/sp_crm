package capital.any.service.analyticsUser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.analyticsUser.IAnalyticsUserDao;
import capital.any.model.base.analytics.AnalyticsUser;
import capital.any.model.base.analytics.AnalyticsUserCampaign;
import capital.any.model.base.analytics.AnalyticsUserTime;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class AnalyticsUserService implements IAnalyticsUserService {

	@Autowired
	private IAnalyticsUserDao analyticsUserDao;

	@Override
	public AnalyticsUser getAnalyticsUser() {
		AnalyticsUser analyticsUser = get();
		if (analyticsUser == null) {
			analyticsUser = new AnalyticsUser();
		}
		analyticsUser.setListAnalyticsUserCampaign(getByCampaign());
		analyticsUser.setListAnalyticsUserTime(getByTime());
		return analyticsUser;
	}
	
	public AnalyticsUser get() {
		return analyticsUserDao.get();
	}

	public List<AnalyticsUserCampaign> getByCampaign() {
		return analyticsUserDao.getByCampaign();
	}

	public List<AnalyticsUserTime> getByTime() {
		return analyticsUserDao.getByTime();
	}
	
}
