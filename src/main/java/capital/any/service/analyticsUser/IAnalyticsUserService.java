package capital.any.service.analyticsUser;

import capital.any.model.base.analytics.AnalyticsUser;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IAnalyticsUserService {

	AnalyticsUser getAnalyticsUser();
	
}
