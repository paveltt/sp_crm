package capital.any.service.messageResourceLanguageHistory;

import java.util.List;

import capital.any.model.base.MsgResLanguage;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IMessageResourceLanguageHistoryService {
	
	boolean insertSelect(List<MsgResLanguage> msgResLanguages) throws Exception;
	
}
