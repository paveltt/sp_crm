package capital.any.service.messageResourceLanguage;

import java.util.List;

import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;

/**
 * @author Eyal Goren
 *
 */
public interface IMessageResourceLanguageService {
	
	/**
	 * update msg Res Language
	 * @param msgResLanguages
	 * @return
	 * @throws Exception 
	 */
	boolean update(List<MsgResLanguage> msgResLanguages) throws Exception;
	
	/**
	 * get all msg res languages for specific key and Action source id
	 * @param msgRes
	 * @return List<MsgResLanguage> 
	 */
	List<MsgResLanguage> getByKey(MsgRes msgRes);
	
	/**
	 * get missing language by for specific key
	 * @param msgRes
	 * @return list of all missing languages
	 */
	List<MsgResLanguage> getKeyMissingLanguage(MsgRes msgRes);

	/**
	 * insert new language to existing key
	 * @param msgResLanguages
	 * @throws Exception
	 */
	void insertMessageResourcesLanguages(List<MsgResLanguage> msgResLanguages) throws Exception;
}
