package capital.any.service.messageResourceLanguage;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.dao.base.messageResource.IMessageResourceDao;
import capital.any.dao.messageResourceLanguage.IMessageResourceLanguageDao;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResLanguage;
import capital.any.service.messageResourceLanguageHistory.IMessageResourceLanguageHistoryService;

/**
 * @author Eyal Goren
 *
 */
@Service
public class MessageResourceLanguageService implements IMessageResourceLanguageService {
	private static final Logger logger = LoggerFactory.getLogger(MessageResourceLanguageService.class);
	@Autowired
	private IMessageResourceLanguageDao messageResourceLanguageDao;
	
	@Autowired
	@Qualifier("MessageResourceLanguageHCDao")
	private IMessageResourceLanguageDao messageResourceLanguageHCDao;
	
	@Autowired
	private IMessageResourceDao messageResourceDao;
	@Autowired
	@Qualifier("MessageResourceHCDao")
	private IMessageResourceDao messageResourceHCDao;
	
	@Autowired
	private IMessageResourceLanguageHistoryService messageResourceLanguageHistoryService;
	
	@Override
	public boolean update(List<MsgResLanguage> msgResLanguages) throws Exception {
		logger.debug("start update " + msgResLanguages);
		messageResourceLanguageHistoryService.insertSelect(msgResLanguages);
		boolean result = messageResourceLanguageDao.update(msgResLanguages);
		if (result) {
			result = messageResourceLanguageHCDao.update(msgResLanguages);
		}
		return result;
	}

	@Override
	public List<MsgResLanguage> getByKey(MsgRes msgRes) {
		/*List<MsgResLanguage>  messageResourcesLanguages = messageResourceLanguageHCDao.getByKey(msgRes);;
		if (messageResourcesLanguages == null || messageResourcesLanguages.size() <= 0) {
			logger.info("messageResources empty / null in hazelcast, Let's check RDBMS");
			messageResourcesLanguages = messageResourceLanguageDao.getByKey(msgRes);
		}		
		return messageResourcesLanguages;*/
		return messageResourceLanguageDao.getByKey(msgRes);
	}

	@Override
	public List<MsgResLanguage> getKeyMissingLanguage(MsgRes msgRes) {
		return messageResourceLanguageDao.getKeyMissingLanguage(msgRes);
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void insertMessageResourcesLanguages(List<MsgResLanguage> msgResLanguages) throws Exception {
		messageResourceDao.insertMsgResLanguage(msgResLanguages);
		messageResourceHCDao.insertMsgResLanguage(msgResLanguages);
	}
	
}
