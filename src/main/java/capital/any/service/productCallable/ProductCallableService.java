package capital.any.service.productCallable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.productCallable.IProductCallableDao;
import capital.any.model.base.ProductCallable;

/**
 * @author EyalG
 *
 */
@Service("ProductCallableServiceCRM")
public class ProductCallableService extends capital.any.service.base.productCallable.ProductCallableService implements IProductCallableService {
	
	@Autowired @Qualifier("ProductCallableDaoCRM")
	private IProductCallableDao productCallableDao;

	@Override
	public void insert(ProductCallable productCallable) {
		productCallableDao.insert(productCallable);
		
	}
	
	@Override
	public void insert(List<ProductCallable> productCallable) {
		productCallableDao.insert(productCallable);		
	}

	@Override
	public boolean update(ProductCallable productCallable) {
		return productCallableDao.update(productCallable);
	}
}
