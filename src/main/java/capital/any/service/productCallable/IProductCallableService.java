package capital.any.service.productCallable;

import java.util.List;

import capital.any.model.base.ProductCallable;

/**
 * @author EyalG
 *
 */
public interface IProductCallableService extends capital.any.service.base.productCallable.IProductCallableService {	
	
	/**
	 * insert Product Callable
	 *
	 */
	void insert(ProductCallable ProductCallable);
	
	/**
	 * insert product callables
	 * @param ProductCallable
	 */
	void insert(List<ProductCallable> ProductCallable);
	
	/**
	 * update by id
	 * @param productCallable
	 * @return true if update was done else false
	 */
	boolean update(ProductCallable productCallable);
}
