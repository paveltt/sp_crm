package capital.any.service.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import capital.any.communication.Response;
import capital.any.model.ClientSpace;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.User;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IUserService extends capital.any.service.base.user.IUserService {	
		
	/**
	 * Insert user
	 * @param httpServletRequest
	 * @param user
	 * @return 
	 * @throws Exception 
	 */
	Response<User> insertUser(HttpServletRequest httpServletRequest, User user) throws Exception;
	
	/**
	 * get client space screen users information
	 * @param {@link SqlFilters)
	 * @return list of {@link ClientSpace}
	 */
	List<ClientSpace> getClientSpace(SqlFilters sqlFilters);
	
	/**
	 * Get user by email
	 * @param email
	 * @return
	 */
	capital.any.model.User getUserByEmail(String email);
	
	/**
	 * Get user by id
	 * @param id
	 * @return
	 */
	capital.any.model.User getUserById(long id);
	
	/**
	 * advanced search user
	 * @param {@link SqlFilters)
	 * @return list of {@link User}
	 */
	List<capital.any.model.User> advancedSearchUser(SqlFilters sqlFilters);
}
