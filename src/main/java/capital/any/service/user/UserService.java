package capital.any.service.user;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.communication.Error;
import capital.any.communication.Message;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.dao.user.IUserDao;
import capital.any.model.ClientSpace;
import capital.any.model.base.SqlFilters;
import capital.any.model.base.User;
import capital.any.service.IUtilService;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.language.ILanguageService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("UserServiceCRM")
public class UserService extends capital.any.service.base.user.UserService implements IUserService {
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	@Qualifier("UserDaoCRM")
	private IUserDao userDao;
	
	@Autowired
	private IUtilService utilService;	
	@Autowired
	private ICountryService countryService; 
	@Autowired
	private ILanguageService languageService;
	@Autowired
	private ICurrencyService currencyService;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Response<User> insertUser(HttpServletRequest httpServletRequest, User user) throws Exception {
		ResponseCode responseCode = ResponseCode.OK;
		Error error = null;
		try {
			super.insertUser(user);
		} catch (DataAccessException dae) { //first dao level exception
			logger.error("cant signup, dao exception");
			user = null;
			responseCode = ResponseCode.OPERATION_DAO_FAILED;
			List<Message> msg = new ArrayList<Message>();
			msg.add(new Message("insert.user.failed.dao", ""));
			error = new Error("register", msg);
        }
		
		return new Response<User>(user, responseCode, null, error);
	}

	@Override
	public List<ClientSpace> getClientSpace(SqlFilters sqlFilters) {
		if (sqlFilters.getFrom() != null) {
			sqlFilters.setFrom(utilService.setStartTime(sqlFilters.getFrom()));
		}
		if (sqlFilters.getTo() != null) {
			sqlFilters.setTo(utilService.setEndTime(sqlFilters.getTo()));
		}
		return userDao.getClientSpace(sqlFilters);
	}
	
	@Override
	public capital.any.model.User getUserByEmail(String email) {		
		User userBase = userDao.getUserByEmail(email);
		capital.any.model.User user = null;
		if (userBase != null) {
			user = new capital.any.model.User();
			BeanUtils.copyProperties(userBase, user);
			setUserForPresentation(user);
		}				
		return user; 
	}
	
	@Override
	public capital.any.model.User getUserById(long id) {		
		User userBase = userDao.getUserById(id);
		capital.any.model.User user = null;
		if (userBase != null) {
			user = new capital.any.model.User();
			BeanUtils.copyProperties(userBase, user);
			setUserForPresentation(user);
		}				
		return user; 
	}

	@Override
	public List<capital.any.model.User> advancedSearchUser(SqlFilters sqlFilters) {
		return userDao.advancedSearchUser(sqlFilters);
	}
	
	/**
	 * Set user additional params in order to display it on the user strip
	 * @param user
	 */
	public void setUserForPresentation(capital.any.model.User user) {
		String mobilePhoneWithPrefix = "(" + countryService.get().get(user.getCountryByPrefix()).getPhoneCode() + ") " + user.getMobilePhone();
		user.setMobilePhoneWithPrefix(mobilePhoneWithPrefix);
		user.setCountryByIpStr(user.getCountryByIP() != 0 ? countryService.get().get(user.getCountryByIP()).getDisplayName() : "");
		user.setCountryByUserStr(user.getCountryByUser() != 0 ? countryService.get().get(user.getCountryByUser()).getDisplayName() : "");
		user.setLanguageStr(languageService.get().get(user.getLanguageId()).getDisplayName());
		user.setCurrencyStr(currencyService.get().get(user.getCurrencyId()).getDisplayName());
	}
	
}
