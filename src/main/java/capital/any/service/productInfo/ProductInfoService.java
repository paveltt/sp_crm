//package capital.any.service.productInfo;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Service;
//
//import capital.any.dao.productInfo.IProductInfoDao;
//import capital.any.model.base.ProductInfo;
//
///**
// * @author EyalG
// *
// */
//@Service("ProductInfoServiceCRM")
//public class ProductInfoService extends capital.any.service.base.productInfo.ProductInfoService implements IProductInfoService {
//	
//	@Autowired @Qualifier("ProductInfoDaoCRM")
//	private IProductInfoDao productInfoDao;
//
//	@Override
//	public void insert(ProductInfo productInfo) {
//		productInfoDao.insert(productInfo);		
//	}
//
//	@Override
//	public void insert(List<ProductInfo> productInfos) {
//		productInfoDao.insert(productInfos);		
//	}
//}
