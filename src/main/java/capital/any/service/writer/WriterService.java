package capital.any.service.writer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.writer.IWriterDao;
import capital.any.model.base.Writer;
import capital.any.security.SecurityService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("WriterServiceCRM")
public class WriterService extends capital.any.service.base.writer.WriterService implements IWriterService {
	
	@Autowired
	private SecurityService securityService; 
	@Autowired @Qualifier("WriterDaoCRM")
	private IWriterDao writerDao;
	
	@Override
	public Writer getLoginFromSession() {
		return securityService.getLoginFromSession();
	}

	@Override
	public boolean changePassword(Writer writer) {
		return writerDao.changePassword(writer);
	}
	
}
