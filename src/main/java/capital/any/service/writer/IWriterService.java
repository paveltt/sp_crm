package capital.any.service.writer;

import capital.any.model.base.Writer;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IWriterService extends capital.any.service.base.writer.IWriterService {	
		
	/**
	 * @return
	 */
	Writer getLoginFromSession();
	
	/**
	 * @param writer
	 * @return
	 */
	boolean changePassword(Writer writer);
	
}
