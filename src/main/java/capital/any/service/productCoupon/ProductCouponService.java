package capital.any.service.productCoupon;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import capital.any.dao.productCoupon.IProductCouponDao;
import capital.any.base.enums.ProductHistoryAction;
import capital.any.model.base.ProductCoupon;
import capital.any.model.base.ProductHistory;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.productHistory.IProductHistoryService;

/**
 * @author EyalG
 *
 */
@Service("ProductCouponServiceCRM")
public class ProductCouponService extends capital.any.service.base.productCoupon.ProductCouponService implements IProductCouponService {
	@Autowired @Qualifier("ProductCouponDaoCRM")
	private IProductCouponDao productCouponDao;
	
	@Autowired
	@Qualifier("ProductCouponHCDaoCRM")
	private IProductCouponDao productCouponHCDao;
	
	@Autowired
	private IServerConfiguration serverConfiguration;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private IProductHistoryService productHistoryService;

	@Override
	public void insert(ProductCoupon productCoupon) {
		productCouponDao.insert(productCoupon);		
	}
	
	@Override
	public void insert(List<ProductCoupon> productCoupons) {
		productCouponDao.insert(productCoupons);		
	}

	@Override
	public boolean update(ProductCoupon productCoupon) {
		return productCouponDao.update(productCoupon);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateObservationLevel(ProductCoupon productCoupon, ProductHistory productHistory) throws Exception {
		boolean result = productCouponDao.updateObservationLevel(productCoupon);
		if (result) {
			result = productCouponHCDao.updateObservationLevel(productCoupon);
		}
		if (result) {
			ObjectNode json = mapper.createObjectNode();
			json.put("id", productCoupon.getId());
			json.put("observationLevel", productCoupon.getObservationLevel());
			productHistory.setDetails(productCoupon.getProductId(),
					new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_PRODUCT_OBSERVATION_LEVEL.getId()),
					json.toString(), serverConfiguration.getServerName());
			productHistoryService.insert(productHistory);
		}
		return result;
	}
}
