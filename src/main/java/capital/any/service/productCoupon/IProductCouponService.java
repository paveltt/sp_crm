package capital.any.service.productCoupon;


import java.util.List;

import capital.any.model.base.ProductCoupon;
import capital.any.model.base.ProductHistory;

/**
 * @author EyalG
 *
 */
public interface IProductCouponService extends capital.any.service.base.productCoupon.IProductCouponService {	
	
	/**
	 * insert Product Coupons
	 * 
	 */
	void insert(ProductCoupon productCoupon);
	
	void insert(List<ProductCoupon> productCoupons);
	
	/**
	 * update by id
	 * @param productCoupon
	 * @return true if update was done else false
	 */
	boolean update(ProductCoupon productCoupon);
	
	/**
	 * update Observation Level
	 * @param productCoupon
	 * @param productHistory 
	 * @return true if update success else false
	 * @throws Exception 
	 */
	boolean updateObservationLevel(ProductCoupon productCoupon, ProductHistory productHistory) throws Exception;
}
