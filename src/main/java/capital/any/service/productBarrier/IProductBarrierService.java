package capital.any.service.productBarrier;

import capital.any.model.base.ProductBarrier;
import capital.any.model.base.ProductHistory;

/**
 * @author EyalG
 *
 */
public interface IProductBarrierService extends capital.any.service.base.productBarrier.IProductBarrierService {	
	
	/**
	 * insert product Barrier
	 * 
	 */
	void insert(ProductBarrier productBarrier);
	
	/**
	 * update product barrier if its occur or not
	 * @param productBarrier
	 * @param productHistory - with writer and actionsource 
	 * @throws Exception 
	 */
	boolean updateProductBarrierOccur(ProductBarrier productBarrier, ProductHistory productHistory) throws Exception;
	
	/**
	 * update product barrier
	 * @param productBarrier
	 * @throws Exception 
	 */
	boolean updateProductBarrier(ProductBarrier productBarrier) throws Exception;
}
