package capital.any.service.productBarrier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.hazelcast.core.IMap;
import capital.any.dao.productBarrier.IProductBarrierDao;
import capital.any.hazelcast.Constants;
import capital.any.hazelcast.HazelCastClientFactory;
import capital.any.base.enums.ProductHistoryAction;
import capital.any.model.base.ProductBarrier;
import capital.any.model.base.ProductBarrierType;
import capital.any.model.base.ProductHistory;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.productHistory.IProductHistoryService;

/**
 * @author EyalG
 *
 */
@Service("ProductBarrierServiceCRM")
public class ProductBarrierService extends capital.any.service.base.productBarrier.ProductBarrierService implements IProductBarrierService {

	@Autowired
	@Qualifier("ProductBarrierDaoCRM")
	private IProductBarrierDao productBarrierDao;
	
	@Autowired
	@Qualifier("ProductBarrierHCDaoCRM")
	private IProductBarrierDao productBarrierHCDao;
	
	@Autowired
	private IServerConfiguration serverConfiguration;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private IProductHistoryService productHistoryService;

	@Override
	public void insert(ProductBarrier productBarrier) {
		productBarrierDao.insert(productBarrier);		
	}

	@Override
	public boolean updateProductBarrierOccur(ProductBarrier productBarrier, ProductHistory productHistory) throws Exception {
		boolean result = productBarrierDao.updateProductBarrierOccur(productBarrier);
		if (result) {
			ObjectNode json = mapper.createObjectNode();
			json.put("isBarrierOccur", productBarrier.isBarrierOccur());
			productHistory.setDetails(productBarrier.getProductId(),
					new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_PRODUCT_BARRIER.getId()),
					json.toString(), serverConfiguration.getServerName());
			productHistoryService.insert(productHistory);
		}
		if (result) {
			result = productBarrierHCDao.updateProductBarrierOccur(productBarrier);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean updateProductBarrier(ProductBarrier productBarrier) throws Exception {
		IMap<Integer, ProductBarrierType> productBarrierTypes = (IMap<Integer, ProductBarrierType>) HazelCastClientFactory.getClient().getMap(Constants.MAP_PRODUCT_BARRIER_TYPE);
		productBarrier.setProductBarrierType(productBarrierTypes.get(productBarrier.getProductBarrierType().getId()));
		
		boolean result = productBarrierDao.updateProductBarrier(productBarrier);
		if (result) {
			result = productBarrierHCDao.updateProductBarrier(productBarrier);
		}
		return result;
	}	
}
