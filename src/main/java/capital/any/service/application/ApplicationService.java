package capital.any.service.application;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import capital.any.model.base.Writer;
import capital.any.service.writer.IWriterService;
import capital.any.service.writerGroupPermission.IWriterGroupPermissionService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("ApplicationServiceCRM")
public class ApplicationService extends capital.any.service.base.application.ApplicationService implements IApplicationService {
	
	@Autowired
	private IWriterService writerService;
	@Autowired
	private IWriterGroupPermissionService writerGroupPermissionService;
	
	@Override
	public Map<String, Object> init() {		
		Writer writer = writerService.getLoginFromSession();		
		Map<String, Object> map = new HashMap<>();		
		map.put("writer", writer) ;
		if (writer != null) {			
			map.put("writer_permissions", writerGroupPermissionService.getWriterPermissions(writer.getId()));
		}
		return map;
	}
}
