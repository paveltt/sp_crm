package capital.any.service.application;

import java.util.Map;

/**
 * @author LioR SoLoMoN
 *
 */
public interface IApplicationService extends capital.any.service.base.application.IApplicationService {	
	
	
	/**
	 * @return
	 */
	Map<String, Object> init();

}
