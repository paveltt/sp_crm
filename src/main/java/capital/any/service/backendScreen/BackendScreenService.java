package capital.any.service.backendScreen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.backendScreen.IBackendScreenDao;
import capital.any.model.BeScreen;

/**
 * @author eranl
 *
 */
@Service
public class BackendScreenService implements IBackendScreenService {
	
	@Autowired
	private IBackendScreenDao backendScreenDao;

	@Override
	public void insert(BeScreen beScreen) {
		backendScreenDao.insert(beScreen);
	}
	
}
