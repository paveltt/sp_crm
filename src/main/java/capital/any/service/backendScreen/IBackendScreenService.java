package capital.any.service.backendScreen;

import capital.any.model.BeScreen;

/**
 * @author eranl
 *
 */
public interface IBackendScreenService  {	
	
	
	/**
	 * @param beScreen
	 */
	void insert(BeScreen beScreen);
		
}
