package capital.any.service.analyticsLogin;

import capital.any.model.base.analytics.AnalyticsLogin;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IAnalyticsLoginService {

	AnalyticsLogin getAnalyticsLogin();

}
