package capital.any.service.analyticsLogin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.analyticsLogin.IAnalyticsLoginDao;
import capital.any.model.base.analytics.AnalyticsLogin;
import capital.any.model.base.analytics.AnalyticsLoginTime;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class AnalyticsLoginService implements IAnalyticsLoginService {
	
	@Autowired
	private IAnalyticsLoginDao analyticsLoginDao;
	
	@Override
	public AnalyticsLogin getAnalyticsLogin() {
		AnalyticsLogin analyticsLogin = get();
		if (analyticsLogin == null) {
			analyticsLogin = new AnalyticsLogin();
		}
		analyticsLogin.setListAnalyticsLoginTime(getByTime());
		return analyticsLogin;
	}
	
	public AnalyticsLogin get() {
		return analyticsLoginDao.get();
	}
	
	public List<AnalyticsLoginTime> getByTime() {
		return analyticsLoginDao.getByTime();
	}
}
