//package capital.any.service.productScenario;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Service;
//
//import capital.any.dao.productScenario.IProductScenarioDao;
//import capital.any.model.base.ProductScenario;
//
///**
// * @author EyalG
// *
// */
//@Service("ProductScenarioServiceCRM")
//public class ProductScenarioService extends capital.any.service.base.productScenario.ProductScenarioService implements IProductScenarioService {
//	
//	@Autowired @Qualifier("ProductScenarioDaoCRM")
//	private IProductScenarioDao productScenarioDao;
//
//	@Override
//	public void insert(ProductScenario productScenario) {
//		productScenarioDao.insert(productScenario);		
//	}
//
//	@Override
//	public void insert(List<ProductScenario> productScenarios) {
//		productScenarioDao.insert(productScenarios);		
//	}
//}
