package capital.any.service.productMarket;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import capital.any.dao.productMarket.IProductMarketDao;
import capital.any.base.enums.ProductHistoryAction;
import capital.any.model.base.ProductHistory;
import capital.any.model.base.ProductMarket;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.productHistory.IProductHistoryService;

/**
 * @author EyalG
 *
 */
@Service("ProductMarketServiceCRM")
public class ProductMarketService extends capital.any.service.base.productMarket.ProductMarketService implements IProductMarketService {

	@Autowired @Qualifier("ProductMarketDaoCRM")
	private IProductMarketDao productMarketDao;
	@Autowired @Qualifier("ProductMarketHCDaoCRM")
	private IProductMarketDao productMarketHCDao;
	
	@Autowired
	private IServerConfiguration serverConfiguration;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private IProductHistoryService productHistoryService;

	@Override
	public void insert(ProductMarket productMarket) {
		productMarketDao.insert(productMarket);
	}

	@Override
	public void insert(List<ProductMarket> productMarkets) {
		productMarketDao.insert(productMarkets);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateStartTradeLevel(List<ProductMarket> productMarkets, ProductHistory productHistory) throws Exception {
		boolean result = productMarketDao.updateStartTradeLevel(productMarkets);		
		if (result) {
			productMarkets.forEach(productMarket -> {
				ObjectNode json = mapper.createObjectNode();
				json.put("startTradeLevel", productMarket.getStartTradeLevel());
				json.put("market.id", productMarket.getMarket().getId());
				productHistory.setDetails(productMarket.getProductId(),
						new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_PRODUCT_MARKET_START_LEVEL.getId()),
						json.toString(), serverConfiguration.getServerName());
				productHistoryService.insert(productHistory);			
			});		
		}
		if (result) {
			result = productMarketHCDao.updateStartTradeLevel(productMarkets);
		}
		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateEndTradeLevel(List<ProductMarket> productMarkets, ProductHistory productHistory) throws Exception {	
		boolean result = productMarketDao.updateEndTradeLevel(productMarkets);
		if (result) {
			productMarkets.forEach(productMarket -> {
				ObjectNode json = mapper.createObjectNode();
				json.put("endTradeLevel", productMarket.getEndTradeLevel());
				json.put("market.id", productMarket.getMarket().getId());
				productHistory.setDetails(productMarket.getProductId(),
						new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_PRODUCT_MARKET_END_LEVEL.getId()),
						json.toString(), serverConfiguration.getServerName());
				productHistoryService.insert(productHistory);
			});
		}
		if (result) {
			result = productMarketHCDao.updateEndTradeLevel(productMarkets);
		}
		return result;
	}

	@Override
	public boolean updateMarketId(ProductMarket productMarket) {
		return productMarketDao.updateMarketId(productMarket);
	}
}
