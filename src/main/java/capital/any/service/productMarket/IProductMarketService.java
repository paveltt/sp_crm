package capital.any.service.productMarket;

import java.util.List;

import capital.any.model.base.ProductHistory;
import capital.any.model.base.ProductMarket;

/**
 * @author EyalG
 *
 */
public interface IProductMarketService extends capital.any.service.base.productMarket.IProductMarketService {	
	
	/**
	 * insert Product Markets
	 * 
	 */
	void insert(ProductMarket productMarket);
	
	void insert(List<ProductMarket> productMarkets);
	
	/**
	 * update product markets start trade level by product id
	 * @param productMarkets
	 * @param productHistory - with writer and actionsource
	 * @throws Exception 
	 */
	boolean updateStartTradeLevel(List<ProductMarket> productMarkets, ProductHistory productHistory) throws Exception;
	
	/**
	 * update product markets end trade level by product id
	 * @param productMarkets
	 * @throws Exception 
	 */
	boolean updateEndTradeLevel(List<ProductMarket> productMarkets, ProductHistory productHistory) throws Exception;
	
	/**
	 * update the market id
	 * @param productMarket
	 * @return true if update was done else false
	 */
	boolean updateMarketId(ProductMarket productMarket);
}
