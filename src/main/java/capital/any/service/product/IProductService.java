package capital.any.service.product;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import capital.any.filter.ProductFilters;
import capital.any.model.base.Product;
import capital.any.model.base.ProductHistory;
import capital.any.model.base.ProductKid;
import capital.any.model.base.SqlFilters;

/**
 * @author EyalG
 *
 */
public interface IProductService extends capital.any.service.base.product.IProductService {	
	
	/**
	 * insert Products
	 * @param product
	 * @throws Exception 
	 */
	@Deprecated
	void insert(Product product) throws Exception;
	
	/**
	 * insert to product table and all other related tables
	 * @param product
	 * @param ProductHistory productHistory - with writerId and actionsource
	 * @throws Exception 
	 */
	void insertFull(Product product, ProductHistory productHistory) throws Exception;
	
	/**
	 * get all filters for the insert product page
	 */
	ProductFilters getInsertProductFilters();
	
	/**
	 * get all filters for the products page
	 */
	ProductFilters getProductsFilters();

	/**
	 * update product ask and bid by product id
	 * @param p the product to update must have ask, bid and id
	 * @param ProductHistory productHistory - with writerId and actionsource
	 * @throws Exception 
	 */
	boolean updateAskBid(Product p, ProductHistory productHistory) throws Exception;
	
	/**
	 * update product suspend
	 * @param product
	 * @param productHistory - with writerId and actionsource
	 * @throws Exception 
	 */
	boolean updateSuspend(Product product, ProductHistory productHistory) throws Exception;
	
	/**
	 * update product
	 * @param product
	 * @param productHistory - with writerId and actionsource
	 * @throws Exception 
	 */
	boolean updateProduct(Product product, ProductHistory productHistory) throws Exception;
	
	/**
	 * update products priority
	 * @param products list to update
	 * @param productHistory - with writerId and actionsource
	 * @return true if we update else false
	 * @throws Exception 
	 */
	boolean updateProductsPriority(List<Product> products, ProductHistory productHistory) throws Exception;
	
	/**
	 * get all products for priority screen
	 * @return list of products
	 */
	List<Product> getProductsForPriority();
	
	/**
	 * publish product to primary or secondary state
	 * @param product product with id to change
	 * @param productHistory
	 * @return true if changed else false;
	 * @throws Exception 
	 */
	public boolean publish(Product product, ProductHistory productHistory) throws Exception;
	
	/**
	 * get all products that fit the criterion
	 * @param filters
	 * @return {@link List<Product>}
	 */
	List<Product> getProducts(SqlFilters filters);
	
	/**
	 * Update KID.
	 * @param product
	 * @param productHistory
	 * @return
	 * @throws Exception
	 */
	boolean updateKidAndUpload(ProductKid productKid, ProductHistory productHistory, MultipartFile fileUpload) throws Exception;
}
