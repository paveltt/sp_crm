package capital.any.service.product;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import capital.any.base.enums.ProductHistoryAction;
import capital.any.base.enums.ProductStatusEnum;
import capital.any.dao.product.IProductDao;
import capital.any.filter.ProductFilters;
import capital.any.model.base.DayCountConvention;
import capital.any.model.base.File;
import capital.any.model.base.Language;
import capital.any.model.base.Market;
import capital.any.model.base.Product;
import capital.any.model.base.ProductAutocall;
import capital.any.model.base.ProductBarrierType;
import capital.any.model.base.ProductCallable;
import capital.any.model.base.ProductCategory;
import capital.any.model.base.ProductCoupon;
import capital.any.model.base.ProductCouponFrequency;
import capital.any.model.base.ProductHistory;
import capital.any.model.base.ProductKid;
import capital.any.model.base.ProductKidLanguage;
import capital.any.model.base.ProductMarket;
import capital.any.model.base.ProductMaturity;
import capital.any.model.base.ProductSimulation;
import capital.any.model.base.ProductStatus;
import capital.any.model.base.ProductType;
import capital.any.model.base.SqlFilters;
import capital.any.service.IUtilService;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.aws.IAwsService;
import capital.any.service.base.currency.ICurrencyService;
import capital.any.service.base.dayCountConvention.IDayCountConventionService;
import capital.any.service.base.language.ILanguageService;
import capital.any.service.base.market.IMarketService;
import capital.any.service.base.productBarrierType.IProductBarrierTypeService;
import capital.any.service.base.productCategory.IProductCategoryService;
import capital.any.service.base.productCouponFrequency.IProductCouponFrequencyService;
import capital.any.service.base.productHistory.IProductHistoryService;
import capital.any.service.base.productIssuerInsuranceType.IProductIssuerInsuranceTypeService;
import capital.any.service.base.productKidLanguage.IProductKidLanguageService;
import capital.any.service.base.productMaturity.IProductMaturityService;
import capital.any.service.base.productStatus.IProductStatusService;
import capital.any.service.base.productType.IProductTypeService;
import capital.any.service.productAutocall.IProductAutocallService;
import capital.any.service.productBarrier.IProductBarrierService;
import capital.any.service.productCallable.IProductCallableService;
import capital.any.service.productCoupon.IProductCouponService;
import capital.any.service.productMarket.IProductMarketService;
import capital.any.service.productSimulation.IProductSimulationService;

/**
 * @author EyalG
 *
 */
@Service("ProductServiceCRM")
public class ProductService extends capital.any.service.base.product.ProductService implements IProductService {
	
	private static final Logger logger = LoggerFactory.getLogger(ProductService.class);
	
	@Autowired @Qualifier("ProductDaoCRM")
	protected IProductDao productDao;
	@Autowired @Qualifier("ProductHCDaoCRM")
	protected IProductDao productHCDao;

	@Autowired @Qualifier("ProductAutocallServiceCRM")
	protected IProductAutocallService productAutocallService;
	
	@Autowired @Qualifier("ProductCallableServiceCRM")
	protected IProductCallableService productCallableService;
	
	@Autowired @Qualifier("ProductCouponServiceCRM")
	protected IProductCouponService productCouponService;
	
//	@Autowired @Qualifier("ProductInfoServiceCRM")
//	protected IProductInfoService productInfoService;
	
	@Autowired @Qualifier("ProductMarketServiceCRM")
	protected IProductMarketService productMarketService;
	
//	@Autowired @Qualifier("ProductScenarioServiceCRM")
//	protected IProductScenarioService productScenarioService;
	
	@Autowired @Qualifier("ProductSimulationServiceCRM")
	protected IProductSimulationService productSimulationService;
	
	@Autowired @Qualifier("ProductBarrierServiceCRM")
	protected IProductBarrierService productBarrierService;
	
	@Autowired
	private IDayCountConventionService dayCountConventionService;
	
	@Autowired
	private IProductIssuerInsuranceTypeService productIssuerInsuranceTypeService;
	@Autowired
	private IProductTypeService productTypeService;
	@Autowired
	private IProductCategoryService productCategoryService;
	@Autowired
	private IMarketService marketService;
	@Autowired
	private IProductCouponFrequencyService productCouponFrequencyService;
	@Autowired
	private ICurrencyService currencyService;
	@Autowired
	private IProductMaturityService productMaturityService;
	@Autowired
	private IProductBarrierTypeService productBarrierTypeService;
	
	@Autowired
	private IProductStatusService productStatusService;
	
	@Autowired
	private IUtilService utilService;
	
	@Autowired
	private IServerConfiguration serverConfiguration;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private IProductHistoryService productHistoryService;
	
	@Autowired
	private IAwsService awsService;
	@Autowired
	private ILanguageService languageService;
	@Autowired
	private IProductKidLanguageService productKidLanguageService;
	
	@Override
	@Deprecated
	public void insert(Product product) throws Exception {
		productDao.insert(product);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insertFull(Product product, ProductHistory productHistory) throws Exception {
		logger.debug("insert Full product " + product);
		//insert to products table and get his id
		
		product.setSubscriptionStartDate(utilService.setStartTime(product.getSubscriptionStartDate()));
		product.setSubscriptionEndDate(utilService.setEndTime(product.getSubscriptionEndDate()));
		product.setInitialFixingDate(utilService.setStartTime(product.getInitialFixingDate()));
		product.setFirstExchangeTradingDate(utilService.setStartTime(product.getFirstExchangeTradingDate()));
		product.setLastTradingDate(utilService.setStartTime(product.getLastTradingDate()));
		product.setFinalFixingDate(utilService.setStartTime(product.getFinalFixingDate()));
		product.setRedemptionDate(utilService.setStartTime(product.getRedemptionDate()));
		
		product.setTimeCreated(new Date());
		product.setProductStatus(new ProductStatus(ProductStatusEnum.UNPUBLISH.getId()));
		productDao.insert(product);
		
		if (product.getProductBarrier() != null && product.getProductBarrier().getBarrierLevel() > 0) {
			product.getProductBarrier().setProductId(product.getId());
			productBarrierService.insert(product.getProductBarrier());
		}
		//insert auto call
		if (product.getProductAutocalls() != null &&
				product.getProductAutocalls().size() > 0) {
			for (ProductAutocall productAutocall : product.getProductAutocalls()) {
				productAutocall.setProductId(product.getId());
			}
			productAutocallService.insert(product.getProductAutocalls());
		}
		
		//insert  call able
		if (product.getProductCallables() != null &&
				product.getProductCallables().size() > 0) {
			for (ProductCallable productCallable : product.getProductCallables()) {
				productCallable.setProductId(product.getId());
			}
			productCallableService.insert(product.getProductCallables());
		}
		
		//insert  coupons
		if (product.getProductCoupons() != null &&
				product.getProductCoupons().size() > 0) {
			for (ProductCoupon productCoupon : product.getProductCoupons()) {
				productCoupon.setProductId(product.getId());
				productCoupon.setObservationDate(utilService.setStartTime(productCoupon.getObservationDate()));
				productCoupon.setPaymentDate(utilService.setStartTime(productCoupon.getPaymentDate()));
			}
			productCouponService.insert(product.getProductCoupons());
		}
		
		//insert  markets
		if (product.getProductMarkets() != null &&
				product.getProductMarkets().size() > 0) {
			for (ProductMarket productMarket : product.getProductMarkets()) {
				productMarket.setProductId(product.getId());
			}
			productMarketService.insert(product.getProductMarkets());
		}
		
		//insert  Product Simulation
		if (product.getProductSimulation() != null &&
				product.getProductSimulation().size() > 0) {
			insertOrUpdateProductSimulation(product);
		}
		
		product = setProductFull(product);
		productHCDao.insert(product);
		productHistory.setDetails(product.getId(),
				new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.INSERT_PRODUCT.getId()),
				mapper.writeValueAsString(product), serverConfiguration.getServerName());
		productHistoryService.insert(productHistory);
	}
	
	/**
	 * call this function from insert and update
	 * @param product
	 */
	private void insertOrUpdateProductSimulation(Product product) {
		int position = 1;
		for (ProductSimulation productSimulation : product.getProductSimulation()) {
			productSimulation.setProductId(product.getId());
			productSimulation.setPosition(position++);
			if (productSimulation.getId() > 0) {
				productSimulationService.update(productSimulation);
			} else {
				productSimulationService.insert(productSimulation);
			}
		}
	}
	
	public Product setProductFull(Product product) {
		logger.info("setProductFull");	
		Map<Integer, Market> markets = marketService.getMarketsWithLastPrice();
		Map<Integer, ProductBarrierType> productBarrierTypes = productBarrierTypeService.get();
		Map<Integer, ProductType> productTypes = productTypeService.get();
		Map<Integer, ProductStatus> productStatus = productStatusService.get();
		Map<Integer, ProductCategory> productCategory = productCategoryService.get();
		Map<Integer, ProductCouponFrequency> productCouponFrequency = productCouponFrequencyService.get();
		Map<Integer, DayCountConvention> dayCountConvention = dayCountConventionService.get();
		Map<Integer, ProductMaturity> productMaturities = productMaturityService.get();	
		setProductsFull(
				product, markets, 
				productBarrierTypes, productTypes, productStatus,
				productCategory, productCouponFrequency, 
				dayCountConvention, productMaturities);
		
		return product;
	}
	
	@Override
	public ProductFilters getInsertProductFilters() {
		ProductFilters productFilters = new ProductFilters();
		productFilters.setProductTypes(productTypeService.get());
		productFilters.setProductCategories(productCategoryService.get());
		productFilters.setMarkets(marketService.getMarketsWithLastPrice());
		productFilters.setDayCountConventions(dayCountConventionService.get());
		productFilters.setProductIssuerInsuranceTypes(productIssuerInsuranceTypeService.getProductIssuerInsuranceType());		
		productFilters.setProductCouponFrequency(productCouponFrequencyService.get());
		productFilters.setCurrencies(currencyService.get());
		productFilters.setProductMaturities(productMaturityService.get());
		productFilters.setProductBarrierTypes(productBarrierTypeService.get());
		return productFilters;
	}

	public ProductFilters getProductsFilters() {
		ProductFilters productFilters = new ProductFilters();
		productFilters.setProductTypes(productTypeService.get());
		productFilters.setProductCategories(productCategoryService.get());
		productFilters.setMarkets(marketService.getMarketsWithLastPrice());
		productFilters.setProductStatus(productStatusService.get());		
		return productFilters;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateAskBid(Product product, ProductHistory productHistory) throws Exception {
		boolean result = productDao.updateAskBid(product);
		if (result) {
			ObjectNode json = mapper.createObjectNode();
			json.put("ask", product.getAsk());
			json.put("bid", product.getBid());
			productHistory.setDetails(product.getId(),
					new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_ASK_BID.getId()),
					json.toString(), serverConfiguration.getServerName());
			productHistoryService.insert(productHistory);
		}
		if (result) {
			result = productHCDao.updateAskBid(product);
		}
		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateSuspend(Product product, ProductHistory productHistory) throws Exception {
		boolean result = productDao.updateSuspend(product);
		if (result) {
			ObjectNode json = mapper.createObjectNode();
			json.put("isSuspend", product.isSuspend());
			productHistory.setDetails(product.getId(),
					new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_SUSPEND.getId()),
					json.toString(), serverConfiguration.getServerName());
			productHistoryService.insert(productHistory);
		}
		if (result) {
			result = productHCDao.updateSuspend(product);
		}
		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateProduct(Product product, ProductHistory productHistory) throws Exception {
		product.setSubscriptionStartDate(utilService.setStartTime(product.getSubscriptionStartDate()));
		product.setSubscriptionEndDate(utilService.setEndTime(product.getSubscriptionEndDate()));
		product.setInitialFixingDate(utilService.setStartTime(product.getInitialFixingDate()));
		product.setFirstExchangeTradingDate(utilService.setStartTime(product.getFirstExchangeTradingDate()));
		product.setLastTradingDate(utilService.setStartTime(product.getLastTradingDate()));
		product.setFinalFixingDate(utilService.setStartTime(product.getFinalFixingDate()));
		product.setRedemptionDate(utilService.setStartTime(product.getRedemptionDate()));
		
		boolean result = productDao.updateProduct(product);
		if (!result) {
			return false;
		}
		if (product.getProductBarrier() != null && product.getProductBarrier().getBarrierLevel() > 0) {
			if (product.getProductBarrier().getProductId() > 0) {
				productBarrierService.updateProductBarrier(product.getProductBarrier());
			} else {
				product.getProductBarrier().setProductId(product.getId());
				productBarrierService.insert(product.getProductBarrier());
			}			
		}
		//insert auto call
		if (product.getProductAutocalls() != null &&
				product.getProductAutocalls().size() > 0) {
			for (ProductAutocall productAutocall : product.getProductAutocalls()) {
				if (productAutocall.getId() > 0) {
					productAutocallService.update(productAutocall);					
				} else {
					productAutocall.setProductId(product.getId());
					productAutocallService.insert(productAutocall);
				}
			}			
		}
		
		//insert  call able
		if (product.getProductCallables() != null &&
				product.getProductCallables().size() > 0) {
			for (ProductCallable productCallable : product.getProductCallables()) {
				if (productCallable.getId() > 0) {
					productCallableService.update(productCallable);
				} else {
					productCallable.setProductId(product.getId());
					productCallableService.insert(productCallable);
				}				
			}
			
		}
		
		//insert  coupons
		if (product.getProductCoupons() != null &&
				product.getProductCoupons().size() > 0) {
			for (ProductCoupon productCoupon : product.getProductCoupons()) {
				if (productCoupon.getId() > 0) {
					productCouponService.update(productCoupon);
				} else {
					productCoupon.setProductId(product.getId());
					productCouponService.insert(productCoupon);
				}				
			}
		}
		
		
		//insert  markets
		if (product.getProductMarkets() != null &&
				product.getProductMarkets().size() > 0) {
			for (ProductMarket productMarket : product.getProductMarkets()) {
				if (productMarket.getId() > 0) {
					productMarketService.updateMarketId(productMarket);
				} else {
					productMarket.setProductId(product.getId());
					productMarketService.insert(productMarket);
				}	
			}
		}
		
		//insert  Product Simulation
		if (product.getProductSimulation() != null &&
				product.getProductSimulation().size() > 0) {
			insertOrUpdateProductSimulation(product);
		}
		
		//load what need from db/HC and replace in HC
		product = setProductFull(product);
		productHCDao.insert(product);
		try {
			productHistory.setDetails(product.getId(),
					new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_PRODUCT.getId()),
					mapper.writeValueAsString(product),
					serverConfiguration.getServerName());
			productHistoryService.insert(productHistory);
		} catch (JsonProcessingException e) {
			logger.error("Can't insert to product history product id " + product.getId(), e);
		}
		
		return result;
	}

	@Override
	public boolean updateProductsPriority(List<Product> products, ProductHistory productHistory) throws Exception {
		boolean result = productDao.updateProductsPriority(products);
		if (result) {
			products.forEach(product -> {
				ObjectNode json = mapper.createObjectNode();
				json.put("priority", product.getPriority());
				productHistory.setDetails(product.getId(),
						new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_PRIORTY.getId()),
						json.toString(),
						serverConfiguration.getServerName());
				productHistoryService.insert(productHistory);
			});
		}
		if (result) {
			result = productHCDao.updateProductsPriority(products);
		}
		return result;
	}

	@Override
	public List<Product> getProductsForPriority() {
		List<Product> products = productHCDao.getProductsForPriority();
		if (products == null || products.size() <= 0) {
			logger.info("products empty / null in hazelcast, Let's check RDBMS");
			products = productDao.getProductsForPriority();
			setProductsFull(products);
		} else {
			//need to sort list from hazelcast
			products.sort((left, right) -> right.getPriority() - left.getPriority());
		}
		return products;
	}
	
	@Override
	public boolean publish(Product product, ProductHistory productHistory) throws Exception {
		Product productToChange = getProduct(product.getId());
		Date now = new Date();
		if (productToChange.getProductStatus().getId() == ProductStatusEnum.UNPUBLISH.getId()) {
			if (productToChange.getSubscriptionStartDate().before(now) && productToChange.getSubscriptionEndDate().after(now)) {
				return updateStatus(product.getId(), ProductStatusEnum.SUBSCRIPTION, productHistory);
			} else if (productToChange.getSubscriptionEndDate().before(now) && productToChange.getLastTradingDate().after(now)) {
				return updateStatus(product.getId(), ProductStatusEnum.SECONDARY, productHistory);
			}
		}
		return false;
	}
	
	@Override
	public List<Product> getProducts(SqlFilters filters) {	
		List<Product> products = productHCDao.getProducts(filters);
		if (products == null || products.size() <= 0) {
			logger.info("products with filter empty / null in hazelcast, Let's check RDBMS");
			products = productDao.getProducts(filters);
			setProductsFull(products);
		}
		return products;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateKidAndUpload(ProductKid productKid, ProductHistory productHistory, MultipartFile fileUpload) throws Exception {
		Product product = productKid.getProduct();
		Language l = productKid.getLanguage();
		Language language = languageService.get().get(l.getId());
		String originalFilename = fileUpload.getOriginalFilename();
		int lastDot = originalFilename.lastIndexOf('.');
	    String fileExtension = originalFilename.substring(lastDot, originalFilename.length());
		product.setKidName(product.getId() + fileExtension);
		boolean result = productDao.updateKid(product);
		if (result) {
			ObjectNode json = mapper.createObjectNode();
			json.put("kidName", product.getKidName());
			productHistory.setDetails(product.getId(),
					new capital.any.model.base.ProductHistoryAction(ProductHistoryAction.UPDATE_PRODUCT_KID.getId()),
					json.toString(), serverConfiguration.getServerName());
			productHistoryService.insert(productHistory);
			List<ProductKidLanguage> productKidLanguageList = productKidLanguageService.get(product);
			if (null == productKidLanguageList || 
					(null != productKidLanguageList && null == productKidLanguageList.stream()
																				.filter(line -> line.getLanguage().getId() == language.getId())
																				.findAny()
																                .orElse(null))) {
				ProductKidLanguage productKidLanguage = new ProductKidLanguage(product, language);
				productKidLanguageService.insert(productKidLanguage);
			}
		}
		if (result) {
			result = productHCDao.updateKid(product);
			awsService.uploadProductKidFile(fileUpload, product, language, null);
		}
		return result;
	}
}
