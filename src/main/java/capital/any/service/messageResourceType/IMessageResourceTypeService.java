package capital.any.service.messageResourceType;

import java.util.Map;

import capital.any.model.MessageResourceType;

/**
 * 
 * @author Eyal.o
 *
 */
public interface IMessageResourceTypeService {
	
	Map<Integer, MessageResourceType> get();

}
