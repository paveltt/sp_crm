package capital.any.service.messageResourceType;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.messageResourceType.IMessageResourceTypeDao;
import capital.any.model.MessageResourceType;

/**
 * 
 * @author Eyal.o
 *
 */
@Service
public class MessageResourceTypeService implements IMessageResourceTypeService {

	@Autowired
	private IMessageResourceTypeDao messageResourceTypeDao;
	
	@Override
	public Map<Integer, MessageResourceType> get() {
		return messageResourceTypeDao.get();
	}

}
