package capital.any.service.backendSpace;

import capital.any.model.BeSpace;

/**
 * @author eranl
 *
 */
public interface IBackendSpaceService  {	
	
	/**
	 * @param beSpace
	 */
	void insert(BeSpace beSpace);
		
}
