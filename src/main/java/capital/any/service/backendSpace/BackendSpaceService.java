package capital.any.service.backendSpace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.backendSpace.IBackendSpaceDao;
import capital.any.model.BeSpace;

/**
 * @author eranl
 *
 */
@Service
public class BackendSpaceService implements IBackendSpaceService {
	
	@Autowired
	private IBackendSpaceDao backendSpaceDao;

	@Override
	public void insert(BeSpace beSpace) {
		backendSpaceDao.insert(beSpace);
	}
	
}
