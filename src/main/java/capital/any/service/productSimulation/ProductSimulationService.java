package capital.any.service.productSimulation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.productSimulation.IProductSimulationDao;
import capital.any.model.base.ProductSimulation;

/**
 * @author EyalG
 *
 */
@Service("ProductSimulationServiceCRM")
public class ProductSimulationService extends capital.any.service.base.productSimulation.ProductSimulationService implements IProductSimulationService {
	
	@Autowired @Qualifier("ProductSimulationDaoCRM")
	private IProductSimulationDao productSimulationDao;

	@Override
	public void insert(ProductSimulation productSimulation) {
		productSimulationDao.insert(productSimulation);
	}

	@Override
	public void insert(List<ProductSimulation> productSimulations) {
		productSimulationDao.insert(productSimulations);
	}

	@Override
	public boolean update(ProductSimulation productSimulation) {
		return productSimulationDao.update(productSimulation);
	}
}
