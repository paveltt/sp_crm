package capital.any.service.productSimulation;

import java.util.List;

import capital.any.model.base.ProductSimulation;

/**
 * @author EyalG
 *
 */
public interface IProductSimulationService extends capital.any.service.base.productSimulation.IProductSimulationService {	
	
	/**
	 * insert Product Simulations
	 * 
	 */
	void insert(ProductSimulation productSimulation);
	
	void insert(List<ProductSimulation> productSimulations);
	
	/**
	 * update product Simulation
	 * @param productSimulation
	 * @return true if update was done else false
	 */
	boolean update(ProductSimulation productSimulation);
}
