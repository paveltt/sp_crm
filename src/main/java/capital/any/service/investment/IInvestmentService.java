package capital.any.service.investment;

import java.util.List;
import java.util.Optional;

import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Investment;

/**
 * @author @author Eyal G
 *
 */
public interface IInvestmentService extends capital.any.service.base.investment.IInvestmentService {
	/**
	 * get investment by id and status
	 * @param id
	 * @param statusIds
	 * @return Optional<Investment>
	 */
	Optional<Investment> getInvestmentByIdAndStatuses(long id, List<Integer> statusIds);

	/**
	 * cancel investment only if user is not inserting into negative balance
	 * @param investment to cancel
	 * @return ResponseCode
	 * @throws Exception
	 */
	ResponseCode cancel(Investment investment) throws Exception;
}
