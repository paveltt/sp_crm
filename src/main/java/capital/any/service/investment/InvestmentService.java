package capital.any.service.investment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.InvestmentStatusEnum;
import capital.any.base.enums.Table;
import capital.any.communication.Response.ResponseCode;
import capital.any.dao.investment.IInvestmentDao;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.BalanceRequest.OperationType;
import capital.any.model.base.Investment;
import capital.any.model.base.InvestmentStatus;
import capital.any.model.base.Transaction;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.Writer;
import capital.any.service.transaction.ITransactionService;

/**
 * @author Eyal G
 *
 */
@Service("InvestmentServiceCRM")
public class InvestmentService extends capital.any.service.base.investment.InvestmentService implements IInvestmentService {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestmentService.class);
	
	@Autowired
	private IInvestmentDao investmentDao;
	
	@Autowired
	private ITransactionService transactionService;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseCode cancel(Investment investment) throws Exception {
		logger.debug("try to cancel investment " + investment.getId());
		List<Integer> statusIds = new ArrayList<Integer>();
		statusIds.add(InvestmentStatusEnum.PENDING.getId());
		statusIds.add(InvestmentStatusEnum.OPEN.getId());
		statusIds.add(InvestmentStatusEnum.SETTLED.getId());		
		Optional<Investment> investmentToCancel = getInvestmentByIdAndStatuses(investment.getId(), statusIds);
		if (investmentToCancel.filter(inv -> statusIds.contains(inv.getInvestmentStatus().getId())).isPresent()) {
			Investment inv = investmentToCancel.get();
			int currentStatusId = inv.getInvestmentStatus().getId();
			inv.setInvestmentStatus(new InvestmentStatus(InvestmentStatusEnum.CANCELED.getId()));
			inv.setInvestmentHistory(investment.getInvestmentHistory());
			inv.getInvestmentHistory().setInvestmentStatus(inv.getInvestmentStatus());
			inv.getInvestmentHistory().setInvestmntId(inv.getId());
			inv.getInvestmentHistory().setServerName(serverConfiguration.getServerName());
			long returnCouponAmonut = 0;
			long returnInvAmonut = 0;
			List<Transaction> transactions = new ArrayList<Transaction>();
			if (currentStatusId != InvestmentStatusEnum.PENDING.getId()) {
				//get coupons
				transactions = transactionService.getTransactionsByReferenceId(inv.getId(), Table.INVESTMENTS.getId());
				for (Transaction transaction : transactions) {
					if (transaction.getStatus().getId() == TransactionStatusEnum.SUCCEED.getId()) {
						returnCouponAmonut -= transaction.getAmount();
					}
				}					
				returnInvAmonut += inv.getAmount() - inv.getReturnAmount();				
			}
			long balance = balanceService.getBalance(inv.getUser().getId());
			logger.debug("user balance " + balance + " returnCouponAmonut " + returnCouponAmonut + " returnInvAmonut " + returnInvAmonut);
			if (balance + returnInvAmonut + returnCouponAmonut < 0) {
				return ResponseCode.INVESTMENT_ERROR_CANCEL_NEGATIVE_BALANCE;
			}
			updateStatus(inv);
			if (returnInvAmonut != 0) {				
				balanceService.changeBalance(
						new BalanceRequest(inv.getUser(), 
								Math.abs(returnInvAmonut),
								BalanceHistoryCommand.CANCEL_INVESTMENT, 
								investment.getId(),
								returnInvAmonut < 0 ? OperationType.DEBIT : OperationType.CREDIT,
										new UserControllerDetails(inv.getInvestmentHistory().getActionSource(),
												inv.getInvestmentHistory().getWriterId())));
			}
			for (Transaction transaction : transactions) {
				if (transaction.getStatus().getId() == TransactionStatusEnum.SUCCEED.getId()) {
					transaction.setStatus(new TransactionStatus(TransactionStatusEnum.CANCEL.getId()));
					TransactionHistory transactionHistory = new TransactionHistory();
					transactionHistory.setActionSource(investment.getInvestmentHistory().getActionSource());
					transactionHistory.setServerName(inv.getInvestmentHistory().getServerName());
					transactionHistory.setStatus(transaction.getStatus());
					transactionHistory.setTransactionId(transaction.getId());
					transactionHistory.setWriter(new Writer(investment.getInvestmentHistory().getWriterId()));
					transactionHistory.setTimeCreated(new Date());
					transactionService.updateTransaction(transaction, transactionHistory);
					balanceService.changeBalance(
							new BalanceRequest(transaction.getUser(), 
									Math.abs(transaction.getAmount()),
									BalanceHistoryCommand.CANCEL_TRANSACTION, 
									transaction.getId(),
									OperationType.DEBIT,
											new UserControllerDetails(transactionHistory.getActionSource(),
													inv.getInvestmentHistory().getWriterId())));
				}
			}
		} else {
			return ResponseCode.INVALID_INPUT;
		}
		return ResponseCode.OK;
	}

	@Override
	public Optional<Investment> getInvestmentByIdAndStatuses(long id, List<Integer> statusIds) {
		return investmentDao.getInvestmentByIdAndStatuses(id, statusIds);
	}	
}
