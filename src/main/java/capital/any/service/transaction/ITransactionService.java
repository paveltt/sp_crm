package capital.any.service.transaction;

import java.util.List;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;

/**
 * @author LioR SoLoMoN
 *
 */
public interface ITransactionService extends capital.any.service.base.transaction.ITransactionService {

	/**
	 * @param transaction
	 * @param transactionHistory
	 * @return
	 */
	ResponseCode withdrawalFirstApproved(Transaction transaction, TransactionHistory transactionHistory);
	
	/**
	 * @param transaction
	 * @param transactionHistory
	 * @return
	 */
	ResponseCode withdrawalSecondApproved(Transaction transaction, TransactionHistory transactionHistory);

	/**
	 * @param transaction
	 * @param transactionHistory
	 * @return
	 * @throws Exception 
	 */
	ResponseCode cancelWithdrawal(Transaction transaction, TransactionHistory transactionHistory) throws Exception;
	
	/**
	 * get transaction by reference id
	 * @param referenceId
	 * @return List of transactions
	 */
	List<Transaction> getTransactionsByReferenceId(long referenceId, int tableId);

	/**
	 * @param transaction
	 * @return 
	 * @throws Exception 
	 */
	ResponseCode approveDeposit(Transaction transaction, TransactionHistory transactionHistory) throws Exception;

}
