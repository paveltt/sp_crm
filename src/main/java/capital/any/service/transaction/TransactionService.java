package capital.any.service.transaction;

import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.DBParameter;
import capital.any.communication.Response.ResponseCode;
import capital.any.dao.transaction.ITransactionDao;
import capital.any.model.base.BalanceRequest;
import capital.any.model.base.Transaction;
import capital.any.model.base.TransactionHistory;
import capital.any.model.base.TransactionStatus;
import capital.any.model.base.UserControllerDetails;
import capital.any.model.base.BalanceHistory.BalanceHistoryCommand;
import capital.any.model.base.BalanceRequest.OperationType;
import capital.any.model.base.Transaction.TransactionStatusEnum;
import capital.any.model.base.TransactionFee;
import capital.any.service.base.IBalanceService;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.dbParameter.IDBParameterService;
import capital.any.service.base.transactionFee.ITransactionFeeService;

/**
 * @author LioR SoLoMoN
 *
 */
@Service("TransactionServiceCRM")
public class TransactionService extends capital.any.service.base.transaction.TransactionService implements ITransactionService {
	private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);
	@Autowired @Qualifier("TransactionDaoCRM")
	private ITransactionDao transactionDao;
	@Autowired
	private IServerConfiguration serverConfiguration;
	@Autowired
	private IBalanceService balanceService;
	@Autowired
	private ITransactionFeeService transactionFeeService;
	@Autowired
	private IDBParameterService dbParameterService;

	@Override
	public ResponseCode withdrawalFirstApproved(Transaction transaction, TransactionHistory transactionHistory) {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		transaction = transactionDao.getTransaction(transaction.getId());
		//authenticate transaction
		if (transaction.getStatus().getId() == TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId()) {	
			transactionHistory.setStatus(transaction.getStatus());
			transactionHistory.setServerName(serverConfiguration.getServerName());
			transactionHistory.setTimeCreated(new Date());
			transactionHistory.setTransactionId(transaction.getId());
			transaction.setStatus(new TransactionStatus(TransactionStatusEnum.WAITING_FOR_SECOND_APPROVE.getId()));		
			responseCode = updateTransaction(transaction, transactionHistory) ? ResponseCode.OK : ResponseCode.OPERATION_FAILED;
		} else {
			responseCode = ResponseCode.INVALID_INPUT;
		}
		return responseCode;
	}
	
	@Override @Transactional(rollbackFor = Exception.class)
	public ResponseCode withdrawalSecondApproved(Transaction transaction, TransactionHistory transactionHistory) {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		transaction = transactionDao.getTransaction(transaction.getId());
		//authenticate transaction
		if (transaction.getStatus().getId() == TransactionStatusEnum.WAITING_FOR_SECOND_APPROVE.getId()) {	
			transactionHistory.setStatus(transaction.getStatus());
			transactionHistory.setServerName(serverConfiguration.getServerName());
			transactionHistory.setTimeCreated(new Date());
			transactionHistory.setTransactionId(transaction.getId());
			transaction.setStatus(new TransactionStatus(TransactionStatusEnum.SUCCEED.getId()));		
			responseCode = updateTransaction(transaction, transactionHistory) ? ResponseCode.OK : ResponseCode.OPERATION_FAILED;
			//insert Fee
			long feeAmount = dbParameterService.get(DBParameter.WITHDRAW_FEE_AMOUNT.getId()).getNumValue();
			transactionFeeService.insert(new TransactionFee(transaction.getId(), feeAmount , new Date()));
		} else {
			responseCode = ResponseCode.INVALID_INPUT;
		}
		return responseCode;
	}

	@Override @Transactional(rollbackFor = Exception.class)
	public ResponseCode cancelWithdrawal(Transaction transaction, TransactionHistory transactionHistory) throws Exception {
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		transaction = transactionDao.getTransaction(transaction.getId());
		//authenticate transaction
		if (transaction.getStatus().getId() == TransactionStatusEnum.WAITING_FOR_FIRST_APPROVE.getId() ||
					transaction.getStatus().getId() == TransactionStatusEnum.WAITING_FOR_SECOND_APPROVE.getId()) {	
			transactionHistory.setStatus(transaction.getStatus());
			transactionHistory.setServerName(serverConfiguration.getServerName());
			transactionHistory.setTimeCreated(new Date());
			transactionHistory.setTransactionId(transaction.getId());
			transaction.setStatus(new TransactionStatus(TransactionStatusEnum.CANCEL.getId()));
			responseCode = updateTransaction(transaction, transactionHistory) ? ResponseCode.OK : ResponseCode.OPERATION_FAILED;
			balanceService.changeBalance(
					new BalanceRequest(transaction.getUser(), 
							transaction.getAmount(),
							BalanceHistoryCommand.CANCEL_TRANSACTION, 
							transaction.getId(), 
							OperationType.CREDIT, new UserControllerDetails( 
									transactionHistory.getActionSource(),
									transactionHistory.getWriter().getId())));			
		} else {
			responseCode = ResponseCode.INVALID_INPUT;
		}
		return responseCode;
	}
	
	//assumption: transaction status is not null. 
	@Override @Transactional(rollbackFor = Exception.class)
	public ResponseCode approveDeposit(Transaction transaction, TransactionHistory transactionHistory) throws Exception {		
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR;
		transactionHistory.setStatus(transaction.getStatus());
		transaction = transactionDao.getTransaction(transaction.getId());
		if (transaction.getStatus().getId() == TransactionStatusEnum.PENDING.getId()) {
			transaction.setStatus(transactionHistory.getStatus());
			transactionHistory.setServerName(serverConfiguration.getServerName());
			transactionHistory.setTimeCreated(new Date());
			transactionHistory.setTransactionId(transaction.getId());
			
			if (transactionHistory.getStatus().getId() == TransactionStatusEnum.SUCCEED.getId()) {
				BalanceRequest balanceRequest = 
				new BalanceRequest(transaction.getUser(), 
						transaction.getAmount(),
						BalanceHistoryCommand.SUCCESS_TRANSACTION, 
						transaction.getId(), 
						OperationType.CREDIT, new UserControllerDetails( 
								transactionHistory.getActionSource(),
								transactionHistory.getWriter().getId()));
				balanceService.changeBalance(balanceRequest);
				updateTransaction(transaction, transactionHistory);
			} else if (transactionHistory.getStatus().getId() == TransactionStatusEnum.FAILED.getId()) {			
				updateTransaction(transaction, transactionHistory);
			}
			responseCode = ResponseCode.OK;
		}
		
		return responseCode;
	}

	@Override
	public List<Transaction> getTransactionsByReferenceId(long referenceId, int tableId) {
		return transactionDao.getTransactionsByReferenceId(referenceId, tableId);
	}
}
