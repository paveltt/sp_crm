package capital.any.service.messageResource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.base.enums.MsgResTypeEnum;
import capital.any.dao.messageResource.IMessageResourceDao;
import capital.any.model.base.MsgRes;
import capital.any.model.base.MsgResFile;
import capital.any.model.base.MsgResLanguage;
import capital.any.service.base.xlsx.IXlsxService;
import capital.any.service.messageResourceLanguage.IMessageResourceLanguageService;

/**
 * @author eranl
 *
 */
@Service("MessageResourceServiceCRM")
public class MessageResourceService extends capital.any.service.base.messageResource.MessageResourceService implements IMessageResourceService {
	
	private static final Logger logger = LoggerFactory.getLogger(MessageResourceService.class);
	
	@Autowired
	@Qualifier("MessageResourceDaoCRM")
	private IMessageResourceDao messageResourceDao;
	@Autowired
	private IXlsxService xlsxService;
	@Autowired
	private IMessageResourceLanguageService messageResourceLanguageService;
		
	@Override
	public Map<Integer, Map<Integer, Map<String, MsgResLanguage>>> getMessageResourcesFull() {
		return messageResourceDao.getMessageResourcesFull();		
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void insertMessageResourcesByFile(MsgResFile msgResFile) throws Exception {
		List<List<String>> docInfo = xlsxService.readDocument(msgResFile.getFileUpload());
		List<MsgResLanguage> listMsgResLanguage = new ArrayList<MsgResLanguage>();
		for (List<String> line : docInfo) {
			MsgRes msgRes = new MsgRes();
			msgRes.setActionSourceId(msgResFile.getActionSourceId());
			msgRes.setKey(line.get(0));
			MsgResLanguage msgResLanguage = new MsgResLanguage();
			msgResLanguage.setMsgRes(msgRes);
			msgResLanguage.setMsgResLanguageId(msgResFile.getLanguageId());
			msgResLanguage.setWriterId(msgResFile.getWriterId());
			if (msgResFile.isAddKey()) {
				if (msgResFile.getTypeId() == MsgResTypeEnum.LARGE_VALUE.getId()) {
					msgResLanguage.setLargeValue(line.get(1));
				} else {
					msgResLanguage.setValue(line.get(1));
				}
				List<MsgResLanguage> listMrl = new ArrayList<MsgResLanguage>();
				listMrl.add(msgResLanguage);
				msgRes.setTypeId(msgResFile.getTypeId());
				insertMessageResources(msgRes, listMrl);
			} else {
				List<MsgResLanguage> listMissingMsgResLanguage = messageResourceLanguageService.getKeyMissingLanguage(msgRes);
				for (MsgResLanguage mrl : listMissingMsgResLanguage) {
					if (mrl.getMsgResLanguageId() == msgResFile.getLanguageId()) {
						if (mrl.getMsgRes().getTypeId() == MsgResTypeEnum.LARGE_VALUE.getId()) {
							msgResLanguage.setLargeValue(line.get(1));
						} else {
							msgResLanguage.setValue(line.get(1));
						}
						listMsgResLanguage.add(msgResLanguage);
					}
				}
			}
		}
		if (null != listMsgResLanguage && !listMsgResLanguage.isEmpty()) {
			messageResourceLanguageService.insertMessageResourcesLanguages(listMsgResLanguage);
		}
	}
	
}