package capital.any.service.messageResource;

import java.util.Map;

import capital.any.model.base.MsgResFile;
import capital.any.model.base.MsgResLanguage;

/**
 * @author eran.levy
 *
 */
public interface IMessageResourceService extends capital.any.service.base.messageResource.IMessageResourceService {
	
	/**
	 * @return
	 */
	Map<Integer, Map<Integer, Map<String, MsgResLanguage>>> getMessageResourcesFull();
	
	/**
	 * 
	 * @param msgResFile
	 * @throws Exception 
	 */
	void insertMessageResourcesByFile(MsgResFile msgResFile) throws Exception;
	
}
