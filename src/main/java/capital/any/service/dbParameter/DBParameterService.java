package capital.any.service.dbParameter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import capital.any.dao.dbParameter.IDBParameterDao;
import capital.any.model.base.DBParameter;

/**
 * @author EyalG
 *
 */
@Service("DBParameterServiceCRM")
public class DBParameterService extends capital.any.service.base.dbParameter.DBParameterService implements IDBParameterService {
	
	private static final Logger logger = LoggerFactory.getLogger(DBParameterService.class);
	
	@Autowired @Qualifier("DBParameterDaoCRM")
	protected IDBParameterDao dbParameterDao;
	@Autowired @Qualifier("DBParameterHCDaoCRM")
	protected IDBParameterDao dbParameterHCDao;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insert(DBParameter dbParameter) throws Exception {
		boolean result = dbParameterDao.insertDBParameters(dbParameter);
		if (result) {
			dbParameterHCDao.insertDBParameters(dbParameter);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean update(DBParameter dbParameter) throws Exception {
		boolean result = dbParameterDao.updateDBParameters(dbParameter);
		if (result) {
			result = dbParameterHCDao.updateDBParameters(dbParameter);
		}
		return result;
	}	
}
