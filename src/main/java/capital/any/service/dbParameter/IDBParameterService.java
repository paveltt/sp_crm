package capital.any.service.dbParameter;

import capital.any.model.base.DBParameter;

/**
 * @author EyalG
 *
 */
public interface IDBParameterService extends capital.any.service.base.dbParameter.IDBParameterService {	
	
	/**
	 * insert db parameter to db and HC
	 * @param dbParameter
	 * @throws Exception
	 */
	void insert(DBParameter dbParameter) throws Exception;
	
	/**
	 * update db parameter by id in db and HC
	 * @param dbParameter
	 * @return true if success else false
	 * @throws Exception
	 */
	boolean update(DBParameter dbParameter) throws Exception;
}
