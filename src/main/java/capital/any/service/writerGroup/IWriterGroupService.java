package capital.any.service.writerGroup;

import java.util.Map;

import capital.any.model.WriterGroup;

/**
 * @author eranl
 *
 */
public interface IWriterGroupService  {	
	
	/**
	 * @param writerGroup
	 */
	void insert(WriterGroup writerGroup);
	
	
	/**
	 * 
	 */
	Map<Integer, WriterGroup> get();

		
}
