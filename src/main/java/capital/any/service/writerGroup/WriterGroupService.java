package capital.any.service.writerGroup;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.writerGroup.IWriterGroupDao;
import capital.any.model.WriterGroup;

/**
 * @author eranl
 *
 */
@Service
public class WriterGroupService implements IWriterGroupService {
	
	@Autowired
	private IWriterGroupDao writerGroupDao;

	@Override
	public void insert(WriterGroup writerGroup) {
		writerGroupDao.insert(writerGroup);
	}

	@Override
	public Map<Integer, WriterGroup> get() {
		return writerGroupDao.get();
	}
	
	
	
}
