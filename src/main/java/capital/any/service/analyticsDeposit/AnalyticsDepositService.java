package capital.any.service.analyticsDeposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.base.analyticsDeposit.IAnalyticsDepositDao;
import capital.any.model.base.analytics.AnalyticsDeposit;

/**
 * 
 * @author eyal.ohana
 *
 */
@Service
public class AnalyticsDepositService implements IAnalyticsDepositService {
	
	@Autowired
	private IAnalyticsDepositDao analyticsDepositDao;
	
	@Override
	public AnalyticsDeposit get() {
		return analyticsDepositDao.get();
	}

}
