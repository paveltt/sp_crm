package capital.any.service.analyticsDeposit;

import capital.any.model.base.analytics.AnalyticsDeposit;

/**
 * 
 * @author eyal.ohana
 *
 */
public interface IAnalyticsDepositService {

	AnalyticsDeposit get();

}
