package capital.any.service.contact;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import capital.any.base.enums.ActionSource;
import capital.any.model.base.Contact;
import capital.any.model.base.Contact.Clazz;
import capital.any.model.base.Contact.Type;
import capital.any.model.base.Country;
import capital.any.model.base.Language;
import capital.any.service.IUtilService;
import capital.any.service.base.IServerConfiguration;
import capital.any.service.base.country.ICountryService;
import capital.any.service.base.language.ILanguageService;
import capital.any.service.base.xlsx.IXlsxService;

/**
 * @author eran.levy
 *
 */
@Service("ContactServiceCRM")
public class ContactService extends capital.any.service.base.contact.ContactService  implements IContactService {
	
	private static final Logger logger = LoggerFactory.getLogger(ContactService.class);
			
	public static int FIRST_NAME_COLUMN = 0;
	public static int LAST_NAME_COLUMN 	= 1;
	public static int COUNTRY_COLUMN 	= 2;
	public static int EMAIL_COLUMN 		= 3;
	public static int MOBILE_COLUMN 	= 4;	
	public static int LANGUAGE_COLUMN	= 5;
	public static int IP_COLUMN 		= 6;
	public static int TYPE_COLUMN 		= 7;
	public static int COMMENTS_COLUMN 	= 8;	
	public static String EMPTY_COLUMN	= "*"; 
	
	@Autowired
	private IXlsxService xlsxService;
	@Autowired
	private IServerConfiguration serverConfiguration;	
	@Autowired
	private IUtilService utilService;
	@Autowired
	private ICountryService countryService;
	@Autowired
	private ILanguageService languageService;

	/**
	 * XLSX file should contain those fields
	 * #1 column - first name - optional
	 * #2 column - last name - optional
	 * #3 column - country - 2 letter code - ISO 3166-1 alpha-2 - mandatory when mobile is not empty
	 * #4 column - email (email or mobile are mandatory)
	 * #5 column - mobile (email or mobile are mandatory)    
	 * #6 column - language - mandatory 
	 * #7 column - ip (IPV4) - optional
	 * #8 column - type id - Contact --> type enum - optional
	 * #9 column - comments - optional    
	 ***/	
	@Override
	public void uploadContacts(MultipartFile fileUpload, int writerId) {
		List<List<String>> docInfo = xlsxService.readDocument(fileUpload);
		int index = 0;
		for (List<String> line : docInfo) {
			index++;
			Contact contact = new Contact();
			// set default parameters 
			contact.setClazz(Clazz.REAL); 
			contact.setIsContactBySms(1);
			contact.setIsContactByEmail(1);
			contact.setActionSource(ActionSource.CRM);
			// parameters from excel
			contact.setFirstName(getColumnValue(line.get(FIRST_NAME_COLUMN)));
			contact.setLastName(getColumnValue(line.get(LAST_NAME_COLUMN)));
			String a2Country = getColumnValue(line.get(COUNTRY_COLUMN));
			boolean a2CountryEmpty = utilService.isArgumentEmptyOrNull(a2Country);
			if (!a2CountryEmpty) {
				Country country = countryService.getByA2(a2Country.toUpperCase());
				if (country != null) {
					contact.setCountryId(country.getId());	
				}				
			}
			String email = getColumnValue(line.get(EMAIL_COLUMN));
			boolean isEmailEmpty = utilService.isArgumentEmptyOrNull(email);
			boolean isEmailValid = true;
			if (!utilService.isArgumentEmptyOrNull(email)) {
				isEmailValid = serverConfiguration.isValidEmail(email);
				if (isEmailValid) {
					contact.setEmail(email);
				} else {
					logger.warn("Email" + email + " is not valid, row:" + index + ", moving to the next line");
				}
			}
			String mobile = getColumnValue(line.get(MOBILE_COLUMN));
			boolean isMobileEmpty = utilService.isArgumentEmptyOrNull(mobile);
			boolean isMobileValid = true;
			if (!isMobileEmpty) {
				if (mobile.length() < 7 || !NumberUtils.isNumber(mobile) || contact.getCountryId() == 0) {
					isMobileValid = false;
				}				
				if (isMobileValid) {
					contact.setMobilePhone(mobile);					
				} else {
					logger.warn("Mobile" + mobile + " is not valid, row:" + index);
				}
			}
			// email or mobile are mandatory fields
			if ((isMobileEmpty && isEmailEmpty) || // when email and mobile are missing
					(!isEmailValid && !isMobileValid) || // when email and mobile are invalid
					(!isEmailEmpty && !isEmailValid && isMobileEmpty) || // when mobile is missing and email invalid
					(!isMobileEmpty && !isMobileValid && isEmailEmpty) // when email is missing and mobile invalid 					
					) {
				continue;
			}
			
			String type = getColumnValue(line.get(TYPE_COLUMN));
			if (!utilService.isArgumentEmptyOrNull(type)) {
				contact.setType(Type.getByToken(Integer.valueOf(type)));
			} else {
				contact.setType(Type.IMPORTED);
			}			
			contact.setIp(getColumnValue(line.get(IP_COLUMN)));
			contact.setComments(getColumnValue(line.get(COMMENTS_COLUMN)));
			contact.setWriterId(writerId);
			
			String languageCode = getColumnValue(line.get(LANGUAGE_COLUMN));
			boolean languageCodeEmpty = utilService.isArgumentEmptyOrNull(languageCode);
			int languageId = 0;
			if (!languageCodeEmpty) {
				Language language = languageService.getByCode(languageCode.toUpperCase());
				if (language != null) {
					languageId = language.getId();	
				}				
			}
			if (languageId == 0) {
				languageId = 2; // EN - default
				logger.warn("language was not found. insert default language");
			}
			contact.setLanguageId(languageId);						
			contactDao.insert(contact);
		}
	}	
	
	
	/**
	 * In case of empty column we will return null as a string 
	 * @param columnValue
	 * @return
	 */
	private String getColumnValue(String columnValue) {		
		if (columnValue.equals(EMPTY_COLUMN)) {
			columnValue = null; 
		}
		return columnValue;
	}
	
}