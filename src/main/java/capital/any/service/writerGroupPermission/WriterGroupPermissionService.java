package capital.any.service.writerGroupPermission;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.writerGroupPermission.IWriterGroupPermissionDao;
import capital.any.model.BeSpace;
import capital.any.model.WriterGroupPermission;

/**
 * @author eranl
 *
 */
@Service
public class WriterGroupPermissionService implements IWriterGroupPermissionService {
	
	@Autowired
	private IWriterGroupPermissionDao writerGroupPermissionDao;

	@Override
	public void insert(WriterGroupPermission writerGroupPermission) {
		writerGroupPermissionDao.insert(writerGroupPermission);
	}
	@Override
	public Map<String, BeSpace> getWriterPermissions(long writerId) {		
		return writerGroupPermissionDao.getWriterPermissions(writerId);
	}
	
}
