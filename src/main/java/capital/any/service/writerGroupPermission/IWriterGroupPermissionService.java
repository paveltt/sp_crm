package capital.any.service.writerGroupPermission;

import java.util.Map;

import capital.any.model.BeSpace;
import capital.any.model.WriterGroupPermission;

/**
 * @author eranl
 *
 */
public interface IWriterGroupPermissionService  {	
		
	/**
	 * @param writerGroupPermission
	 */
	void insert(WriterGroupPermission writerGroupPermission);
		
	Map<String, BeSpace> getWriterPermissions(long writerId);
		
}
