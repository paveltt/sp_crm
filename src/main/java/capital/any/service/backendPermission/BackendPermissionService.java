package capital.any.service.backendPermission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import capital.any.dao.backendPermission.IBackendPermissionDao;
import capital.any.model.BePermission;

/**
 * @author eranl
 *
 */
@Service
public class BackendPermissionService implements IBackendPermissionService {
	
	@Autowired
	private IBackendPermissionDao backendPermissionDao;

	@Override
	public void insert(BePermission bePermission) {
		backendPermissionDao.insert(bePermission);
	}
	
}
