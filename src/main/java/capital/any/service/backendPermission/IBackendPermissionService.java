package capital.any.service.backendPermission;

import capital.any.model.BePermission;

/**
 * @author eranl
 *
 */
public interface IBackendPermissionService  {	
	
	/**
	 * @param bePermission
	 */
	void insert(BePermission bePermission);
		
}
