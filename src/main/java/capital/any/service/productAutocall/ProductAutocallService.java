package capital.any.service.productAutocall;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import capital.any.dao.productAutocall.IProductAutocallDao;
import capital.any.model.base.ProductAutocall;

/**
 * @author EyalG
 *
 */
@Service("ProductAutocallServiceCRM")
public class ProductAutocallService extends capital.any.service.base.productAutocall.ProductAutocallService implements IProductAutocallService {
	
	@Autowired @Qualifier("ProductAutocallDaoCRM")
	private IProductAutocallDao productAutocallDao;

	@Override
	public void insert(ProductAutocall productAutocall) {
		productAutocallDao.insert(productAutocall);
	}

	@Override
	public void insert(List<ProductAutocall> productAutocalls) {
		productAutocallDao.insert(productAutocalls);		
	}

	@Override
	public boolean update(ProductAutocall productAutocall) {
		return productAutocallDao.update(productAutocall);
	}
}
