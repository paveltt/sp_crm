package capital.any.service.productAutocall;

import java.util.List;

import capital.any.model.base.ProductAutocall;

/**
 * @author EyalG
 *
 */
public interface IProductAutocallService extends capital.any.service.base.productAutocall.IProductAutocallService {	
	
	/**
	 * insert Product Autocall
	 *
	 */
	void insert(ProductAutocall productAutocall);
	
	/**
	 * insert batch Product Autocalls
	 * @param list of productAutocalls
	 */
	void insert(List<ProductAutocall> productAutocalls);
	
	/**
	 * update by id
	 * @param productAutocall
	 * @return true if we update else false
	 */
	boolean update(ProductAutocall productAutocall);
}
