backendApp.controller('AdminCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Permissions', 'MenuTree', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Permissions, MenuTree) {
	
}]);


backendApp.controller('WireWithdrawCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect) {
	$scope.submitForm = function(isValid) {};
}]);


backendApp.controller('AnalyticsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','$state', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, $state) {
	$scope.chart1 = {};
	$scope.chart2 = {};
	var methodRequest = {};
	methodRequest.data = {};
	$http.post('analytics/user/get', methodRequest).then(
		function(response) {
			$scope.countUsers = response.data.data.countUsers;
			var campaigns = response.data.data.listAnalyticsUserCampaign;
			var users = response.data.data.listAnalyticsUserTime;
			var campaignsNameArr = [];
			var campaignsCountArr = [];
			for (campaign in campaigns) {
				if (campaigns.hasOwnProperty(campaign)) {
					campaignsNameArr.push(
						campaigns[campaign].marketingCampaignName
					);
					campaignsCountArr.push(
						campaigns[campaign].countUsers
					);
				}
			}
			
			$scope.chart1.labels = campaignsNameArr;
			//$scope.chart1.series = ['Series A', 'Series B'];
			$scope.chart1.data = campaignsCountArr;
			$scope.chart1.options = { 
					showTooltips: true,
					legend: { 
						labels: {
							fontSize: 30
						},
						display: true ,
						
					}
					
			};
			
					
			

			var usersCountArr = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
			var usersTimeArr =  [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
			for (user in users) {
				if (users.hasOwnProperty(user)) {
					usersCountArr.splice(users[user].timeOfDay, 0 ,users[user].countUsers);
//					usersTimeArr.push(
//						users[user].timeOfDay
//					);
				}
			}
			$scope.chart2.labels = usersTimeArr;
			$scope.chart2.series = ['# of registered users by hour'];
			$scope.chart2.data = [usersCountArr];
			$scope.chart2.options = { 
					legend: {
						display: true 
						} 
			};
		}, function(response) {
			$scope.hideLoading();
			$scope.handleNetworkError(response);
		});
	


	
	
	
	

	  
	// Simulate async data update
//	$timeout(function () {
//	$scope.data = [
//	    [28, 48, 40, 19, 86, 27, 90],
//	    [65, 59, 80, 81, 56, 55, 40]
//	  ];
//	 }, 3000);
	
	
}]);



function getTinyMceOptions(tinymceParams){
	return {
		selector: 'textarea#annex',
		menubar: 'view edit insert table format tools',
		plugins: 'advlist autolink link image lists table paste searchreplace charmap fullscreen noneditable code',
		//content_css: 'https://www.anycapital.com/css/style.css?version=' + new Date().getTime(),
		content_style: 'body{background:none!important}',
		paste_convert_word_fake_lists: true,
		paste_remove_styles_if_webkit: true,
		paste_remove_styles: true,
		paste_strip_class_attributes: true,
		table_appearance_options: false,
		/*invalid_elements : 'table,thead,tbody,tr,td',*/
		style_formats: [
			{ title: 'Item title', block : 'h3', exact : true },
			{ title: 'Green list', selector : 'ul', wrapper: true, classes : 'green-list', exact : true },
			{ title: 'Footer big text', selector : 'p', wrapper: true, classes : 'footer-big', exact : true }
		],
		formats: {
		},
		setup: function (editor) {
			for(var i = 0; i < tinymceParams.length; i++){
				var insertParam = function(param){
					var content = '<span class="mceNonEditable">${' + param.name + '}</span>';
					if(param.display == 'block'){
						content = '<div class="mceNonEditable">${' + param.name + '}</div><br/>';
					}
					editor.addMenuItem('insert' + param.name, {
						text: param.title,
						context: 'insert',
						onclick: function () {
							editor.insertContent(content);
						}
					});
				}(tinymceParams[i]);
			}
			editor.addMenuItem('custominsertseparator', {
				text: '-',
				context: 'insert'
			});
		}
	};
}