backendApp.controller('DBParametersCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination, growl) {		
	$scope.action = 'loading';
	$scope.selectedDBParameter = {};
	$scope.newDBParameter = {};
	$scope.filtered = {};
	$scope.filtered.DBParameterArr = [];
	$scope.DBParameterArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.DBParameterArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.DBParameterArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$rootScope.initScreenCtr($scope);
	$scope.search = function() {
		$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data = 0;
        $http.post('dbparameter/get', methodRequest).then(
        function (response) {
        		$scope.isLoading = false;
				var dbParameters = response.data.data;
				var dbParametersArr = [];
				for (dbParam in dbParameters) {
					if (dbParameters.hasOwnProperty(dbParam)) {
						dbParametersArr.push({
						id: dbParameters[dbParam].id,
						name:dbParameters[dbParam].name,						
						numValue:dbParameters[dbParam].numValue,
						stringValue:dbParameters[dbParam].stringValue,
						dateValue:dbParameters[dbParam].dateValue,
						comments:dbParameters[dbParam].comments,
	                    })
                   }
				}
				$scope.DBParameterArr = dbParametersArr;
				$scope.pagination.setPages($scope.page, $scope.DBParameterArr.length, $scope.resultsPerPage);
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
		
	}
	$scope.checkAction = function(){
		if($rootScope.$state.includes("ln.admin.dbParameters.edit")){
			$scope.action = 'edit';
			$scope.id = $rootScope.$stateParams.id;
			$scope.getSelectedDBParameter();
		}else if($rootScope.$state.includes("ln.admin.dbParameters.add")){
			$scope.action = 'add';
		}
		else{
			$scope.action = '';
		}
	}
	$scope.add = function(){
		$rootScope.$state.go('ln.admin.dbParameters.add', {ln: $rootScope.$state.params.ln});
	}
	$scope.addDBParameter = function(){
		var methodRequest = {};
	    methodRequest.data = 0;
	    methodRequest.data = {
	    	    name: $scope.newDBParameter.name,
	    	    numValue: $scope.newDBParameter.numValue,
	    	    stringValue: $scope.newDBParameter.stringValue,
	    	    dateValue: $scope.newDBParameter.dateValue,
	    	    comments: $scope.newDBParameter.comments,
	    }
        $http.post('dbparameter/insert', methodRequest).then(
		function (response) {
			$scope.hideLoading();
			$rootScope.$state.go('ln.admin.dbParameters', {ln: $rootScope.$state.params.ln});
			if(response.data.responseCode==0){
				growl.success('dbParameters added');
			}
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
        $scope.newDBParameter={};
        $rootScope.$state.go('ln.admin.dbParameters', {ln: $rootScope.$state.params.ln});
	}
	$scope.getSelectedDBParameter = function() {
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.id,			
		};
		$http.post('dbparameter/get/id', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'id')) {
					$scope.selectedDBParameter = Utils.parseResponse(response);
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.save = function() {
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.selectedDBParameter.id,
			name: $scope.selectedDBParameter.name,
			numValue: $scope.selectedDBParameter.numValue,
			stringValue: $scope.selectedDBParameter.stringValue,
			dateValue: $scope.selectedDBParameter.dateValue,
			comments: $scope.selectedDBParameter.comments,
		};
		$http.post('dbparameter/update', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'update')) {
					$scope.selectedDBParameter = Utils.parseResponse(response);
					$rootScope.$state.go('ln.admin.dbParameters', {ln: $rootScope.$state.params.ln});
					if(response.data.responseCode==0){
						growl.success('dbparameter update');
					}
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
}]);


backendApp.controller('writersCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination, growl) {
	$scope.filter = {};
	$scope.filter.writerGroups;
	$scope.action = 'loading';
	$scope.selectedWriter = {};
	$scope.newWriter = {};
	$scope.filtered = {};
	$scope.filtered.writerArr = [];
	$scope.writerArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.initFiltersOnReady = function () {
		  $scope.filter.writerGroups = new SingleSelect.InstanceClass();  
	}	
	var writer_groups = "writer_groups";
	var methodRequest = {};
	methodRequest.data = 0;
    methodRequest.data = [];
    methodRequest.data.push (
    		writer_groups
    )
	$http.post('filters/get', methodRequest).then(
		function (response) {
			$scope.hideLoading();
				$scope.init = Utils.parseResponse(response);
				$scope.filter.writerGroups.fillOptions($scope.parseFilter(Utils.parseResponse(response).writer_groups));
		}, function (response) {
			$scope.hideLoading();
			$scope.handleNetworkError(response);
		});		
	$scope.parseFilter = function (filter) {
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].name});
			}
		}
		return arr;
	}
	
	
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.writerArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.writerArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$rootScope.initScreenCtr($scope);
	$scope.search = function() {
		$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data = 0;
        $http.post('writer/get', methodRequest).then(
        function (response) {
        		$scope.isLoading = false;
				var writers = response.data.data;
				var writersArr = [];
				for (writer in writers) {
					if (writers.hasOwnProperty(writer)) {
						writersArr.push({
						id: writers[writer].id,
						firstName:writers[writer].firstName,						
						lastName:writers[writer].lastName,
						userName:writers[writer].userName,
						timeCreated:writers[writer].timeCreated,
						email:writers[writer].email,
						active:writers[writer].active,
						groupId:writers[writer].groupId,
						comments:writers[writer].comments,
	                    })
                   }
				}
				$scope.writerArr = writersArr;
				$scope.pagination.setPages($scope.page, $scope.writerArr.length, $scope.resultsPerPage);
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
		
	}
	$scope.checkAction = function(){
		if($rootScope.$state.includes("ln.admin.writers.edit")){
			$scope.action = 'edit';
			$scope.id = $rootScope.$stateParams.id;
			$scope.getSelectedWriter();
		}else if($rootScope.$state.includes("ln.admin.writers.add")){
			$scope.action = 'add';
		}
		else{
			$scope.action = '';
		}
	}
	$scope.add = function(){
		$rootScope.$state.go('ln.admin.writers.add', {ln: $rootScope.$state.params.ln});
	}
	$scope.addWriter = function(){
		var methodRequest = {};
	    methodRequest.data = 0;
	    methodRequest.data = {	    	    
				firstName: $scope.newWriter.firstName,						
				lastName: $scope.newWriter.lastName,
				userName: $scope.newWriter.userName,
				password: $scope.newWriter.password,
				email: $scope.newWriter.email,
				isActive: $scope.newWriter.active,
				groupId: $scope.filter.writerGroups.getId(),
				comments: $scope.newWriter.comments,					    		
	    }
        $http.post('writer/insert', methodRequest).then(
		function (response) {
			$scope.hideLoading();
			$rootScope.$state.go('ln.admin.writers', {ln: $rootScope.$state.params.ln});
			if(response.data.responseCode==0){
				growl.success('writer added');
			}
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
        $scope.newWriter={};
        $rootScope.$state.go('ln.admin.writers', {ln: $rootScope.$state.params.ln});
	}
	$scope.getSelectedWriter = function() {
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.id,			
		};
		$http.post('writer/get/id', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'id')) {
					$scope.selectedWriter = Utils.parseResponse(response);
					$scope.filter.writerGroups.setModel($scope.filter.writerGroups.getOptionById($scope.selectedWriter.groupId));					
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.save = function() {
		var methodRequest = {};
		methodRequest.data = {			
				id: $scope.selectedWriter.id,
				firstName: $scope.selectedWriter.firstName,						
				lastName: $scope.selectedWriter.lastName,
				userName: $scope.selectedWriter.userName,
				email: $scope.selectedWriter.email,
				isActive: $scope.selectedWriter.active,
				comments: $scope.selectedWriter.comments,
				groupId: $scope.filter.writerGroups.getId(),
		};
		$http.post('writer/update', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'update')) {
					$scope.selectedWriter = Utils.parseResponse(response);
					$rootScope.$state.go('ln.admin.writers', {ln: $rootScope.$state.params.ln});
					if(response.data.responseCode==0){
						growl.success('writer update');
					}
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
}]);
backendApp.controller('WriterChangePassCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect,growl) {
	$scope.submitForm = function(isValid) {};
	$scope.changePassword = function() {
		var methodRequest = {};
	    methodRequest.data = 0;
	    methodRequest.data =  {
	    		password: $scope.newPassword,
	    		retypePassword: $scope.verifyPassword 
	    		};
        $http.post('writer/changePassword', methodRequest).then(
		       function (response) {
		    	   hideLoading();  
		    	   if(response.data.responseCode==0)
		    		   {
		    	   		growl.success('password changed'); 
		    		    $scope.hide = true;
		    		   }
			}, function (response) {
				$scope.hide = false;
			});
	}
}]);
backendApp.controller('AdminPanelCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect,growl) {
	$scope.submitForm = function(isValid) {};
	$scope.reloadHC = function() {
		var methodRequest = {};
        $http.post('jmx/reloadHC', methodRequest).then(
		       function (response) {
		    	   hideLoading();  
		    	   if(response.data.responseCode==0)
		    		   {
		    	   		growl.success('Success to reload HazelCast'); 
		    		    $scope.hide = true;
		    		   }
			}, function (response) {
				$scope.hide = false;
			});
	}
}]);
