package capital.any.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Request;
import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.WriterChangePassword;
import capital.any.model.WriterChangePassword.WriterChangePasswordValidatorGroup;
import capital.any.model.base.Writer;
import capital.any.security.ISecurityService;
import capital.any.service.writer.IWriterService;

/**
 * @author LioR SoLoMoN
 *
 */
@RestController
@RequestMapping(value = "/writer", method = RequestMethod.POST)
public class WriterController {
	private static final Logger logger = LoggerFactory.getLogger(WriterController.class);
	
	@Autowired
	@Qualifier("WriterServiceCRM")
	private IWriterService writerService;
	
	@Autowired
	private ISecurityService securityService;
	
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<Map<Integer, Writer>>> get() {
		logger.info("about to get all writers");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		Map<Integer, Writer> list = null;
		try {
			if (securityService.isAuthenticate()) {
				list = writerService.get();
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting all writers", e);
		}
		Response<Map<Integer, Writer>> response = new Response<Map<Integer, Writer>>(list, responseCode);
		return new ResponseEntity<Response<Map<Integer, Writer>>>(response, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/insert")
	public ResponseEntity<Response<Writer>> insert(@RequestBody Request<Writer> request) {
		logger.debug("insert writer  " + request);
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		Writer writer = null;
		try {
			if (securityService.isAuthenticate()) {
				writer = (Writer) request.getData();
				writerService.insert(writer);
				responseCode = ResponseCode.OK;
			}
		} catch (Exception e) {			
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on insert writer ", e);
		}
		Response<Writer> response = new Response<Writer>(writer, responseCode);
		return new ResponseEntity<Response<Writer>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update")
	public ResponseEntity<Response<Object>> update(@RequestBody Request<Writer> request) {
		logger.info("start update writer");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		try {
			if (securityService.isAuthenticate()) {
				Writer writer = (Writer) request.getData();
				if (writerService.update(writer)) {
					responseCode = ResponseCode.OK;
				} else {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on update writer.", e);
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get/id")
	public ResponseEntity<Response<Writer>> getById(@RequestBody Request<Writer> request) {
		logger.info("start get writer by id");
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED;
		Writer writer = null;
		try {
			if (securityService.isAuthenticate()) {
				Writer writerRequest = (Writer) request.getData();
				writer = writerService.getWriterById(writerRequest.getId());
				if (writer != null) {
					responseCode = ResponseCode.OK;
				} else {
					responseCode = ResponseCode.INVALID_INPUT;
				}
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error get writer by id.", e);
		}
		Response<Writer> response = new Response<Writer>(writer, responseCode);
		return new ResponseEntity<Response<Writer>>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changePassword")
	public ResponseEntity<Response<Object>> changePassword(@Validated({WriterChangePasswordValidatorGroup.class}) @RequestBody Request<WriterChangePassword> request) {		
		ResponseCode responseCode = ResponseCode.UNKNOWN_ERROR; 
		try {
			if (securityService.isAuthenticate()) {
				Writer writer = writerService.getLoginFromSession();
				WriterChangePassword writerChangePassword = (WriterChangePassword) request.getData();				
				writer.setPassword(writerChangePassword.getPassword());
				boolean result = writerService.changePassword(writer);
				if (result) {
					responseCode = ResponseCode.OK;
				}
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			logger.error("ERROR! change writer password", e);
			responseCode = ResponseCode.UNKNOWN_ERROR;
		}
		Response<Object> response = new Response<Object>(null, responseCode);
		return new ResponseEntity<Response<Object>>(response, HttpStatus.OK);
	}
		
	/**
	 * @return Response with the writer. writer is null if not found in the session.
	 */
	@RequestMapping(value = "/isLoggedIn")
	public ResponseEntity<Response<Writer>> isLoggedIn(@RequestBody Request<Object> request) {
		logger.info("isLogin - get current login");
		Writer writer = writerService.getLoginFromSession();
		List<String> messages = new ArrayList<String>();;
		String msg = "not_logged_in";
		ResponseCode responseCode = ResponseCode.ACCESS_DENIED; 
		if (writer != null) {
			responseCode = ResponseCode.OK;
			msg = "logged_in";
		}
		messages.add(msg);
		Response<Writer> response = new Response<Writer>(writer, responseCode, messages);
		
		logger.info(response.toString());
		return new ResponseEntity<Response<Writer>>(response, HttpStatus.OK);	
	}
}
