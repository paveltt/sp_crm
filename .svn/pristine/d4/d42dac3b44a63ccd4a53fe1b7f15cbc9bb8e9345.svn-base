package capital.any.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import capital.any.communication.Response;
import capital.any.communication.Response.ResponseCode;
import capital.any.model.base.analytics.AnalyticsInvestment;
import capital.any.security.ISecurityService;
import capital.any.service.analyticsInvestment.IAnalyticsInvestmentService;

/**
 * 
 * @author eyal.ohana
 *
 */
@RestController
@RequestMapping(value = "/analytics/investment", method = RequestMethod.POST)
public class AnalyticsInvestmentController {
	private static final Logger logger = LoggerFactory.getLogger(AnalyticsInvestmentController.class);

	@Autowired
    private ISecurityService securityService;
	@Autowired
	private IAnalyticsInvestmentService analyticsInvestmentService;
	
	@RequestMapping(value = "/get")
	public ResponseEntity<Response<AnalyticsInvestment>> get() {
		logger.info("About to get AnalyticsInvestment.");
		ResponseCode responseCode = ResponseCode.OK;
		AnalyticsInvestment analyticsInvestment = null;
		try {
			if (securityService.isAuthenticate()) {
				analyticsInvestment = analyticsInvestmentService.getAnalyticsInvestment();
			} else {
				responseCode = ResponseCode.ACCESS_DENIED;
			}
		} catch (Exception e) {
			responseCode = ResponseCode.UNKNOWN_ERROR;
			logger.error("Error on getting AnalyticsInvestment. ", e);
		}
		Response<AnalyticsInvestment> response = new Response<AnalyticsInvestment>(analyticsInvestment, responseCode);
		return new ResponseEntity<Response<AnalyticsInvestment>>(response, HttpStatus.OK);
	}
}
