backendApp.controller('AllTransactionsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination) {	
	$scope.filter = {};
	$scope.filter.from;
	$scope.filter.to;
	$scope.filter.searchBy;
	$scope.filter.transactionType;
	$scope.filter.transactionStatus;
	$scope.dateManager = new Utils.DateManager();
	$scope.transactionsArr = [];
	$scope.filtered = {};
	$scope.filtered.transactionsArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.filter.userClass;
	$scope.searchDisabled = true;
	$scope.isSearchDisabled  = function () {
		return $scope.searchDisabled; 
	} 			
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.transactionsArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.transactionsArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$scope.initFiltersOnReady = function () {		
		$scope.filter.transactionStatus = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'},mutateCallback: $rootScope.getMsgs});
		$scope.filter.transactionType = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'},mutateCallback: $rootScope.getMsgs});
		$scope.filter.userClass = new SingleSelect.InstanceClass({aggregateOption: {id: null, name: 'All'}});
	}
	var initDependencies = {name: 'initList', dependencies: {getTransactionInit: false}};
	$rootScope.initScreenCtr($scope, initDependencies, function(){$scope.getTransactionInit('initList');});
    $scope.getTransactionInit = function (dependencyName) {
		$scope.resetGlobalErrorMsg($scope);
		$scope.showLoading();
		var transactionStatuses = "transaction_statuses";
		var transactionTypes = "transaction_payment_types";
		var user_class = "user_class";
		var methodRequest = {};
		methodRequest.data = 0;
	    methodRequest.data = [];
	    methodRequest.data.push (
	    		transactionStatuses,
	    		transactionTypes,
	    		user_class
	    )
		$http.post('filters/get', methodRequest).then(
			function (response) {
				$scope.hideLoading();
				$rootScope.markDependencyDone($scope, dependencyName, 'getTransactionInit');
				if (!$scope.handleErrors(response, 'getTransactionInit')) {
					$scope.init = Utils.parseResponse(response);
					$scope.filter.transactionStatus.fillOptions($scope.parseFilter(Utils.parseResponse(response).transaction_statuses));
					$scope.filter.transactionType.fillOptions($scope.parseFilter(Utils.parseResponse(response).transaction_payment_types));
					$scope.filter.userClass.fillOptions(Utils.parseResponse(response).user_class);
					$scope.filter.userClass.setModel($scope.filter.userClass.getOptionById(2));	
					$scope.searchDisabled = false;
				}
			}, function (response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});		
	}
	$scope.parseFilter = function (filter) {
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].displayName});
			}
		}
		return arr;
	}
	$scope.listTransactionsFilter = function (transaction) {
		var hide = false;
		// transaction type filter
		if ($scope.filter.transactionType.getId() > -1) {
				if($scope.filter.transactionType.getId() != transaction.typeId) {
					hide = true;
				}
		}	
		// transaction status filter
		if ($scope.filter.transactionStatus.getId() > -1) {
				if($scope.filter.transactionStatus.getId() != transaction.statusId) {
					hide = true;
				}
		}
//		// transaction created date filter        
//		if ($scope.filter.created != null) {
//			if( $scope.filter.created.getTime() != transaction.created) {
//				hide = true;
//			}
//		}
//		// transaction settled date filter        
//		if ($scope.filter.settled != null) {
//			if( $scope.filter.settled.getTime() != transaction.settled) {
//				hide = true;
//			}
//		}
		
		return !hide;
	}
	$scope.search = function() {
		$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data = {
	    		settledAtFrom : $scope.filter.from != null && $scope.filter.searchBy == '2'? $scope.filter.from.getTime() : null,
	    		settledAtTo : $scope.filter.to != null && $scope.filter.searchBy == '2'? ($scope.filter.to.getTime() + 24 * 60 * 60 * 1000 - 1000) : null,
	    		from : $scope.filter.from != null && $scope.filter.searchBy == '1'? $scope.filter.from.getTime() : null,
	    		to : $scope.filter.to != null && $scope.filter.searchBy == '1'? ($scope.filter.to.getTime() + 24 * 60 * 60 * 1000 - 1000) : null,
	    		userClassId: $scope.filter.userClass.getId()
	    };
        $http.post('transaction/getAll', methodRequest).then(
   		function (response) {
   				$scope.isLoading = false;
				$scope.getByUserId = response;
				var transactions = response.data.data;
				var transactionsArr = [];
				for (transaction in transactions) {
					if (transactions.hasOwnProperty(transaction)) {
						transactionsArr.push({
						id: transactions[transaction].id,
						created: transactions[transaction].timeCreated,
						settled: transactions[transaction].timeSettled,
						type: transactions[transaction].paymentType.displayName,
						typeId: transactions[transaction].paymentType.id,
						status: transactions[transaction].status.displayName,
						statusId: transactions[transaction].status.id,
						userId: transactions[transaction].user.id,
						ip: transactions[transaction].ip,
						comments: transactions[transaction].comments,
						amount: transactions[transaction].amount,
						actionSource: transactions[transaction].actionSource,
						clazz: transactions[transaction].clazz,
						operation: transactions[transaction].operation.displayName,
	                    })
                   }
				}
				$scope.transactionsArr = transactionsArr;
				$scope.updatePages();
   		}, function (response) {
				// failure callback
				hideLoading();
				$scope.handleNetworkError(response);
			});
	}	
	$scope.pending = function(transactionId) {
		$scope.transactionId = transactionId;
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/pending-popup.html',
			scope: $scope,
			controller: 'pendingPopupCtr',
			windowClass: 'pending-popup'
		});
	}
	
	$scope.loadUser = function(userId){
		Utils.loadUserAndRedirect(userId);
	}
}]);

backendApp.controller('pendingPopupCtr', ['$scope', '$http', 'growl', 'userService', function($scope, $http, growl, userService) {	
	$scope.approve = function() {
    var methodRequest = {};
    methodRequest.data = {
    		id:$scope.transactionId,
    		status:{
    			id:2
    		}
    };
    $http.post('transaction/deposit/approve', methodRequest).then(
			function(response) {
				if(response.data.responseCode == 0){
					growl.success('transaction '+ $scope.transactionId + ' has been approved');
					$scope.search();
					var user = {};
					user = userService.getLoadedUser();
					if(user.hasOwnProperty('id')){
						userService.loadUser(user.id,user.email);
					}
				}
	        },
	        function(response) {		

			}
        )
	}	
	$scope.notApprove = function() {
	    var methodRequest = {};
	    methodRequest.data = {
	    		id:$scope.transactionId,
	    		status:{
	    			id:3
	    		}
	    };
    $http.post('transaction/deposit/approve', methodRequest).then(
			function(response) {
				if(response.data.responseCode == 0){
					growl.success('transaction '+ $scope.transactionId + ' has failed');
					$scope.search();
					var user = {};
					user = userService.getLoadedUser();
					if(user.hasOwnProperty('id')){
						userService.loadUser(user.id,user.email);
					}
				}
	        },
	        function(response) {		

			}
        )
	}
}]);


