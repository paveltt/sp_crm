/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}


if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
	'use strict';
	if (this == null) {
	  throw new TypeError();
	}
	var n, k, t = Object(this),
		len = t.length >>> 0;

	if (len === 0) {
	  return -1;
	}
	n = 0;
	if (arguments.length > 1) {
	  n = Number(arguments[1]);
	  if (n != n) { // shortcut for verifying if it's NaN
		n = 0;
	  } else if (n != 0 && n != Infinity && n != -Infinity) {
		n = (n > 0 || -1) * Math.floor(Math.abs(n));
	  }
	}
	if (n >= len) {
	  return -1;
	}
	for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
	  if (k in t && t[k] === searchElement) {
		return k;
	  }
	}
	return -1;
  };
}

function cloneObj(obj){
	if(obj == null || typeof(obj) != 'object'){
		return obj;
	}
	if(obj instanceof Date){
		return new Date(obj);
	}

	var temp = obj.constructor(); // changed

	for(var key in obj){
		temp[key] = cloneObj(obj[key]);
	}
	return temp;
}

//check where ever something is undefined or null
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}

//use ?logType=num to load page with loggin on
//0:do not log,1:log info,2:log warnings,3:log errors,4:log all
var logIt_type_timer = ''; 
function logIt(params){//type,msg
	if((params.type === 1) && (settings.logIt_type === 1 || settings.logIt_type === 4)){//info
		console.info(params.msg);
	}
	else if((params.type === 2) && (settings.logIt_type === 2 || settings.logIt_type === 4)){//warnings
		console.warn(params.msg);
	}
	else if((params.type == 3) && (settings.logIt_type === 3 || settings.logIt_type === 4)){//errors
		console.error(params.msg);
	}
	// console.log('test.log') - just info
	// console.debug('test.debug') - just info
	// console.info('test.info') - with info ico
	// console.warn('test.warn') - with warning info
	// console.error('test.error') - with error ico
	if(logIt_type_timer == null){ 
		logIt_type_timer = setInterval(function(){console.clear()},1000*60*5);
	}
}

var error_stack = [];
function handleErrors(data, name) {//returns true on error
	angular.element('#backendAppHolder').scope().requestsStatus[data.serviceName] = 1;
	if (data == '') {
		logIt({'type':3,'msg':name});
		logIt({'type':3,'msg':data});
		alert('Response is empty string. Check eclipse console for exeptions.');
		return true;
	} else if (data.errorCode == null) {
		logIt({'type':3,'msg':name});
		logIt({'type':3,'msg':data});
		alert('Error code is null. Check eclipse console for exeptions. ');
		return true;
	} else if (data.errorCode > errorCodeMap.success) {//0
		var defMsg = angular.element('#backendAppHolder').scope().getMsgs('error-' + data.errorCode);
		logIt({'type':3,'msg':name + " (" + defMsg + ")"});
		logIt({'type':3,'msg':data});
		var globalErrorField;
		if (typeof data.globalErrorField != 'undefined') {
			globalErrorField = g(data.globalErrorField);
		} else if (g('globalErrorField') != null) {
			globalErrorField = g('globalErrorField');
		} else {
			globalErrorField = document.createElement('span');
			globalErrorField.id = 'globalErrorField';
			document.body.appendChild(globalErrorField);
		}
		globalErrorField.innerHTML = '';
		resetErrorMsgs(data);
		if (data.errorCode == errorCodeMap.session_expired) { //session expired 6999
			if (isUndefined(data.loginPage)) {
				try {
					/*angular.element('#backendAppHolder').scope().changeHeader(false);
					angular.element('#backendAppHolder').scope().$state.go('ln.login', {ln: settings.skinLanguage});
					angular.element('#backendAppHolder').scope().initSettings();*/
					window.location.hash = '';
					window.location.replace(window.location.pathname.replace(/index.html/, ''));
				} catch(e) {}
			}
		} else if(data.errorCode == errorCodeMap.date_range_too_large){
			addGlobalErrorMsg(angular.element('#backendAppHolder').scope().getMsgs('sales.deposits.date.range'));
		} else if(data.errorCode == errorCodeMap.role_name_exists){
			addGlobalErrorMsg(angular.element('#backendAppHolder').scope().getMsgs('permissions.role.name.exists'));
		} else {
			if (data.userMessages != null) {
				for (var i = 0; i < data.userMessages.length; i++) {
					if (data.userMessages[i].field == '' || data.userMessages[i].field == null) {
						globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
					} else {
						var input = g(data.userMessages[i].field);
						if (input != null) {
							input.className += " error";
						}
						var error_el_id = data.userMessages[i].field + '-error';
						error_stack.push(data.userMessages[i].field);
						var err_place = g(error_el_id);
						if (err_place != null) {
							err_place.innerHTML = data.userMessages[i].message;
						} else {
							var field_place = g(data.userMessages[i].field + '-container');
							if (field_place != null) {
								var el = document.createElement('span');
								el.id = error_el_id;
								el.className = 'error-message icon';
								el.innerHTML = data.userMessages[i].message;
								field_place.appendChild(el);
							} else {
								globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
							}
						}
					}
				}
			} else {
				logIt({'type':3,'msg':'unhandled errorCode: ' + data.errorCode});
				globalErrorField.innerHTML += "<span>" + defMsg + "</span>";
			}
		}
		return true;
	} else {
		logIt({'type':1,'msg':data});
		return false;
	}
}

function handleSuccess(data) {
	if (data.userMessages != null && data.userMessages[0].message != null) {
		alert(data.userMessages[0].message);
	} else {
		alert('Great, it works!')
	}
	logIt({'type':1,'msg':data});
}

function handleNetworkError(data) {
	// alert('Network error (aka 404, 500 etc)');
	logIt({'type':1,'msg':data});
}

function resetErrorMsgs(data) {
	if (typeof data.stopClear == 'undefined' || !data.stopClear) {
		for (var i = 0; i < error_stack.length; i++) {
			var error_holder = g(error_stack[i]);
			if (error_holder != null) {
				g(error_stack[i]).className = g(error_stack[i]).className.replace(/ error/g, '');
			}
			var error_holder = g(error_stack[i] + '-error');
			if (error_holder != null) {
				g(error_stack[i] + '-error').remove();
			}
		}
		error_stack = [];
	}
}

function addGlobalErrorMsg(data){
	$('[data-global-errors]').html('<span class="error">' + data + '</span>');
}

function resetGlobalErrorMsg(){
	$('[data-global-errors]').html('');
}

function searchJsonKeyInArray(arr, key, val) {
	if (!isUndefined(arr)) {
		for (var i = 0; i < arr.length; i++) {
			if (arr[i][key] == val) {
				return i;
			}
		}
	}
	return -1;
}
function searchJsonKeyInJson(obj, key, val) {
	if (!isUndefined(obj)) {
		for (var el in obj) {
			if (obj[el][key] == val) {
				return el;
			}
		}
	}
	return -1;
}
function formatAmount(params) {//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
	var amount = params.amount;
	var currency = params.currency;
	var centsPart = params.centsPart;
	var withoutDecimalPoint = params.withoutDecimalPoint;
	var round = params.round;
	try{
		if (!isNaN(amount)) {
			if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100){
				amount = amount/100;
			}
			var rtn = '';
			if (amount.toString().charAt(0) == '-') {
				rtn = '-';
				amount *= -1;
			}
			if (typeof withoutDecimalPoint != 'undefined' && withoutDecimalPoint) {
				amount = amountToFloat(amount);
			}
			if (typeof round == 'undefined' || !round || amount < 1000 ) {
				amount = Math.round(amount*100)/100;
				var tmp = amount.toString().split('.');
				var decimal = parseInt(tmp[0]);
				var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
				if((cents === '') || (cents === 0)){cents = '00';}
				else if(cents.length == '1'){cents = cents + '0';}
				else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}
				
				if(currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
				rtn += numberWithCommas(decimal);
				if(currency.decimalPointDigits == 0){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
					rtn += "<span>"+cents+"</span>";
				}
				else if((centsPart == 3) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 2) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else{
					rtn += "."+cents;
				}
				if(!currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
			} else {
				if(currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
				rtn += (Math.round(amount / 100)) / 10 + "K";
				if(!currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
			}
			return rtn;
		} else {
			return ' ';
		}
	} catch(e) {
		logIt({'type':3,'msg':e});
	}
}

function numberWithCommas(x) {//convert value with camas every 1000
	x = x.toString().replace(/,/g,'');
	var tmp = x.split('.');
	var rtn = tmp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	if(typeof tmp[1] != 'undefined'){
		rtn += '.' + tmp[1];
	}
	return rtn;
}

function checkOwnProperty(obj, property){
	if(obj.hasOwnProperty(property) && obj[property]){
		return true;
	}else{
		return false;
	}
}

var loadingOverlayId = 'loadingOverlay';
function showLoading(globalOverlay){
	if(($('.content').length > 0) && !document.getElementById(loadingOverlayId)){
		var overlay = document.createElement('div');
		overlay.id = loadingOverlayId;
		overlay.className = 'loading-overlay';
		if(globalOverlay){
			overlay.className += ' loading-overlay-global';
		}
		$('.content').append(overlay);
	}
}
function hideLoading(){
	if(document.getElementById(loadingOverlayId)){
		document.getElementById(loadingOverlayId).parentNode.removeChild(document.getElementById(loadingOverlayId));
	}
}

function showToast(type, message){
	var toast = document.createElement('div');
	toast.className = 'toast';
	if(type == TOAST_TYPES.error){
		toast.className += ' toast_error';
	}
	toast.innerHTML = message;
	toast.onclick = function(){removeToast(toast, true);};
	document.body.appendChild(toast);
	setTimeout(function(){removeToast(toast, false);}, 3000);
}
function removeToast(toast, forceClose){
	var removeDelay = 0.5;
	if(toast && !toast.isBeingRemoved){
		toast.isBeingRemoved = true;
		$(toast).css('visibility', 'hidden');
		$(toast).css('opacity', '0');
		$(toast).css('transition', 'visibility 0s ' + removeDelay + 's, opacity ' + removeDelay + 's linear');
		$(toast).on('transitionend msTransitionEnd webkitTransitionEnd',function(){
			if(toast){
				toast.parentNode.removeChild(toast);
				toast = null;
			}
		});
		//Backup for browsers where transitionend didn't fire
		setTimeout(function(){
			if(toast){
				if(toast.parentNode){
					toast.parentNode.removeChild(toast);
				}
				toast = null;
			}
		}, (removeDelay + 1)*1000);
	}else if(toast && toast.isBeingRemoved && forceClose){
		toast.parentNode.removeChild(toast);
		toast = null;
	}
}