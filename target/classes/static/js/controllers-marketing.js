backendApp.controller('SourcesCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination, growl) {		
	$scope.action = 'loading';
	$scope.selectedSource = {};
	$scope.newSource = {};
	$scope.filtered = {};
	$scope.filtered.sourcesArr = [];
	$scope.sourcesArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.sourcesArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.sourcesArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$rootScope.initScreenCtr($scope);
	$scope.search = function() {
		$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data = 0;
        $http.post('marketing/source/getAll', methodRequest).then(
        function (response) {
        		$scope.isLoading = false;
				var sources = response.data.data;
				var sourcesArr = [];
				for (source in sources) {
					if (sources.hasOwnProperty(source)) {
						sourcesArr.push({
						id: sources[source].id,
						name:sources[source].name,
						writerUserName:sources[source].writer.userName,
						timeCreated:sources[source].timeCreated,
						timeModified:sources[source].timeModified,
	                    })
                   }
				}
				$scope.sourcesArr = sourcesArr;
				$scope.pagination.setPages($scope.page, $scope.sourcesArr.length, $scope.resultsPerPage);
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
		
	}
	$scope.checkAction = function(){
		if($rootScope.$state.includes("ln.admin.sources.edit")){
			$scope.action = 'edit';
			$scope.sourceId = $rootScope.$stateParams.sourceId;
			$scope.getSelectedSource();
		}else if($rootScope.$state.includes("ln.admin.sources.add")){
			$scope.action = 'add';
		}
		else{
			$scope.action = '';
		}
	}
	$scope.add = function(){
		$rootScope.$state.go('ln.admin.sources.add', {ln: $rootScope.$state.params.ln});
	}
	$scope.addSource = function(){
		var methodRequest = {};
	    methodRequest.data = 0;
	    methodRequest.data = {
	    	    name: $scope.newSource.sourceName,
	    }
        $http.post('marketing/source/insert', methodRequest).then(
		function (response) {
			$scope.hideLoading();
			$rootScope.$state.go('ln.admin.sources', {ln: $rootScope.$state.params.ln});
			if(response.data.responseCode==0){
				growl.success('source added');
			}
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
        $scope.newSource={};
        $rootScope.$state.go('ln.admin.sources', {ln: $rootScope.$state.params.ln});
	}
	$scope.getSelectedSource = function() {
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.sourceId,
			
		};
		$http.post('marketing/source/getMarketingSource', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'getMarketingSource')) {
					$scope.selectedSource = Utils.parseResponse(response);
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.save = function() {
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.selectedSource.id,
			name: $scope.selectedSource.name,
		};
		$http.post('marketing/source/edit', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'edit')) {
					$scope.selectedSource = Utils.parseResponse(response);
					$rootScope.$state.go('ln.admin.sources', {ln: $rootScope.$state.params.ln});
					if(response.data.responseCode==0){
						growl.success('source edited');
					}
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
}]);
backendApp.controller('LandingPagesCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination,growl) {	
	$scope.action = 'loading';
	$scope.selectedLandingPage = {};
	$scope.newLandingPage = {};
	$scope.landingPagesArr = [];
	$scope.filtered = {};
	$scope.filtered.landingPagesArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.landingPagesArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.landingPagesArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$rootScope.initScreenCtr($scope);
	$scope.search = function() {
		$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data = 0;
        $http.post('marketing/lp/getAll', methodRequest).then(
		function (response) {
				$scope.isLoading = false;
				var landingPages = response.data.data;
				var landingPagesArr = [];
				for (landingPage in landingPages) {
					if (landingPages.hasOwnProperty(landingPage)) {
						landingPagesArr.push({
						id: landingPages[landingPage].id,
						name:landingPages[landingPage].name,
						path:landingPages[landingPage].path,
						writerUserName:landingPages[landingPage].writer.userName,
						timeCreated:landingPages[landingPage].timeCreated,
						timeModified:landingPages[landingPage].timeModified,
	                    })
                   }
				}
				$scope.landingPagesArr = landingPagesArr;
				$scope.pagination.setPages($scope.page, $scope.landingPagesArr.length, $scope.resultsPerPage);
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.checkAction = function(){
		if($rootScope.$state.includes("ln.admin.landingPages.edit")){
			$scope.action = 'edit';
			$scope.landingPageId = $rootScope.$stateParams.landingPageId;
			$scope.getSelectedLandingPage();
		}else if($rootScope.$state.includes("ln.admin.landingPages.add")){
			$scope.action = 'add';
		}
		else{
			$scope.action = '';
		}
	}
	$scope.add = function(){
		$rootScope.$state.go('ln.admin.landingPages.add', {ln: $rootScope.$state.params.ln});
	}
	$scope.addLandingPage = function(){
		var methodRequest = {};
	    methodRequest.data = 0;
	    methodRequest.data = {
	    	    name: $scope.newLandingPage.landingPageName,
	    	    path: $scope.newLandingPage.landingPagePath,
	    }
        $http.post('marketing/lp/insert', methodRequest).then(
		function (response) {
			$scope.hideLoading();
			$rootScope.$state.go('ln.admin.landingPages', {ln: $rootScope.$state.params.ln});
			if(response.data.responseCode==0){
				growl.success('landing page added');
			}
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
        $scope.newLandingPage={};
		$rootScope.$state.go('ln.admin.landingPages', {ln: $rootScope.$state.params.ln});
	}
	$scope.getSelectedLandingPage = function() {
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.landingPageId,	
		};
		$http.post('marketing/lp/getMarketingLandingPage', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'getMarketingLandingPage')) {
					$scope.selectedLandingPage = Utils.parseResponse(response);	
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.save = function() {
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.selectedLandingPage.id,
			name: $scope.selectedLandingPage.name,
			path: $scope.selectedLandingPage.path,	
		};
		$http.post('marketing/lp/edit', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'edit')) {
					$scope.selectedLandingPage = Utils.parseResponse(response);
					$rootScope.$state.go('ln.admin.landingPages', {ln: $rootScope.$state.params.ln});
					if(response.data.responseCode==0){
						growl.success('landing page edited');
					}
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
}]);
backendApp.controller('ContentsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl','fileUpload', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination, growl, fileUpload) {	
	$scope.action = 'loading';
	$scope.selectedContent = {};
	$scope.newContent = {};
	$scope.contentsArr = [];
	$scope.filtered = {};
	$scope.filtered.contentsArr = [];
	$rootScope.initScreenCtr($scope);
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.myFile = {};
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.contentsArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.contentsArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$scope.search = function() {
		$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data = 0;
        $http.post('marketing/content/getAll', methodRequest).then(
		function (response) {
				$scope.isLoading = false;
				var contents = response.data.data;
				var contentsArr = [];
				for (content in contents) {
					if (contents.hasOwnProperty(content)) {
						contentsArr.push({
						id: contents[content].id,
						name:contents[content].name,
						comments:contents[content].comments,
						writerUserName:contents[content].writer.userName,
						timeCreated:contents[content].timeCreated,
						timeModified:contents[content].timeModified,
	                    })
                   }
				}
				$scope.contentsArr = contentsArr;
				$scope.pagination.setPages($scope.page, $scope.contentsArr.length, $scope.resultsPerPage);
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.checkAction = function(){
		if($rootScope.$state.includes("ln.admin.contents.edit")){
			$scope.action = 'edit';
			$scope.contentId = $rootScope.$stateParams.contentId;
			$scope.getSelectedContent();
		}else if($rootScope.$state.includes("ln.admin.contents.add")){
			$scope.action = 'add';
		}
		else{
			$scope.action = '';
		}
	}
	$scope.add = function(){
		$rootScope.$state.go('ln.admin.contents.add', {ln: $rootScope.$state.params.ln});
	}
	$scope.addContent = function(){
		
		var file = $scope.myFile.file;
        var request = {};
        request = {"actionSource":2}
        request.data ={
        		name: $scope.newContent.contentName,
	    	    comments: $scope.newContent.contentComments,
        }
        var uploadUrl = "marketing/content/insert"; 
	    var fd = new FormData();
	    fd.append('fileUpload', file);
	    fd.append('request', JSON.stringify(request));
	       $http.post(uploadUrl, fd, {
	          transformRequest: angular.identity,
	          headers: {'Content-Type': undefined}
	       })
	       .success(function(){
	    	   growl.success('content inserted');
	    	   $rootScope.$state.go('ln.admin.contents', {ln: $rootScope.$state.params.ln});
	       })
	       .error(function(){
	    	   growl.success('error');
	       });
	}
	       
	$scope.getSelectedContent = function() {
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.contentId,
			
		};
		$http.post('marketing/content/getMarketingContent', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'getMarketingContent')) {
					$scope.selectedContent = Utils.parseResponse(response);
					$scope.url = $sce.trustAsResourceUrl($scope.selectedContent.htmlurl);					
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.save = function() {
		var file = $scope.myFile.file;
        var request = {};
        request = {"actionSource":2}
        request.data ={
        		id: $scope.selectedContent.id,
    			name: $scope.selectedContent.name,
    			comments: $scope.selectedContent.comments,
    			htmlfilePath: $scope.selectedContent.htmlfilePath,    			
        }
        var uploadUrl = "marketing/content/edit"; 
	    var fd = new FormData();
	    fd.append('fileUpload', file);
	    fd.append('request', JSON.stringify(request));
	       $http.post(uploadUrl, fd, {
	          transformRequest: angular.identity,
	          headers: {'Content-Type': undefined}
	       })
	       .success(function(){
	    	   growl.success('content updated');
	    	   $rootScope.$state.go('ln.admin.contents', {ln: $rootScope.$state.params.ln});
	       })
	       .error(function(){
	    	   growl.success('error');
	       });


	}
}]);
backendApp.controller('CampaignsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination,growl) {	
	$scope.submitForm = function(isValid) {};
	$scope.filter = {};
	$scope.action = 'loading';
	$scope.selectedCampaign = {};
	$scope.newCampaign = {};
	$scope.campaignsArr = [];
	$scope.filtered = {};
	$scope.filtered.campaignsArr = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.initFiltersOnReady = function () {
		  $scope.filter.source = new SingleSelect.InstanceClass();                      
		  $scope.filter.landingPage =  new SingleSelect.InstanceClass();
		  $scope.filter.content = new SingleSelect.InstanceClass();
		  $scope.filter.domain = new SingleSelect.InstanceClass();
		}
	var initDependencies = {name: 'initList', dependencies: {getCampaignInit: false}};
	$rootScope.initScreenCtr($scope, initDependencies, function(){$scope.getCampaignInit('initList');});
    $scope.getCampaignInit = function (dependencyName) {
		$scope.resetGlobalErrorMsg($scope);
		$scope.showLoading();
		var sources = "marketing_sources";
		var landingPages = "marketing_landing_pages";
		var contents = "marketing_contents";
		var domains = "marketing_domains";
		var methodRequest = {};
		methodRequest.data = 0;
	    methodRequest.data = [];
	    methodRequest.data.push (
	    		sources,
	    		landingPages,
	    		contents,
	    		domains
	    )
		$http.post('filters/get', methodRequest).then(
			function (response) {
				$scope.hideLoading();
				$rootScope.markDependencyDone($scope, dependencyName, 'getCampaignInit');
				if (!$scope.handleErrors(response, 'getCampaignInit')) {
					$scope.init = Utils.parseResponse(response);
					$scope.filter.source.fillOptions($scope.parseFilter(Utils.parseResponse(response).marketing_sources));
					$scope.filter.landingPage.fillOptions($scope.parseFilter(Utils.parseResponse(response).marketing_landing_pages));
					$scope.filter.content.fillOptions($scope.parseFilter(Utils.parseResponse(response).marketing_contents));
					$scope.filter.domain.fillOptions($scope.parseDomainFilter(Utils.parseResponse(response).marketing_domains));
					
				}
			}, function (response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});		
	}
	$scope.parseFilter = function (filter) {
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].name});
			}
		}
		return arr;
	}
	$scope.parseDomainFilter = function (filter) {
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].domain});
			}
		}
		return arr;
	}
	$scope.updatePages = function () {
		$timeout(function () {
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.campaignsArr.length, $scope.resultsPerPage);
		});
    }	
	$scope.changePage = function(page) {
		 $scope.page = page;
		 $scope.pagination.setPages($scope.page, $scope.filtered.campaignsArr.length, $scope.resultsPerPage);
	}	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	$scope.search = function() {
		$scope.isLoading = true;
	    var methodRequest = {};
	    methodRequest.data = 0;
        $http.post('marketing/campaign/getAll', methodRequest).then(
		function (response) {
				$scope.isLoading = false;
				var campaigns = response.data.data;
				var campaignsArr = [];
				for (campaign in campaigns) {
					if (campaigns.hasOwnProperty(campaign)) {
						campaignsArr.push({
						id: campaigns[campaign].id,
						name:campaigns[campaign].name,
						sourceId:campaigns[campaign].sourceId,
						landingPageId:campaigns[campaign].landingPageId,
						contentId:campaigns[campaign].contentId,
						domainId:campaigns[campaign].domainId,
						url:campaigns[campaign].url,
	                    })
                   }
				}
				$scope.campaignsArr = campaignsArr;
				$scope.pagination.setPages($scope.page, $scope.campaignsArr.length, $scope.resultsPerPage);
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.checkAction = function(){
		if($rootScope.$state.includes("ln.admin.campaigns.edit")){
			$scope.action = 'edit';
			$scope.campaignId = $rootScope.$stateParams.campaignId;
			$scope.getSelectedCampaign();
		}else if($rootScope.$state.includes("ln.admin.campaigns.add")){
			$scope.action = 'add';
			 $scope.filter.source.setModel(null);
		     $scope.filter.landingPage.setModel(null);
		     $scope.filter.content.setModel(null);
		     $scope.filter.domain.setModel(null);
		}
		else{
			$scope.action = '';
		}
	}
	$scope.add = function(){
		$rootScope.$state.go('ln.admin.campaigns.add', {ln: $rootScope.$state.params.ln});
	}
	$scope.addCampaign = function(){
		var methodRequest = {};
	    methodRequest.data = 0;
	    methodRequest.data = {
	    		name: $scope.newCampaign.campaignName,
	    		sourceId: $scope.filter.source.getId(),
	    		landingPageId: $scope.filter.landingPage.getId(),
	    		contentId: $scope.filter.content.getId(),
	    		domainId: $scope.filter.domain.getId(),
	    }
        $http.post('marketing/campaign/insert', methodRequest).then(
   
		function (response) {
				$scope.hideLoading();
				$rootScope.$state.go('ln.admin.campaigns', {ln: $rootScope.$state.params.ln});
				if(response.data.responseCode==0){
					growl.success('campaign added');
				}
				
			}, function (response) {
				hideLoading();
				$scope.handleNetworkError(response);
			});
        $scope.filter.source.setModel(null);
        $scope.filter.landingPage.setModel(null);
        $scope.filter.content.setModel(null);
        $scope.filter.domain.setModel(null);
        $scope.newCampaign={};
		$rootScope.$state.go('ln.admin.campaigns', {ln: $rootScope.$state.params.ln});

	}
	$scope.getSelectedCampaign = function() {

		$scope.showLoading();
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.campaignId,	
		};
		$http.post('marketing/campaign/getMarketingCampaign', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'getMarketingCampaign')) {
					$scope.selectedCampaign = Utils.parseResponse(response);
					$scope.filter.source.setModel($scope.filter.source.getOptionById($scope.selectedCampaign.sourceId));
					$scope.filter.landingPage.setModel($scope.filter.landingPage.getOptionById($scope.selectedCampaign.landingPageId));
					$scope.filter.content.setModel($scope.filter.content.getOptionById($scope.selectedCampaign.contentId));
					$scope.filter.domain.setModel($scope.filter.domain.getOptionById($scope.selectedCampaign.domainId));
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	$scope.save = function() {
		var methodRequest = {};
		methodRequest.data = {
			id: $scope.selectedCampaign.id,
			name: $scope.selectedCampaign.name,
			sourceId: $scope.filter.source.getId(),
    		landingPageId: $scope.filter.landingPage.getId(),
    		contentId: $scope.filter.content.getId(),
    		domainId: $scope.filter.domain.getId(),	
		};
		$http.post('marketing/campaign/edit', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'edit')) {
					$scope.selectedCampaign = Utils.parseResponse(response);
					$rootScope.$state.go('ln.admin.campaigns', {ln: $rootScope.$state.params.ln});
				}
				if(response.data.responseCode==0){
					growl.success('campaign edited');
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
}]);

backendApp.controller('UploadContactsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination, growl) {
	$scope.myFile = {};
	$scope.submit = function() {
		
		var file = $scope.myFile.file;
        var request = {};
        request = {"actionSource":2}
        var uploadUrl = "contact/uploadContacts"; 
	    var fd = new FormData();
	    fd.append('fileUpload', file);
	    fd.append('request', JSON.stringify(request));
	       $http.post(uploadUrl, fd, {
	          transformRequest: angular.identity,
	          headers: {'Content-Type': undefined}
	       })
	       .success(function(){
	    	   growl.success('upload contacts inserted');
	    	   $rootScope.$state.go('ln.admin.uploadContacts', {ln: $rootScope.$state.params.ln});
	       })
	       .error(function(){
	    	   growl.success('error');
	       });
		
	}	
		
}]);
