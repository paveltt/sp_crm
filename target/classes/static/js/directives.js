backendApp.directive('autoFocus', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $element) {
			$timeout(function() {
				$element[0].focus();
			});
		}
	}
}]);

backendApp.directive('compareTo', function() {
	return {
		require: "ngModel",
		scope: {
			otherModelValue: "=compareTo"
		},
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.compareTo = function(value) {
				return value == scope.otherModelValue;
			};

			scope.$watch("otherModelValue", function() {
				ngModel.$validate();
			});
		}
	};
});

backendApp.directive('validCcNum', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validCcNum', !isNaN(detectCCtype(value, true)));
				return value;
			});
		}
	};
});

backendApp.directive('validEmail', function () { 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				ngModel.$setValidity('validEmail', regEx_email.test(value));
				return value;
			});
		}
	};
});

backendApp.directive('restrictNickname', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				// ngModel.$setValidity('nickname', (regEx_nickname.test(value)));
				value = value.replace(regEx_nickname_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

backendApp.directive('restrictDigits', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				value = value.replace(regEx_digits_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

backendApp.directive('restrictFloat', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				var val = value.replace(',', '.').split('.');
				value = val[0].replace(regEx_digits_reverse, '');
				if (!isUndefined(val[1])) {
					value += '.' + val[1].replace(regEx_digits_reverse, '');
				}
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

backendApp.directive('restrictLetters', function() {
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function (value) {
				//var regEx_letters_reverse = new RegExp((skinMap[settings.skinId].regEx_letters+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				var regEx_letters_reverse = new XRegExp((regEx_lettersOnly+'').replace(/\//g,'').replace('^[','[^').replace('+$',''),'g');
				value = value.replace(regEx_letters_reverse, '');
				elem[0].value = value;
				ngModel.$setViewValue(value);
				return value;
			});
		}
	};
});

backendApp.directive('device', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $element) {
			$timeout(function() {
				var device = detectDevice();
				if (device != '') {
					$element.addClass('mobile-device');
				}
				$element.addClass(device);
			});
		}
	}
}]);

backendApp.directive('compile', ['$compile', function($compile) {
	// directive factory creates a link function
	return function(scope, elem, attrs) {
		scope.$watch(
			function(scope) {
				// watch the 'compile' expression for changes
				return scope.$eval(attrs.compile);
			},
			function(value) {
				// when the 'compile' expression changes
				// assign it into the current DOM
				elem.html(value);

				// compile the new DOM and link it to the current
				// scope.
				// NOTE: we only compile .childNodes so that
				// we don't get into infinite loop compiling ourselves
				$compile(elem.contents())(scope);
			}
		);
	}
}]);

backendApp.directive('fallbackSrc', function () {
	var fallbackSrc = {
		link: function postLink(scope, iElement, iAttrs) {
			iElement.bind('error', function() {
				if(iAttrs.fallbackSrc != ""){
					angular.element(this).attr("src", iAttrs.fallbackSrc);
					angular.element(this).css("visibility", "visible");
				}else{
				//	angular.element(this).css("visibility", "hidden"); //Causes problems, sometimes, with some markets
				}
			});
		}
	}
	return fallbackSrc;
});

backendApp.directive('loading', ['$timeout','$compile', function($timeout, $compile) {
	return {
		restrict: "A",
		link: function(scope,element,attrs,ngCtrl) {
			scope.ready;
			var loader = angular.element('<i class="loading-section-el" ng-class="{dN: ready}" ng-hide="ready" ></i>');
			element.addClass('loading-section');
			element.append($compile(loader)(scope));
		}
	};
}]);

backendApp.directive('pagination', ['$rootScope', function($rootScope) {
	return {
		restrict: 'E',
		templateUrl: folderPrefix + 'components/pagination.html', // markup for template
		scope: {
			pagination: '=model'
		},
		link: function(scope, element, attrs) {
			scope.getMsgs = $rootScope.getMsgs;
		}
	};
}]);

backendApp.directive('multiselect', ['$document', '$timeout', function($document, $timeout) {
	return {
		restrict: 'E',
		require: '?ngModel',
		templateUrl: folderPrefix + 'components/multiselect.html', // markup for template
		scope: {
			multiselect: '=ngModel',
			ngDisabled: '='
		},
		link: function(scope, element, attrs, ngModel) {
			scope.toggleOpen = function(forceClose){
				ngModel ? ngModel.$setTouched() : '';
				if(!scope.multiselect.visible && !forceClose){
					if(scope.ngDisabled){
						return;
					}
					scope.multiselect.visible = true;
					$timeout(function() {
						$(element[0]).find('[data-filter]:eq(0)').focus();
					});
					$document.bind('mousedown', documentClickBind);
				}else{
					scope.multiselect.visible = false;
					scope.multiselect.filterText = '';
					$document.unbind('mousedown', documentClickBind);
				}
			}
			function documentClickBind(event){
				if(!element[0].contains(event.target)){
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
				}
			}
			
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element[0]).on('keydown', function(e){
				if(scope.ngDisabled){
					return;
				}
				
				if(e.keyCode == 13){//Enter
					scope.$apply(function(){
						scope.toggleOpen();
					});
					return false;
				}else if(e.keyCode == 27){//Escape
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
					return false;
				}
			});
			
			scope.$watch(function(){
				return scope.multiselect ? scope.multiselect.getCheckedIds() : null;
			}, function(){
				scope.updateScroll();
			}, true);
			
			scope.updateScroll = function(){
				$timeout(function(){
					if($(element[0]).find('.multiselect-selected-options:eq(0)').height() > $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height()){
						scope.showScroll = true;
					}else{
						scope.showScroll = false;
					}
				});
			}
			
			scope.selectedOptionsScrollUp = function(){
				$(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop($(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() - $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height());
			}
			
			scope.selectedOptionsScrollDown = function(){
				$(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop($(element[0]).find('.multiselect-selected-options-holder:eq(0)').scrollTop() + $(element[0]).find('.multiselect-selected-options-holder:eq(0)').height());
			}
		}
	};
}]);

backendApp.directive('singleSelect', ['$document', '$timeout', function($document, $timeout) {
	return {
		restrict: 'E',
		require: '?ngModel',
		templateUrl: folderPrefix + 'components/single-select.html', // markup for template
		scope: {
			ngDisabled: '='
		},
		link: function(scope, element, attrs, ngModel) {
			
			
			ngModel.$options = {
				allowInvalid: true
			};
			ngModel.$isEmpty = function(value){
				if(!value.model || !value.model.model || !value.model.model.name){
					return true;
				}
				return false;
			}
			ngModel.$formatters.push(function(model){
				return {model: model};
			});
			ngModel.$parsers.push(function(viewValue){
				return viewValue.model;
			});
			ngModel.$render = function(){
				scope.singleSelect = ngModel.$viewValue.model;
			};
			scope.selectOption = function(option){
				scope.toggleOpen();
				scope.updateModel(option);
			}
			scope.updateModel = function(option){
				if(scope.ngDisabled){
					return;
				}
				ngModel.$modelValue.setModel(option);
				ngModel.$setViewValue({model: ngModel.$modelValue.clone()});
				ngModel.$commitViewValue();
				ngModel.$render();
			}
			
			scope.toggleOpen = function(forceClose){
				ngModel.$setTouched();
				if(!scope.singleSelect.visible && !forceClose){
					if(scope.ngDisabled){
						return;
					}
					scope.singleSelect.visible = true;
					$timeout(function() {
						$(element[0]).find('[data-filter]:eq(0)').focus();
					});
					$document.bind('mousedown', documentClickBind);
				}else{
					scope.singleSelect.visible = false;
					scope.singleSelect.filterText = '';
					$document.unbind('mousedown', documentClickBind);
				}
			}
			function documentClickBind(event){
				if(!element[0].contains(event.target)){
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
				}
			}
			
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element[0]).on('keydown', function(e){
				if(scope.singleSelect.isSmart){
					return;
				}
				if(scope.ngDisabled){
					return;
				}
				if(e.keyCode == 38){//Up key
					scope.$apply(function(){
						var option = ngModel.$modelValue.getPreviousOption();
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
						}
					});
					return false;
				}else if(e.keyCode == 40){//Down key
					scope.$apply(function(){
						var option = ngModel.$modelValue.getNextOption()
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
						}
					});
					return false;
				}else if(e.keyCode == 13){//Enter
					scope.$apply(function(){
						scope.toggleOpen();
					});
					return false;
				}else if(e.keyCode == 27){//Escape
					scope.$apply(function(){
						scope.toggleOpen(true);
					});
					return false;
				}else if(e.key && !e.ctrlKey){
					scope.$apply(function(){
						var option = ngModel.$modelValue.getNextOptionByKey(e.key);
						if(option){
							scope.updateModel(option);
							scope.updateScroll(option);
							return false;
						}
					});
				}
			});
			
			scope.$watch(function(){
				return scope.singleSelect ? scope.singleSelect.options : null;
			}, function(){
				scope.updateSize();
			}, true);
			
			scope.updateSize = function(){
				$timeout(function(){
					var width;
					if($(element[0]).find('[data-options-box]:eq(0)') && $(element[0]).find('[data-options-box]:eq(0)').length > 0){
						var rect = $(element[0]).find('[data-options-box]:eq(0)')[0].getBoundingClientRect();
						if(rect.width){
							width = rect.width;
						}else{
							width = rect.right - rect.left;
						}
					}
					if(width > 0){
						$(element[0]).width(Math.ceil(width));
					}
				});
			}
			
			scope.updateScroll = function(option){
				$timeout(function(){
					var optionEl = $(element[0]).find('[data-options-box] .single-select-options > span:eq(' + option.index + ')');
					if(optionEl.position().top < 0 || (optionEl.position().top + optionEl.outerHeight() > optionEl.offsetParent().height())){
						optionEl.offsetParent().scrollTop(optionEl.offsetParent().scrollTop() + optionEl.position().top);
					}
					optionEl = null;
				});
			}
			
			scope.$watch(function() {
			    return ngModel.$modelValue.model;
			   }, function(model) {
			    if (model === null) {
			     ngModel.$validate();
			    }
			   });
		}
	};
}]);

backendApp.directive('checkLastCallback', ['$parse', function($parse) {
	return {
		restrict: 'A',
		scope: true,
		link: function(scope, element, attrs) {
			if(attrs.checkLastCallback != ''){
				if(scope.$last === true){
					var invoker = $parse(attrs.checkLastCallback);
					invoker(scope)();
				}
			}
		}
	};
}]);

backendApp.directive('convertToNumber', function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function(val) {
				return parseInt(val, 10);
			});
			ngModel.$formatters.push(function(val) {
				return '' + val;
			});
		}
	};
});

backendApp.directive('unselectable', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element[0]).on('selectstart', function(){
				return false;
			});
		}
	};
}]);

backendApp.directive('ngSrcNoCache', function() {
	return {
		priority: 99,
		link: function(scope, element, attrs) {
			attrs.$observe('ngSrcNoCache', function(ngSrcNoCache) {
				var prefix = '?';
				if(ngSrcNoCache.indexOf('?') != -1){
					prefix = '&';
				}
				ngSrcNoCache += prefix + (new Date()).getTime();
				attrs.$set('src', ngSrcNoCache);
			});
		}
	}
});

backendApp.directive('toggableOn', ['$document', function($document) {
	return {
		restrict: 'A',
		compile: function(tElement, tAttrs){
			tElement.attr('data-toggable-on', tAttrs.toggableOn);
		}
	}
}]);
backendApp.directive('toggableOff', ['$document', function($document) {
	return {
		restrict: 'A',
		compile: function(tElement, tAttrs){
			tElement.attr('data-toggable-off', tAttrs.toggableOff);
		}
	}
}]);
backendApp.directive('toggler', ['$timeout', function($timeout) {
	return {
		restrict: 'E',
		template: '<span></span>',
		scope: {
			onToggle: '='
		},
		link: function(scope, element, attrs) {
			$(element[0]).on('selectstart', function(){
				return false;
			});
			$(element).on('click', function(){
				if($(element).data('isOn')){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
				}else{
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
				}
				toggleTogglerState();
			});
			
			function setDefaultState(){
				if(attrs.defaultState == 'on'){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					toggleTogglerState({forceOn: true, preventOnToggle: true});
				}else if(attrs.defaultState == 'off'){
					$('[data-toggable-on="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, false);
					});
					$('[data-toggable-off="' + attrs.toggle + '"]').each(function(index, el){
						toggle(el, true);
					});
					toggleTogglerState({forceOn: false, preventOnToggle: true});
				}
			};
			
			setDefaultState();
			
			function toggle(el, isOn){
				if(isOn){
					$(el).css('display', '');
				}else{
					$(el).css('display', 'none');
				}
			}
			function toggleTogglerState(params){
				if(!params){
					params = {};
				}
				var isOn = $(element).data('isOn');
				if(typeof params.forceOn == 'undefined' && isOn || typeof params.forceOn != 'undefined' && !params.forceOn){
					$(element).removeClass('block-toggler-on');
					$(element).data('isOn', false);
				}else{
					$(element).addClass('block-toggler-on');
					$(element).data('isOn', true);
				}
				if(scope.onToggle && !params.preventOnToggle){
					scope.$apply(function(){
						scope.onToggle(attrs.toggle, !isOn);
					});
				}
			}
		}
	};
}]);

backendApp.directive('onKeydown', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var onKeydown = scope.$eval(attrs.onKeydown);
			$(element[0]).on('keydown', function(e){
				for(key in onKeydown){
					if(onKeydown.hasOwnProperty(key)){
						if(e.keyCode == key){
							var func = onKeydown[key];
							scope.$eval(func);
						}
					}
				}
			});
		}
	}
}]);

backendApp.directive('onEnter', ['$document', function($document) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element[0]).on('keydown', function(e){
				if(e.keyCode == 13){
					scope.$eval(attrs.onEnter);
				}
			});
		}
	}
}]);


//This directive is used to force UTC for the datepicker (otherwise there are problems with daylight saving time and wrong day)
backendApp.directive('datepickerTimezone', function () {
	return {
		restrict: 'A',
		priority: 1,
		require: 'ngModel',
		link: function (scope, element, attrs, ctrl) {
			ctrl.$formatters.push(function (value) {
				var date = new Date(Date.parse(value));
				date = new Date(date.getTime() + (60000 * date.getTimezoneOffset()));
				//if (!dateFormat || !modelValue) return "";
				//var retVal = moment(modelValue).format(dateFormat);
				return date;
			});

			ctrl.$parsers.push(function (value) {
				if(!value){
					return null;
				}
				var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
				return date;
			});
		}
	};
});

//Source: https://github.com/nikolassv/angular-textarea-fit
backendApp.directive('textareaFit', ['$log', function ($log) {
	var copyCssStyles = function (elSrc, elDest) {
		var stylesToCopy = ['width', 'font-family', 'font-size', 'line-height', 'min-height', 'padding'];
		var destStyles = {};

		angular.forEach(stylesToCopy, function (style) {
			destStyles[style] = elSrc.css(style); 
		});

		elDest.css(destStyles); 
	};

	return {
		restrict: 'A',
		link : function ($scope, $element) {
			if (!angular.isFunction($element.height)) {
				$log.error('textareaFit directive only works when jQuery is loaded');
			} else if (!$element.is('textarea')) {
				$log.info('textareaFit directive only works for elements of type "textarea"');
			} else {
				var elClone = angular.element('<div>');
				var setEqualHeight = function () {
					var curText = $element.val();
					if (/\n$/.test(curText)) {
						curText += ' ';
					}
					copyCssStyles($element, elClone);
					elClone.text(curText);
					$element.height(elClone.height());
				};

				elClone
					.hide()
					.css({
						'white-space': 'pre-wrap',
						'word-wrap' : 'break-word'
					});
				$element.parent().append(elClone);
				$element.css('overflow', 'hidden');

				$element
					.on('change', setEqualHeight)
					.on('keyup', setEqualHeight)
					;

				$scope.$on('destroy', function () {
					elClone.remove();
					elClone = null;
				});
			}
		}
	};
}]);

backendApp.component('productsPriority', {
	templateUrl: folderPrefix + 'admin/productsPriority.html',
	controller: ['productsPriorityService', '$scope', '$rootScope', function(productsPriorityService, $scope, $rootScope) {
		var ctrl = this;

		ctrl.getMsgs = $rootScope.getMsgs;
		ctrl.isUpdatePriorityBtnDisabled = false;
		
		ctrl.updatePriorityClickHandler = function() {
			ctrl.isUpdatePriorityBtnDisabled = true;
			productsPriorityService.updateProductsPriority()
				.then(function() {
					ctrl.isUpdatePriorityBtnDisabled = false;
				});
		};
		
		ctrl.getMaxMove = function(index, length) {
			return Math.max(index, (length - (index + 1)));
		};
		
		ctrl.moveProduct = function(oldIndex, delta) {
			var newIndex = oldIndex + delta;
			if (newIndex >= ctrl.priorityProducts.length) {
				console.log('Wrong index');
				return;
			}
			ctrl.priorityProducts.splice(newIndex, 0, ctrl.priorityProducts.splice(oldIndex, 1)[0]);
		};
		
		productsPriorityService.loadNewPriorityProducts();
		
		$scope.$watch(productsPriorityService.getPriorityProducts, function(newPriorityProducts) {
			ctrl.priorityProducts = newPriorityProducts;
		});
	}],
	controllerAs: 'productsPriorityCtrl'
});
/*
//DO NOT UNCOMMENT, IT'S GOING TO BLAW
//Can be used instead of the watch of popUps in controllers.js

backendApp.directive('popups', ['$compile', '$timeout', function(compile, timeout){
	return {
		restrict: 'E',
		link: function(scope, element, attrs) {
			showPopups();
			scope.$watch('popUps', function(){
				showPopups();
			}, true);
			
			function showPopups(){
				for(var i = 0; i < scope.popUps.length; i++) {
					var template = '<div data-popup ng-include="\'' + scope.popUps[i].url + '\'" ng-controller="' + scope.popUps[i].ctr + '" ng-init="passParams(popUps[' + i + '].config)"></div>';
					var cTemplate = compile(template)(scope);
					element.append(cTemplate);
				}
				timeout(function(){
					console.log(element.children().length, scope.popUps.length);
					if(scope.popUps.length == 0 && element.children().length != 0){
						element.empty();
					//	element.remove();
					}else{
						var childCount = 0;
						element.children().each(function(index, el){
							childCount++;
							if(childCount > scope.popUps.length){
								el.remove();
							}
						});
					}
				}, 0);
			}
		}
	}
}]);
*/
backendApp.directive('userStrip',  function() {
	return {
		restrict: 'E',
		link: function(scope, element, attrs, ctrl) {
			
		},
		templateUrl: folderPrefix + 'admin/userStrip.html',
		controller: ['userService', '$scope', function(userService, $scope) {
			$scope.isFullStripVisible = false;
			$scope.toggleFullStripVisible = function() {
				$scope.isFullStripVisible = !$scope.isFullStripVisible;
			};
			$scope.refreshUserStrip = function() {
				var user = {};
				user = userService.getLoadedUser();
				userService.loadUser(user.id,user.email);	
			};
			$scope.userLogout = function() {
				var user = {};
				user = userService.logoutUser();
				$scope.selectedUser = {};
				$scope.filter.countries.setModel(null);
				$scope.filter.userGender.setModel(null);
				$scope.filter.userClass.setModel(null);
			};
			
		}],
		
	}
}

);

backendApp.directive('validDecimal', function() {
	return {
		link : function(scope, ele, attrs) {
			ele.bind('keypress', function(e) {
				var newVal = $(this).val() + (e.charCode !== 0 ? String.fromCharCode(e.charCode) : '');
				if ($(this).val().search(/(.*)\.[0-9][0-9]/) === 0 && newVal.length > $(this).val().length) {
					e.preventDefault();
				}
			});
		}
	};
});

backendApp.directive('fileModel', ['$parse', function ($parse) {
	return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
 }]);

backendApp.directive("showOnLoad", function() {
    return {
        link: function(scope, element) {
            element.on("load", function() {
                scope.$apply(function() {
                    scope.imageLoading = false;
                });
            });
        }
    };
});

backendApp.directive("noSpaces", function() {
    return {
    	restrict: 'A',
    	link: function(scope, elem, attr, ngModel) {
			$(elem[0]).on('keydown', function(e){
				//Forbid space
				if(e.keyCode == 32){
					return false;
				}
			})
    	}
    };
});

backendApp.directive('countTo', ['$timeout', function ($timeout) {
    return {
        replace: false,
        scope: true,
        link: function (scope, element, attrs) {

            var e = element[0];
            var num, refreshInterval, duration, steps, step, countTo, value, increment;

            var calculate = function () {
                refreshInterval = 30;
                step = 0;
                scope.timoutId = null;
                countTo = parseInt(attrs.countTo) || 0;
                scope.value = parseInt(attrs.value, 10) || 0;
                duration = (parseFloat(attrs.duration) * 1000) || 0;

                steps = Math.ceil(duration / refreshInterval);
                increment = ((countTo - scope.value) / steps);
                num = scope.value;
            }

            var tick = function () {
                scope.timoutId = $timeout(function () {
                    num += increment;
                    step++;
                    if (step >= steps) {
                        $timeout.cancel(scope.timoutId);
                        num = countTo;
                        e.textContent = countTo;
                    } else {
                        e.textContent = Math.round(num);
                        tick();
                    }
                }, refreshInterval);

            }

            var start = function () {
                if (scope.timoutId) {
                    $timeout.cancel(scope.timoutId);
                }
                calculate();
                tick();
            }

            attrs.$observe('countTo', function (val) {
                if (val) {
                    start();
                }
            });

            attrs.$observe('value', function (val) {
                start();
            });

            return true;
        }
    }
}]);