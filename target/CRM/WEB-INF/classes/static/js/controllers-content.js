backendApp.controller('TranslationsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'Pagination', 'SingleSelect', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, Pagination, SingleSelect) {
	var lastSearchActionSource = null;
	$scope.action = 'loading';
	$scope.msgs = {};
	$scope.parsedMsgs = {};
	$scope.msgsArr = [];
	$scope.filtered = {};
	$scope.filtered.msgsArr = [];
	$scope.filter = {};
	$scope.filter.actionSource = new SingleSelect.InstanceClass({aggregateOption: {id: 0, name: ''}});
	$scope.searchFilter = {
		key: ''
	};
	$scope.newKeys = [];
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.updatePages = function(){
		$timeout(function(){
			var page = 1;
			$scope.page = page;
			$scope.pagination.setPages($scope.page, $scope.filtered.msgsArr.length, $scope.resultsPerPage);
		});
	}
	
	$scope.changePage = function(page){
		$scope.page = page;
		$scope.pagination.setPages($scope.page, $scope.filtered.msgsArr.length, $scope.resultsPerPage);
	}
	
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage});
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getTranslationsScreenSettings: false}};
	$rootScope.initScreenCtr($scope, initDependencies, function(){$scope.getTranslationsScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$state.includes("ln.admin.translations.add")){
			$scope.action = 'add';
		}else{
			$scope.action = '';
		}
	}
	
	$scope.getTranslationsScreenSettings = function(dependencyName){
		$scope.resetGlobalErrorMsg($scope);
		$scope.showLoading();
		var methodRequest = {};
		
		var actionSources = [];
		for(key in settings.actionSources){
			if(settings.actionSources.hasOwnProperty(key)){
				actionSources.push({id: settings.actionSources[key].id, name: settings.actionSources[key].name});
			}
		}
		$scope.filter.actionSource.fillOptions(actionSources);
		
		$rootScope.markDependencyDone($scope, dependencyName, 'getTranslationsScreenSettings');
		$scope.hideLoading();
		
		$scope.initTinymce();
	}
	
	
	$scope.search = function(){
		$scope.getMsgsJson($scope.filter.actionSource.getId());
	}
	
	$scope.getMsgsJson = function(actionSource){
		$scope.showLoading();
		$scope.parsedMsgs = {};
		msgsArr = [];
		var methodRequest = {};
		methodRequest.data = {
			actionSourceId: actionSource
		};
		$http.post('message/getMessageResourcesFull', methodRequest).then(
			function(response) {
				$scope.hideLoading();
				if (!$scope.handleErrors(response, 'getMsgsJson')) {
					if(Utils.parseResponse(response)){
						$scope.msgs = Utils.parseResponse(response);
						
						for(langKey in $scope.msgs){
							if($scope.msgs.hasOwnProperty(langKey)){
								for(key in $scope.msgs[langKey]){
									if($scope.msgs[langKey].hasOwnProperty(key)){
										if(!$scope.parsedMsgs[key]){
											obj = {};
											for(var i = 0; i < $rootScope.languages.length; i++){
												if($rootScope.languages[i].languageId == langKey){
													obj[$rootScope.languages[i].languageId] = $scope.msgs[langKey][key];
												}else{
													obj[$rootScope.languages[i].languageId] = "";
												}
											}
											$scope.parsedMsgs[key] = {
												values: obj,
												typeId: $scope.msgs[langKey][key].msgRes.typeId
											};
										}else{
											$scope.parsedMsgs[key].values[langKey] = $scope.msgs[langKey][key];
										}
									}
								}
							}
						}
						
						for(key in $scope.parsedMsgs){
							if($scope.parsedMsgs.hasOwnProperty(key)){
								msgsArr.push({key: key, value: $scope.parsedMsgs[key].values, typeId: $scope.parsedMsgs[key].typeId});
							}
						}
						
						msgsArr.sort(function(a, b){
							return a.key.localeCompare(b.key, undefined, {sensitivity: 'base'})
						});
						
						lastSearchActionSource = actionSource;
						$scope.msgsArr = msgsArr;
						
						$scope.updatePages();
					}
				}
			}, function(response) {
				$scope.hideLoading();
				$scope.handleNetworkError(response);
			});
	}
	
	$scope.add = function(){
		$rootScope.$state.go('ln.admin.translations.add', {ln: $rootScope.$state.params.ln});
	}
	
	$scope.addKey = function(){
		var obj = {key: '', actionSource: $scope.filter.actionSource.getId(), value: {}};
		for(var i = 0; i < $rootScope.languages.length; i++){
			obj.value[$rootScope.languages[i].languageId] = "";
		}
		$scope.newKeys.push(obj);
	}
	
	$scope.saveNewKeys = function(){
		var parsed = [];
		for(var i = 0; i < newKeys.length; i++){
			var obj = {};
			
		}
	}
	
	$scope.save = function(){
		var methodRequest = {};
	}
	
	
	$scope.tinymceReady = false;
	$scope.tinymceParams = [
		{name: 'index_page', display: 'inline', title: 'Insert index page link'}
	];
	
	$scope.initTinymce = function(){
		$scope.tinymceOptions = {
			selector: 'textarea#annex',
			menubar: 'view edit insert table format tools',
			plugins: 'advlist autolink link image lists table paste searchreplace charmap fullscreen noneditable code',
			content_style: 'body{background:none!important}',
			paste_convert_word_fake_lists: true,
			paste_remove_styles_if_webkit: true,
			paste_remove_styles: true,
			paste_strip_class_attributes: true,
			table_appearance_options: false,
			/*invalid_elements : 'table,thead,tbody,tr,td',*/
			style_formats: [
				{ title: 'title', block : 'h1', classes : 'general_terms_h1', exact : true },
				{ title: 'sub-title', block : 'h2', classes : 'general_terms_h2', exact : true },
				{ title: 'item-title', inline : 'span', classes : 'item-title', exact : true },
				{ title: 'sub-item-title', inline : 'span', classes : 'sub-item-title', exact : true },
				{ title: 'item-no', inline : 'span', classes : 'item-no', exact : true }
			],
			formats: {
			},
			setup: function (editor) {
				for(var i = 0; i < $scope.tinymceParams.length; i++){
					var insertParam = function(param){
						var content = '<span class="mceNonEditable">${' + param.name + '}</span>';
						if(param.display == 'block'){
							content = '<div class="mceNonEditable">${' + param.name + '}</div><br/>';
						}
						editor.addMenuItem('insert' + param.name, {
							text: param.title,
							context: 'insert',
							onclick: function () {
								editor.insertContent(content);
							}
						});
					}($scope.tinymceParams[i]);
				}
				editor.addMenuItem('custominsertseparator', {
					text: '-',
					context: 'insert'
				});
			}
		};
		$scope.tinymceReady = true;
	}
	
	$scope.openEditMsgModal = function (msg, type) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: folderPrefix + 'admin/editTranslationsModal.html',
			controller: 'openEditMsgModalController',
			controllerAs: '$ctrl',
			size: 'lg',
			windowClass: 'insert-edit-msg-modal',
			resolve: {
				msgResLanguage: ['TranslationsService', function(TranslationsService) {
					var data = {
						key: msg.key,
						actionSourceId: lastSearchActionSource
					};
					return TranslationsService.getMsgResLanguageByKey(data)
						.then(function(response) {
							return response.data.data;
						});
				}],
				msg: function () {
					return angular.copy(msg);
				},
				actionSource: function() {
					return lastSearchActionSource;	
				},
				key: function() {
					return msg.key;
				},
				languages: function() {
					return $rootScope.languages.reduce(
						function(prev, lang) {
							prev[lang.languageId] = lang.languageCode;
							return prev;
						}, 
						{}
					);
				},
				type: function() {
					return type;
				}
			}
		});
	
		modalInstance.result.then(function (closeMsg) {
			if (closeMsg === 'saved') {
				$scope.search();
			}
		}, function (err) {
			console.log(err);
		});
	};
	
	$scope.openAddLangModal = function (msg) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: folderPrefix + 'admin/addLanguageModal.html',
			controller: 'openAddLangModalController',
			controllerAs: '$ctrl',
			size: 'lg',
			windowClass: 'insert-edit-msg-modal',
			scope: $scope,
			resolve: {
				msgResLanguage: ['TranslationsService', function(TranslationsService) {
					var data = {
						key: msg.key,
						actionSourceId: lastSearchActionSource
					};
					return TranslationsService.getMsgResLanguageByKey(data)
						.then(function(response) {
							return response.data.data;
						});
					}],
					missingLanguages: ['TranslationsService', function(TranslationsService) {
						var data = {
							key: msg.key,
							actionSourceId: lastSearchActionSource
						};
						return TranslationsService.getMissingLanuages(data)
							.then(function(response) {
								return response.data.data;
							});
					}],
					languagesFilter: ['FiltersService', function(FiltersService) {
						return FiltersService.getFilters(['languages'])
						   .then(function(response) {
						    return response.languages;
						   })
						   .catch(function(response) {
						    $scope.handleNetworkError(response);
						   })
					}],
					msg: function () {
						return angular.copy(msg);
					},
					actionSource: function() {
						return lastSearchActionSource;	
					},
					key: function() {
						return msg.key;
					},
					languages: function() {
						return $rootScope.languages.reduce(
							function(prev, lang) {
								prev[lang.languageId] = lang.languageCode;
								return prev;
							}, 
							{}
						);
					}
				}
		});
		modalInstance.result.then(function (closeMsg) {
			if (closeMsg === 'saved') {
				$scope.search();
			}
		}, function (err) {
			console.log(err);
		});
	};		
	$scope.openInsertMsgModal = function (type) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: folderPrefix + 'admin/insertTranslationsModal.html',
			controller: 'openInsertMsgModalController',
			controllerAs: '$ctrl',
			size: 'lg',
			windowClass: 'insert-edit-msg-modal',
			resolve: {
				actionSourceId: function() {
					return lastSearchActionSource;	
				},
				languages: function() {
					return angular.copy($rootScope.languages);
				},
				keys: function() {
					return msgsArr.map(function(msg) {
						return msg.key;
					});
				},
				key: function() {
					return $scope.searchFilter.key;
				},
				type: function() {
					return type;
				}
			}
		});
		modalInstance.result.then(function (closeMsg) {
			if (closeMsg === 'saved') {
				$scope.search();
			}
		}, function (err) {
			console.log(err);
		});
	};
}]);
backendApp.controller('openEditMsgModalController', ['$uibModalInstance', 'msgResLanguage', 'msg', 'actionSource','key', 'languages', 'type', 'TranslationsService', 'growl', function($uibModalInstance, msgResLanguage, msg, actionSource, key, languages, type, TranslationsService, growl) {
	var ctrl = this;
	ctrl.msg = msg;
	ctrl.msgResLanguageList = msgResLanguage.map(function(msg) {
		return {
			id: msg.msgRes.id,
			actionSourceId: actionSource, 
			key: key, 
			value: msg.largeValue || msg.value,
			msgResLanguageId: msg.msgResLanguageId,
			type: type,
			html: msg.largeValue || msg.value
		};
	});
	ctrl.languages = languages;
	
	ctrl.type = type;
	
	if(type == 'html'){
		ctrl.tinymceReady = false;
		ctrl.tinymceParams = [
			{name: 'company_address', display: 'inline', title: 'Company address'},
			{name: 'company_email', display: 'inline', title: 'Company email'},
			{name: 'support_phone', display: 'inline', title: 'Support phone'}
		];
		
		ctrl.initTinymce = function(){
			ctrl.tinymceOptions = getTinyMceOptions(ctrl.tinymceParams);
			ctrl.tinymceReady = true;
		}
		ctrl.initTinymce();
	}
	
	ctrl.submitHandler = function() {
		TranslationsService.updateMsgResLanguage(ctrl.msgResLanguageList)
			.then(function(response) {
				if (response.data.responseCode === 0) {
					ctrl.closeModal('saved');
					growl.success('key edited');
				} else {
					growl.error('edit failed');
				}
			});
	};
	
	ctrl.closeModal = function(closeMessage) {
		closeMessage = closeMessage || 'close';
		$uibModalInstance.close(closeMessage);
	};
}]);

backendApp.controller('openAddLangModalController', ['$uibModalInstance', 'msgResLanguage', 'msg', 'actionSource','key', 'languages', 'TranslationsService','$http','$scope','SingleSelect','$rootScope','Utils','missingLanguages','languagesFilter','growl', function($uibModalInstance, msgResLanguage, msg, actionSource, key, languages, TranslationsService, $http, $scope, SingleSelect, $rootScope, Utils, missingLanguages,languagesFilter,growl) {
	var ctrl = this;
	ctrl.msg = msg;
	ctrl.closeModal = function(closeMessage) {
		closeMessage = closeMessage || 'close';
		$uibModalInstance.close(closeMessage);
	};
	$scope.submitForm = function(isValid) {};
    $scope.filter = {};
    $scope.filter.missinglanguages =  new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs});
    
    var missingLangs = [];
	var missingLangsResponse = missingLanguages;
	for(key in missingLangsResponse){
		if(missingLangsResponse.hasOwnProperty(key)){
					missingLangs.push({id: missingLangsResponse[key].msgResLanguageId, name: languagesFilter[missingLangsResponse[key].msgResLanguageId].displayName});
		}
	}
	$scope.missingLangsExist = false;
	if(missingLangs.length != 0){
		$scope.missingLangsExist = true;
	}
    $scope.filter.missinglanguages.fillOptions(missingLangs);
    $scope.newLang = {};
    $scope.addLanguage = function(missingLangId,value){
    	var methodRequest = {};
    	methodRequest.data = [];
    	methodRequest.data.push ( {
        		msgRes:{
        			id: msgResLanguage[0].msgRes.id,
        			key:msg.key,
        			actionSourceId:actionSource,
        		},
        		
        		msgResLanguageId:	missingLangId,
        		value: value.length <= 2000 ? value : null,
        		largeValue: value.length > 2000 ? value : null
        		}
    	)
    	$http.post('message/insert/MessageResourceLanguage', methodRequest).then(
    			function (response) {
    				$scope.search();
    				$uibModalInstance.close();
    				if(response.data.responseCode == 0){
    					growl.success('missing language added');
    				}
    					
    			}, function (response) {
    				
    			});
        };
}]);


backendApp.controller('openInsertMsgModalController', ['$uibModalInstance', '$scope', 'actionSourceId', 'languages', 'keys', 'key', 'type', 'TranslationsService', function($uibModalInstance, $scope, actionSourceId, languages, keys, key, type, TranslationsService) {
	var ctrl = this;
	ctrl.key = key || '';
	ctrl.languages = languages;
	ctrl.type = type;
	
	if(type == 'html'){
		ctrl.tinymceReady = false;
		ctrl.tinymceParams = [
			{name: 'company_address', display: 'inline', title: 'Company address'},
			{name: 'company_email', display: 'inline', title: 'Company email'},
			{name: 'support_phone', display: 'inline', title: 'Support phone'}
		];
		
		ctrl.initTinymce = function(){
			ctrl.tinymceOptions = getTinyMceOptions(ctrl.tinymceParams);
			ctrl.tinymceReady = true;
		}
		ctrl.initTinymce();
	}
	
	for(key in settings.actionSources){
		if(settings.actionSources.hasOwnProperty(key) && settings.actionSources[key].id === actionSourceId){
			ctrl.actionSourceName = settings.actionSources[key].name;
			break;
		}
	}
	
	ctrl.submitHandler = function() {
		var dataObj = {
			key: ctrl.key,
			actionSourceId: actionSourceId,
			languages: ctrl.languages,
			type: type
		};
		TranslationsService.insertMsgResLanguage(dataObj)
			.then(function(response) {
				if (response.data.responseCode === 0) {
					ctrl.closeModal('saved');
				}
			});
	};
	
	
	// Calculate it one time, when it changes;
	$scope.$watch('$ctrl.key', function(key) {
		ctrl.isKeyExists = keys.indexOf(key) !== -1;
	});

	ctrl.closeModal = function(closeMessage) {
		closeMessage = closeMessage || 'close';
		$uibModalInstance.close(closeMessage);
	};
}]);

backendApp.controller('BulkTranslationsCtr', ['$sce', '$rootScope', '$scope', '$http', '$templateCache', '$timeout', '$compile', '$filter', '$uibModal', 'Utils', 'Permissions', 'MenuTree', 'SingleSelect','Pagination','growl', function($sce, $rootScope, $scope, $http, $templateCache, $timeout, $compile, $filter, $uibModal, Utils, Permissions, MenuTree, SingleSelect, Pagination, growl) {
	$scope.filter = {};
	$scope.filter.actionSource = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs});
	$scope.filter.language = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs});
	$scope.filter.messageResourceTypes = new SingleSelect.InstanceClass({mutateCallback: $rootScope.getMsgs});
	$scope.newText = 0;
	$scope.resultsPerPage = 10;
	$scope.page = 1;
	$scope.myFile = {};
	var languages = "languages";
	var actionSources = "action_sources";
	var messageResourceTypes  = "message_resource_types";
	var methodRequest = {};
	methodRequest.data = 0;
	methodRequest.data = [];
    methodRequest.data.push(
    		languages,
    		actionSources,
    		messageResourceTypes
    )
	$http.post('filters/get', methodRequest).then(
		function (response) {
			$scope.isLoading = false;
			$scope.init = Utils.parseResponse(response);
			$scope.filter.language.fillOptions($scope.parseFilter(Utils.parseResponse(response).languages));
			$scope.filter.messageResourceTypes.fillOptions($scope.parseFilter(Utils.parseResponse(response).message_resource_types));
			$scope.filter.actionSource.fillOptions($scope.parseFilter(Utils.parseResponse(response).action_sources));			
		}, function (response) {
			$scope.isLoading = false;
			$scope.handleNetworkError(response);
		});		
	
	$scope.parseFilter = function (filter) {	
		var arr = [];
		for (key in filter) {
			if(filter.hasOwnProperty(key)) {
				arr.push({id: filter[key].id, name: filter[key].displayName});
			}
		}
		return arr;
	}
	$scope.submit = function() {
		
		var file = $scope.myFile.file;
        var request = {};
        request = {"actionSource":2}
        request.data ={
    			actionSourceId: $scope.filter.actionSource.getId(),
    			typeId: $scope.filter.messageResourceTypes.getId(),
    			languageId: $scope.filter.language.getId(),
    			addKey: $scope.newText			
        }
        var uploadUrl = "message/insert/MessageResourceByFile"; 
	    var fd = new FormData();
	    fd.append('fileUpload', file);
	    fd.append('request', JSON.stringify(request));
	       $http.post(uploadUrl, fd, {
	          transformRequest: angular.identity,
	          headers: {'Content-Type': undefined}
	       })
	       .success(function(){
	    	   growl.success('Bulk translations inserted successfully');
	    	   $rootScope.$state.go('ln.admin.bulkTranslations', {ln: $rootScope.$state.params.ln});
	       })
	       .error(function(){
	    	   growl.success('error');
	       });		
	}	
		
}]);